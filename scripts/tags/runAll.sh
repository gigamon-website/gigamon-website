DATA_DIR='data'
SCRIPTS_DIR='scripts'
LOG_DIR='log'
SRC_DIR='src'

# clean up output from previous run
rm -rf $SCRIPTS_DIR 
mkdir $SCRIPTS_DIR

## hard wired tags
cp $SRC_DIR/Library.sh $SCRIPTS_DIR/.

rm -rf $LOG_DIR
mkdir $LOG_DIR

# iterate through datafiles
for filename in $DATA_DIR/*.src
do
	# get the datafile name without path and extension
	filename=${filename##*/}
	filename=${filename%.*}
	
	# create shell script
	touch $SCRIPTS_DIR/${filename}.sh
	chmod +x $SCRIPTS_DIR/${filename}.sh

	# populate shell script
	python $SRC_DIR/processTagSrcFile.py -f $DATA_DIR/${filename}.src > $SCRIPTS_DIR/${filename}.sh

	# execute shell script
	./$SCRIPTS_DIR/${filename}.sh > $LOG_DIR/${filename}.html
done
