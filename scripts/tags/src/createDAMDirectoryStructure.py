#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by adam n trissel on 2015-12-02.
Copyright (c) 2015 Hero Digital. All rights reserved.
"""

import sys
import getopt


help_message = '''
The help message goes here.
'''

'''
We need to create two curl statements, one for the master entry and one for the jcr:content

{
    "jcr:content": {
        "jcr:primaryType": "nt:unstructured",
        "jcr:title": "AdamTest"
    },
    "jcr:created": "Wed Dec 02 2015 13:09:11 GMT-0800",
    "jcr:createdBy": "admin",
    "jcr:primaryType": "sling:OrderedFolder"
}
'''

curlTemplateMain = "curl -u admin:admin -F\"jcr:primaryType=sling:OrderedFolder\" http://localhost:4502/content/dam/gigamon/%s"
curlTemplateContent = "curl -u admin:admin -F\"jcr:primaryType=nt:unstructured\" -F\"jcr:title=%s\" http://localhost:4502/content/dam/gigamon/%s/jcr:content"
dirTemplate = "mkdir ~/projects/zeltiq/assets/%s";

#curlTemplate = "curl -u admin:admin -F\"sling:resourceType=cq/tagging/components/tag\" -F\"jcr:title=%s\" -F\"jcr:primaryType=cq:Tag\" http://localhost:4502/etc/tags/wd/%s"

class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg

def createFlatName(name):

	name = name.lower();

	name=name.replace(" ", "_").replace("(", "").replace(")", "").replace("/", "_").replace("&", "\&").replace("'", "");

	return name;

def processFile(filename):

	infile = open(filename, 'r');

	tagArray = [];

	line = infile.readline();
	currentHighWaterMark = 0

	lineCounter = 0;

	while line != '':

		lineCounter = lineCounter + 1;

		tabCount = 0;
		for char in line:
			if char == '\t':
				tabCount = tabCount + 1;

		if currentHighWaterMark > tabCount:

			tagArray = tagArray[0:tabCount];
			currentHighWaterMark = tabCount;

		elif currentHighWaterMark == tabCount:

			tagArray = tagArray[:-1]
			currentHighWaterMark = tabCount;

		else:

			currentHighWaterMark = tabCount;

		line = line.strip();

		tagArray.append(createFlatName(line));

		# print '/'.join(tagArray), tabCount, currentHighWaterMark;

		print curlTemplateMain % ('/'.join(tagArray));
		print curlTemplateContent % (line, '/'.join(tagArray));
		print dirTemplate % ('/'.join(tagArray));

		line = infile.readline();

def main(argv=None):
	if argv is None:
		argv = sys.argv
	try:
		try:
			opts, args = getopt.gnu_getopt(argv[1:], "ho:f:v", ["help", "output=", "filename="])
		except getopt.error, msg:
			raise Usage(msg)

		filename = "/Users/adamtrissel/projects/zeltiq/scripts/tags/data/createDamStructure.txt";	
	
		# option processing
		for option, value in opts:
			if option == "-v":
				verbose = True
			elif option in ("-h", "--help"):
				raise Usage(help_message)
			elif option in ("-o", "--output"):
				output = value
			elif option in ("-f", "--filename"):
				filename = value
	
	except Usage, err:
		print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
		print >> sys.stderr, "\t for help use --help"
		return 2

	if filename == None:
		print "Please input filename (-f filename)"
	else:
		processFile(filename);


if __name__ == "__main__":
	sys.exit(main())
