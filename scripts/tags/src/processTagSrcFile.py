#!/usr/bin/env python
# encoding: utf-8
"""
processTagSrcFile.py

This takes a tab delimited file and creates tag hierarchies
The toplevel namespace domain is hard coded

Created by adam n trissel on 2015-11-09.
Copyright (c) 2015 Hero Digital. All rights reserved.
"""

import sys
import getopt


help_message = '''
The help message goes here.
'''
curlTemplate = "curl -u admin:admin -F\"sling:resourceType=cq/tagging/components/tag\" -F\"jcr:title=%s\" -F\"jcr:primaryType=cq:Tag\" http://localhost:4502/etc/tags/gigamon/%s"

class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg

def createFlatName(name):

	name = name.lower();

	name=name.replace(" ", "_").replace("(", "").replace(")", "").replace("/", "_").replace("&", "\&").replace("'", "");

	return name;

def processFile(filename):

	infile = open(filename, 'r');

	tagArray = [];

	line = infile.readline();
	currentHighWaterMark = 0

	lineCounter = 0;

	while line != '':

		lineCounter = lineCounter + 1;

		tabCount = 0;
		for char in line:
			if char == '\t':
				tabCount = tabCount + 1;

		if currentHighWaterMark > tabCount:

			tagArray = tagArray[0:tabCount];
			currentHighWaterMark = tabCount;

		elif currentHighWaterMark == tabCount:

			tagArray = tagArray[:-1]
			currentHighWaterMark = tabCount;

		else:

			currentHighWaterMark = tabCount;

		line = line.strip();

		tagArray.append(createFlatName(line));

		# print '/'.join(tagArray), tabCount, currentHighWaterMark;

		print curlTemplate % (line.replace("/","%2F"), '/'.join(tagArray));

		line = infile.readline();

def main(argv=None):
	if argv is None:
		argv = sys.argv
	try:
		try:
			opts, args = getopt.gnu_getopt(argv[1:], "ho:f:v", ["help", "output=", "filename="])
		except getopt.error, msg:
			raise Usage(msg)

		filename = None;	
	
		# option processing
		for option, value in opts:
			if option == "-v":
				verbose = True
			elif option in ("-h", "--help"):
				raise Usage(help_message)
			elif option in ("-o", "--output"):
				output = value
			elif option in ("-f", "--filename"):
				filename = value
	
	except Usage, err:
		print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
		print >> sys.stderr, "\t for help use --help"
		return 2

	if filename == None:
		print "Please input filename (-f filename)"
	else:
		processFile(filename);


if __name__ == "__main__":
	sys.exit(main())
