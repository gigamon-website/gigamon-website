#!/usr/bin/env python
# encoding: utf-8

import random
import requests
from requests.auth import HTTPBasicAuth
import sys

host = 'cosmo-aem-auth.herodigital.com' # host = raw_input('Host: ')
port = 4502 # port = input('Port: ')
username = 'admin' # username = raw_input('Username: ')
password = 'M4C@rjdN*m58!S1e' # password = raw_input('Password: ')
base_url = "http://{}:{}/".format(host, port)
asset_folder = 'set1'

def retrieve_assets():
	assets = []

	url = "{}{}".format(base_url, "content/dam/tcolv/{}.infinity.json".format(asset_folder))
	response = requests.get(url, auth=HTTPBasicAuth(username, password))
	payload = response.json()
	
	for key in payload.keys():
		if ':' not in key:
			assets.append(key)

	return assets

def retrieve_shot_types():
	shot_types = []

	url = "{}{}".format(base_url, 'etc/tags/tcolv/shot_type.infinity.json')
	response = requests.get(url, auth=HTTPBasicAuth(username, password))
	payload = response.json()

	for shot_type in payload.keys():
		if ':' not in shot_type:
			shot_types.append("tcolv:shot_type/" + shot_type)

	return shot_types

def retrieve_skus():
	skus = []

	url = "{}{}".format(base_url, 'etc/tags/tcolv/SKU.infinity.json')
	response = requests.get(url, auth=HTTPBasicAuth(username, password))
	payload = response.json()

	for business_unit in payload.keys():
		if ':' not in business_unit:
			for product in payload[business_unit].keys():
				if ':' not in product:
					for family in payload[business_unit][product].keys():
						if ':' not in family:
							for parent_part in payload[business_unit][product][family].keys():
								if ':' not in parent_part:
									for sku in payload[business_unit][product][family][parent_part].keys():
										if ':' not in sku:
											skus.append("tcolv:SKU/{}/{}/{}/{}/{}".format(business_unit, product, family, parent_part, sku))

	return skus

def create_asset_tags(assets, shot_types, skus, gallery_orders):
	for asset in assets:
		url = "{}{}{}/{}{}".format(base_url, 'content/dam/tcolv/', asset_folder, asset, '/jcr%3Acontent/metadata')
		data = {'cq:tags': ['tcolv:asset_use/web_image_gallery', random.choice(shot_types), random.choice(skus)]}
		requests.post(url, auth=HTTPBasicAuth(username, password), data=data)
		data = {'gallery_order': random.choice(gallery_orders)}
		requests.post(url, auth=HTTPBasicAuth(username, password), data=data)

def main(argv=None):
	assets = retrieve_assets();
	shot_types = retrieve_shot_types();
	skus = retrieve_skus();
	gallery_orders = ['1', '2', '3', '4']
	create_asset_tags(assets, shot_types, skus, gallery_orders);

if __name__ == '__main__':
	sys.exit(main())
