#!/usr/bin/env python
# encoding: utf-8
"""
gigamonFileMigration.py

Created by adam n trissel on 2017-05-31.
Copyright (c) 2016 Hero Digital. All rights reserved.
"""

import sys
import getopt
import os.path

help_message = '''
The help message goes here.
'''

class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg

LIBRARYNAME = 0;
CONTENTID = 1;
FILENAME = 2;
FINALAEMTITLE = 3;
FILETYPE = 4;
ECOSYSTEMPARTNERTYPE = 5;
LANGUAGE = 6;
PRODUCTLINE = 7;
PROGRAMS = 8;
RESOURCETYPE = 9;
LIBRARIES = 10;
UPLOADOWNER = 11;	
DUMMY = 12;	

def processFile():

	infile = open("/Users/adamtrissel/projects/gigamon/scripts/migration/data/AssetUpload0617v2.txt", 'r');

	line = infile.readline(); ##burn header
	line = infile.readline();

	while line != '':

		DummyStart, \
		LibraryName, \
		ContentID, \
		FileName, \
		FinalAEMTitle, \
		FileType, \
		EcosystemPartnerType, \
		Language, \
		ProductLine, \
		Programs, \
		ResourceType, \
		Libraries, \
		UploadOwner, \
		Dummy = line.strip().split("\t");

		##print (FileName + " : " + ResourceType);	

		fileName = "/Users/adamtrissel/projects/gigamon/scripts/migration/data/Files/" + FileName + "." + FileType.lower();

		if os.path.isfile(fileName):

			##print fileName, "exists"
			pass;
		
		else:
		
			print fileName, "does not exist"

		line = infile.readline();	

def main(argv=None):
	if argv is None:
		argv = sys.argv
	try:
		try:
			opts, args = getopt.gnu_getopt(argv[1:], "ho:v", ["help", "output="])
		except getopt.error, msg:
			raise Usage(msg)
	
		# option processing
		for option, value in opts:
			if option == "-v":
				verbose = True
			elif option in ("-h", "--help"):
				raise Usage(help_message)
			elif option in ("-o", "--output"):
				output = value
	
	except Usage, err:
		print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
		print >> sys.stderr, "\t for help use --help"
		return 2

	processFile();


if __name__ == "__main__":
	sys.exit(main())
