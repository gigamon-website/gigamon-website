#!/usr/bin/env python
# encoding: utf-8
"""
moveAssets.py

This is a utility program to move assets from the original Source taxonomy (folder structure) to 
a local disk image of the AEM Asset Taxonomy.  

You could make the argument that it would be better to use the webDAV functionality built into AEM, but we 
have found this unstable at high volume.

The object would be to move these into folders, localize them, then use cadaver to move them into AEM.

python ./src/processAssets.py -f ./data/assetMove030716.csv -s "/Volumes/My Passport 1/MT/" -d "/Volumes/BelkinAssets//belkin/assets/"

Created by adam n trissel on 2017-05-31.
Copyright (c) 2017 Hero Digital. All rights reserved.
"""

import sys
import getopt
import os.path
import shutil
import datetime;

help_message = '''
The help message goes here.
'''

class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg

def createFlatName(name):

	name = name.lower();

	name=name.replace(" ", "_").replace("(", "").replace(")", "").replace("/", "_").replace("'", "").replace(".", "");

	return name;

def createDatePartition(creationDate, partition):

	## 2008-12-20 21:37:31,	2015-11-10 12:53:21

	dateArray = [];

	if partition:

		datePart, timePart = creationDate.split(" ");
		mm, dd, yy = datePart.split("/");

		##dateArray = creationDate.split("-")[:2];
		dateArray.append("20" + yy);

	##print creationDate, partition, dateArray;	

	return dateArray;

def appendArrayNoNull(levelArray, element):

	if element != None and element != "":
		levelArray.append(element);

	return levelArray;

"""

This is used to decrease the amount of duplicate filenames in the system
Scene 7 strips directory and extension from the base filename. so myfile.jpg and myfile.psd
Are hashed to the same name. Scene 7 has a "flat" naming hierarchy, so these files are effectivly the 
same file in Scene 7.

So we want to rename our files myfile_jpg.jpg and myfile_psd.psd within assets to decrease naming contention.

"""

def mungeFileName(filename, extension):

	return filename.lower().replace("&", "").replace("  ","").replace(" ", "_") + "." + extension;

def processAssets(filename, source, destination, test):

	infile = open(filename, 'r');

	line = infile.readline();

	lineCount = 0;

	while line != '':

		lineCount = lineCount + 1;

		line = line.strip();

		if len(line.split("\t")) >= 3 :

			##Filepath, Filepath_no_dot, level1, level2, level3

			DummyStart, \
			LibraryName, \
			ContentID, \
			FileName, \
			FinalAEMTitle, \
			FileType, \
			EcosystemPartnerType, \
			Language, \
			ProductLine, \
			Programs, \
			ResourceType, \
			Libraries, \
			UploadOwner, \
			Dummy = line.strip().split("\t");

			filePath = source + FileName + "." + FileType.lower();
			fileN = mungeFileName(FileName, FileType.lower());
			destPath = destination + ResourceType.lower().replace(" ", "_") + "/" + fileN;

			print filePath, "->", destPath;

			## if the file exists, move it
			if os.path.isfile(filePath):

				try:
					if test:

						print "Copied:", filePath, "To", destPath;

						if os.path.isdir((destPath)):
							pass
						else:
							print "destination path does not exist", (destPath)
					
					else:

						shutil.copy2(filePath, destPath);

				except:
					print "Failed to copy file", filePath, "To", destPath;
			else:
				print "Could not find file:", filePath, "Not copied";  

		else:

			print "ERROR:", len(line.split("\t")), line;

		if lineCount % 100 == 0:
			print lineCount, datetime.datetime.now();

		line = infile.readline();

	infile.close();

def main(argv=None):

	filename = "/Users/adamtrissel/projects/gigamon/scripts/migration/data/AssetUpload0617v2.txt";
	##source = "/Volumes/Seagate Expansion Drive";
	source = "/Users/adamtrissel/projects/gigamon/scripts/migration/data/Files/"
	destination = "/Volumes/dam/gigamon/";
	##destination = "/Users/adamtrissel/projects/tcolv/assets/";
	test = False;

	if argv is None:
		argv = sys.argv
	try:
		try:
			opts, args = getopt.gnu_getopt(argv[1:], "ho:d:s:f:v:t:p", ["help", "output=", "filename=", "source=", "destination="])
		except getopt.error, msg:
			raise Usage(msg)
	
		# option processing
		for option, value in opts:

			if option == "-v":
				verbose = True
			elif option in ("-h", "--help"):
				raise Usage(help_message)
			elif option in ("-o", "--output"):
				output = value
			elif option in ("-f", "--filename"):
				filename = value
			elif option in ("-s", "--source"):
				source = value
			elif option in ("-d", "--destination"):
				destination = value
			elif option in ("-t"):
				test = True
	
	except Usage, err:
		print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
		print >> sys.stderr, "\t for help use --help"
		return 2

	if filename == None or source == None or destination == None:
		print "filename, source and destination parameters are necessary";
		exit();

	processAssets(filename, source, destination, test);	


if __name__ == "__main__":
	sys.exit(main())
