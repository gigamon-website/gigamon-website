package tagUtil;

import java.util.ArrayList;
import java.util.List;

public class AttributeArray {

	private List<String> tagList = new ArrayList<String>();

	private String nodeName;
	private String title;
	private String description;

	public List<String> getTagList() {
		return tagList;
	}

	public void setTagList(List<String> tagList) {
		this.tagList = tagList;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String toString() {
		
		String retVal = "Node Name: " + nodeName + "\n" ;
		retVal += "\tTitle: " + title + "\n";
		retVal += "\tDescription: " + description + "\n";
		
		for (String tag : tagList){
			
			retVal += "\tTag: " + tag + "\n";
			
		}
		
		return retVal;
		
	}
	
}
