package com.hero.jcr.commandline;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

//import org.apache.commons.codec.binary.Base64;


public class TestMultiPartFormUpload {

	public static void main(String[] args) {
		
		String pathOnDisk = "/Users/adamtrissel/projects/magic/vectordl.m4v";
		
		try{

			byte[] payload = loadFileAsByteArray(pathOnDisk);
			String outputString = new String(Base64.getEncoder().encode(payload));
			System.out.println(outputString);

		} catch (IOException e) {
			
			System.out.println("failed to load file, sorry: " + e.getMessage());
			
		}
		
	}
	
    // Simplest read of an entire file into a byte array
    // Will throw exceptions for file not found etc
    private static byte[] loadFileAsByteArray(String path) throws IOException {
        File file = new File(path);

        InputStream is = new FileInputStream(file);

        byte[] data = new byte[(int) file.length()];

        is.read(data);

        return data;
    }    

}
