package com.hero.jcr.commandline;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import tagUtil.AttributeArray;
import tagUtil.DateArray;
import tagUtil.ShotTypeArray;

import com.hero.jcr.util.CqHelper;

/*
 * This program takes a file of formated asset data and applies tags to the matching
 * Asset in the DAM.
 * 
 * Adam Trissel
 * Hero Digital
 * 08.01.16
 */

public class UpdateAssetTags {
	
	static String REPOLOCATION = "http://localhost:4502";
	static String REPOUSER = "admin";
	static String REPOPASSWORD = "admin";
	static String DEFAULTFILE = "/Users/adamtrissel/projects/WD/repo/WDDAM/scripts/migration/data/newFormatShortTest.txt";

	static String tagPrototype = "wd:%s/%s";
	static String destinationPrototype = "http://%s:%s/content/dam/wd/%s";

	static String tagFieldPrototype = "-F \"%s\"";	
	
	static final HashMap<String, String> dateTagArray = DateArray.getDateArray();
	static final HashMap<String, String> shotTypeArray = ShotTypeArray.getShotTypeArray();
	
	static HashMap<String, String> tagHashMap = new HashMap<String, String>();
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(UpdateAssetTags.class.getName());	

    private static final Map<String, String> destDict;
    static
    {
        destDict = new HashMap<String, String>();

    	destDict.put("Print - Collateral - Product Overview", "print/collateral/product_overview");
    	destDict.put("Print - Collateral - Comparison Chart", "print/collateral/comparison_chart");
    	destDict.put("Video - Product Overview", "video/product_overview");
    	destDict.put("Print - Retail Tradeshow Display - Shelf Talkers", "print/retail_tradeshow_display/shelf_talkers");
    	destDict.put("Print - Retail Tradeshow Display - Pull-Up-Banner", "print/retail_tradeshow_display/pull-up-banners");
    	destDict.put("Private Marcom - Eblast - Source File", "private_Marcom/eblast/source_files");
    	destDict.put("Print - Collateral - Quick Installation Guide", "print/collateral/quick_installation_guide");
    	destDict.put("Print - Collateral - Postcard", "print/collateral/postcard");
    	destDict.put("Print - Retail Tradeshow Display - POP Display", "print/retail_tradeshow_display/pop_display");
    	destDict.put("Print - Collateral - Guidelines", "print/collateral/guidelines");
    	destDict.put("Video - Tutorials", "video/tutorials");
    	destDict.put("Print - Collateral - Case Study", "print/collateral/case_study");
    	destDict.put("Private Marcom - A+ Pages", "private_marcom/a+_pages");
    	destDict.put("Print - Collateral - Spec Data Sheet", "print/collateral/spec_data_sheet");
    	destDict.put("Print - Retail Tradeshow Display - Endcap", "print/retail_tradeshow_display/endcap");
    	destDict.put("Digital Assets - Web Banner - JPG", "digital_assets/web_banner/jpg");
    	destDict.put("Print - Advertisement - Full Page Ad", "print/advertisement/full_page_ad");
    	destDict.put("Private Marcom - Eblast", "private_marcom/eblast");
    	destDict.put("Print - Retail Tradeshow Display - Wobbler", "print/retail_tradeshow_display/wobbler");
    	destDict.put("Private Marcom - A Pages - Source File", "private_marcom/a+_pages");
    	destDict.put("Print - Collateral - User Manual", "print/collateral/user_manual");
    	destDict.put("Print - Collateral - Flyer", "print/collateral/flyer");
    	destDict.put("Print - Collateral - Setup Sheet", "print/collateral/setup_sheet");
    	destDict.put("Video - Features", "video/advertisement");

    }	
	
	public static void main(String[] args) throws Exception {
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		processFile(cliHash.get("repo"),
					cliHash.get("username"),
					cliHash.get("password"),
					cliHash.get("filename"));
		
	}
	
	private static String createFlatName(String name){
		
		String retVal = null;
		
		name = name.toLowerCase();
		
		//retVal = name.replace(" ", "_").replace("(", "").replace(")", "").replace("/", "_").replace("&", "\\&").replace("'", "");
		retVal = name.replace(" ", "_").replace("(", "").replace(")", "").replace("/", "_").replace("'", "");
		
		return retVal;
	}
	
	private static String processNodeName(String filename, String language, String asset_type){
		
		String destination = "./content/dam/wd/";
		destination = destination + language.toLowerCase() + "/";
		destination = destination + destDict.get(asset_type) + "/";
		destination = destination + filename;
		
		return destination;
		
	}
	
	private static String processAssetType(String asset_type){
		
		return makeFormalName("wd", "asset_type", asset_type);
		
	}
	
	private static String processCampaign(String campaign){
		
		return makeFormalName("wd", "campaign", campaign);
		
	}
	
	private static String processProduct(String product){
		
		return makeFormalName("wd", "product", product);
		
	}
	
	private static String processResolution(String resolution){
		
		String resolutionTag = "";
		
		resolution = resolution.toLowerCase();
		if (resolution.startsWith("high")){
			
			resolutionTag = "wd:resolution/high_resolution_print_ready";
			
		} else if(resolution.startsWith("low")){
			
			resolutionTag = "wd:resolution/low_resolution_print_ready";
			
		} else {
			
			resolutionTag = "wd:resolution/cmyk";
			
		}
		
		return resolutionTag;
		
	}
	
	private static void processFile(String repo, String username, String password, String filename) throws IOException {
		
		/*
		 * open up the repo establish a session and find the root node
		 */
		
		CqHelper helper = new CqHelper();
		
		Session session = null;
		try {
			session = helper.getSession(repo, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Node root = null;
		
		try {
			root = session.getRootNode();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		if (session == null || root == null){
			return;
		}
		
		Path file = Paths.get(filename);
        BufferedReader reader = Files.newBufferedReader(file,Charset.forName("UTF-8"));
        StringBuilder content = new StringBuilder();
        String line = null;

        long recordCounter = 0;
        
        line = reader.readLine();  //burn the header
        
        while ((line = reader.readLine()) != null) {
        	
        	recordCounter++;
        	
            String[] columns = line.split("\t");
            
            if (columns.length >= 9){
            	
            	AttributeArray attributes = createAttributeArray(columns);

            	System.out.println(attributes.toString());
            	System.out.println();
            	
                Boolean success = setPropertiesOnNode(root, attributes);
                
                //save work every 1000 records
                if (recordCounter % 1000 == 0) {
                	
                	try {
                		log.info("****saving session****");
						session.save();
					} catch (RepositoryException e) {
						e.printStackTrace();					}
                	
                }

            }
            
        }
        
        /*
         * close the session
         */
        
        try {
			session.save();
		} catch (RepositoryException e) {
			System.out.println("rolled back");
		}
        
        session.logout();
        
        reader.close();
		
	}
	
	private static String decodeTags(String tag){
		
		String returnTag = "";
		
		if (tagHashMap.keySet().contains(tag)){
			
			returnTag = tagHashMap.get(tag);
			
		}
		
		return returnTag;
	}
	
	private static List<String> unwrapTags(String newTags){
		
		List<String> retVal = new ArrayList<String>();

		if (newTags.contains("|")){

			String[] tags = newTags.split("\\|");
			
			retVal = Arrays.asList(tags);
			
		} else {
			
			retVal.add(newTags);
			
		}
		
		return retVal;
		
	}
	
	/*
	 * marshal the attribute array bean
	 */
	
	private static AttributeArray createAttributeArray(String[] columns){
		
		String asset_path;
		String asset_type;
		String campaign;
		String capacity;
		String compatibility;
		String form_factor;
		String internal_audience;
		String language;
		String partner_brands;
		String product;
		String regional;
		String resolution;
		String size;
		String target_audience;
		String themes;
		String year;
		
        ArrayList<String> tagList = new ArrayList<String>();

        String mt_keys_concatenated;
        String internal_only;
        String id2;     
        
    	asset_path = columns[0];
    	asset_type = columns[1];
    	campaign = columns[2];
    	capacity = columns[3];
    	compatibility = columns[4];
    	form_factor = columns[5];
    	internal_audience = columns[6];
    	language = columns[7];
    	partner_brands = columns[8];
    	product = columns[9];
    	regional = columns[10];
    	resolution = columns[11];
    	size = columns[12];
    	target_audience = columns[13];
    	themes = columns[14];
    	year = columns[15];
        
        tagList.addAll(unwrapTags(asset_type));
        tagList.addAll(unwrapTags(campaign));
        tagList.addAll(unwrapTags(capacity));
        tagList.addAll(unwrapTags(compatibility));
        tagList.addAll(unwrapTags(form_factor));
        tagList.addAll(unwrapTags(internal_audience));
        tagList.addAll(unwrapTags(language));
        tagList.addAll(unwrapTags(partner_brands));
        tagList.addAll(unwrapTags(product));
        tagList.addAll(unwrapTags(regional));
        tagList.addAll(unwrapTags(resolution));
        tagList.addAll(unwrapTags(size));
        tagList.addAll(unwrapTags(target_audience));
        tagList.addAll(unwrapTags(themes));
        tagList.addAll(unwrapTags(year));
        
//        for (String tag : unwrapTags(newTags)){
//        	tagList.add(tag);
//        }
        
        AttributeArray attributes = new AttributeArray();
        
        attributes.setNodeName(asset_path);
        
        attributes.setTagList(tagList);
        
		return attributes;
		
	}
	
	/*
	 * Combine tag arrays
	 */
	
	private static String[] concatTagArrays(String[] existingTags, String[] newTags){
		
			if(existingTags == null){
				
				return newTags;
				
			} else {

				   int aLen = existingTags.length;
				   int bLen = newTags.length;
				   String[] combinedArray= new String[aLen+bLen];
				   System.arraycopy(existingTags, 0, combinedArray, 0, aLen);
				   System.arraycopy(newTags, 0, combinedArray, aLen, bLen);
				   return combinedArray;		

			}

	}
	
	private static String[] getExistingTags(Node n){
		
		Boolean hasTags = false;
		Property existingTags = null;

    	try{

    		existingTags = n.getProperty("cq:tags");
    		hasTags = true;
    		
    	} catch (Exception e){
    		hasTags = false;
    	}

    	if(hasTags){
    		
    		try{

    			Value[] tagValues = existingTags.getValues();
            	String[] strTagValues = new String[tagValues.length];
            	
            	for (int i = 0; i < tagValues.length; i++){
            		
            		strTagValues[i] = tagValues[i].getString();
            		
            	}
            	
            	return strTagValues;
    			
    		} catch (Exception e){
    			
    			return null;
    			
    			
    		}

    		
    	} else {

    		return null;
    		
    	}
    	
	}
	
	/*
	 * take the attributes, and attach them to the node specified by the URL, which will be relative to "/content/dam/belkin"
	 */
	
	private static Boolean setPropertiesOnNode(Node root, AttributeArray attributes){
		
        String fullNodeName = attributes.getNodeName() + "/jcr:content/metadata";
        
        Boolean success = false;
        
        try {
			
        	Node n = root.getNode("." + fullNodeName);
        	
        	String[] existingTags = getExistingTags(n);
        	
            String[] tagsArray = new String[attributes.getTagList().size()];
            tagsArray = attributes.getTagList().toArray(tagsArray);
            
            String[] combinedArray = concatTagArrays(existingTags, tagsArray);
            
            
            //n.setProperty("cq:tags", combinedArray); //retain old tags	
            n.setProperty("cq:tags", tagsArray); //blow old tags away
            
            success = true;
            
		} catch (RepositoryException e) {
			e.printStackTrace();
			//log.severe("failed to find node: " + fullNodeName + "/jcr:content/metadata");
		}
        
        return success;
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		String filename = DEFAULTFILE;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("f", "filename", true, "Input file name");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}
			
			if(cmd.hasOption('f')){
				filename = cmd.getOptionValue("f");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		cliHash.put("filename", filename);
		
		return cliHash;
		
	}
	
	private static String flattenName(String name) {
		
		name = name.toLowerCase();
		
		name = name.replace(" - ","/");
		name = name.replace(" ", "_");
		
		return name;
		
	}
	
	private static String makeFormalName(String namespace, String type, String value) {
		
		String retVal = flattenName(namespace) + ":" +
	                    flattenName(type) + "/" + 
	                    flattenName(value);
		
		return retVal;
		
	}	

}
