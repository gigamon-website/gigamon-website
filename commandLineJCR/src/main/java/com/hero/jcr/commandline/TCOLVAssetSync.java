package com.hero.jcr.commandline;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hero.jcr.model.ResourceBean;
import com.hero.jcr.util.CqHelper;

/**
 * @author adamtrissel
 * 
 * This is a command line utility that scans a country specific set of DAM assets
 * Then replaces references to matching references in page content
 * 
 * The logic is a follows:
 * 
 * 1) pull all the DAM assets for a specific country. We store these in a hashmap with the key as the asset path 
 *    and the values containing the legacy id and asset key.
 *    
 * 2) select all references to the resource mosaic in the new language pages. Itereate through all the mosaics, 
 *    pulling JSON from the overrideField array. check to see if the asset mentioned in the field (damResource) matches, 
 *    after modifying it for the new language, a value in the DAM asset hashmap, then replace it, otherwise don't.
 *    
 * 3) select all references to the image component, and replace any fileReferences that occure in the hashmap
 * 
 * 4) select all cta's and replace any that occur in the hashmap
 * 
 * 5) select all textImage components and replace the cta link if it matches the hashmap.
 *
 */
public class TCOLVAssetSync {
	
	//these should be passed in on the command line
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(TCOLVAssetSync.class.getName());
	
	private static ArrayList<String> assetArray = new ArrayList<String>();
	private static HashMap<String, ArrayList<String>> assetTagMap = new HashMap<String, ArrayList<String>>();
	private static ArrayList<String> uniqueTagList = new ArrayList<String>();
	
	static String REPOLOCATION = "http://tcolv-aem-auth.herodigital.com:4502";
	static String REPOUSER = "admin";
	static String REPOPASSWORD = "M4C@rjdN*m58!S1e";
	static String FILLER = "Unknown";
	
	static ArrayList<String> allowedTypes = new ArrayList<String>();
	static HashMap<String, String> languageMap = new HashMap<String, String>();
	static HashMap<String, AssetBean> damAssets = new HashMap<String, AssetBean>();
	
	public static void main(String[] args) throws Exception {
		
		//get command line attributes
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		String repo = cliHash.get("repo");
		String username = cliHash.get("username");
		String password = cliHash.get("password");
		
		if (repo == null){
			log.log(Level.WARNING, "Starting to create asset map");
			return;
		}
		
		log.log(Level.INFO, "Starting to query assets");
		damAssets = getDamAssets(repo, username, password);
		log.log(Level.INFO, "Finished asset map");
		
		System.out.println("Number of Assets: " + assetArray.size());
		System.out.println("Number of Tags: " + uniqueTagList.size());
		System.out.println("Number in Tag Asset Map: " + assetTagMap.size());
		
		outputAssetsToFile(assetArray);
		outputTagsToFile(uniqueTagList);
		outputAssetTagMapToFile(assetTagMap);
		
	}
	
	private static void outputAssetsToFile(ArrayList<String> assetList){
		
		try{

			PrintWriter writer = new PrintWriter("/tmp/assets.txt", "UTF-8");

			for(String nodePath : assetList){

				writer.println(nodePath);
				
			}
			
			writer.close();
			
		} catch (Exception e){
			//no operation
		}
		
	}
	
	private static void outputTagsToFile(ArrayList<String> tagList){
		
		try{

			PrintWriter writer = new PrintWriter("/tmp/tags.txt", "UTF-8");

			for(String nodePath : tagList){

				writer.println(nodePath);
				
			}
			
			writer.close();
			
		} catch (Exception e){
			//no operation
		}
		
	}
	
	private static void outputAssetTagMapToFile(HashMap<String, ArrayList<String>> tagMap){
		
		try{

			PrintWriter writer = new PrintWriter("/tmp/assetTagMap.txt", "UTF-8");

			for(String nodePath : tagMap.keySet()){
				
				ArrayList<String> tags = tagMap.get(nodePath);
				
				for(String tag :tags){
					
					writer.println(nodePath + "\t" + tag);
					
				}

				
			}
			
			writer.close();
			
		} catch (Exception e){
			//no operation
		}		
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		
		return cliHash;
		
	}
	
	private static void outputAssets(String assetPath){
		
		if (!assetArray.contains(assetPath)){
			
			assetArray.add(assetPath);
			
		}
		
	}
	
	private static void outputAssetTagMap(String assetPath, ArrayList<String> tagValues){
		
		ArrayList<String> tags = new ArrayList<String>();
		
		if(!assetTagMap.containsKey(assetPath)){
			
			
			for (String v : tagValues){
				
					tags.add(v);
					outputTags(v);
					
			}
			
		} //we don't expect assets to be duplicated, but if for some reason they are, ignore the subsequent versions (n.getPath() is unique)
		
		//if the asset has tags, then put it in the map;
		
		if(tags.size() != 0){

			assetTagMap.put(assetPath, tags);
			
		}
		
	}
	
	private static void outputTags(String tag){
		
		if(!uniqueTagList.contains(tag)){
			
			uniqueTagList.add(tag);
			
		}
		
	}
	
	private static void processAssets(String assetPath, ArrayList<String> tagValues){
		
		outputAssets(assetPath);
		outputAssetTagMap(assetPath, tagValues);
		
	}
	
	private static HashMap<String, AssetBean> getDamAssets(String repo, String username, String password) throws Exception {
		
		HashMap<String,AssetBean> damAssets = new HashMap<String,AssetBean>();

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		
		String q = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([/content/dam/tcolv]) and" + 
		           "[jcr:primaryType] = 'dam:Asset' and [jcr:createdBy] <> 'workflow-process-service'";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		long nodeCounter = 0;
		
		while (ni.hasNext() && nodeCounter <= 20000){
			

			Node n = ni.nextNode();
			
			String nodeName = n.getPath();
			
			if (!nodeName.contains("subasset") && !nodeName.contains("jcr:content"))
			{
				
				nodeCounter++;
				ArrayList<String> tags = new ArrayList<String>();
				
				try{
					Property tagProperty = n.getProperty("./jcr:content/metadata/cq:tags");
					Value[] tagValues = tagProperty.getValues();
					
					for (Value tagValue : tagValues){
						
						System.out.println("\t" + tagValue.getString());
						
						try{
							tags.add(tagValue.getString());
						} catch (Exception e){
							//no operation
						}
						
					}
					
					
				} catch(Exception e){
					
					System.out.println("\t" + "Strange, there is not tag associated with this asset");
				
				}
				
				System.out.println(nodeCounter + " : " + n.getPath());
				processAssets(n.getPath(), tags);
				
			}
			
		}
		
		session.logout();
		
		return damAssets;
		
	}
	
}
