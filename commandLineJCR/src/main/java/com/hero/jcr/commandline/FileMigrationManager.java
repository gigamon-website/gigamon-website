package com.hero.jcr.commandline;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.methods.HttpPost;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.amazonaws.util.StringUtils;
import com.googlecode.sardine.Sardine;
import com.googlecode.sardine.SardineFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.nio.charset.Charset;
import java.nio.file.Files;


public class FileMigrationManager {
	
//	static String REPOLOCATION = "http://tcolv-aem-auth.herodigital.com:4502";
//	static String REPOUSER = "admin";
//	static String REPOLOCATION = "http://52.27.163.130:4502";
//	static String REPOLOCATION = "http://52.88.106.180:4502";
	static String REPOLOCATION = "http://localhost:4502";
	static String REPOUSER = "admin";
//	static String REPOPASSWORD = "M4C@rjdN*m58!S1e";
//	static String REPOPASSWORD = "K#7051X1*dn3";
//	static String REPOPASSWORD = "~s12bY+M@I6Zv";
	static String REPOPASSWORD = "admin";
	static String DEFAULTFILE = "/Users/adamtrissel/projects/zeltiq/zeltiq/scripts/migration/data/zeltiqAssetsFile.txt";
	static String DEFAULTACCESSKEY = "AKIAI4CNPS7CZNTMUF6Q";
	static String DEFAULTSECRETKEY = "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";
	static String DEFAULTJCRROOT = "/content/dam/gigamon";
	
	private static final Logger log = Logger.getLogger(AddGigamonMetaData.class.getName());	
	
	private static Options options = new Options();

	public static void main(String[] args) {
		
		HashMap<String, String> cliOptions = getCommandLineArgs(args);
		
		//String filename = "/Users/adamtrissel/Pictures/Jellies/IMG_0058.JPG";
		String host = cliOptions.get("repo");
		String path = cliOptions.get("jcrroot");
		String user = cliOptions.get("username");
		String password = cliOptions.get("password");
		String assetFileName = cliOptions.get("filename");
		
		String accesskey=cliOptions.get("accesskey");
		String secretkey=cliOptions.get("secretkey");
		
		ArrayList<String> files = new ArrayList<String>();
		
		files.add("Becker-Edward/AB-Beck-002F/adjusted/AB-Beck-002F(1)-FV-RGB.psd");
		files.add("Becker-Edward/AB-Beck-002F/adjusted/AB-Beck-002F(17w)-FV-RGB.psd");
		files.add("Becker-Edward/AB-Beck-002F/adjusted/AB-Beck-002F(8w)-FV-RGB.psd");
		files.add("Becker-Edward/AB-Beck-002F/Original/Becker - Part 1.jpg");
		files.add("Becker-Edward/AB-Beck-002F/Original/Becker - Part 2.jpg");
		files.add("Becker-Edward/AB-Beck-002F/Original/Becker - Part 3.jpg");
		files.add("Becker-Edward/AB-Beck-002F/Original/Becker_002_After.jpg");
		files.add("Becker-Edward/AB-Beck-002F/Original/Becker_002_Before.jpg");
		files.add("Becker-Edward/AB-Beck-002F/PSD files/AB-Beck-002F(17w)-FV-3Set.psd");
		files.add("Becker-Edward/AB-Beck-002F/PSD files/AB-Beck-002F(9w)2-FV-2Set.psd");
		files.add("Becker-Edward/FL-Beck-001M/Original/BEC 001_2up.psd");
		files.add("Becker-Edward/FL-Beck-001M/PSD Files/FL-Beck-001M(12W)-RV-2Set.psd");
		files.add("Becker-Edward/FL-Beck-001M/singles/FL-Beck-001M(1)-RV.jpg");
		files.add("Becker-Edward/FL-Beck-001M/singles/FL-Beck-001M(12W)-RV.jpg");

		AmazonS3 s3client = getS3Client(accesskey, secretkey);
		
		try{

			Path file = Paths.get(assetFileName);
	        BufferedReader reader = Files.newBufferedReader(file,Charset.forName("UTF-8"));
	        StringBuilder content = new StringBuilder();
	        String line = null;
	        
	        long recordCounter = 0;
	        
	        while ((line = reader.readLine()) != null) {
	        	
	        	recordCounter++;
	        	
	        	String filename = line;
	        	
				try{
					
					System.out.println("Processing: " + getFileRoot(filename));

					byte[] data = getFileFromS3(s3client, filename);
					
					webDavPostAEM(data, 
				      getFileRoot(filename), 
				      host, 
				      path, 
				      user, 
				      password);			
					
				}catch(Exception e){
					
					System.out.println("failed to move file: " + filename + " : " + e.getMessage());
				
				}
				
	        }
			
			reader.close();
        	
		} catch (Exception e){
			
			System.out.println("Failed to open asset definition file: " + assetFileName);
			
		}
		

		
//		for (String filename : files){
//			
//			try{
//				
//				System.out.println("Processing: " + getFileRoot(filename));
//
//				byte[] data = getFileFromS3(s3client, filename);
//				
//				webDavPostAEM(data, 
//			      getFileRoot(filename), 
//			      host, 
//			      path, 
//			      user, 
//			      password);			
//				
//			}catch(Exception e){
//				
//				System.out.println("failed to move file: " + filename + " : " + e.getMessage());
//			
//			}
//			
//		}
			
	}

	private static String getFileRoot(String fullPath){
		
		String[] elements = fullPath.split("/");
		
		String root = elements[elements.length - 1];
		
		return root;
	}
	
	private static AmazonS3 getS3Client(String accesskey, String secretkey){
		
		AWSCredentials credentials = new BasicAWSCredentials(accesskey, secretkey);
		
		AmazonS3 s3client = new AmazonS3Client(credentials);
		
		return s3client;
		
		
	}
	
	private static void webDavPostAEM(byte[] data, 
									  String filename, 
									  String host, 
									  String path, 
									  String user, 
									  String password){
		
		try{

			Sardine sardine = SardineFactory.begin(user, password);
			//byte[] data = FileUtils.readFileToByteArray(new File(fullname));
			sardine.put(host + path + "/" + filename.replace(" ", "_"), data);
			
		} catch (Exception e){
			
			System.out.println("failed to move file: " + filename + " : " + e.getMessage());
			
		}
		
	}
	
	private static byte[] getFileFromS3(AmazonS3 s3client, String filename) throws IOException{
		
		final String BUCKETNAME = "test-migration-assets";
		final String KEYNAME = filename;
		
		S3Object object = s3client.getObject(new GetObjectRequest(BUCKETNAME, KEYNAME));
		
		S3ObjectInputStream inputStream = object.getObjectContent();
		
		byte[] bytes = IOUtils.toByteArray(inputStream);
		
		return bytes;
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		String filename = DEFAULTFILE;
		String accessKeyID = DEFAULTACCESSKEY;
		String secretKey = DEFAULTSECRETKEY;
		String jcrRoot = DEFAULTJCRROOT;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("f", "filename", true, "Input file name");

		options.addOption("a", "accesskey", true, "AWS Access Key");
		options.addOption("s", "secretkey", true, "AWS Secret Key");
		options.addOption("j", "jcrroot", true, "JCR Root Directory - where are the files to be put");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}
			
			if(cmd.hasOption('f')){
				filename = cmd.getOptionValue("f");
			}
			
			if(cmd.hasOption('a')){
				accessKeyID = cmd.getOptionValue("a");
			}
			
			if(cmd.hasOption('s')){
				secretKey = cmd.getOptionValue("s");
			}
			
			if(cmd.hasOption('j')){
				jcrRoot = cmd.getOptionValue("j");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		cliHash.put("filename", filename);
		cliHash.put("accesskey", accessKeyID);
		cliHash.put("secretkey", secretKey);
		cliHash.put("jcrRoot", jcrRoot);
		
		return cliHash;
		
	}	

}
