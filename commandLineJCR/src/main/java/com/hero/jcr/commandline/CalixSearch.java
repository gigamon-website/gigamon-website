package com.hero.jcr.commandline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hero.jcr.model.ResourceBean;
import com.hero.jcr.util.CqHelper;

/**
 * @author adamtrissel
 * 
 * This is a command line utility that scans a country specific set of DAM assets
 * Then replaces references to matching references in page content
 * 
 * The logic is a follows:
 * 
 * 1) pull all the DAM assets for a specific country. We store these in a hashmap with the key as the asset path 
 *    and the values containing the legacy id and asset key.
 *    
 * 2) select all references to the resource mosaic in the new language pages. Itereate through all the mosaics, 
 *    pulling JSON from the overrideField array. check to see if the asset mentioned in the field (damResource) matches, 
 *    after modifying it for the new language, a value in the DAM asset hashmap, then replace it, otherwise don't.
 *    
 * 3) select all references to the image component, and replace any fileReferences that occure in the hashmap
 * 
 * 4) select all cta's and replace any that occur in the hashmap
 * 
 * 5) select all textImage components and replace the cta link if it matches the hashmap.
 *
 */
public class CalixSearch {
	
	//these should be passed in on the command line
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(Search.class.getName());
	
	static String REPOLOCATION = "http://localhost:4502";
	static String REPOUSER = "admin";
	static String REPOPASSWORD = "admin";
	static String FILLER = "Unknown";
	static String COUNTRYLAN = "es";
	static Boolean PREVIEW = false;
	static String TYPE = "all";
	
	static ArrayList<String> allowedTypes = new ArrayList<String>();
	static HashMap<String, String> languageMap = new HashMap<String, String>();
	static HashMap<String, CalixAsset> damAssets = new HashMap<String, CalixAsset>();
	
	public static void main(String[] args) throws Exception {
		
		//set possible allowed types
		allowedTypes.add("all");
		allowedTypes.add("mosaic");
		allowedTypes.add("textImage");
		allowedTypes.add("cta");
		allowedTypes.add("images");
		allowedTypes.add("rcfixedLinks");
		allowedTypes.add("linkUrl");
		
		languageMap.put("de", "de_de");
		languageMap.put("fr", "fr_fr");
		languageMap.put("it", "it_it");
		languageMap.put("es", "es_es");
		languageMap.put("ja", "ja_jp");
		languageMap.put("ko", "ko_kr");
		languageMap.put("zh", "zh_cn");
		languageMap.put("pt", "pt_br");
		
		//get command line attributes
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		String repo = cliHash.get("repo");
		String username = cliHash.get("username");
		String password = cliHash.get("password");
		String language = cliHash.get("language");
		String type = cliHash.get("type");
		
		log.info("repo: " + repo);
		log.info("user: " + username);
		log.info("password: " + password);
		
		if (!allowedTypes.contains(type)){
			log.log(Level.SEVERE, "supplied type does not match allowable types (all, mosaic, testImage, cta, images). Default is all. Stoping");
			return;
		}
		
		log.log(Level.INFO, "processing " + type + " record types");
		
		log.log(Level.INFO, "Starting to create asset map");
		damAssets = getDamAssets(repo, username, password, language);
		log.log(Level.INFO, "Finished creating asset map");
		
		for (String key : damAssets.keySet()){
		
			if(damAssets.get(key).getFacetList().size() != 0){

				System.out.println(damAssets.get(key).toString());
				
			}
			
		}
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		String language = COUNTRYLAN;
		Boolean preview = PREVIEW;
		String type = TYPE;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("c", "countrylan", true, "Country Language.");
		options.addOption("t", "type", true, "Type of asset set to run against, default is all");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}

			if(cmd.hasOption('c')){
				language = cmd.getOptionValue("c");
			}

			if(cmd.hasOption('t')){
				type = cmd.getOptionValue("t");
			}
			
			
		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		cliHash.put("language", language);
		cliHash.put("type", type);
		
		return cliHash;
		
	}
	
	private static String getAssetRootPath(String language){
		
		String retVal = null;
		
		//Assume that the primary path is /content/dam/informatica-com/global/amer/us
		//format for new asset: /content/dam/informatica-com/de
		
		String canonicalPath = "/content/dam";
		
		retVal = "";
		
		return retVal;
	}
	
	/*
	 * We have three rules for changing asset uri
	 * This method modifies the found URI and changes it 
	 * based on these rules
	 */
	private static String modifyNodeName(String nodeName, String language){
		
		String retVal = null;
		
		String fullPath = FilenameUtils.getFullPath(nodeName);
		String extension = FilenameUtils.getExtension(nodeName);
		String fileName = FilenameUtils.getName(nodeName);
		
		//code change request from Thy 8/28/15
		//
		//		So the EN version of the asset is under:                              /content/dam/informatica-com/global/amer/us/image/
		//		I want this replace with the same image under the language directory: /content/dam/informatica-com/ja/images/
		
		// so the trick is to change image to images in the origin us page, then change the language domain, the result should be 
		// original: /content/dam/informatica-com/global/amer/us/image/
		// intermediate: /content/dam/informatica-com/global/amer/us/images/
		// final: /content/dam/informatica-com/ja/images/

		//adjust the image -> images to match the above request
		
		fullPath = fullPath.replace("global/amer/us/image/", "global/amer/us/images/");
		
		String newPath = fullPath.replace("global/amer/us", language);
		
		if (extension.equals("pdf")){
			
			if(fileName.startsWith("en_")){
				
				fileName = fileName.replace("en_", language + "_");
				
			}
		
		}
		
		retVal = newPath + fileName;
		
		return retVal;
		
	}
	
	private static HashMap<String, CalixAsset> getDamAssets(String repo, String username, String password, String language) throws Exception {
		
		HashMap<String,CalixAsset> damAssets = new HashMap<String,CalixAsset>();

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		
		String rootPath = getAssetRootPath(language);
		
		rootPath = "/content/dam/calix";
		
		String q = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([" + rootPath + "])";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){

			Node n = ni.nextNode();
			
			CalixAsset ab = new CalixAsset();
			ab.setNodeName(n.getPath());
			
			String metaNodeName = "." + n.getPath() + "/jcr:content/metadata";
			
			//if the meta data exists put it in the bean, otherwise set to null
			try{

				Node metaNode = root.getNode(metaNodeName);
				
				try{
					
					Property p = metaNode.getProperty("./dc:searchfacetentry");
					Value[] values = p.getValues();
					
					for (Value v : values){
						
						ab.getFacetList().add(v.getString());
						
					}
					
				} catch (Exception e){
					
				}
				
			} catch (Exception e){
			
			}
	
			damAssets.put(ab.getNodeName(), ab);

		}
		
		session.logout();
		
		return damAssets;
		
	}
	
	/*
	 * Methods to handle the data manipulation on the nodes for:
	 * 
	 * mosaic, 
	 * testImage, 
	 * cta, 
	 * images
	 * 	
	 */
	
	/*
	 * find images
	 * defined as everything in the page nodes for the selected language having a non-null fileReference attribute.
	 */
	private static void updateImages(String repo, String username, String password, String language) throws Exception	{

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		//Node baseNode = root.getNode("./content/");
		
		String q = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([/content/informatica-com-live/"+ languageMap.get(language) +"]) and [fileReference] is not null";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){
			
			Node n = ni.nextNode();
			Property fileReference = n.getProperty("./fileReference");
			
			//change the original nodeName (the one that points to the en assets to the new asset name based on language...and stuff
			String newNodeName = modifyNodeName(fileReference.getString(), language);
			
			if (damAssets.get(newNodeName) != null){

				System.out.println("Image\t" + n.getPath() + '\t' + fileReference.getString() + "\t" + newNodeName);
				n.setProperty("fileReference", newNodeName);
			
			}
			
		}
		
		session.save();
		session.logout();
	}

}
