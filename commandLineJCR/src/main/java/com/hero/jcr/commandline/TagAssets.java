package com.hero.jcr.commandline;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import tagUtil.AttributeArray;
import tagUtil.DateArray;
import tagUtil.ShotTypeArray;

import com.hero.jcr.util.CqHelper;

/*
 * This program takes a file of formated asset data and applies tags to the matching
 * Asset in the DAM.
 * 
 * Adam Trissel
 * Hero Digital
 * 06.02.17
 * 
 */

public class TagAssets {
	
	static String REPOLOCATION = "http://52.35.196.241:4502";
	static String REPOUSER = "admin";
	static String REPOPASSWORD = "mSt1O+Oqc{0Q";
	static String DEFAULTFILE = "/Users/adamtrissel/projects/gigamon/scripts/migration/data/AssetUpload0617v2.txt";

	static String tagPrototype = "gigamon:%s/%s";
	static String destinationPrototype = "http://%s:%s/content/dam/gigamon/%s";

	static String tagFieldPrototype = "-F \"%s\"";	
	
	static final HashMap<String, String> dateTagArray = DateArray.getDateArray();
	static final HashMap<String, String> shotTypeArray = ShotTypeArray.getShotTypeArray();
	
	static HashMap<String, String> tagHashMap = new HashMap<String, String>();
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(TagAssets.class.getName());	

	
	public static void main(String[] args) throws Exception {
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		processFile(cliHash.get("repo"),
					cliHash.get("username"),
					cliHash.get("password"),
					cliHash.get("filename"));
		
	}
	
	private static String createFlatName(String name){
		
		String retVal = null;
		
		name = name.toLowerCase();
		
		//retVal = name.replace(" ", "_").replace("(", "").replace(")", "").replace("/", "_").replace("&", "\\&").replace("'", "");
		retVal = name.replace("&", "").replace("  ","").replace(" ", "_");
		
		return retVal;
	}
	
	private static void processFile(String repo, String username, String password, String filename) throws IOException {
		
		/*
		 * open up the repo establish a session and find the root node
		 */
		
		CqHelper helper = new CqHelper();
		
		Session session = null;
		try {
			session = helper.getSession(repo, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Node root = null;
		
		try {
			root = session.getRootNode();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		if (session == null || root == null){
			return;
		}
		
		Path file = Paths.get(filename);
        BufferedReader reader = Files.newBufferedReader(file,Charset.forName("UTF-8"));
        StringBuilder content = new StringBuilder();
        String line = null;

        long recordCounter = 0;
        
        while ((line = reader.readLine()) != null) {
        	
        	recordCounter++;
        	
            String[] columns = line.split("\t");
            
            if (columns.length >= 9){
            	
            	AttributeArray attributes = createAttributeArray(columns);

            	System.out.println(attributes.toString());
            	System.out.println();
            	
                Boolean success = setPropertiesOnNode(root, attributes);
                
                //save work every 1000 records
                if (recordCounter % 1000 == 0) {
                	
                	try {
                		log.info("****saving session****");
						session.save();
					} catch (RepositoryException e) {
						e.printStackTrace();					}
                	
                }

            }
            
        }
        
        /*
         * close the session
         */
        
        try {
			session.save();
		} catch (RepositoryException e) {
			System.out.println("rolled back");
		}
        
        session.logout();
        
        reader.close();
		
	}
	
	private static String decodeTags(String tag){
		
		String returnTag = "";
		
		if (tagHashMap.keySet().contains(tag)){
			
			returnTag = tagHashMap.get(tag);
			
		}
		
		return returnTag;
	}
	
	/*
	 * marshal the attribute array bean
	 */
	
	private static AttributeArray createAttributeArray(String[] columns){
		
		String dummy;
		String libraryName;
		String contentId;
		String fileName;
		String finalAEMTitle;
		String fileType;
		String ecosystemPartnerType;
		String language;
		String productLine;
		String programs;
		String resourceType;
		String libraries;
		String uploadowner;
		String dummy2;
		
        ArrayList<String> tagList = new ArrayList<String>();

        String mt_keys_concatenated;
        String internal_only;
        String id2;     
        
    	dummy = columns[0];
    	libraryName = columns[1];
    	contentId = columns[2];
    	fileName = columns[3];
    	finalAEMTitle = columns[4];
    	fileType = columns[5];
    	ecosystemPartnerType = columns[6];
    	language = columns[7];
    	productLine = columns[8];
    	programs = columns[9];
    	resourceType = columns[10];
    	libraries = columns[11];
    	uploadowner = columns[12];
    	dummy2 = columns[13];
    	
    	tagList = addTagsToArray("ecosystem_partner_type", ecosystemPartnerType, tagList);
    	tagList = addTagsToArray("language", language, tagList);
    	tagList = addTagsToArray("product_line", productLine, tagList);
    	tagList = addTagsToArray("programs", programs, tagList);
    	tagList = addTagsToArray("resource_type", resourceType, tagList);
    	tagList = addLibraryTagsToArray("library", libraries, tagList);
        
        AttributeArray attributes = new AttributeArray();
        
        String toDir = "/content/dam/gigamon/" + resourceType.toLowerCase().replace(" ", "_").replace("&", "").replace("__", "_");
        
        attributes.setNodeName(toDir + "/" + createFlatName(fileName) + "." + fileType.toLowerCase());
        
        attributes.setTitle(finalAEMTitle);
        attributes.setTagList(tagList);
        
		return attributes;
		
	}
	
	/*
	 * We are going to assume that tags can have multiple values, delimited by ";"
	 */
	private static ArrayList<String> addTagsToArray(String namespace, String tags, ArrayList<String> tagList){
		
		//catch an empty tag string
		if (tags.isEmpty()){
			return tagList;
		}
		
		String[] tagArray = tags.split(";");
		
		if (tagArray.length != 0){
			
			for (String tag : tagArray){
				
				String fullTag = "gigamon:partner_portal/" + namespace.toLowerCase().replace(" ", "_") + "/" + tag.toLowerCase().replace(" ", "_");
				
				tagList.add(fullTag);
				
			}
			
		}
		
		return tagList;
	}
	
	/*
	 * We are going to assume that tags can have multiple values, delimited by ";"
	 * Libraries take some extra processing, the tags are based on number and there are primary and secondary tags
	 */
	private static ArrayList<String> addLibraryTagsToArray(String namespace, String tags, ArrayList<String> tagList){
		
		//catch an empty tag string
		if (tags.isEmpty()){
			return tagList;
		}
		
		String[] tagArray = tags.split(";");
		
		if (tagArray.length != 0){
			
			int count = 0;
			
			for (String tag : tagArray){
				
				count++;
				
				String libraryNumber = tag.substring(0, 2);
				
				String libraryNamespace = "";
				
				if(count == 1){
					libraryNamespace = "library";
				} else {
					libraryNamespace = "shared_library";
				}
				
				String fullTag = "gigamon:partner_portal/" + libraryNamespace + "/" + libraryNumber;
				
				tagList.add(fullTag);
				
			}
			
		}
		
		return tagList;
	}	
	
	/*
	 * take the attributes, and attach them to the node specified by the URL, which will be relative to "/content/dam/gigamon"
	 */
	
	private static Boolean setPropertiesOnNode(Node root, AttributeArray attributes){
		
        String fullNodeName = attributes.getNodeName() + "/jcr:content/metadata";
        
        Boolean success = false;
        
        try {
			
        	Node n = root.getNode("." + fullNodeName);
			
            String[] tagsArray = new String[attributes.getTagList().size()];
            tagsArray = attributes.getTagList().toArray(tagsArray);
            
            n.setProperty("dc:title", attributes.getTitle());
            n.setProperty("cq:tags", tagsArray);
            
            success = true;
            
		} catch (RepositoryException e) {
			e.printStackTrace();
			//log.severe("failed to find node: " + fullNodeName + "/jcr:content/metadata");
		}
        
        return success;
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		String filename = DEFAULTFILE;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("f", "filename", true, "Input file name");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}
			
			if(cmd.hasOption('f')){
				filename = cmd.getOptionValue("f");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		cliHash.put("filename", filename);
		
		return cliHash;
		
	}
	
	private static String flattenName(String name) {
		
		name = name.toLowerCase();
		
		name = name.replace(" - ","/");
		name = name.replace(" ", "_");
		
		return name;
		
	}
	
	private static String makeFormalName(String namespace, String type, String value) {
		
		String retVal = flattenName(namespace) + ":" +
	                    flattenName(type) + "/" + 
	                    flattenName(value);
		
		return retVal;
		
	}	

}
