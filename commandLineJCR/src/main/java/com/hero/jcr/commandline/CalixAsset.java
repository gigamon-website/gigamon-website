package com.hero.jcr.commandline;

import java.util.ArrayList;
import java.util.List;

public class CalixAsset {
	
	private String nodeName;
	private List<String> facetList = new ArrayList<String>();
	
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	
	public List<String> getFacetList() {
		return facetList;
	}
	public void setFacetList(List<String> facetList) {
		this.facetList = facetList;
	}
	public String toString(){
		
		String facetString = String.join("\t", this.facetList);
		
		String retVal = nodeName + "\t" +
						facetString;
		return retVal;
	}

}
