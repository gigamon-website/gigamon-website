package com.hero.jcr.commandline;

public class AssetBean {
	
	private String nodeName;
	private String legacyId;
	private String assetId;
	
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public String getLegacyId() {
		return legacyId;
	}
	public void setLegacyId(String legacyId) {
		this.legacyId = legacyId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	
	public String toString(){
		
		String retVal = nodeName + "\t" +
					    legacyId + "\t" +
					    assetId;
		
		return retVal;
	}

}
