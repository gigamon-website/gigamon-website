package com.hero.jcr.commandline;

import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hero.jcr.model.ResourceBean;
import com.hero.jcr.util.CqHelper;

import org.apache.felix.scr.annotations.Reference;

/**
 * @author adamtrissel
 * 
 * This program scans a jcr instance and reports what assets have been loaded by MT ID Number
 *
 */
public class AssetReport {
	
	//these should be passed in on the command line
	
	private static HashMap<String, String> assetIDMap = new HashMap<String, String>();
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(AssetReport.class.getName());
	
//	static String REPOLOCATION = "http://52.71.112.131:4502";
	static String REPOLOCATION = "http://52.203.39.138:4502";
	//static String REPOUSER = "adam.trissel";
	static String REPOUSER = "admin";
//	static String REPOPASSWORD = "P@ssw0rd";
//	static String REPOPASSWORD = "s%k9K%fXJ{g2";
	static String REPOPASSWORD = "vJPK#6011j%j";
	static String FILLER = "Unknown";
	
	static ArrayList<String> allowedTypes = new ArrayList<String>();
	static HashMap<String, String> languageMap = new HashMap<String, String>();
	static HashMap<String, AssetBean> damAssets = new HashMap<String, AssetBean>();
	
	public static void main(String[] args) throws Exception {
		
		//get command line attributes
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		String repo = cliHash.get("repo");
		String username = cliHash.get("username");
		String password = cliHash.get("password");
		String filename = cliHash.get("filename");
		
		if (repo == null){
			log.log(Level.WARNING, "Starting to create asset map");
			return;
		}
		
		log.log(Level.INFO, "Starting to create asset map");
		getDamAssets(repo, username, password);
		log.log(Level.INFO, "Finished creating asset map");
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("f", "filename", true, "id to file filename.");
		
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		
		return cliHash;
		
	}
	
	private static String getTagNamespace(String tag){
		
		String retVal = null;
		
		String[] elements = tag.split("/");
		
		retVal = elements[0].replace("wd:", "");
		
		return retVal;
		
	}
	
	private static void getDamAssets(String repo, String username, String password) throws Exception {
		
		HashMap<String,AssetBean> damAssets = new HashMap<String,AssetBean>();

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		
		ArrayList<String> headerArray = new ArrayList<String>();
		
	    headerArray.add("Asset Path");
	    headerArray.add("Asset Title");
		
	    System.out.println(String.join("\t", headerArray));
	    
		String q = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([/content/dam/wd]) and [jcr:primaryType] = 'dam:Asset'";
				
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		long nodeCounter = 0;
		
		while (ni.hasNext()){
			
			nodeCounter++;

			Node n = ni.nextNode();
			
			ArrayList<String> tagArray = new ArrayList<String>();
			
			HashMap<String, String> tagMap = new HashMap<String, String>();
			
			if (!n.getPath().contains("subasset")){
				
				try{

					Node metaData = root.getNode("." + n.getPath() + "/jcr:content/metadata");
					
					String title = "";

					try{

						Property titlep = metaData.getProperty("dc:title");
						title = titlep.getString();
						
					} catch (Exception e){
						
						title = "";
						
					}
					
					System.out.println(n.getPath() + "\t" + title);
					
				} catch (Exception e){
					//pass
				}
				
			}
			
		}
		
		session.logout();
		
	}

}
