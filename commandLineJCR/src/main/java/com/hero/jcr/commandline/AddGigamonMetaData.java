package com.hero.jcr.commandline;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

import tagUtil.AttributeArray;
import tagUtil.DateArray;
import tagUtil.ShotTypeArray;

import com.hero.jcr.util.CqHelper;

/*
 * This program takes a file of formated asset data and applies tags to the matching
 * Asset in the DAM.
 * 
 * Adam Trissel
 * Hero Digital
 * 02.27.17
 */

public class AddGigamonMetaData {
	
//	static String REPOLOCATION = "http://tcolv-aem-auth.herodigital.com:4502";
//	static String REPOUSER = "admin";
//	static String REPOLOCATION = "http://52.27.163.130:4502";
//	static String REPOLOCATION = "http://52.88.106.180:4502";
	static String REPOLOCATION = "http://52.35.196.241:4502";
	static String REPOUSER = "admin";
//	static String REPOPASSWORD = "M4C@rjdN*m58!S1e";
//	static String REPOPASSWORD = "K#7051X1*dn3";
//	static String REPOPASSWORD = "~s12bY+M@I6Zv";
	static String REPOPASSWORD = "mSt1O+Oqc{0Q";
	static String DEFAULTFILE = "/Users/adamtrissel/projects/zeltiq/zeltiq/scripts/migration/data/assetV1.txt";

	static String tagPrototype = "zeltiq:%s/%s";
	static String destinationPrototype = "http://%s:%s/content/dam/zeltiq/%s";

	static String tagFieldPrototype = "-F \"%s\"";	
	
	static final HashMap<String, String> dateTagArray = DateArray.getDateArray();
	static final HashMap<String, String> shotTypeArray = ShotTypeArray.getShotTypeArray();
	
	static HashMap<String, String> tagHashMap = new HashMap<String, String>();
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(AddGigamonMetaData.class.getName());	
	
	/* New file format
		Include in Migration
		duptarget
		fullpath
		fullname
		updated unique filename
		Asset Type
		Asset Use
		Resource Type
		Localization
		Personalization
		Geographic Restriction 1
		Geographic Restriction 2
		Geographic Restriction 3
		Target Area
		Intended End User 1
		Intended End User 2
		Applicators
		Part Number
		Photo Attributes/View
		Photo Attributes/Example Type
		Patient Attributes/Gender
		Patient Attributes/Time Period
		Patient Attributes/Clinician
		Patient Attributes/Number of Sessions
		PatientID
		Asset Use 2
		Asset Use 3
		Asset Title
		Asset Description
		Title
	 */
	
//	private static int NR = 0;
//	private static int FILENAME = 1;
//	private static int ASSETTYPE = 2;
//	private static int ASSETUSE = 3;
//	private static int RESOURCETYPE = 4;
//	private static int LOCALIZATION = 5;
//	private static int PERSONALIZATION = 6;
//	private static int GEOGRAPHIC_REGION = 7;
//	private static int TARGETAREA = 8;
//	private static int INTENDEDENDUSER = 9;
//	private static int APPLICATORS = 10;
//	private static int ASSETUSE2 = 11;
//	private static int ASSETUSE3 = 12;
//	private static int ASSETTITLE = 13;
//	private static int ASSETDESCRIPTION = 14;
//	private static int DUMMY = 15;
	
	private static int INCLUDE_IN_MIGRATION = 0;
	private static int DUPTARGET = 1;
	private static int FULLPATH = 2;
	private static int FULLNAME = 3;
	private static int UPDATED_UNIQUE_FILENAME = 4;
	private static int ASSET_TYPE = 5;
	private static int ASSET_USE = 6;
	private static int RESOURCE_TYPE = 7;
	private static int LOCALIZATION = 8;
	private static int PERSONALIZATION = 9;
	private static int GEOGRAPHIC_RESTRICTION_1 = 10;
	private static int GEOGRAPHIC_RESTRICTION_2 = 11;
	private static int GEOGRAPHIC_RESTRICTION_3 = 12;
	private static int TARGET_AREA = 13;
	private static int INTENDED_END_USER_1 = 14;
	private static int INTENDED_END_USER_2 = 15;
	private static int APPLICATORS = 16;
	private static int PART_NUMBER = 17;
	private static int PHOTO_ATTRIBUTES_VIEW = 18;
	private static int PHOTO_ATTRIBUTES_EXAMPLE_TYPE = 19;
	private static int PATIENT_ATTRIBUTES_GENDER = 20;
	private static int PATIENT_ATTRIBUTES_TIME_PERIOD = 21;
	private static int PATIENT_ATTRIBUTES_CLINICIAN = 22;
	private static int PATIENT_ATTRIBUTES_NUMBER_OF_SESSIONS = 23;
	private static int PATIENTID = 24;
	private static int ASSET_USE_2 = 25;
	private static int ASSET_USE_3 = 26;
	private static int ASSET_TITLE = 27;
	private static int ASSET_DESCRIPTION = 28;
	private static int TITLE = 29;
	private static int DUMMY = 30;	

	public static void main(String[] args) throws Exception {
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		processFile(cliHash.get("repo"),
					cliHash.get("username"),
					cliHash.get("password"),
					cliHash.get("filename"));
		
	}
	
	private static String createFlatName(String name){
		
		String retVal = null;
		
		name = name.toLowerCase();
		
		//retVal = name.replace(" ", "_").replace("(", "").replace(")", "").replace("/", "_").replace("&", "\\&").replace("'", "");
		retVal = name.replace(" ", "_").replace("(", "").replace(")", "").replace("/", "_").replace("'", "");
		
		return retVal;
	}
	
	private static String processNodeName(String filename, String language, String asset_type){
		
		String destination = "./content/dam/tcolv/";
//		destination = destination + language.toLowerCase() + "/";
//		destination = destination + destDict.get(asset_type) + "/";
//		destination = destination + filename;
		
		return destination;
		
	}
	
	private static String processAssetType(String asset_type){
		
		return makeFormalName("wd", "asset_type", asset_type);
		
	}
	
	private static String processCampaign(String campaign){
		
		return makeFormalName("wd", "campaign", campaign);
		
	}
	
	private static String processProduct(String product){
		
		return makeFormalName("wd", "product", product);
		
	}
	
	private static String processResolution(String resolution){
		
		String resolutionTag = "";
		
		resolution = resolution.toLowerCase();
		if (resolution.startsWith("high")){
			
			resolutionTag = "wd:resolution/high_resolution_print_ready";
			
		} else if(resolution.startsWith("low")){
			
			resolutionTag = "wd:resolution/low_resolution_print_ready";
			
		} else {
			
			resolutionTag = "wd:resolution/cmyk";
			
		}
		
		return resolutionTag;
		
	}
	
	private static void processFile(String repo, String username, String password, String filename) throws IOException {
		
		/*
		 * open up the repo establish a session and find the root node
		 */
		
		CqHelper helper = new CqHelper();
		
		Session session = null;
		try {
			session = helper.getSession(repo, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Node root = null;
		
		try {
			root = session.getRootNode();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		if (session == null || root == null){
			return;
		}
		
		Path file = Paths.get(filename);
        BufferedReader reader = Files.newBufferedReader(file,Charset.forName("UTF-8"));
        StringBuilder content = new StringBuilder();
        String line = null;

        long recordCounter = 0;
        
        while ((line = reader.readLine()) != null) {
        	
        	recordCounter++;
        	
            String[] columns = line.split("\t");
            
            if (columns.length >= 9){
            	
            	AttributeArray attributes = createAttributeArray(columns);

            	System.out.println(attributes.toString());
            	System.out.println();
            	
                Boolean success = setPropertiesOnNode(root, attributes);
                
                //save work every 1000 records
                if (recordCounter % 1000 == 0) {
                	
                	try {
                		log.info("****saving session****");
						session.save();
					} catch (RepositoryException e) {
						e.printStackTrace();					}
                	
                }

            }
            
        }
        
        /*
         * close the session
         */
        
        try {
			session.save();
		} catch (RepositoryException e) {
			System.out.println("rolled back");
		}
        
        session.logout();
        
        reader.close();
		
	}
	
	private static String decodeTags(String tag){
		
		String returnTag = "";
		
		if (tagHashMap.keySet().contains(tag)){
			
			returnTag = tagHashMap.get(tag);
			
		}
		
		return returnTag;
	}
	
	private static ArrayList<String> addStandardTags(ArrayList<String> tagList, String fieldName, String tag){
		
		if (!tag.equals("")){
			tagList.add("zeltiq:" + fieldName.toLowerCase() + "/" + tag.toLowerCase());
		}
		
		return tagList;
		
	}
		/*
	 * marshal the attribute array bean
	 */
	
	private static AttributeArray createAttributeArray(String[] columns){
		
//		String nr;
//		String fileName;
//		String assetType;
//		String assetUse;
//		String resourceType;
//		String localization;
//		String personalization;
//		String geographic_Region;
//		String targetArea;
//		String intendedEndUser;
//		String applicators;
//		String assetUse2;
//		String assetUse3;
//		String assetTitle;
//		String assetDescription;
//		String dummy;
		
		String includeInMigration;
		String duptarget;
		String fullPath;
		String fullName;
		String updatedUniqueFilename;
		String assetType;
		String assetUse;
		String resourceType;
		String localization;
		String personalization;
		String geographicRestriction1;
		String geographicRestriction2;
		String geographicRestriction3;
		String targetArea;
		String intendedEndUser1;
		String intendedEndUser2;
		String applicators;
		String partNumber;
		String photoAttributesView;
		String photoAttributesExampleType;
		String patientAttributesGender;
		String patientAttributesTimePeriod;
		String patientAttributesClinician;
		String patientAttributesNumberofSessions;
		String patientID;
		String assetUse2;
		String assetUse3;
		String assetTitle;
		String assetDescription;
		String title;
		String dummy;		
		
        ArrayList<String> tagList = new ArrayList<String>();

        String mt_keys_concatenated;
        String internal_only;
        String id2;     
        
		//fileName = columns[FULLPATH];
		
//		assetType = columns[ASSETTYPE];
//		assetUse = columns[ASSETUSE];
//		resourceType = columns[RESOURCETYPE];
//		localization = columns[LOCALIZATION];
//		personalization = columns[PERSONALIZATION];
//		geographic_Region = columns[GEOGRAPHIC_REGION];
//		targetArea = columns[TARGETAREA];
//		intendedEndUser = columns[INTENDEDENDUSER];
//		applicators = columns[APPLICATORS];
//		assetUse2 = columns[ASSETUSE2];
//		assetUse3 = columns[ASSETUSE3];
//		assetTitle = columns[ASSETTITLE];
//		assetDescription = columns[ASSETDESCRIPTION];
//		dummy = columns[DUMMY];

		includeInMigration  = columns[INCLUDE_IN_MIGRATION];
		duptarget  = columns[DUPTARGET];
		fullPath  = columns[FULLPATH];
		fullName  = columns[FULLNAME];
		updatedUniqueFilename  = columns[UPDATED_UNIQUE_FILENAME];
		assetType  = columns[ASSET_TYPE];
		assetUse  = columns[ASSET_USE];
		resourceType  = columns[RESOURCE_TYPE];
		localization  = columns[LOCALIZATION];
		personalization  = columns[PERSONALIZATION];
		geographicRestriction1  = columns[GEOGRAPHIC_RESTRICTION_1];
		geographicRestriction2  = columns[GEOGRAPHIC_RESTRICTION_2];
		geographicRestriction3  = columns[GEOGRAPHIC_RESTRICTION_3];
		targetArea  = columns[TARGET_AREA];
		intendedEndUser1  = columns[INTENDED_END_USER_1];
		intendedEndUser2  = columns[INTENDED_END_USER_2];
		applicators  = columns[APPLICATORS];
		partNumber  = columns[PART_NUMBER];
		photoAttributesView  = columns[PHOTO_ATTRIBUTES_VIEW];
		photoAttributesExampleType  = columns[PHOTO_ATTRIBUTES_EXAMPLE_TYPE];
		patientAttributesGender  = columns[PATIENT_ATTRIBUTES_GENDER];
		patientAttributesTimePeriod  = columns[PATIENT_ATTRIBUTES_TIME_PERIOD];
		patientAttributesClinician  = columns[PATIENT_ATTRIBUTES_CLINICIAN];
		patientAttributesNumberofSessions  = columns[PATIENT_ATTRIBUTES_NUMBER_OF_SESSIONS];
		patientID  = columns[PATIENTID];
		assetUse2  = columns[ASSET_USE_2];
		assetUse3  = columns[ASSET_USE_3];
		assetTitle  = columns[ASSET_TITLE];
		assetDescription  = columns[ASSET_DESCRIPTION];
		title  = columns[TITLE];
		dummy  = columns[DUMMY];
        
		addStandardTags(tagList, "asset_type", assetType);
		addStandardTags(tagList, "asset_use", assetUse);
		addStandardTags(tagList, "resource_type", resourceType);
		addStandardTags(tagList, "localization", localization);
		addStandardTags(tagList, "personalization", personalization);
		addStandardTags(tagList, "geographic_Region", geographicRestriction1);
		addStandardTags(tagList, "geographic_Region", geographicRestriction2);
		addStandardTags(tagList, "geographic_Region", geographicRestriction3);
		addStandardTags(tagList, "target_area", targetArea);
		addStandardTags(tagList, "intended_end_user", intendedEndUser1);
		addStandardTags(tagList, "intended_end_user", intendedEndUser2);
		addStandardTags(tagList, "applicators", applicators);
		addStandardTags(tagList, "asset_use", assetUse2);
		addStandardTags(tagList, "asset-use", assetUse3);
		addStandardTags(tagList, "dummy", dummy);
        
        AttributeArray attributes = new AttributeArray();
        
        attributes.setTitle(assetTitle);
        attributes.setDescription(assetDescription);
        
        String ext1 = FilenameUtils.getExtension(fullPath); // returns "txt"
        String fileBase = FilenameUtils.getBaseName(fullPath);
        
        String filename = "/content/dam/zeltiq/" + assetType + "/" + fullName;
        
        attributes.setNodeName(filename);
        
        attributes.setTagList(tagList);
        
		return attributes;
		
	}
	
	/*
	 * take the attributes, and attach them to the node specified by the URL, which will be relative to "/content/dam/belkin"
	 */
	
	private static Boolean setPropertiesOnNode(Node root, AttributeArray attributes){
		
        String fullNodeName = attributes.getNodeName() + "/jcr:content/metadata";
        
        Boolean success = false;
        
        try {
			
        	Node n = root.getNode("." + fullNodeName);
			
            String[] tagsArray = new String[attributes.getTagList().size()];
            tagsArray = attributes.getTagList().toArray(tagsArray);
            
            n.setProperty("cq:tags", tagsArray);
            n.setProperty("dc:title", attributes.getTitle());
            n.setProperty("dc:description",attributes.getDescription());
            
            success = true;
            
		} catch (RepositoryException e) {
			e.printStackTrace();
			//log.severe("failed to find node: " + fullNodeName + "/jcr:content/metadata");
		}
        
        return success;
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		String filename = DEFAULTFILE;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("f", "filename", true, "Input file name");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}
			
			if(cmd.hasOption('f')){
				filename = cmd.getOptionValue("f");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		cliHash.put("filename", filename);
		
		return cliHash;
		
	}
	
	private static String flattenName(String name) {
		
		name = name.toLowerCase();
		
		name = name.replace(" - ","/");
		name = name.replace(" ", "_");
		
		return name;
		
	}
	
	private static String makeFormalName(String namespace, String type, String value) {
		
		String retVal = flattenName(namespace) + ":" +
	                    flattenName(type) + "/" + 
	                    flattenName(value);
		
		return retVal;
		
	}	

}
