package com.hero.jcr.commandline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hero.jcr.model.ResourceBean;
import com.hero.jcr.util.CqHelper;

/**
 * @author adamtrissel
 * 
 * This is a command line utility that scans a country specific set of DAM assets
 * Then replaces references to matching references in page content
 * 
 * The logic is a follows:
 * 
 * 1) pull all the DAM assets for a specific country. We store these in a hashmap with the key as the asset path 
 *    and the values containing the legacy id and asset key.
 *    
 * 2) select all references to the resource mosaic in the new language pages. Itereate through all the mosaics, 
 *    pulling JSON from the overrideField array. check to see if the asset mentioned in the field (damResource) matches, 
 *    after modifying it for the new language, a value in the DAM asset hashmap, then replace it, otherwise don't.
 *    
 * 3) select all references to the image component, and replace any fileReferences that occure in the hashmap
 * 
 * 4) select all cta's and replace any that occur in the hashmap
 * 
 * 5) select all textImage components and replace the cta link if it matches the hashmap.
 *
 */
public class Search {
	
	//these should be passed in on the command line
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(Search.class.getName());
	
	static String REPOLOCATION = "http://localhost:4502";
	static String REPOUSER = "admin";
	static String REPOPASSWORD = "admin";
	static String FILLER = "Unknown";
	static String COUNTRYLAN = "es";
	static Boolean PREVIEW = false;
	static String TYPE = "all";
	
	static ArrayList<String> allowedTypes = new ArrayList<String>();
	static HashMap<String, String> languageMap = new HashMap<String, String>();
	static HashMap<String, AssetBean> damAssets = new HashMap<String, AssetBean>();
	
	public static void main(String[] args) throws Exception {
		
		//set possible allowed types
		allowedTypes.add("all");
		allowedTypes.add("mosaic");
		allowedTypes.add("textImage");
		allowedTypes.add("cta");
		allowedTypes.add("images");
		allowedTypes.add("rcfixedLinks");
		allowedTypes.add("linkUrl");
		
		languageMap.put("de", "de_de");
		languageMap.put("fr", "fr_fr");
		languageMap.put("it", "it_it");
		languageMap.put("es", "es_es");
		languageMap.put("ja", "ja_jp");
		languageMap.put("ko", "ko_kr");
		languageMap.put("zh", "zh_cn");
		languageMap.put("pt", "pt_br");
		
		//get command line attributes
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		String repo = cliHash.get("repo");
		String username = cliHash.get("username");
		String password = cliHash.get("password");
		String language = cliHash.get("language");
		String type = cliHash.get("type");
		
		if (!allowedTypes.contains(type)){
			log.log(Level.SEVERE, "supplied type does not match allowable types (all, mosaic, testImage, cta, images). Default is all. Stoping");
			return;
		}
		
		log.log(Level.INFO, "processing " + type + " record types");
		
		log.log(Level.INFO, "Starting to create asset map");
		damAssets = getDamAssets(repo, username, password, language);
		log.log(Level.INFO, "Finished creating asset map");
		
//		//read asset array and print out -- test
//		for (String nodeName : damAssets.keySet()){
//			
//			System.out.println(damAssets.get(nodeName).toString());
//			
//		}
		
		if (type.equals("mosaic") || type.equals("all")){

			log.log(Level.INFO, "Starting to process resource mosaic");
			updateResourceMosaic(repo, username, password, language);
			log.log(Level.INFO, "Finished processing resource mosaic");
			
		}

		if (type.equals("images") || type.equals("all")){

			log.log(Level.INFO, "Starting to process images");
			updateImages(repo, username, password, language);
			log.log(Level.INFO, "Finished processing images");
			
		}
		
		if (type.equals("cta") || type.equals("all")){
			
			log.log(Level.INFO, "Starting to cta links");
			updateCTA(repo, username, password, language);
			log.log(Level.INFO, "Finished cta links");

		}

		if (type.equals("textImage") || type.equals("all")){

			log.log(Level.INFO, "Starting to textImage links");
			updateTextImage(repo, username, password, language);
			log.log(Level.INFO, "Finished textImage links");
			
		}
		
		if (type.equals("rcfixedLinks") || type.equals("all")){

			log.log(Level.INFO, "Starting to rcfixedLinks links");
			updateRcFixedLinks(repo, username, password, language);
			log.log(Level.INFO, "Finished rcfixedLinks links");
			
		}
		
		if (type.equals("linkUrl") || type.equals("all")){

			log.log(Level.INFO, "Starting to linkUrl links");
			updateLinkUrl(repo, username, password, language);
			log.log(Level.INFO, "Finished linkUrl links");
			
		}		
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		String language = COUNTRYLAN;
		Boolean preview = PREVIEW;
		String type = TYPE;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("c", "countrylan", true, "Country Language.");
		options.addOption("t", "type", true, "Type of asset set to run against, default is all");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}

			if(cmd.hasOption('c')){
				language = cmd.getOptionValue("c");
			}

			if(cmd.hasOption('t')){
				type = cmd.getOptionValue("t");
			}
			
			
		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		cliHash.put("language", language);
		cliHash.put("type", type);
		
		return cliHash;
		
	}
	
	private static String getAssetRootPath(String language){
		
		String retVal = null;
		
		//Assume that the primary path is /content/dam/informatica-com/global/amer/us
		//format for new asset: /content/dam/informatica-com/de
		
		String canonicalPath = "/content/dam/informatica-com/global/amer/us";
		retVal = canonicalPath.replace("global/amer/us", language);
		
		return retVal;
	}
	
	/*
	 * We have three rules for changing asset uri
	 * This method modifies the found URI and changes it 
	 * based on these rules
	 */
	private static String modifyNodeName(String nodeName, String language){
		
		String retVal = null;
		
		String fullPath = FilenameUtils.getFullPath(nodeName);
		String extension = FilenameUtils.getExtension(nodeName);
		String fileName = FilenameUtils.getName(nodeName);
		
		//code change request from Thy 8/28/15
		//
		//		So the EN version of the asset is under:                              /content/dam/informatica-com/global/amer/us/image/
		//		I want this replace with the same image under the language directory: /content/dam/informatica-com/ja/images/
		
		// so the trick is to change image to images in the origin us page, then change the language domain, the result should be 
		// original: /content/dam/informatica-com/global/amer/us/image/
		// intermediate: /content/dam/informatica-com/global/amer/us/images/
		// final: /content/dam/informatica-com/ja/images/

		//adjust the image -> images to match the above request
		
		fullPath = fullPath.replace("global/amer/us/image/", "global/amer/us/images/");
		
		String newPath = fullPath.replace("global/amer/us", language);
		
		if (extension.equals("pdf")){
			
			if(fileName.startsWith("en_")){
				
				fileName = fileName.replace("en_", language + "_");
				
			}
		
		}
		
		retVal = newPath + fileName;
		
		return retVal;
		
	}
	
	private static HashMap<String, AssetBean> getDamAssets(String repo, String username, String password, String language) throws Exception {
		
		HashMap<String,AssetBean> damAssets = new HashMap<String,AssetBean>();

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		
		String rootPath = getAssetRootPath(language);
		
		String q = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([" + rootPath + "])";
		
		//String q = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([/content/dam/informatica-com/" + language + "])";

		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){

			Node n = ni.nextNode();
			
			AssetBean ab = new AssetBean();
			ab.setNodeName(n.getPath());
			
			String metaNodeName = "." + n.getPath() + "/jcr:content/metadata";
			
			//if the meta data exists put it in the bean, otherwise set to null
			try{

				Node metaNode = root.getNode(metaNodeName);
				
				try{
					ab.setLegacyId(metaNode.getProperty("./dc:legacyid").getString());
				} catch (Exception e){
					ab.setLegacyId("");
				}
				try{
					ab.setAssetId(metaNode.getProperty("./dc:assetKey").getString());
				} catch (Exception e) {
					ab.setAssetId("");
				}
				
			} catch (Exception e){
				
				ab.setLegacyId("");
				ab.setAssetId("");
			
			}
	
			damAssets.put(ab.getNodeName(), ab);

		}
		
		session.logout();
		
		return damAssets;
		
	}
	
	/*
	 * Methods to handle the data manipulation on the nodes for:
	 * 
	 * mosaic, 
	 * testImage, 
	 * cta, 
	 * images
	 * 	
	 */
	
	/*
	 * find images
	 * defined as everything in the page nodes for the selected language having a non-null fileReference attribute.
	 */
	private static void updateImages(String repo, String username, String password, String language) throws Exception	{

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		//Node baseNode = root.getNode("./content/");
		
		String q = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([/content/informatica-com-live/"+ languageMap.get(language) +"]) and [fileReference] is not null";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){
			
			Node n = ni.nextNode();
			Property fileReference = n.getProperty("./fileReference");
			
			//change the original nodeName (the one that points to the en assets to the new asset name based on language...and stuff
			String newNodeName = modifyNodeName(fileReference.getString(), language);
			
			if (damAssets.get(newNodeName) != null){

				System.out.println("Image\t" + n.getPath() + '\t' + fileReference.getString() + "\t" + newNodeName);
				n.setProperty("fileReference", newNodeName);
			
			}
			
		}
		
		session.save();
		session.logout();
	}
	
	//new stuff
	
	/*
	 * find images
	 * defined as everything in the page nodes for the selected language having a non-null fileReference attribute.
	 */
	private static void updateLinkUrl(String repo, String username, String password, String language) throws Exception	{

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		//Node baseNode = root.getNode("./content/");
		
		String q = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([/content/informatica-com-live/"+ 
					languageMap.get(language) +
					"]) and [linkUrl] is not null and [linkUrl] <> ''";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){
			
			Node n = ni.nextNode();
			Property linkUrl = n.getProperty("./linkUrl");
			
			//change the original nodeName (the one that points to the en assets to the new asset name based on language...and stuff
			String newNodeName = modifyNodeName(linkUrl.getString(), language);
			
			if (damAssets.get(newNodeName) != null){

				System.out.println("LinkUrl\t" + n.getPath() + '\t' + linkUrl.getString() + "\t" + newNodeName);
				n.setProperty("linkUrl", newNodeName);
			
			}
			
		}
		
		session.save();
		session.logout();
	}
	
	/*
	 * find rcfixedLinks
	 * defined as an array of strings that *might* link back to the dam
	 */
	private static void updateRcFixedLinks(String repo, String username, String password, String language) throws Exception	{

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		//Node baseNode = root.getNode("./content/");
		
		String q = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([/content/informatica-com-live/"+ languageMap.get(language) +"]) and [rcfixedLinks] is not null";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){
			
			Node n = ni.nextNode();
			Property rcFixedLinks = n.getProperty("./rcfixedLinks");
			
			//we need to load an array of strings and run through it
			
			try{
				
				Value[] links = rcFixedLinks.getValues();
				
				String[] newlinks = new String[links.length]; 
				
				int length = links.length;
				
				for (int x = 0; x < length; x++){
					
					String linkSource = links[x].getString();

					String newLinkPath = modifyNodeName(linkSource, language);
					
					if (damAssets.get(newLinkPath) != null){
						
						System.out.println("rcFixedLinks\t" + n.getPath() + '\t' + "in position:" + (x + 1) + "\t" + linkSource + "\t" + newLinkPath);
						newlinks[x] = newLinkPath;
						
					} else {
						newlinks[x] = linkSource;
					}
				
				}
				
				//update the property
				n.setProperty("rcfixedLinks", newlinks);
			
			} catch (ValueFormatException e){
				
				//The data if foo barred and what we expect to be an array is not one
				
				//String override = overrideField.getString();
				//System.out.println(override);
				
			}
			
		}
		
		session.save();
		session.logout();
	}	
	
	/*
	 * find all nodes in the selected language page nodes where the sling:resourcetype = resourceMosaic and the overrideField is not null
	 */
	
	private static void updateResourceMosaic(String repo, String username, String password, String language) throws Exception	{
		
		Gson gson = new GsonBuilder().create();  
		
		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		//Node baseNode = root.getNode("./content/");
		
		//This query can be tested in crx/de tools/query screen
		
		String q = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([/content/informatica-com-live/"+ languageMap.get(language) +"]) " +
		           "and [sling:resourceType] = \"informatica-com/components/resourceMosaic\" and overrideField is not null";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){
			
			Node n = ni.nextNode();
			Property overrideField = n.getProperty("./overrideField");
			
			try{
				
				Value[] override = overrideField.getValues();
				
				String[] newOverride = new String[override.length]; 
				
				int length = override.length;
				
				for (int x = 0; x < length; x++){
					
					String jsonResource = override[x].getString();
					
					//System.out.println(jsonResource);
					
					ResourceBean r = gson.fromJson(jsonResource, ResourceBean.class);
					
					//if (!r.getDamResource().isEmpty() && !r.getDamResource().endsWith(".jpg")){
					if (!r.getDamResource().isEmpty()){
						
						String resourcePath = r.getDamResource();
						
						String newResourcePath = modifyNodeName(resourcePath, language);
						
						if (damAssets.get(newResourcePath) != null){
							
							//ok the asset is found so we need to deal with it
							System.out.println("Resource Bundle\t" + n.getPath() + '\t' + "in position:" + (x + 1) + "\t" + resourcePath + "\t" + newResourcePath);
							
							r.setDamResource(newResourcePath);
							
							String resourceString = gson.toJson(r);
							
							newOverride[x] = resourceString;
							
						} else {
							newOverride[x] = override[x].getString();
						}
						
					}
				
				}
				
				//update the property
				n.setProperty("overrideField", newOverride);
			
			} catch (ValueFormatException e){
				
				//The data if foo barred and what we expect to be an array is not one
				
				String override = overrideField.getString();
				//System.out.println(override);
				
			}
			
		}
		
		session.save();
		session.logout();
		
	}
	
	/*
	 * find all textImage components with non null cta links
	 */
	
	private static void updateTextImage(String repo, String username, String password, String language) throws Exception {
		
		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		//Node baseNode = root.getNode("./content/");
		
		String q = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([/content/informatica-com-live/"+ languageMap.get(language) +"]) " +
		           "and [sling:resourceType] = 'informatica-com/components/textImage' and [ctaLink] is not null";
		
//		System.out.println("Query: " + q);
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){
			
			Node n = ni.nextNode();
			try{
				Property ctaLink = n.getProperty("./ctaLink");
				if (ctaLink.getString().startsWith("/content/dam/informatica-com")){
					
					//does the modified cta link exist in the damAsset List
					String oldLink = ctaLink.getString();
					
					//change the original nodeName (the one that points to the en assets to the new asset name based on language...and stuff
					String newNodeName = modifyNodeName(oldLink, language);
					
//					System.out.println(newNodeName);
					
					if (damAssets.get(newNodeName) != null){
						
						System.out.println("text image ctaLink\t" + n.getPath() + '\t' + ctaLink.getString() + "\t" + newNodeName);
						n.setProperty("ctaLink", newNodeName);
						
					}
					
				}
			} catch (PathNotFoundException e){
				//for some reason there was no ctalink parameter which is strange
				//log.log(Level.WARNING, "ctaLink attribute not found in " + n.getPath());
			}
			
		}
		
		session.save();
		session.logout();
		
	}
	
	/*
	 * find all cta components
	 */
	
	private static void updateCTA(String repo, String username, String password, String language) throws Exception {
		
		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		
		String q = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([/content/informatica-com-live/"+ languageMap.get(language) +"]) " +
		           "and [sling:resourceType] = 'informatica-com/components/cta'";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		while (ni.hasNext()){
			
			Node n = ni.nextNode();
			try{
				Property ctaLink = n.getProperty("./ctaLink");
				if (ctaLink.getString().startsWith("/content/dam/informatica-com")){
					
					String oldNodeName = ctaLink.getString();
					
					//change the original nodeName (the one that points to the en assets to the new asset name based on language...and stuff
					String newNodeName = modifyNodeName(oldNodeName, language);
					
					if(damAssets.get(newNodeName) != null){
						
						System.out.println("cta\t" + n.getPath() + '\t' + ctaLink.getString() + '\t' + newNodeName);
						//n.setProperty("ctaLink", newNodeName);
						
					}
					
				}
			} catch (PathNotFoundException e){
				//for some reason there was no ctalink parameter which is strange
				//log.log(Level.WARNING, "ctaLink attribute not found in " + n.getPath());
			}
			
		}
		
		session.save();
		session.logout();
		
	}

}
