package com.hero.jcr.commandline;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import tagUtil.AttributeArray;
import tagUtil.DateArray;
import tagUtil.ShotTypeArray;

import com.hero.jcr.util.CqHelper;

/*
 * This program takes a file of formated asset data and applies tags to the matching
 * Asset in the DAM.
 * 
 * Adam Trissel
 * Hero Digital
 * 03.22.16
 */

public class ValidateTags {
	
	static String REPOLOCATION = "http://localhost:4502";
	static String REPOUSER = "admin";
	static String REPOPASSWORD = "admin";

	static String tagPrototype = "belkin:%s/%s";
	static String destinationPrototype = "http://%s:%s/content/dam/belkin/%s";

	static String tagFieldPrototype = "-F \"%s\"";	
	
	static final HashMap<String, String> dateTagArray = DateArray.getDateArray();
	static final HashMap<String, String> shotTypeArray = ShotTypeArray.getShotTypeArray();
	
	static HashMap<String, String> tagHashMap = new HashMap<String, String>();
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(ValidateTags.class.getName());	
	
	public static void main(String[] args) throws Exception {
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		processFile(cliHash.get("repo"),
					cliHash.get("username"),
					cliHash.get("password"),
					cliHash.get("filename"));
		
	}
	
	private static void processFile(String repo, String username, String password, String filename)  throws IOException{
		
		CqHelper helper = new CqHelper();
		
		Session session = null;
		try {
			session = helper.getSession(repo, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Node root = null;
		
		try {
			root = session.getRootNode();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		if (session == null || root == null){
			return;
		}
		
		Path file = Paths.get(filename);
        BufferedReader reader = Files.newBufferedReader(file,Charset.forName("UTF-8"));
        StringBuilder content = new StringBuilder();
        String line = null;
        

        long recordCounter = 0;
        
        while ((line = reader.readLine()) != null) {
        	
        	recordCounter++;
        	
            String[] columns = line.split("\t");
            
            if (columns.length >= 3){
            	
            	String namespace = columns[0];
            	String type = columns[1];
            	String value = columns[2];
            	
            	try {

                	validateTag(session, root, namespace, type, value);
            		
            	} catch (Exception e){
            		
            		System.out.println("Failed to validate tag: " + makeFormalName(namespace, type, value));
            		
            	}

            }
            
        }		
		
        session.logout();
		
	}
	
	private static void validateTag(Session session, Node root, String namespace, String type, String value) throws IOException {
		
		/*
		 * open up the repo establish a session and find the root node
		 */
		

        String tagLocation = "./etc/tags/" +
               flattenName(namespace) + "/" +
        	   flattenName(type) + "/" +
               flattenName(value);
        
        String tagFound = "Tag Not Found";
        
        try{

        	root.getNode(tagLocation);
        	tagFound = "Tag Found";

        } catch (Exception e) {
        	
        	tagFound = "Tag Not Found";
        	
        }
		

        
        System.out.println(tagFound + " : " + makeFormalName(namespace, type, value) + " : " + tagLocation);
        
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		String filename = "";

		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("f", "filename", true, "Input file name");

		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}
			
			if(cmd.hasOption('f')){
				filename = cmd.getOptionValue("f");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		cliHash.put("filename", filename);
		
		return cliHash;
		
	}
	
	private static String flattenName(String name) {
		
		name = name.toLowerCase();
		
		name = name.replace(" - ","/");
		name = name.replace(" ", "_");
		
		return name;
		
	}
	
	private static String makeFormalName(String namespace, String type, String value) {
		
		String retVal = flattenName(namespace) + ":" +
	                    flattenName(type) + "/" + 
	                    flattenName(value);
		
		return retVal;
		
	}

}
