package com.hero.jcr.commandline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hero.jcr.model.ResourceBean;
import com.hero.jcr.util.CqHelper;

/**
 * @author adamtrissel
 * 
 * This is a command line utility that scans a country specific set of DAM assets
 * Then replaces references to matching references in page content
 * 
 * The logic is a follows:
 * 
 * 1) pull all the DAM assets for a specific country. We store these in a hashmap with the key as the asset path 
 *    and the values containing the legacy id and asset key.
 *    
 * 2) select all references to the resource mosaic in the new language pages. Itereate through all the mosaics, 
 *    pulling JSON from the overrideField array. check to see if the asset mentioned in the field (damResource) matches, 
 *    after modifying it for the new language, a value in the DAM asset hashmap, then replace it, otherwise don't.
 *    
 * 3) select all references to the image component, and replace any fileReferences that occure in the hashmap
 * 
 * 4) select all cta's and replace any that occur in the hashmap
 * 
 * 5) select all textImage components and replace the cta link if it matches the hashmap.
 *
 */
public class FindBogusAssetNodes {
	
	//these should be passed in on the command line
	
	private static Options options = new Options();
	
	private static final Logger log = Logger.getLogger(FindBogusAssetNodes.class.getName());
	
	static String REPOLOCATION = "http://52.89.25.10:4502";
	static String REPOUSER = "admin";
	static String REPOPASSWORD = "admin";
	static String FILLER = "Unknown";
	
	static ArrayList<String> allowedTypes = new ArrayList<String>();
	static HashMap<String, String> languageMap = new HashMap<String, String>();
	static HashMap<String, AssetBean> damAssets = new HashMap<String, AssetBean>();
	
	public static void main(String[] args) throws Exception {
		
		//get command line attributes
		
		HashMap<String, String> cliHash = getCommandLineArgs(args);
		
		String repo = cliHash.get("repo");
		String username = cliHash.get("username");
		String password = cliHash.get("password");
		
		if (repo == null){
			log.log(Level.WARNING, "Starting to create asset map");
			return;
		}
		
		log.log(Level.INFO, "Starting to create asset map");
		damAssets = getDamAssets(repo, username, password);
		log.log(Level.INFO, "Finished creating asset map");
		
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		
		return cliHash;
		
	}
	
	private static HashMap<String, AssetBean> getDamAssets(String repo, String username, String password) throws Exception {
		
		HashMap<String,AssetBean> damAssets = new HashMap<String,AssetBean>();

		CqHelper helper = new CqHelper();
		
		Session session = helper.getSession(repo, username, password);
		
		Node root = session.getRootNode();
		
		String q = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([/content/dam/belkin/belkin/photography/product]) and" + 
		"[jcr:content/jcr:primaryType] <> 'dam:AssetContent' and [jcr:content/metadata/dc:title] is not null";
		
		Query myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
		
		QueryResult result = myQuery.execute();
		
		NodeIterator ni = result.getNodes();
		
		long nodeCounter = 0;
		
		while (ni.hasNext() && nodeCounter <= 20000){
			
			nodeCounter++; 

			Node n = ni.nextNode();
			
			//System.out.println(n.getPath());
			
			n.remove();

			if (nodeCounter % 100 == 0){
				System.out.println(new Long(nodeCounter).toString());
				session.save();
			}
			
		}
		
		session.logout();
		
		return damAssets;
		
	}

}
