package com.hero.jcr.commandline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.InvalidQueryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import reportingUtil.NodeBean;
import reportingUtil.NodeReportingBean;
import reportingUtil.NodeReportingBeanList;
import reportingUtil.ProjectBean;
import reportingUtil.TasksBean;
import reportingUtil.WorkflowInstanceBean;
import reportingUtil.XmlUtil;
import reportingUtil.ProjectList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;

import com.hero.jcr.util.CqHelper;

public class ProjectReportingExtract {
	
	static String REPOLOCATION = "http://54.183.80.2:4502";
	static String REPOUSER = "admin";
	static String REPOPASSWORD = "M4C@rjdN*m58!S1e";
	static String DEFAULTFILE = "/tmp/test.txt";
	
	private static Options options = new Options();
	private static final Logger log = Logger.getLogger(TagAssets.class.getName());	

	public static void main(String[] args) {
		
		HashMap<String, String> commandLineParameters = getCommandLineArgs(args);
		
		processRequest(commandLineParameters.get("repo"),
				       commandLineParameters.get("username"),
				       commandLineParameters.get("password"));

	}
	
	private static void processRequest(String repo, String username, String password){
		
		/*
		 * open up the repo establish a session and find the root node
		 */
		
		HashMap<String, List<NodeBean>> models = new HashMap<String,List<NodeBean>>();
		
		CqHelper helper = new CqHelper();
		
		Session session = null;
		try {
			session = helper.getSession(repo, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Node root = null;
		
		try {
			root = session.getRootNode();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		if (session == null || root == null){
			return;
		}		
		
		//String queryString = "SELECT * FROM [sling:OrderedFolder] AS s WHERE ISDESCENDANTNODE([/content/dam/projects]) and [projectPath] is not null";
		String queryString = "SELECT * FROM [sling:OrderedFolder] AS s WHERE ISDESCENDANTNODE([/content/dam/projects]) and [projectPath] is not null and [testFlag] is not null";
		Query myQuery;
		
		ProjectList projectList = new ProjectList();
		
		try {

			myQuery = session.getWorkspace().getQueryManager().createQuery(queryString, Query.JCR_SQL2);
			QueryResult result = myQuery.execute();
			
			NodeIterator ni = result.getNodes();	
			
			while (ni.hasNext()){
				
				ProjectBean pb = new ProjectBean();
				
				Node n = (Node)ni.next();
				
				try{

					/*
						private String projectTitle;
						private String projectPath;
						private String dateCreated;
						private String description;
						private String requester;
						private String status;
						private String proposed_workflows;
						private String bi_values;
					*/

					String title = getStringFromProperty(n, "./jcr:title", "No Title");
					String projectPath = getStringFromProperty(n, "./projectPath", "No Project Path");
					
					Node p2 = null;
					
					String newProjectPath = "." + projectPath + "/jcr:content";

					p2 = session.getRootNode().getNode(newProjectPath); 

					String dateCreated = getStringFromProperty(p2, "./jcr:created", "2016-03-01T00:00:00.000Z");  //default to a safe value
					String description = getStringFromProperty(p2, "./jcr:description", "");  //this translates to the "brief" field in the project
					String requester = getStringFromProperty(p2, "./bi.requestor", "");
					String status = getStringFromProperty(p2, "./bi.status", "");
					String proposed_workflows = getStringFromProperty(p2, "./bi.workflows", "");
					
					ArrayList<String> biValuesList = new ArrayList<String>();
					
					PropertyIterator biProperties = p2.getProperties();
					
					while (biProperties.hasNext()){
						
						Property biNode = (Property)biProperties.next();
						String propertyTitle = biNode.getName();
						if (propertyTitle.startsWith("bi.")){
							
							String propertyValue = getStringFromProperty(p2, "./" + propertyTitle, "");
							
							biValuesList.add(propertyTitle + " : " + propertyValue);
						}
						
					}
					
					String bi_values = String.join("/", biValuesList);
					
					pb.setTitle(title);
					pb.setProjectPath(projectPath);
					pb.setDateCreated(dateCreated);
					pb.setDescription(description);
					pb.setRequester(requester);
					pb.setStatus(status);
					pb.setProposed_workflows(proposed_workflows);
					pb.setBi_values(bi_values);
					
					projectList.add(pb);
					
				} catch (Exception e){
					
					//e.printStackTrace();
					
				}
				
			}

		} catch (InvalidQueryException e) {
			// TODO Auto-generated catch block
			log.severe(e.getMessage());
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			log.severe(e.getMessage());
		}
		
		System.out.println("-- Number of Projects: " + projectList.getPlist().size() );
		int projectCounter = 0;
		
		for (ProjectBean pb : projectList.getPlist()){

			System.out.println("-- " + projectCounter++ + " : " + pb.getTitle() + " : " + pb.getProjectPath());
			List<TasksBean> taskList = getProjectInstanceNodes(session, pb.getProjectPath());
			List<WorkflowInstanceBean> wfInstnaces = getWorkflows(session, pb.getProjectPath());
			
			//print out tasks under project
//			for (TasksBean t : taskList){
//				
//				System.out.println("\tTask: " + t.getName() + " : " + t.getWfInstanceId() + " : " + t.getWorkItemId());
//				
//			}
			
			//print out workflow models
			for (WorkflowInstanceBean w : wfInstnaces){
				
				//System.out.println("\tWorkflow: " + w.getModelid() + " : " + w.getWorkflowid());
				
				NodeReportingBeanList nodeInstances = new NodeReportingBeanList();
				//List<NodeReportingBean> nodeInstances = new ArrayList<NodeReportingBean>();
				
				//We are caching workflow models in a hashmap to keep from having to fetch the data
				//from the JCR for workflows we have seen before
				
				String modelId = w.getModelid();
				
				models = getWorkflowNodes(session, modelId, models);
				
				if (models.containsKey(modelId)){
					
					List<NodeBean> nodes = models.get(modelId);
					
					for (NodeBean node : nodes){
						
						NodeReportingBean nodeReportingBean = new NodeReportingBean();
						
						Boolean matchesFound = false;
						
						for (TasksBean task : taskList){
							
							if (w.getWorkflowid() == task.getWfInstanceId() && 
								node.getTitle() == task.getName()){

								nodeReportingBean.setWorkflowNode(node);
								nodeReportingBean.setTaskNode(task);
								
								nodeInstances.getNodeInstances().add(nodeReportingBean);
								
								matchesFound = true;
								
							}
							
						}
					
						if (!matchesFound){
							
							nodeReportingBean.setWorkflowNode(node);
							//create and empty taskbean as there is no matching task
							nodeReportingBean.setTaskNode(new TasksBean());
							
							nodeInstances.getNodeInstances().add(nodeReportingBean);
							
						}
						
						//System.out.println("\t\tWorkflowNode: " + node.getTitle());
					
					}

					for (NodeReportingBean nrb : nodeInstances.getNodeInstances()){
						
						//System.out.println("\t\tNode Instances: " + nrb.getNode_title() + " : " + nrb.getTask_name());
						
					}
					
					w.setNodes(nodeInstances.getNodeInstances());
				
				}
			
			}
			
			pb.setWorkflows(wfInstnaces);
			
		}	
		
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		
		String json = gson.toJson(projectList);
		
		System.out.println(json);	
		
		XmlUtil xmlutil = new XmlUtil();
		
		String xml = xmlutil.convertToXml(projectList, projectList.getClass());
		
		System.out.println(xml);
		
		session.logout();
		
	}
	
	private static HashMap<String, List<NodeBean>> getWorkflowNodes(Session session, String modelId, HashMap<String, List<NodeBean>> models){
		
		if (!models.containsKey(modelId)) {
			
			models = getWorkflow(session, modelId, models);
			
			if (!models.containsKey(modelId)){
				
				//Ok something really really bad happened, the workflow no longer exits
				
				log.severe("Workflow no longer exits: " + modelId);
				
			}
			
		}
		
		return models;
		
	}
	
	private static List<TasksBean> getProjectInstanceNodes(Session session, String projectPath){
		
		List<TasksBean> taskList = new ArrayList<TasksBean>();
		
		String taskQuery = "SELECT * FROM [granite:Task] AS s WHERE ISDESCENDANTNODE([" + projectPath +  "])";
		Query myQuery;
		
		try {

			myQuery = session.getWorkspace().getQueryManager().createQuery(taskQuery, Query.JCR_SQL2);
			QueryResult result = myQuery.execute();
			
			NodeIterator ni = result.getNodes();	
			
			while (ni.hasNext()) {
				
				Node taskNode = (Node)ni.next();
				
				TasksBean taskBean = new TasksBean();
				
				Property p = null;
				
				/*
					private String assignee;
					private String completedBy;
					private String name;
					private String nameHierarchy;
					private Date dueDate;
					private String priority;
					private String status;
					private Date startTime;
					private String wfInstanceId;
					private String workItemId;
				 */
				
				taskBean.setAssignee(getStringFromProperty(taskNode, "./assignee", "none"));
				taskBean.setCompletedBy(getStringFromProperty(taskNode, "./completedBy", "none"));
				taskBean.setName(getStringFromProperty(taskNode, "./name", "none"));
				taskBean.setNameHierarchy(getStringFromProperty(taskNode, "./nameHierarchy", "none"));
				taskBean.setDueDate(getStringFromProperty(taskNode, "./dueDate", "none"));
				taskBean.setPriority(getStringFromProperty(taskNode, "./priority", "none"));
				taskBean.setStatus(getStringFromProperty(taskNode, "./status", "none"));
				taskBean.setStartTime(getStringFromProperty(taskNode, "./startTime", "none"));
				taskBean.setWfInstanceId(getStringFromProperty(taskNode, "./wfInstanceId", "none"));
				taskBean.setWorkItemId(getStringFromProperty(taskNode, "./workItemId", "none"));
				
				taskList.add(taskBean);
				
			}
			
		} catch (Exception e) {
			
			log.warning(e.getMessage());
			e.printStackTrace();
			
		}
		
		return taskList;
		
	}
	
	private static List<WorkflowInstanceBean>getWorkflows(Session session, String projectPath) {
		
		List<WorkflowInstanceBean> workflowInstances = new ArrayList<WorkflowInstanceBean>();
		
		String wfinstanceQuery = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(["+ projectPath + "/jcr:content/work]) and [model.id] is not null";
		Query myQuery;
		
		try {

			myQuery = session.getWorkspace().getQueryManager().createQuery(wfinstanceQuery, Query.JCR_SQL2);
			QueryResult result = myQuery.execute();
			
			NodeIterator ni = result.getNodes();	
			
			while (ni.hasNext()) {
				
				WorkflowInstanceBean wfInstnace = new WorkflowInstanceBean();
				Node n = (Node)ni.next();
				
				wfInstnace.setModelid(getStringFromProperty(n,"./model.id", "none"));
				wfInstnace.setWorkflowid(getStringFromProperty(n,"./workflow.id", "none"));

				workflowInstances.add(wfInstnace);
				
			}
			
		} catch (Exception e) {
			
			log.warning(e.getMessage());
			e.printStackTrace();
			
		}
		
		return workflowInstances;
	}
	
	private static HashMap<String, List<NodeBean>> getWorkflow(Session session, String modelId, HashMap<String, List<NodeBean>> models){
		
		List<NodeBean> nodes = new ArrayList<NodeBean>();
		
		String nodeQuery = "SELECT * FROM [cq:WorkflowNode] AS s WHERE ISDESCENDANTNODE([" + modelId + "]) and [type] = 'PROCESS'";
		Query myQuery;
		
		try {
			
			myQuery = session.getWorkspace().getQueryManager().createQuery(nodeQuery, Query.JCR_SQL2);
			QueryResult result = myQuery.execute();
			
			NodeIterator ni = result.getNodes();	
			
			while (ni.hasNext()) {
				
				Node n = (Node)ni.next();
				
				NodeBean node = new NodeBean();
				
				node.setTitle(getStringFromProperty(n, "./title", "none"));
				node.setDescription(getStringFromProperty(n, "./description", "none"));
				
				Node metaNode = n.getNode("./metaData");
				
				node.setArgTaskDueDeltaDays(getStringFromProperty(metaNode, "./argTaskDueDeltaDays", "1"));
				
				nodes.add(node);
				
			}
			
			models.put(modelId, nodes);
			
			
		} catch (Exception e) {
			
			log.severe(e.getMessage());
			e.printStackTrace();
			
		}
		
		return models;
	}
	
	private static String getStringFromProperty(Node n, String propertyName, String defaultValue) throws Exception{
		
		String retVal = null;
		
		Property property = null;
		
		try{

			property = n.getProperty(propertyName);
			
		} catch (Exception e){
			
			retVal = defaultValue;
			
		}
		
		if ((retVal == null) && property.isMultiple()){
			
			try{
				
				retVal = property.getValues()[0].getString();
				
			} catch (Exception e) {
				
				retVal = defaultValue;
				
			}
			
		} else if ((retVal == null)) {

			try{
				
				retVal = property.getValue().getString();
				
			} catch (Exception e) {
				
				retVal = defaultValue;
				
			}
			
		}
		
		return retVal;
	}
	
	private static HashMap<String, String> getCommandLineArgs(String[] args){
		
		HashMap<String, String> cliHash = new HashMap<String, String>();
		String repo = REPOLOCATION;
		String username = REPOUSER;
		String password = REPOPASSWORD;
		String filename = DEFAULTFILE;
		
		options.addOption("r", "repo", true, "Repo Location.");
		options.addOption("u", "username", true, "Repo Username.");
		options.addOption("p", "password", true, "Repo User Password.");
		options.addOption("f", "filename", true, "Input file name");
		
		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		
		try{
			
			cmd = parser.parse(options, args);
			
			if(cmd.hasOption('r')){
				repo = cmd.getOptionValue("r");
			}

			if(cmd.hasOption('u')){
				username = cmd.getOptionValue("u");
			}
			
			if(cmd.hasOption('p')){
				password = cmd.getOptionValue("p");
			}
			
			if(cmd.hasOption('f')){
				filename = cmd.getOptionValue("f");
			}

		} catch (ParseException e){
			
		   log.log(Level.SEVERE, "Failed to parse comand line properties", e);
			
		}
		
		cliHash.put("repo", repo);
		cliHash.put("username", username);
		cliHash.put("password", password);
		cliHash.put("filename", filename);
		
		return cliHash;
		
	}	

}
