# Gigamon Project - Author Widgets (UI)

The primary bundle for non-Java code (JS, CSS, JSP) related to extending or
creation of AEM dialog fields.

This is kept separate from the main __ui.apps__ module in case other projects
wish to re-use the functionality.
