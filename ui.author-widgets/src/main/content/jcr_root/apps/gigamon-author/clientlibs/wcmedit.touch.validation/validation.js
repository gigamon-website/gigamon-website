
(function(window, $) {
    'use strict';

    /*
     * Generic regular expression validation for text fields.
     *
     * Dialog field requires the data-foundation-validation attribute
     * containing 'regex' and a data-regex attribute with the regular
     * expression.
     *
     * Example:
     * @DialogField(fieldLabel = "Name", name = "./name", ranking = 100,
     *     additionalProperties={
     *             @Property(name = "regex", value = "^data-.+$"),
     *             @Property(name = "foundation-validation", value = "regex")})
     */
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: "[data-foundation-validation~='regex'],[data-validation~='regex']",
        validate: function(el) {
            var $el = $(el);
            var v = $el.val();
            var regexStr = $el.data('regex');

            if (!regexStr) {
                console.log('Missing regex string for ', el);
                return;
            }

            try {
                var re = new RegExp(regexStr);

                if (!v) v = '';

                if (v.match(re)) {
                    return;
                }

                return 'Value [' + v + '] is not valid.';
            } catch (e) {
                console.log(e);
            }
        }
    });

})(window, Granite.$);


(function(window, $) {
    'use strict';

    /**
     * Multifield validation that enforces the max limit.
     */
    $(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
        selector: "input, select",
        validate: function(el) {
            var $el = $(el);
            var $multifield = $el.closest('.coral-Multifield');

            if ($multifield.length == 0) {
                return;
            }

            var limit = $multifield.attr('data-maxlimit');

            if (!limit) {
                return;
            }

            try {
                var $items = $multifield.find('li.coral-Multifield-input');
                if ($items.length > limit) {
                    return 'This component is not designed to support more that ['+limit+'] items in this multifield dialog';
                }
            } catch (e) {
                console.log(e);
            }
        }
    });

})(window, Granite.$);
