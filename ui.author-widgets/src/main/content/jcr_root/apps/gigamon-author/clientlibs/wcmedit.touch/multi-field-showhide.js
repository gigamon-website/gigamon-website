(function () {
    
    /*
        This is a custom implementation of the out of the box functionality
        that allows a dialog field to be hidden/shown based on another select
        field's value.
        
        This is different because it allows to show/hide multiple fields instead
        of being tied to a single field.
        
        Aditional Properties on SELECT field:
        @Property(name = "class", value = "multi-field-showhide"),
        @Property(name = "multi-field-showhide-target", value = ".XXXXX-showhide-target")
    
        Additional Properties on SHOW/HIDE field:
        @Property(name = "class", value = "XXXXX-showhide-target"),
        @Property(name = "showhidetargetvalue", value = THE_VALUE_FROM_THE_SELECT_FIELD)
    */

    function updateSelection($target, selectedVal) {
        var $fieldWrapper = $target.closest('.coral-Form-fieldwrapper');
        
        // special case for image field
        if ($fieldWrapper.length < 1) {
            $fieldWrapper = $target;
        }
        
        var showHideTargetValue = $target.attr('data-showhidetargetvalue');

        $fieldWrapper.hide();
        if (showHideTargetValue === selectedVal) {
            $fieldWrapper.show();
        }
    }
    
    $(document).on('dialog-ready', function() {
        var $fields = $('.multi-field-showhide');
        $fields.each(function() {
            var $field = $(this);
            
            var $selected = $field.find('select :selected');
            var selectedVal = $selected.attr('value');
            var targetAttr = $field.attr('data-multi-field-showhide-target');
            var $target = $(targetAttr);

            $target.each(function() {
                updateSelection($(this), selectedVal)
            });

            $field.on('selected', function() {
                selectedVal = $(this).find('select :selected').attr('value');

                $target.each(function() {
                    updateSelection($(this), selectedVal);
                });
            });
        });
    });

})();
