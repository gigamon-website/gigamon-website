/*
 * Custom Author mode editor that uses the Ace editor 
 * (https://ace.c9.io/#nav=about).
 *
 * To use this editor [component]/cq:editConfig/cq:inplaceEditing/@editorType 
 * must be set to 'ace'.
 *
 * If using component dialog annotations this is done with 
 * @Component.inPlaceEditingEditorType = 'ace' class annotation.
 */
(function ($, ns, channel, window, undefined) {
    "use strict";
    
    var PROPERTY_NAME = 'text'
    
    var AceEditor = function() {}

    AceEditor.prototype.setUp = function (editable) {
        var self = this
        
        // hide the authoring overlay
        channel.trigger("cq-hide-overlays")
        
        // close the side panel if it is opened
        if (ns.ui.SidePanel.isOpened()) {
            ns.ui.SidePanel.close()
        }
        
        // Add the stub markup to the main Content div
        self.$modal = $('#Content').append(
            '<div class="ace-modal">'+
            '<div class="ace-modal-content">'+
            '<div class="ace-modal-nav">'+
                '<a href="#" class="ace-btn ace-modal-close">Close</a>'+
                '<a href="#" class="ace-btn ace-modal-save">Save</a>'+
                '<span class="ace-modal-msg">MESSAGE</span>'+
            '</div>'+
            '<div class="ace-editor-wrap">'+
                '<div class="ace-editor"></div>'+
            '</div>'+
            '</div>'+
            '</div>').find('.ace-modal')
        
        // Wire up save
        self.$modal.find('.ace-modal-save').on('click', function() {
            self.save(editable)
            return false
        })
        // Wire up close
        self.$modal.find('.ace-modal-close').on('click', function() {
            self.tearDown(editable)
            return false
        })
        
        // Initialize Ace editor
        self.editor = ace.edit(self.$modal.find('.ace-editor')[0])
        self.editor.session.setMode("ace/mode/html")
        self.editor.session.setOption("useWorker", false)
        self.editor.commands.addCommand({
            name: 'save',
            bindKey: {win: 'Ctrl-S', mac: 'Command-S'},
            exec: function(editor) {
                self.save(editable)
            }
        })
        
        // Load data into the editor
        ns.persistence.readParagraphContent(editable, true)
            .done(function (data) {
                var content = $.parseJSON(data)
                var initialContent = content[PROPERTY_NAME] || ''
                self.editor.setValue(initialContent)
                self.editor.clearSelection()
                self.editor.gotoLine(0, 0)
            }).fail(function (data) {
                console.log('Error reading paragraph content!', editable)
                // ns.persistence.updateParagraph(editable, {"./sling:resourceType": editable.type}).done(function () {
                //     ns.persistence.readParagraphContent(editable, true).done(function (data) {
                //         
                //     })
                // })
            })
    }

    AceEditor.prototype.save = function (editable) {
        var self = this
        var props = {}
        props['./'+PROPERTY_NAME] = self.editor.getValue()
        
        self.toast('Saving...')
        
        var doneFunc = function() {
            self.toast('Updates saved.', 2000)
        }
        var failFunc = function(e) {
            self.toast('An error occured while saving!')
            console.log('An error occured during save: ', e)
        }
        
        ns.edit.EditableActions.UPDATE.execute(editable, props)
            .then(doneFunc)
            .fail(failFunc)
    }

    AceEditor.prototype.tearDown = function (editable) {
        var self = this
        self.editor.destroy()
        self.editor = null
        self.$modal.remove()
        self.$modal = null
        channel.trigger("cq-show-overlays")
    }
    
    /**
     * @param msg: message to show
     * @param hideAfter: (optional) hide after milliseconds 
     */
    AceEditor.prototype.toast = function (msg, hideAfter) {
        var self = this
        var $msg = self.$modal.find('.ace-modal-msg')
        $msg.text(msg)
        $msg.addClass('active')
        
        if (hideAfter) {
            setTimeout(function() {
                $msg.removeClass('active')
            }, hideAfter)
        }
    }

    ns.editor.register('ace', new AceEditor())

}(jQuery, Granite.author, jQuery(document), this));
