# Gigamon Project - UI Apps

The primary bundle for non-Java code that is to be deployed to AEM.

Contains all HTL (Sightly) component code and customizations via
overlays to OOB AEM.

## Component Development

### Creating a New Component

Refer to the __generator-gigamon-component__ README for instructions on creating
creating starter files for new components.

### HTL (Sightly)

Every component will have an HTL file in this module which contains the markup
for that component.

### Model Classes

Model classes are contain in the __core__ module. Refer to the README for
more information.

### CSS and JavaScript

All CSS (Sass) and JavaScript is managed withing the __ui.clientlibs__ module.
Refer to the README for more information.
