/*
================================================================================
When a new component is dragged into a Responsive Layout it creates both the
new column component as well as the new component nested under that new column
component node.

Initial JCR:
responsive-column
  |- [no children]

JCR after dragging Text Jumbo inside Responsive Column:
responsive-column
  |- column-1
    |- text-jumbo

This JS is required because AEM author only refreshes the content of the
component just dragged onto the page. However, in our case we need to actually
refresh the entire Responsive Layout component (2 levels up). We need this
refresh because the Responsive Layout component is responsible for rendering
layout/grid classes based on the column components contained within it.
================================================================================
*/
;(function ($, ns, channel) {
    "use strict";

    channel.on("cq-persistence-after-create", function(e, component, relativePosition, editableNeighbor, additionalData) {
        if (!editableNeighbor) {
            console.log('ResponsiveLayout: Expected a value for editableNeighbor', e, component, relativePosition, editableNeighbor, additionalData);
            return;
        }

        if (editableNeighbor.type == 'gigamon/components/content/responsive-layout/column/new') {
            console.log('ResponsiveLayout: Attempting to refresh "Columns Advanced" component post new column insertion');
            var columnEditable = Granite.author.editables.getParent(editableNeighbor);
            if (!columnEditable) {
                console.log('ResponsiveLayout: Could not find parent Column instance! Skipping refresh', editableNeighbor);
                return;
            }
            var responsiveLayoutEditable = Granite.author.editables.getParent(columnEditable);
            if (!responsiveLayoutEditable) {
                console.log('ResponsiveLayout: Could not find parent Responsive Layout instance! Skipping refresh', columnEditable, editableNeighbor);
                return;
            }

            Granite.author.edit.EditableActions.REFRESH.execute(responsiveLayoutEditable);
        }
    });
}(jQuery, Granite.author, jQuery(document)));
