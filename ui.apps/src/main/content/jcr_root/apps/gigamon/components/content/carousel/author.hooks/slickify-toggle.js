/*
================================================================================
This file handles initializing and tearing down the carousel component as the
use switches between edit, design, and preview modes.

Listens for the "cq-layer-activated" event in order to trigger the logic.

Refer to LayerManager.js for details on "cq-layer-activated" event:
/libs/cq/gui/components/authoring/editors/clientlibs/core/js/layers/LayerManager.js
================================================================================
*/
;(function ($, ns, channel) {
    "use strict";

    channel.on("cq-layer-activated", function(e) {
        // First get the jquery object that the iframed application has loaded.
        // This is needed because the application has the jQuery object that
        // has the Slick plugin loaded.
        var $j = $("#ContentFrame")[0].contentWindow.$;
        if (!$j) return;

        var $carousels = $("#ContentFrame").contents().find('.component-carousel');
        $carousels.each(function() {
            switch(e.layer) {
                case "Edit":
                    teardownCarousel($j(this));
                    break;
                case "Design":
                    teardownCarousel($j(this));
                    break;
                case "Preview":
                    initCarousel($j(this));
                    break;
            }
        });
    });

    function teardownCarousel($carousel) {
        if (!isSlickified($carousel)) return;
        $carousel.slick('unslick');
    }

    function initCarousel($carousel) {
        if (isSlickified($carousel)) return;
        $carousel.slick();
        removeInvalidSlides($carousel);
    }

    function isSlickified($carousel) {
        if (typeof $carousel.slick !== 'function') return false;
        if (!$carousel.hasClass('slick-initialized')) return false;
        return true;
    }

    /**
        Preview mode in AEM creates extra divs (things like a parsys for dragging a
        new component). This function removes any slides that resulted from those
        author only divs.

        This code should only affect preview mode. AEM does not pump out the invalid
        divs in publish mode.

        @param $carousel jQuery object representing a single instance of a carousel
    */
    function removeInvalidSlides($carousel) {
        var $slides = $carousel.find('.slick-slide').not('.slick-cloned');

        for (var i = 0; i < $slides.length; i++) {
            var $slide = $($slides[i]);
            var remove = null;

            if ($slide.hasClass('new') && $slide.hasClass('section')) {
                remove = 'author only new section div';
            }
            if ($slide.prop('tagName') == 'CQ') {
                remove = 'author only cq div';
            }

            if (remove) {
                var index = $carousel.find('.slick-slide').index($slide) - 1;
                if (window.console) console.log('Removing #' + index + ' slide: ' + remove);
                $carousel.slick('slickRemove', index);
            }
        }
    }
}(jQuery, Granite.author, jQuery(document)));
