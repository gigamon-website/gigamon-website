<%@page session="false"%><%@include file="/libs/foundation/global.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="
    com.day.cq.wcm.api.variants.PageVariant,
    com.day.cq.wcm.api.variants.PageVariantsProvider,
    com.day.cq.wcm.api.devicedetection.DeviceIdentificationMode,
    com.day.cq.wcm.api.WCMMode,
    com.day.cq.commons.DiffService"
        %>
<%
    final PageVariantsProvider p = sling.getService(PageVariantsProvider.class);
    if(p == null) {
        throw new IllegalStateException("Missing PageVariantsProvider service");
    }
    for(PageVariant v : p.getVariants(currentPage, slingRequest)) {
        final String curVar = v.getAttributes().get("data-bmap-currentvar");
        String devicegroups = v.getAttributes().get("data-bmap-devgroups");
        if (devicegroups != null) {
            devicegroups = devicegroups.replaceAll(" ", "");
        }
  String langCode = xssAPI.encodeForHTMLAttr(v.getAttributes().get("hreflang"));
			switch(langCode){

                case "en_us":
                langCode = "en-us";
                break;

                case "de":
                langCode = "de-de";
                break;

                case "fr":
                langCode = "fr-fr";
                break;

                case "kr":
                langCode = "ko-kr";
                break;

                case "jp":
                langCode = "ja-jp";
                break;

                case "it":
                langCode = "it-it";
                break;

            }

%>
<link
        rel="alternate"
        data-cq-role="site.variant"
        title="<%= xssAPI.encodeForHTMLAttr(v.getTitle()) %>"
        hreflang="<%=langCode%>"
        data-bmap-devgroups="<%= xssAPI.encodeForHTMLAttr(devicegroups) %>"
        href="<%= xssAPI.getValidHref(v.getURL()) %>"
        <% if(curVar != null) { %> data-bmap-currentvar="<%= new Boolean(curVar) %>"<% } %>
        />
<%
    }
    Boolean browserMapEnabled = true;
    final DeviceIdentificationMode dim = sling.getService(DeviceIdentificationMode.class);
    String[] selectors  = slingRequest.getRequestPathInfo().getSelectors();
    boolean isPortletRequest = false;
    for (int i = 0; i < selectors.length; i++) {
        if ("portlet".equals(selectors[i])) {
            isPortletRequest = true;
            break;
        }
    }
    if (isPortletRequest) {
        log.debug("Request was made by a portlet container - BrowserMap will not be embedded");
    } else {
        final WCMMode wcmMode = WCMMode.fromRequest(slingRequest);
        boolean shouldIncludeClientLib = false;
        if (WCMMode.EDIT != wcmMode && WCMMode.PREVIEW != wcmMode && WCMMode.DESIGN != wcmMode &&
                (WCMMode.DISABLED != wcmMode || request.getParameter(DiffService.REQUEST_PARAM_DIFF_TO) == null)) {
            if (dim != null) {
                final String mode = dim.getDeviceIdentificationModeForPage(currentPage);
                shouldIncludeClientLib = DeviceIdentificationMode.CLIENT_SIDE.equals(mode);
                if (shouldIncludeClientLib) {
                    browserMapEnabled = (Boolean) request.getAttribute("browsermap.enabled");
                    if (browserMapEnabled == null) {
                        browserMapEnabled = true;
                    }
                }
            }
        }
%>
<c:if test="<%= !browserMapEnabled %>">
    <meta name="browsermap.enabled" content="false">
</c:if>
<c:if test="<%= shouldIncludeClientLib %>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <cq:includeClientLib categories="browsermap"/>
</c:if>
<%
    }
%>
