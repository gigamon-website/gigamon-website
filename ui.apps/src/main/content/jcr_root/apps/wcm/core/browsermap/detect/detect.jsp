<%@page session="false"%><%--

* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2012 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
--%>
<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.day.cq.wcm.api.WCMMode" %>
<%
    String title = "BrowserMap Test Page";
    final String detectedTitle = properties.get("jcr:title", String.class);
    if (detectedTitle != null && !"".equals(detectedTitle)) {
        title = detectedTitle;
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
    <title><%= xssAPI.encodeForHTML(title) %></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <script src="/libs/cq/ui/resources/cq-ui.js" type="text/javascript"></script>

    <cq:include script="/libs/wcm/core/browsermap/browsermap.jsp" />

    <%
        // hack for author instances; browsermap.jsp will only add the page variants to a page on an author instance;
        // we also need the client library and the viewport settings
        final WCMMode wcmMode = WCMMode.fromRequest(slingRequest);
        if (WCMMode.EDIT == wcmMode || WCMMode.PREVIEW == wcmMode || WCMMode.DESIGN == wcmMode) {
    %>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <cq:includeClientLib categories="browsermap" />
    <%
        }
    %>

    <style type="text/css">
        body,h1,h2,h3,h4 {
            font-size:17px;
            font-family:'HelveticaNeue-Light',Helvetica,Arial,sans-serif;
            font-weight:300;
        }
        p {
            width: 100%;
            padding-left: 0px;
            margin-top: 0;
            margin-bottom: 0px;
        }
        h1 {
            font-size:300%;
            font-weight:bold;
        }
        h2 {
            font-size:125%;
            font-weight:bold;
        }
        h3 {
            font-size:110%;
            font-weight:bold;
            margin: 0px;
        }
        #result {
            font-size:200%;
            margin:0.5em;
            padding: 0.5em;

            /* gradient, from http://www.puremango.co.uk/2010/04/css-gradient-background/ */
            background: yellow;

            /* Mozilla: */
            background: -moz-linear-gradient(left, yellow, #00FF00);

            /* Chrome, Safari:*/
            background: -webkit-gradient(linear,
            left top, right top, from(yellow), to(#00FF00));
            /* MSIE */
            filter: progid:DXImageTransform.Microsoft.Gradient(
                StartColorStr='yellow', EndColorStr='#00FF00', GradientType=1);

            /* rounder corners */
            -moz-border-radius: 1em;
            border-radius: 1em;
        }

        #result span {
            color:blue;
            font-weight: bold;
        }
        #result .note {
            font-size:75%
        }
        #result .note span {
            color:black;
            font-weight: normal;
            font-style: italic;
        }
        div.debug {
            font-family: monospace;
            margin-top: 20px;
            margin-bottom: 50px;
        }
        div.console-output {
            overflow: auto;
            padding: 10px;
        }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div id="result">
    <p>
        The BrowserMap client-side detection library thinks your device type is <span id="devicegroup-description">UNIDENTIFIED??</span>,
        which is part of the <span id="devicegroup-name">NOT FOUND??</span> device group.
    </p>
</div>
<div id="description">
    <p>
        BrowserMap is a JavaScript browser features detection library integrated into AEM. It uses modular probes and code snippets that detect
        specific features of the client, which can then be grouped in a simple way to create device groups. By identifying the client type,
        BrowserMap can help optimize page rendering or it can provide the client with alternate representations of the current page.
    </p>
    <br />
    <p>
        The following is a list of the device groups BrowserMap is capable of identifying. To override the detected device groups you can use the
        <code>device</code> <code>GET</code> request parameter. To showcase this behaviour, you can click on any device group name from below:
    <ol id="devicegroups-list"></ol>
    </p>
    <p>
        To reset the override, you need to call <a href="#" onclick="javascript:BrowserMap.removeOverride()"><code>BrowserMap.removeOverride()</code></a>.
    </p>
</div>
<div class="debug">
    <h3>Debugging info</h3>
    <div class="console-output" id="debug-info">
    </div>
</div>
<script language="javascript">
    BrowserMap.forwardRequest();
    var matchedGroups = BrowserMap.getMatchedDeviceGroups(),
            matchedGroupsDescription = [],
            matchedGroupsNames = [],
            testFunctions = [],
            dgs = [],
            deviceGrps,
            g,
            element,
            i;
    for (g in matchedGroups) {
        matchedGroupsDescription.push(matchedGroups[g].description);
        matchedGroupsNames.push(matchedGroups[g].name);
        testFunctions.push(matchedGroups[g].testFunction);
    }
    deviceGrps = BrowserMap.getDeviceGroups();
    for (var g in deviceGrps) {
        dgs.push(deviceGrps[g].name);
    }

    element = document.getElementById('devicegroup-description');
    element.innerHTML = matchedGroupsDescription.join(', ')
    element = document.getElementById('devicegroup-name');
    element.innerHTML = matchedGroupsNames.join(', ');

    element = document.getElementById('debug-info');
    var probingResults = BrowserMap.getProbingResults();
    for (i in probingResults) {
        if (probingResults.hasOwnProperty(i)) {
            element.innerHTML += i + '=' + probingResults[i] + '<br />';
        }
    }
    element.innerHTML += '<br />';
    element.innerHTML += 'Test functions: <br />';
    for (i in testFunctions) {
        element.innerHTML += testFunctions[i] + '<br />';
    }
    element.innerHTML += '<br />';

    var dgs = BrowserMap.getDeviceGroupsInRankingOrder(),
            urlNoParams = window.location.href.replace(BrowserMapUtil.Url.getURLParametersString(window.location.href), ''),
            i;
    element = document.getElementById('devicegroups-list');
    for (i = 0; i < dgs.length; i++) {
        element.innerHTML += '<li><a href="' + urlNoParams + '?device=' + dgs[i].name + '">' + dgs[i].name + '</a></li>';
    }
</script>
</body>
</html>
