'use strict';

/**
 *  This is the main file for simple-navigation
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-simple-navigation').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing SimpleNavigation');
    
    $(window).scroll(function() {
		if ($(this).scrollTop() > 200) {
		$('.component-simple-navigation').addClass('showbtn');
		}
		else if ($(this).scrollTop() < 200) {
			$('.component-simple-navigation').removeClass('showbtn');
			}
		});
}
