'use strict';

/**
 *  This is the main file for gated-marketo-form
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-gated-marketo-form').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing GatedMarketoForm');
}
