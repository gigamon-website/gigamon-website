'use strict';

/**
 *  This is the main file for sub-menu-block
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-sub-menu-block').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing SubMenuBlock');
}
