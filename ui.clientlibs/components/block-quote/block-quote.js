'use strict';

/**
 *  This is the main file for block-quote
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-block-quote').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing BlockQuote');
}
