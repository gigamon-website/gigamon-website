'use strict';

/**
 *  This is the main file for sticky-footer
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-sticky-footer').length) {
    $(function() {
        init();
    });
}


function init() {
    $(window).scroll(function() {
    	
		if ($(this).scrollTop() > 50) {
			$('.component-sticky-footer').addClass('show');
		} else if ($(this).scrollTop() < 50) {
			$('.component-sticky-footer').removeClass('show');
		}
		 if($(window).scrollTop()+ $(window).height() >= $(document).height())
			{
				$('.component-sticky-footer').removeClass('show');	
			}
		
	});
}
