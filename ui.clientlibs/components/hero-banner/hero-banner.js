'use strict';

/**
 *  This is the main file for hero-banner
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-hero-banner').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing HeroBanner');
}
