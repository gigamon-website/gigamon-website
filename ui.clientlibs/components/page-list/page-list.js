'use strict';

/**
 *  This is the main file for page-list
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-page-list').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing PageList');
}
