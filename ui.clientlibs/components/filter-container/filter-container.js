'use strict';

/**
 *  This is the main file for filter-container
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-filter-container').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing FilterContainer');
}
