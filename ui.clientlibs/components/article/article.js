'use strict';

/**
 *  This is the main file for article
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-article').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Article');
}
