
var $ = require('jquery');

module.exports = {
    isEdit: function() {
        return $('body').hasClass('edit');
    },
    isPreview: function() {
        return $('body').hasClass('preview');
    },
    isDesign: function() {
        return $('body').hasClass('design');
    }
};
