'use strict';

/**
 *  This is the main file for sub-menu
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-sub-menu').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing SubMenu');
}
