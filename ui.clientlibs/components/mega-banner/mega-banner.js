'use strict';

/**
 *  This is the main file for mega-banner
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-mega-banner').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing MegaBanner');
}
