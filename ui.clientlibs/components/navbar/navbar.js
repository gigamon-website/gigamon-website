'use strict';

/**
 *  This is the main file for navbar
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-navbar').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Navbar');
}
