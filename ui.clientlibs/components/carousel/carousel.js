'use strict';

/**
 *  This is the main file for carousel
 */

var $ = require('jquery');
var log = require('loglevel');
var wcmmode = require('../wcmmode');

if ($('.component-carousel').length) {
    if (!wcmmode.isEdit() && !wcmmode.isDesign() && !wcmmode.isPreview()) {
        $(function() {
            init();
        });
    }
}

function init() {
    log.trace('Initializing Carousel');
    //$('.component-carousel').slick();  To Customize Navigation arrows and dots and to apply break points.
        $('.component-carousel').slick({
            prevArrow: '<button class="custom-prev" type="button"><svg class="navArrows" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="512px" height="50px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><path d="M256,0C114.609,0,0,114.609,0,256c0,141.391,114.609,256,256,256c141.391,0,256-114.609,256-256		C512,114.609,397.391,0,256,0z M256,472c-119.297,0-216-96.703-216-216S136.703,40,256,40s216,96.703,216,216S375.297,472,256,472z"/><polygon points="384,240 185.594,240 224,198.844 202.656,176 128,256 202.656,336 224,313.125 185.594,272 384,272 "/></g></svg></button>',
            nextArrow: '<button class="custom-next" type="button"><svg class="navArrows" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="512px" height="50px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; transform: rotate(180deg);" xml:space="preserve"><g><path d="M256,0C114.609,0,0,114.609,0,256c0,141.391,114.609,256,256,256c141.391,0,256-114.609,256-256		C512,114.609,397.391,0,256,0z M256,472c-119.297,0-216-96.703-216-216S136.703,40,256,40s216,96.703,216,216S375.297,472,256,472z"/><polygon points="384,240 185.594,240 224,198.844 202.656,176 128,256 202.656,336 224,313.125 185.594,272 384,272 "/></g></svg></button>',
             centerMode: true,
              variableWidth: false,
              centerPadding: '0px',
              responsive: [
                     {
                       breakpoint: 420,
                       settings: {
                         arrows: false,
                         centerPadding: '0px',
                         slidesToShow: 1,
                         slidesToScroll: 1,
                         dots: true
                       }
                     },
					 {
                      breakpoint: 769,
                      settings: {
                        arrows: false,
                        centerPadding: '0px',
                          slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true
                      }
                    }
                   ]
            });
}
