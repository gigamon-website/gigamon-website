'use strict';

/**
 * Module is the JS equivalent of com.wdc.aem.core.util.ImageRendition
 *
 * Provides utility methods to convert an image path to one with rendition selectors.
 *
 * Any changes to the Java or JavaScript version of this file should be propagated to both.
 */

exports.FULL_WIDTH_X2_DESKTOP   = new Rendition(3840,  1500);
exports.FULL_WIDTH_x1_DESKTOP   = new Rendition(1920,  750);
exports.FULL_WIDTH_x2_MOBILE    = new Rendition(960,   375);
exports.FULL_WIDTH_X1_MOBILE    = new Rendition(960,   375); // Currently, X1 is the same as X2
exports.HALF_WIDTH_LARGE        = new Rendition(1000,  1000);
exports.HALF_WIDTH_SMALL        = new Rendition(500,   500);
exports.ORIGINAL                = new Rendition(0,     0);

function Rendition(width, height) {
    this.width = width;
    this.height = height;
}

Rendition.prototype.getRendition = function(imagePath) {
    if (!imagePath) return imagePath;

    if (this.width == 0 || this.height == 0) {
        var ext = getExtension(imagePath);
        return imagePath + '.imgo.' + ext;
    }

    // web images should always be jpg extension
    return imagePath + ".imgw." + this.width + "." + this.height + "." + "jpg";
};

function getExtension(imagePath) {
    if (imagePath && imagePath.indexOf(".png") > 0) {
        return "png";
    } else if (imagePath && imagePath.indexOf(".jpg") > 0) {
        return "jpg";
    } else if (imagePath && imagePath.indexOf(".svg") > 0) {
        return "svg";
    } else {
        return "jpg"; // default
    }
}
