'use strict';

/**
 *  This is the main file for footer
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-footer').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Footer');
}
