'use strict';

/**
 *  This is the main file for columns
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-columns').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Columns');

	$('.component-columns.hoverable .component-text-jumbo').hover(function() {
		$('.component-columns.hoverable .component-text-jumbo').removeClass('active');
		$(this).addClass('active');
	},function(){
		$('.component-columns.hoverable .component-text-jumbo').removeClass('active');
	});


}
