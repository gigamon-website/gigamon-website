'use strict';

/**
 *  This is the main file for search
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-search').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Search');
}
