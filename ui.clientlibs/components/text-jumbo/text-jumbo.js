'use strict';

/**
 *  This is the main file for text-jumbo
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-text-jumbo').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing TextJumbo');
}
