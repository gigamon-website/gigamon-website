'use strict';

/**
 *  This is the main file for jump-navigation
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-jump-navigation').length) {
    $(function() {
        init();
    });
}

function init() {
	log.trace('Initializing JumpNavigation');
	 var x= $('.component-jump-navigation').offset().top;
	     $(window).scroll(function() {
			if ($(this).scrollTop() > x) {

			$('.component-jump-navigation').addClass('sticky');
			$('.simple-navigation').addClass('absolute');
			$('.component-simple-navigation').addClass('absolute');
			}
			else if ($(this).scrollTop() < x) {
				$('.component-jump-navigation').removeClass('sticky');
				$('.simple-navigation').removeClass('absolute');
				$('.component-simple-navigation').removeClass('absolute');
				}
			});
}


function jump(id) {
    $('html, body').animate({
        scrollTop: $('#'+id).offset().top
    }, 1500);
}


$('#jump-arrow').click(function(){
	if($(window).width()>415)
	{
    $('.component-jump-navigation .jump-main').addClass('hide');
	 $('.component-jump-navigation .arrow-open').removeClass('hide');
	}
	if($(window).width()<=415)
			{
		
			 if($('.component-jump-navigation .jump-list').hasClass('hide'))
    				{
				 $('.component-jump-navigation .jump-head .mp-downarrow').attr('class','mp-downarrow hide');
					$('.component-jump-navigation .jump-head .mp-uparrow').attr('class','mp-uparrow');
				 $('.component-jump-navigation .jump-list').removeClass('hide');
    				}
    				else{
    					$('.component-jump-navigation .jump-list').addClass('hide');
    					$('.component-jump-navigation .jump-head .mp-downarrow').attr('class','mp-downarrow');
    					$('.component-jump-navigation .jump-head .mp-uparrow').attr('class','mp-uparrow hide');	
    				
    				}
			}

});

$('#arrow-open').click(function(){
		if($(window).width()>=415)
			{
    			$('.component-jump-navigation .jump-main').removeClass('hide');
	 			$('.component-jump-navigation .arrow-open').addClass('hide');
			}
});


$(document).ready(function(){
	if($(window).width()<=415)
    {
$('.component-jump-navigation .jump-main').removeClass('hide');
$('.component-jump-navigation .jump-list').addClass('hide');
$('.component-jump-navigation .jump-head .left-arrow').attr('class','left-arrow hide');
$('.component-jump-navigation .jump-head .mp-downarrow').attr('class','mp-downarrow');
    }
});