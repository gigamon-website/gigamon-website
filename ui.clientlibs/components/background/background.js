'use strict';

/**
 *  This is the main file for background
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-background').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Background');
}
