'use strict';

/**
 *  This is the main file for marketo-form
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-marketo-form').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Marketoform');
}
