/*
--------------------------------------------------
    Video Player Functionality
--------------------------------------------------
*/

var $ = require('jquery');
var log = require('loglevel');

var youtube = require('./youtube');
var vimeo = require('./vimeo');
var cssmodal = require( '../general-modal/general-modal' );

if ($('.component-video').length) {
    $(function() {

        /*
         * This will immediately create video players for every instance
         * that is on the page.
         *
         * If you, for instance, need a custom thumbnail image, you may want
         * to instead execute the below code on-click or on-modal-open.
         */
        $('.component-video').each(function() {
            var $videoComponent = $(this);

            if ($videoComponent.hasClass('youtube')) {
                var videoParms = youtube.gatherVideoParameters($videoComponent);
                log.debug('Creating YouTube player for ', videoParms);
                youtube.createPlayer(videoParms);

            } else if ($videoComponent.hasClass('vimeo')) {
                var videoParms = youtube.gatherVideoParameters($videoComponent);
                log.debug('Creating Vimeo player for ', videoParms);
                vimeo.createPlayer(videoParms);

            } else {
                log.warn('Missing video provider for ', $videoComponent);
            }
        });
    });
}
