/*
--------------------------------------------------
    YouTube Module
--------------------------------------------------
*/

var Q = require('q');
var log = require('loglevel');

var videoMgr = require('./videoManager');

module.exports = {
    gatherVideoParameters: videoMgr.gatherVideoParameters,
    createPlayer: createPlayer
};

var YOUTUBE_API_URL = "https://www.youtube.com/player_api";
var youtubeReady = Q.defer();

// YouTube API will call this on API being ready.
// https://developers.google.com/youtube/iframe_api_reference#Requirements
window.onYouTubeIframeAPIReady = function() {
    // resolve deferred object. player creation will only happen once this is resolved.
    youtubeReady.resolve();
};

function createPlayer(videoParms) {
    if (!videoParms) return;

    // first check if player has already been created
    if (videoMgr.getPlayer(videoParms)) return;


    // create player once API script load is done AND youtube reports as ready.
    Q.all([loadYoutubeApi(), youtubeReady.promise]).done(function() {
        var player = new YT.Player(videoParms.videoPlayerId, {
            videoId: videoParms.videoId,
            playerApiId: videoParms.videoPlayerId,
            playerVars: {
                autoplay: videoParms.autoplay && videoParms.modal === 'false' ? 1 : 0,
                modestbranding: 1,
                controls: 1,
                showinfo: 0,
                autohide: 1,
                color: 'white'
            },
            events: {
                'onStateChange': onPlayerStateChange,
                'onReady': onPlayerReady
            }
        });


        videoMgr.addPlayer(videoParms, player);
    });
}

function onPlayerStateChange(event) {
    if (event && event.target && event.target.getVideoData) {

        //
        // Below are placeholder for if we need to add analytics hooks.
        //

        if (event.data == 1) {
            // playing
            log.debug('Playing video: ', event.target.getVideoData());
        }
        if (event.data == 2) {
            // paused
            log.debug('Pause video: ', event.target.getVideoData());
        }
        if (event.data == 0) {
            // ended
            log.debug('End video: ', event.target.getVideoData());
        }
    }
}

function onPlayerReady(event) {
    if (event && event.target && event.target.getVideoData) {

        //
        // Below is placeholder for if we need to add analytics hooks.
        //

        log.debug('Video ready: ', event.target.getVideoData());
    }
}

/**
 * Load YouTube API
 *
 * @return promise for ajax response
 */
function loadYoutubeApi() {
    options = {
        dataType: "script",
        cache: true,
        url: YOUTUBE_API_URL
    };
    return  Q(jQuery.ajax(options));
}
