/*
--------------------------------------------------
    Vimeo Module
--------------------------------------------------
*/

var log = require('loglevel');

var videoMgr = require('./videoManager');

module.exports = {
    gatherVideoParameters: videoMgr.gatherVideoParameters,
    createPlayer: createPlayer
};

/**
 * Create the player defined in the videoParms.
 *
 * Once done, getPlayer(videoParms) can be used to get the player.
 */
function createPlayer(videoParms) {
    if (!videoParms) return;

    // first check if player has already been created
    if (videoMgr.getPlayer(videoParms)) return;

    // get Thumbnail for modal cta
    vimeoLoadingThumb(videoParms.videoId);

    var options = {
        id: videoParms.videoId,
        loop: false,
        autoplay: videoParms.autoplay && videoParms.modal === 'false' ? 1 : 0
    };


    var player = new Vimeo.Player(videoParms.videoPlayerId, options);
    attachEvents(player, videoParms);

    videoMgr.addPlayer(videoParms, player);
}

function attachEvents(player, videoParms) {
    //
    // Below are placeholder for if we need to add analytics hooks.
    //

    player.on('play', function(data) {
        // { duration: 61.857 percent: 0 seconds: 0 }
        log.debug('Playing video: ', data, videoParms);
    });

    player.on('pause', function(data) {
        // { duration: 61.857 percent: 0 seconds: 0 }
        log.debug('Pause video: ', data, videoParms);
    });

    player.on('ended', function(data) {
        // { duration: 61.857 percent: 0 seconds: 0 }
        log.debug('Ended video: ', data, videoParms);
    });

    player.on('loaded', function(data) {
        // { id: 76979871 }
        log.debug('Loaded video: ', data, videoParms);
    });
}

function vimeoLoadingThumb(id){
    $.ajax({
        url: 'http://www.vimeo.com/api/v2/video/' + id + '.json',
        dataType: 'jsonp',
        success: function(data)
        {
            thumbnail = data[0].thumbnail_large;
            var id_img = "#vimeo-" + id;
            $(id_img).attr('src',thumbnail);
        }
    });
}

