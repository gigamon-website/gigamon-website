/*
 * Stores the player object instances in case they are needed for
 * further manipulation in the future.
 *
 * This is a helper module, for video functionality, you should generally
 * be using the provider specific modules.
 */

module.exports = {
    gatherVideoParameters: gatherVideoParameters,
    addPlayer: addPlayer,
    getPlayer: getPlayer
};

var PLAYERS = {};

/**
 * Default implenentation that gathers creates the video parameters object
 * based on a jquery video element.
 *
 * @return video parameters or null
 */
function gatherVideoParameters($videoElem) {
    if ($videoElem.hasClass('component-video')) {
        if ($videoElem.length) {
            var videoId = $videoElem.attr('data-video-id');
            var videoPlayerId = $videoElem.attr('data-player-id');
            var autoplay = $videoElem.hasClass('autoplay');
            var modal = $videoElem.attr('data-modal');
            
            if (videoId && videoPlayerId) {
                return {
                    videoId: videoId,
                    videoPlayerId: videoPlayerId,
                    autoplay: autoplay,
                    modal: modal
                };
            }
        }
    }
    return null;
}

/**
 * @param key: the videoPlayerId string or object containing videoPlayerId field
 * @param player: player object
 */
function addPlayer(key, player) {
    var videoPlayerId = (typeof key === 'object') ? key.videoPlayerId : key;

    // Safety check, don't overwrite player
    if (PLAYERS[videoPlayerId]) return;
    PLAYERS[videoPlayerId] = player;
};

/**
 * @param key: the videoPlayerId string or object containing videoPlayerId field
 */
function getPlayer(key) {
    var videoPlayerId = (typeof key === 'object') ? key.videoPlayerId : key;

    return PLAYERS[videoPlayerId];
};
