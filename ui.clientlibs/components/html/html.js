'use strict';

/**
 *  This is the main file for <%= jsModuleName %>
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-html').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Html');
}
