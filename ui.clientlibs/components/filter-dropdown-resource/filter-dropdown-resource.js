'use strict';

/**
 *  This is the main file for filter-dropdown-resource
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-filter-dropdown-resource').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing FilterDropdownResource');
}
