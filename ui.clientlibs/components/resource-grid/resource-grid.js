'use strict';

/**
 *  This is the main file for resource-grid
 */

/* ========================================================================
 * HERO app - Grid Filter
 * 
 * V 1.0.0, April 2016
 * http://www.herodigital.com
 *
 * Copyright 2016, HERO Digital
 * http://www.herodigital.com
 * ======================================================================== */

require( 'datatables' )();


if ($('.component-grid-filter').length) {
    $(function() {
        init();
    });
}

function init(){
 	var eventListLimit = 12;
 	var activeFilters = [];

   	$('.agendaDay').hide();
 	$(document).ready(function(){
 		$('.date-btn').first().addClass('activeDateBtn'); 
 		$('.agendaDay').first().show();
	        $('.agenda-carousel').slick({
	            dots: true,
	            infinite: false,
	            speed: 300,
	            slidesToShow: 1,
	            variableWidth: false,
	            mobileFirst: true,
	            centerMode: true,
	            centerPadding: '0px',
	            adaptiveHeight: true
	        });
            if(window.location.href.split("#")[1]){
                activeFilters=window.location.href.split("#")[1].split("&");
            }
	        filterDisplay();

            $($(".select-dropdown").get().reverse()).each(function( index ) {
               $(this).css('zIndex', index);
            });    

            $(".reseller-content").each(function() {
                $(this).click(
                    function(event) {
                        event.stopImmediatePropagation();
                        event.preventDefault();
                        var url = $(this).attr('href')+"#";
                        for(var i=0;i < activeFilters.length;i++){
                            url=url+activeFilters[i];
                            if(i < activeFilters.length - 1){
                                url=url+"&";
                            }
                        }
                        window.location.href = url;
                    }
                ); 
            });    
            $('.zoomTitle').css('display','-webkit-box');
   	});


    $('.btn-line-back').click(
        function(event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            var url = $(this).attr('href');
            if(window.location.href.split("#")[1]){
                url = $(this).attr('href')+"#"+window.location.href.split("#")[1];
            }
            window.location.href = url;
        }
    ); 

    $('.paramtest').click(
        function(event) {
            var url = window.location.href+"?location/north-america&type/breakfast";    
            window.location.href = url;
        }
    ); 

	$('.date-btn').click(
		function(ev) {
	   		$('.agendaDay').hide();
	   	 	$('.date-btn').removeClass('activeDateBtn');
	   	 	$(this).addClass('activeDateBtn');
			$("#"+$(this).data("daykey")).show();
		}
	); 

	$('.load-more-bar .btn').click(
		function(ev) {
			eventListLimit = eventListLimit + 12;
			filterDisplay();
		}
	); 

	$('.zero-results [data-component-name=cta-button] .btn').click(function(ev) {

		$( ".select-dropdown" ).each(function() {
			$(this).attr('data-selected-value', '');
			$(this).find('span').first().html($(this).attr('data-placeholder-text'));
			$(this).removeClass('value-selected');
		});

		activeFilters=[];
		filterDisplay();
	 });

		// Dropdown
        function SelectDropDown(el) {
            this.dd = el;
            this.placeholder = this.dd.children('span');
            this.opts = this.dd.find('ul.dropdown > li');
            this.val = '';
            this.index = -1;
            this.initEvents();
            this.placeholderText = '';
        }

        SelectDropDown.prototype = {
            initEvents : function() {
                var obj = this;

                obj.placeholder.html(obj.dd.attr('data-placeholder-text'));

                if(window.location.href.split("#")[1]){
                        $(".select-dropdown").each(function(index) {
                            var filterData = $(this).find(".dropdown li a").first().attr("data-value");
                            for(var i=0;i < activeFilters.length;i++){
                                if(filterData.split("/")[filterData.split("/").length - 2] == activeFilters[i].split("/")[filterData.split("/").length - 2]){
                                    var displayStr = activeFilters[i].split("/")[filterData.split("/").length - 1].replace("-", " ");
                                    $(this).find(".filter-label").html("All <strong>"+displayStr+"</strong>");
                                    $(this).attr('data-selected-value', activeFilters[i]);
                                    $(this).addClass('value-selected');
                                }
                            }
                        }); 
                    }
                    
                obj.dd.on('click', 'ul.dropdown', function(e){
                    e.preventDefault();
                    $(this).parent('.select-dropdown').toggleClass('active');
                    return false;
                });

                obj.dd.on('click', '.open-dropdown', function(e){
                    e.preventDefault();
                    closeAllMenu();
                    $(this).parent('.select-dropdown').toggleClass('active');
                    return false;
                });

                obj.dd.on('click', '.close-dropdown', function(e){
                    e.preventDefault();
                    $(this).parent('.select-dropdown').toggleClass('active');
                    return false;
                });

                obj.dd.on('click', '.reset-value', function(e){
                    e.preventDefault();
                    $(this).parent('.select-dropdown').removeClass('value-selected');

                    obj.opts.removeClass('selected');
                    obj.placeholder.html(obj.dd.attr('data-placeholder-text'));
                    var filterToClear = $(this).parent('.select-dropdown').attr('data-selected-value');
                    activeFilters = [];
		    		var myHash ="";
					$( ".select-dropdown" ).each(function() {
		    			if($(this).attr('data-selected-value') != filterToClear && $(this).attr('data-selected-value') != ""){
		    				activeFilters.push($(this).attr('data-selected-value'));
                            myHash=myHash+$(this).attr('data-selected-value')+"&";
		    			}
					});
                    obj.dd.attr('data-selected-value', '');
                    window.location.hash = myHash;
					filterDisplay();
                    return false;
                });

		        obj.opts.on('click',function(){
		            var opt = $(this);
		            obj.val = opt.text();
		            obj.index = opt.index();
					obj.placeholder.html("All <strong>"+obj.val+"</strong>");
		           	$(obj.dd).attr('data-selected-value', $(opt).find('a').data('value'));
                    $(obj.dd).addClass('value-selected');
					//declare an empty new array
					activeFilters=[];
                    var myHash="";
					//loop over all the input labels
					$( ".select-dropdown" ).each(function() {
						if($(this).attr('data-selected-value') != ""){
							//populate our active filters with the data attribute filters found
							activeFilters.push($(this).attr('data-selected-value'));
                            myHash=myHash+$(this).attr('data-selected-value')+"&";
						}
					});
                    window.location.hash = myHash;
					filterDisplay();
		        });
            },
            getValue : function() {
                return this.attr('data-value');
            },
            getIndex : function() {
                return this.index;
            }
        }

	$(function() {
		var dropDowns = [];
        $('.select-dropdown').each( function(){
            var filterDropDown = new SelectDropDown( $(this) );
        });

		$(document).click(function() {
			closeAllMenu();
		});		
	});

    function filterDisplay(){
    	closeAllMenu();
        $(".featured-primary, .featured-secondary").removeClass("not-filtered");
        if(activeFilters.length < 1 || activeFilters[0] == "/"){
            $(".featured-primary, .featured-secondary").addClass("not-filtered");
        }

        var showIndex = 0;
		$(".data-table-row, [class*='-results-cell']").hide();

    	$("[class*='-results-cell'], .data-table-row").each(function(){
    		var match = 0;
    		for(var i = 0; i < activeFilters.length; i++){
    			if($(this).attr('data-tags') && $(this).attr('data-tags').indexOf(activeFilters[i]) > -1){
    				match++;
    			}
    		}
    		if((match == activeFilters.length) && ((showIndex) < (eventListLimit))){
    			$(this).show();
    			showIndex++;
    		}
			
			
			
    	});
    	showIndex = 0;
		var rcount =0;
    	$("[class*='-results-cell']").each(function(){
    		var match = 0;
    		for(var i = 0; i < activeFilters.length; i++){
    			if($(this).attr('data-tags') && $(this).attr('data-tags').indexOf(activeFilters[i]) > -1){
    				match++;
    			}
    		}
    		if((match == activeFilters.length) && ((showIndex) < (eventListLimit))){
    			$(this).show();
    			showIndex++;
    		}
			
			 if((match == activeFilters.length)){
              rcount++;
            }
    	});
    	if(showIndex == 0){
    		$('.zero-results').show();
    	}else{
    		$('.zero-results').hide();
    	}
		if(eventListLimit < ($("[class*='-results-cell']").length)&&eventListLimit < rcount){
			$('.load-more-bar').show();
		}else{
			$('.load-more-bar').hide();
		}
		 //$('span.count').text(rcount);
		  if(rcount==1)
            $('h2.count').text(rcount+' Resource');
          else
		    $('h2.count').text(rcount+' Resources');
		 
        setTimeout(closeAllMenu, 100);
    }	

 	function closeAllMenu(e){
 		$('.select-dropdown').removeClass('active');
 	}

 };
