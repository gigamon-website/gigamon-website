'use strict';

/**
 *  This is the main file for cta
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-cta').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Cta');
}
