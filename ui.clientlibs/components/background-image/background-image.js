'use strict';

/**
 *  This is the main file for background-image
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-background-image').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing BackgroundImage');
}
