'use strict';

/**
 *  This is the main file for responsive-layout
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-responsive-layout').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Responsive Layout');
}
