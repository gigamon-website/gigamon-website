'use strict';

/**
 *  This is the main file for global-navigation
 */

var $ = require('jquery');
var log = require('loglevel');

if ($('.component-global-navigation').length) {
    $(function() {
        init();
    });
}


//function to inject functionality to the top of a node module function, for pushMenu
$.fn.bindFirst = function(name, fn) {
    // bind as you normally would
    this.on(name, fn);
    // namespaced events too.
    this.each(function() {
        var handlers = $._data(this, 'events')[name.split('.')[0]];
        // take out the handler we just inserted from the end
        var handler = handlers.pop();
        // move it at the beginning
        handlers.splice(0, 0, handler);
    });
};

$( window ).resize(function() {
    //resize mobile menu for smaller screen sizes
    if($('.mp-pushed').length < 1){
        if($(window).width() > 380){
          $('#mp-menu').width(360);
        }else{
          $('#mp-menu').width($(window).width() - 40);
        }
    }
});

// Select language options menu
$('body').on('click','.lang-options',function(){
  $(this).parent('li').find('.option-nav').toggle();
  $(this).attr('aria-expanded', $(this).attr('aria-expanded') == 'false' ? 'true' : 'false');
});

$(document).on('click', function(event){
  if (!$(event.target).closest('.lang-options,.option-nav').length) {
    $('.option-nav').hide();
  }
});

// Toggle second tier mobile nav
$('.toggle-menu').on('touchend, click',function(){
  $(this).parent('li').find('.teir-options').slideToggle();
  $(this).toggleClass('expanded');
  $(this).attr('aria-expanded', $(this).attr('aria-expanded') == 'false' ? 'true' : 'false');
});


//prevent mobile device touch scrolling on everything except the mobile menu when it is open
$(document).on('touchmove',function(e){
    if($('.mp-pushed').length){
        if (!$(e.target).parents('#mp-menu')[0]) {
            e.preventDefault();
        }
    }
});

function init() {
    console.log('Initializing GlobalNavigation');

    $(window).scroll(function() {
		if ($(this).scrollTop() > 200) {
		$('.global-navigation').addClass('shrink');
		}
		else if ($(this).scrollTop() < 200) {
			$('.global-navigation').removeClass('shrink');
			}
		});

    if($(window).width() > 380){
      $('#mp-menu').width(360);
    }else{
      $('#mp-menu').width($(window).width() - 40);
    }

    $('#mp-menu').pushMenu({
        type: 'overlap',
        levelSpacing: 0,
        backClass: 'mp-back',
        trigger: '.mobile-trigger',
        pusher: '.site-wrapper',
        scrollTop: false
    });


  //Force correct ordering and display for push menu slide navigation custimization
  $(".mp-back").bindFirst('click', function() {
    if($(this).parent().hasClass("mp-level-open")){
      $(this).parent().addClass("slide-priority");
      var $currentMenu = $(this).parent();
      setTimeout(function(){
          $currentMenu.removeClass("slide-priority");
      },500);
    }
  });

  // Safari, in Private Browsing Mode, all calls to setItem throw QuotaExceededError.
  if (typeof localStorage === 'object') {
      try {
        if(window.sessionStorage.getItem('animateLogo') === 'false') {
          $("#logo-ani").removeClass('hoverPlay').addClass('stoppedAnimation');
        } else {
          $("#logo-ani").removeClass('stoppedAnimation').addClass('hoverPlay');
          window.sessionStorage.setItem('animateLogo', 'false');
        }
      } catch (e) {
          console.log('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.');
      }
  }

  $("#hero-logo-container").hover(
    function() {
        $("#hero-logo-ani").removeClass('stoppedAnimation').addClass('hoverPlay');
    }, function() {
        $("#hero-logo-ani").removeClass('hoverPlay').addClass('stoppedAnimation');
    }
  );

  $('#navbar-menu-toggle').click(function() {
    $(".mobile-menu").toggleClass("reveal");
  });

  /* Search feature*/
  $('#searchInput').on('keyup',function(event){
    if(event.keyCode == 13){
      var query = $(this).val(),
      q = query.replace(/ /g, '+');
      var searchResultsURL = $('#searchInput').data('path');
      window.location.href = searchResultsURL+'.html?q='+q;
    }
  });

  $('body').on('click','.search-trigger-mobile',function(event){
    $(this).toggleClass('active');
    $('.search-bar-mobile').toggle();
    $('.search-bar-mobile').find("input").focus();
  });

  $('body').on('click','.mobile-search-trigger',function(event){
    var searchQuery = $('#searchInputMobile').val(),
    q = searchQuery.replace(/ /g, '+');
    var searchResultsURL = $('#searchInputMobile').data('path');
    window.location.href = searchResultsURL+'.html?q='+q;
  });

  $('#searchInputMobile').on('input', function() {
      var searchResultsURL = $('#searchInput').data('path');
      $("#searchInputMobileForm").attr("action", searchResultsURL+'.html?q='+$('#searchInputMobile').val());
  });

  $('.search-trigger').click(function() {
  // Commented by Srinivas - As the Search text Input should always open with out any aditional click.
    /*if($('.search-bar').hasClass('reveal')){
      var query = $('#searchInput').val(),
      q = query.replace(/ /g, '+');
      var searchResultsURL = $('#searchInput').data('path');
      var search = searchResultsURL+'.html?q='+q;
      window.location.href = search;
    } else {
      $('.search-bar').addClass('reveal');
      $('.search-container, .search-results').removeClass('active');
      $('.search-bar').find('input').focus();
    }*/
    // if we want to roll back this change then please comment the below section and un comment the above the section.
          $('.search-bar').addClass('reveal');
          $('.search-container, .search-results').removeClass('active');
          $('.search-bar').find('input').focus();
          var query = $('#searchInput').val(),
                q = query.replace(/ /g, '+');
                var searchResultsURL = $('#searchInput').data('path');
                var search = searchResultsURL+'.html?q='+q;
                window.location.href = search;
  });


  var menuIndex = 0, menuIndexOld;
  var touchPrevious;

  var opts = {
    timeout: 500,
    sensitivity: 7,
    interval: 50
  };


  $(".desktop-nav-top>a").hoverIntent( function(event) {
    menuIndex = $(this).attr("data-index");
    if($('.desktop-nav-top a').hasClass('active')){
      hideNav(touchPrevious);
    }
    showNav(menuIndex);
  }, function(event) {
    $("body").on("mouseover", function(event) {
      if(!$(event.target).closest('.desktop-nav-top>a,.nav-dropdown,.component-sub-menu-wrapper').length) {
        hideNav(touchPrevious);
      }
    });
  });


  // Touch device on desktop view meganav
  $(".desktop-nav-top>a").on("touchstart", function(e) {
    e.stopPropagation();
    e.preventDefault();
    if(!$(this).hasClass('active')){
      if($('.desktop-nav-top a').hasClass('active')){
        hideNav(touchPrevious);
      }
      menuIndex = $(this).attr("data-index");
      showNav(menuIndex);
    } else {
      hideNav(touchPrevious);
    }
  });

  // Show meganav
  function showNav(menuIndex){
    menuIndexOld = menuIndex;
    $("a[data-index="+menuIndex+"]").addClass("active");
    $(".nav-dropdown[data-index="+menuIndex+"]").css({'zIndex':9});
    $(".component-sub-menu-wrapper[data-index="+menuIndex+"] .nav-dropdown > .container").slideDown();
    touchPrevious = menuIndex;
  }

  // Hide meganav
  function hideNav(touchPrevious){
    $("a[data-index="+touchPrevious+"]").removeClass("active");
    $(".nav-dropdown[data-index="+touchPrevious+"]").css("zIndex",0);
    $(".component-sub-menu-wrapper[data-index="+touchPrevious+"]  .nav-dropdown > .container").slideUp();
    menuIndexOld = null;
  }

  $(document).keyup(function(e) {
      //ESCAPE KEY
      if (e.keyCode === 27){
        if($('.search-bar').hasClass("reveal")){
          $('.search-bar').removeClass("reveal");
          $('.search-bar').find("input").val("");
        }
        $(".search-container, .search-results").removeClass("active");
        $("body").removeClass("fixed");
      }
      //TAB KEY
      if (e.keyCode === 9) {
        $(':focus').addClass("accessibility-focus");
        $(".nav-dropdown").removeClass("active");
        $(".nav-background").removeClass("active");
        $("*[data-nav-trigger]").removeClass("active");
        if($('.component-sub-menu-block a:focus').length){
          $(".nav-dropdown").css("zIndex",0);
          $(".nav-background").css("zIndex",0);
          var menuIndex = $('.component-sub-menu-block a:focus').closest('.nav-dropdown').attr('data-index');
          $(".nav-dropdown[data-index="+menuIndex+"]").css("zIndex",2);
          $(".nav-background[data-index="+menuIndex+"]").css("zIndex",1);
          showNav(menuIndex);
        }
      }
  });
}
