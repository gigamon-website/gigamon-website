'use strict';

/**
 *  This is the main file for general-modal
 */

var $ = require('jquery');
var log = require('loglevel');
var cssmodal = require( '../../node_modules/css-modal/modal.js' );

// module.exports = [publish public functions here];

if ($('.component-general-modal').length) {
    $(function() {
        generalModal();
    });
}

var generalModal = function () {

    var xPos;

	$('.component-general-modal').each(function() {
        $(this).insertAfter($('.site-content'));
    });

    $(".general-modal").click(function (event) {
        var hash = event.currentTarget.getAttribute('href').replace('#', '');
        var modalElement = document.getElementById(hash);
        if (modalElement) {
            var $modalElement = $(modalElement);
            if ($modalElement.hasClass("component-general-modal")) {
                cssmodal.mainHandler(event, true);
            }
        }
    });

    $(document).on('cssmodal:show', function (event, modalParms) {
        var $target = $(modalParms.detail.modal);
        if ($target.hasClass('component-general-modal')) {

            $(".site-wrapper").addClass("general-modal-open");

            xPos = $('body').scrollTop();
            // fix background
            
        }
    });

    $(document).on('cssmodal:hide', function (event, modalParms) {
        var $target = $(modalParms.detail.modal);
        if ($target.hasClass('component-general-modal')) {

            $(".site-wrapper").removeClass("general-modal-open");

            // fix background
            $('body').css({
                  overflow: '',
                  position: '',
                  top: ''
            });

            if($target.find('iframe')){
                $($target.find('iframe')).attr("src", $($target.find('iframe')).attr("src"));
            }

        }
    });

    var resizeWindow = function () {
        var siteHeader = $('.site-header');
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
        var modalWindow = $('.component-general-modal');
        modalWindow.each(function (index) {
            var innerModal = $(this).find('.modal-inner');

            if ($(window).width() >= 463) {
                var innerH = h - 250;
                log.debug('innerModal h ' + h + ' innerH ' + innerH);
                // innerModal.css('max-height', innerH + 'px');
            }
            else {
                log.debug('width' + $(window).width());
                // innerModal.css('max-height', '');
            }
        });
    };

    var modalWindow = $('.component-general-modal');
    if (modalWindow.length) {
        resizeWindow();
        $(window).resize(function () {
            resizeWindow();
        });
    }
};
