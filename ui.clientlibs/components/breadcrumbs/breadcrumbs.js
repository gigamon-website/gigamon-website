'use strict';

/**
 *  This is the main file for breadcrumbs
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-breadcrumbs').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing Breadcrumbs');
}
