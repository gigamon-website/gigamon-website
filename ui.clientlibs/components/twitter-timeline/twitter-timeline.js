'use strict';

/**
 *  This is the main file for twitter-timeline
 */

var log = require('loglevel');

if ($('.component-twitter-timeline').length) {
    loadTwitter();
}

function loadTwitter() {
    /* Twitter function, only loads if the page loads including twitter component. */
    !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");
    log.debug( "twitter loaded." );
}
