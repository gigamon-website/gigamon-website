'use strict';

/**
 *  This is the main file for resource-card
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-resource-card').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing ResourceCard');
    
    
    $('.resource-card:has(.border)').css('border', '1px solid #ddd'); 
}
