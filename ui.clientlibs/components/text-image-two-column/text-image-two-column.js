'use strict';

/**
 *  This is the main file for text-image-two-column
 */

var $ = require('jquery');
var log = require('loglevel');

// module.exports = [publish public functions here];

if ($('.component-text-image-two-column').length) {
    $(function() {
        init();
    });
}

function init() {
    log.trace('Initializing TextImageTwoColumn');
}
