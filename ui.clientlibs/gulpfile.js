'use strict';

var gulp = require('gulp');

var browserify = require('./build-tasks/browserify');
var sass = require('./build-tasks/sass');
var copyFonts = require('./build-tasks/copy-fonts');
var watchDev = require('./build-tasks/watch-dev.js');
var watchUiApps = require('./build-tasks/watch-uiapps.js');
var pullUiContent = require('./build-tasks/pull-uicontent.js');

/*
 * Helper gulp tasks.
 *
 * You will likely never invoke these directly.
 */
gulp.task( 'browserify', browserify );
gulp.task( 'sass', sass );
gulp.task( 'copyFonts', copyFonts );
gulp.task( 'watchUiAppsAuth', watchUiApps.auth);
gulp.task( 'watchUiAppsPub', watchUiApps.pub);

/**
 * Standard watch that does NOT publish to localhost.
 */
gulp.task('watch', watchDev);

/**
 * Watch that also deploys all changed frontend files to your localhost
 * 4502 AEM server.
 *
 * This includes HTML (HTL), JavaScript, and Sass.
 */
gulp.task('watchPush', ['watch', 'watchUiAppsAuth']);

/**
 * Watch that also deploys all changed frontend files to your localhost
 * 4502 and 4503 (author and publish) AEM server.
 *
 * This includes HTML (HTL), JavaScript, and Sass.
 */
gulp.task('watchPush2', ['watch', 'watchUiAppsAuth', 'watchUiAppsPub']);

/**
 * Pulls down the sample content from an AEM server (by default localhost:4502)
 *
 * Should be used when syncing ui.content from localhost (or any other server)
 * into this repo.
 *
 * Accepts the following optional parameters:
 * --images Include dam assets (by default only includes page content)
 * -u username
 * -p password
 * -s server (ex. http://dev-server:4502)
 * -x package path (ex. /etc/packages/gigamon/hero-demo.ui.content-6.2.0-SNAPSHOT.zip)
 */
gulp.task('pullUiContent', pullUiContent);

/**
 * Main build task.
 *
 * - Builds Sass and JavaScript into AEM client library.
 * - Copies font files to AEM client library.
 */
gulp.task('build',   ['sass', 'browserify', 'copyFonts']);
gulp.task('default', ['build']);
