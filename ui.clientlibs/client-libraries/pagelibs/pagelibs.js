/**
 *  This is the main file for pagelibs
 */

// Logging setup
require('./js/logsetup.js');

// Custom Base Scripts
require('./js/custom-polyfills.js');

// Bootstrap JS
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/transition');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/dropdown');
require('../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse');

// slick carousel
require('../../node_modules/slick-carousel/slick/slick');

// GSAP Animation
require('../../node_modules/gsap/TweenMax');
require('../../node_modules/gsap/TimelineMax');

// Component Scripts
require('./pagelibs.components');

// DataTables jQuery Plugin
require('../../node_modules/datatables/media/js/jquery.dataTables');

// Push Menu
// require('../../node_modules/push-menu/dist/js/jquery.pushMenu.min');
require('../vendor/jquery.pushMenu');

// Analytics
require('./js/digitaldata.js');

// Modal
require('../../node_modules/css-modal/modal.js').init();

// Hover Intent 
require('../vendor/jquery.hoverIntent');

