(function() {
	
	$(function() {
		initialize();
	});
	
	//------------------------------------
	// STARTERS
	//------------------------------------
	var initialize = function() {
		// TODO: test that satellite/DTM scripts loaded?
		attachAdditionalPageLoadVars();
		setUpEvents();
	}
	
	//------------------------------------
	// PAGE LOAD
	//------------------------------------
	var attachAdditionalPageLoadVars = function() {
		attachInternalSearchVars();
	}
	
	var attachInternalSearchVars = function() {
		if($('section.component-search').length) {
			var numResults = $('section.component-search').data('result-count');
			
			digitalData.page.pageInfo.onsiteSearchResults = numResults;
			
			_satellite.track('search results page load');
		}
	}
	
	//------------------------------------
	// EVENT ATTACHMENT
	//------------------------------------
	var setUpEvents = function() {
		attachMarketoEvents();
	}
	
	var attachMarketoEvents = function() {
		if(typeof MktoForms2 !== 'undefined' && typeof MktoForms2.whenReady !== 'undefined') {
			MktoForms2.whenReady(function(form) {
				
				form.onSuccess(function(values, followUpUrl) {
					var formId = form.getId();
					
					var formType = formId;
					var formName = document.title;
					var transactionId = 'mktoForm_' + formId + '_' + guid();
					
					digitalData.form = {};
					digitalData.form.appName = formType + ':' + formName;
					digitalData.form.transactionId = transactionId;
					
					_satellite.track('form submit');
					
					return true;
				});
				
			});
		}
	}
	
	//------------------------------------
	// UTILITY
	//------------------------------------
	/*
	 * Generates a random guid
	 * http://stackoverflow.com/a/2117523
	 */
	function guid() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
			return v.toString(16);
		});
	}
})();
