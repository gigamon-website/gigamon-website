'use strict';

var log = require('loglevel');
var queryString = require('query-string');

var parms = queryString.parse(location.search);

if (parms.trace) {
    log.setLevel('trace');

} else if (parms.debug) {
    log.setLevel('debug');

} else if (parms.info) {
    log.setLevel('info');

} else if (parms.warn) {
    log.setLevel('warn');

} else if (parms.error) {
    log.setLevel('error');
    
} else if (parms.silent) {
    log.setLevel('silent');
}
