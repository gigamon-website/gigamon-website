var gulp = require('gulp');
var path = require('path');
var rp = require('request-promise');
var fs = require('fs-extra');
var gutil = require('gulp-util');
var chalk = require('chalk');

function watchDev(port) {

  var watcher = gulp.watch([
    '../ui.apps/src/main/content/jcr_root/**/*.{html,js,less,css,jsp,txt}',
    '../ui.author-widgets/src/main/content/jcr_root/**/*.{html,js,less,css,jsp,txt}',
    'src/main/content/jcr_root/**/*.{html,js,less,css,jsp,txt}'
  ]);

  watcher.on('change', function(event) {
    if (event.type == 'changed') {
      var idx = event.path.indexOfEnd('/jcr_root/');
      var aemPath = event.path.substr(idx);
      var filename = path.basename(aemPath);
      var dirname = path.dirname(aemPath);
      var formData = {};
      formData[filename] = fs.createReadStream(event.path);

      rp({
        method: 'POST',
        url: "http://localhost:" + port + "/" + dirname,
        formData: formData,
        headers: {
          'Authorization': 'Basic ' + new Buffer('admin:admin').toString('base64')
        }
      }).then(function(resp) {
        if (resp) {
          gutil.log('Pushed: ' + chalk.cyan(port + ', ' + event.path));
        } else {
          throw 'Unexpected result pushing ' + event.path;
        }
      }).catch(function(e) {
        gutil.log(chalk.red('Error: ' + e));
      });
    }
  });
}

function watchDevAuth() {
  return watchDev(4502);
}

function watchDevPub() {
  return watchDev(4503);
}

String.prototype.indexOfEnd = function(string) {
  var io = this.indexOf(string);
  return io == -1 ? -1 : io + string.length;
}

exports.auth = watchDevAuth;
exports.pub = watchDevPub;
