var gulp = require( 'gulp' );

function moveFontsToCq() {
    // font awesome fonts
    gulp.src( ["node_modules/font-awesome/fonts/**/**"] )
        .pipe( gulp.dest('src/main/content/jcr_root/etc/clientlibs/gigamon-pagelibs/fonts/fontawesome') );
}

module.exports = moveFontsToCq;
