var gulp = require( 'gulp' );

function watchDev() {

    // Production files
    var cssToWatch = [
        "components/**/styles/**/**.scss",
        "client-libraries/**/styles/**/**.scss",
        "client-libraries/vendor/**/**.scss"
    ]

    var jsToWatch = [
        "components/**/**.js",
        "client-libraries/**/**.js"
    ];

    gulp.watch( cssToWatch, ['sass'] );
    gulp.watch( jsToWatch, ['browserify'] );

}

module.exports = watchDev;
