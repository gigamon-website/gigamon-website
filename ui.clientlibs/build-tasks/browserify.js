var gulp                = require( 'gulp' );
var browserify          = require( 'browserify' );
var source              = require( 'vinyl-source-stream' );
var buffer              = require( 'vinyl-buffer' );
var rename              = require( 'gulp-rename' );
var eol                 = require( 'gulp-line-ending-corrector' );
var strip               = require( 'gulp-strip-comments' );

function browserifyTask() {
    browserify('client-libraries/pagelibs/pagelibs.js')
        // https://github.com/thlorenz/browserify-shim/issues/191
        .transform('browserify-shim', {global: true})
        .bundle()
        //Pass desired output filename to vinyl-source-stream
        .pipe( source('pagelibs.bundle.js') )
        .pipe( buffer() )
        .pipe( strip() )
        .pipe( eol({eolc: 'LF', encoding: 'utf8'}) )
        .pipe( gulp.dest('src/main/content/jcr_root/etc/clientlibs/gigamon-pagelibs/js') );
}

module.exports = browserifyTask;
