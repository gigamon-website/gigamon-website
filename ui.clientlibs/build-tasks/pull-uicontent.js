'use strict';

/*
 * Gulp task that rebuilds the ui.content package, downloads it, then copies
 * the content into the ui.content module.
 *
 * Useful for syncing ui.content updates made on localhost (or any other server)
 * back into this project's git repository.
 */

var request = require('request');
var rp = require('request-promise');
var Promise = require("bluebird");
var read = require('read-file');
var xpath = require('xpath');
var dom = require('xmldom').DOMParser;
var path = require('path');
var util = require('util');
var fs = require('fs-extra');
var gutil = require('gulp-util');
var chalk = require('chalk');
var AdmZip = require('adm-zip');

var argv = require('yargs')
  .default('images', false)
  .default('u', 'admin') // user
  .default('p', 'admin') // password
  .default('s', 'http://localhost:4502') // server
  .default('x', getUiContentPackagePath()) // path to ui.content package
  .argv;

module.exports = function pullUiContent(cb) {
  cleanFunc()
  .then(buildFunc)
  .then(downloadFunc)
  .then(unzipFunc)
  .then(copyFunc)
  .catch(function(e) {
    gutil.log(chalk.red('Error: ' + e));
  })
  .finally(function() {
    cb();
  });
}

var cleanFunc = function() {
  return new Promise(function(resolve, reject) {
    try {
      fs.emptyDirSync('target');
      gutil.log('Clean: ' + chalk.cyan('target'));

      resolve('done');
    } catch (e) {
      reject(e);
    }
  });
}

function buildFunc() {
  gutil.log('Building: ' + chalk.cyan(argv.x));
  return rp({
    method: 'POST',
    uri: argv.s + '/crx/packmgr/service/.json' + argv.x + '?cmd=build',
    headers: {
      'Authorization': 'Basic ' + new Buffer(argv.u + ':' + argv.p).toString('base64')
    }
  });
};

function downloadFunc() {
  return new Promise(function(resolve, reject) {
    gutil.log('Downloading: ' + chalk.cyan(argv.s + argv.x));
    try {
      request({
        method: 'GET',
        uri: argv.s + argv.x,
        headers: {
          'Authorization': 'Basic ' + new Buffer(argv.u + ':' + argv.p).toString('base64')
        }
      })
      .pipe(fs.createWriteStream('target/ui.content.zip'))
      .on('close', function() {
        resolve('target/ui.content.zip');
      });
    } catch (e) {
      reject(e);
    }
  });
}

function unzipFunc() {
  return new Promise(function(resolve, reject) {
    gutil.log('Unzipping: ' + chalk.cyan('target/ui.content.zip'));
    try {
      var zip = new AdmZip('./target/ui.content.zip');
      zip.extractAllTo("./target/ui.content", true);
      resolve('done');
    } catch(e) {
      reject(e);
    }
  });
}

function copyFunc() {
  return new Promise(function(resolve, reject) {
    var sourcePages = './target/ui.content/jcr_root/content/gigamon-components';
    var targetPages = '../ui.content/src/main/content/jcr_root/content/gigamon-components';

    var sourceDam = './target/ui.content/jcr_root/content/dam/gigamon-components';
    var targetDam = '../ui.content/src/main/content/jcr_root/content/dam/gigamon-components';

    try {
      fs.copySync(sourcePages, targetPages, {clobber: true});
      gutil.log('Copied: ' + chalk.cyan(sourcePages) + ' -> ' + chalk.cyan(targetPages));

      if (argv.images) {
        fs.copySync(sourceDam, targetDam, {clobber: true});
        gutil.log('Copied: ' + chalk.cyan(sourceDam) + ' -> ' + chalk.cyan(targetDam));
      }

      resolve('done');
    } catch (e) {
      reject(e);
    }
  });
}

function getUiContentPackagePath() {
  var pom = read.sync(path.join('..', 'ui.content/pom.xml'));
  var doc = new dom().parseFromString(pom.toString());
  var select = xpath.useNamespaces({'pom': 'http://maven.apache.org/POM/4.0.0'});
  var artifactId = select('/pom:project/pom:artifactId/text()', doc)[0] + '';
  var pomVersion = select('/pom:project/pom:version/text()', doc)[0];
  var parentVersion = select('/pom:project/pom:parent/pom:version/text()', doc)[0];
  var version = pomVersion || parentVersion;
  var group = select("//pom:plugin[pom:artifactId='content-package-maven-plugin']/pom:configuration/pom:group/text()", doc)[0];

  return '/etc/packages/' + group + '/' + artifactId + '-' + version + '.zip';
}
