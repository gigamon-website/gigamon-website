# Gigamon Project - UI Clientlibs

1. [Overview](#1-overview)
2. [Environment Setup](#2-envrionment-setup)
3. [Gulp.js Tasks](#2-gulpjs-tasks)
4. [Adding a New Component](#3-adding-a-new-component)
5. [What Does the Build Do?](#31-what-does-the-build-do)
6. [Further Reading](#5-further-reading)

## 1. Overview

This directory contains the AEM client libraries used by this
project.

It is hybrid gulp.js / Maven project.

This hybrid setup allows us to take advantage of best practices in
JavaScript and CSS (Sass) development while still having the end result
be a package (zip) containing AEM client libraries.

Features this gulp managed module allows for:

* Automatically push CSS, JS and HTML changes to your localhost:4502 instance
* Robust JavaScript module system ([browserify](http://browserify.org/))
* Sass CSS language ([sass](http://sass-lang.com/))
* Automatically add vendor specific CSS prefixes ([autoprefixer](https://github.com/postcss/autoprefixer))
* Automatically strip out comments from JS and CSS ([gulp-strip-comments](https://www.npmjs.com/package/gulp-strip-comments))
* Automatically split CSS files suitably for IE < 10 ([gulp-bless](https://www.npmjs.com/package/gulp-bless))*
* Automatically style check JavaScript (lint) ([gulp-eslint](https://github.com/adametry/gulp-eslint))*
* Automatically create SVG sprite files ([svg-sprite](https://www.npmjs.com/package/svg-sprite))*

Must be configured if needed*

## 2. Environment Setup

It is highly recommended that you manage your node/npm installations with
the [Node Version Manager](https://github.com/creationix/nvm).

Refer to *ui.clientlibs/pom.xml* for the version of Node this project requires.

Install gulp:

    npm install -g gulp

Install all npm dependencies:

    cd ./ui.clientlibs
    npm install

If you will be creating a new component you will need our bundled
[Yeoman](http://yeoman.io) generator.

    npm install -g yo
    cd ./generator-gigamon-component
    npm link

## 3. Gulp.js Tasks

For a full list of gulp tasks that can be run check the `gulpfile.js` file.

Maven build project (same as gulp build):

    mvn clean install

Gulp build project (same as maven build):

    gulp build

Gulp watch:

    gulp watch

Gulp watch and deploy changes to localhost:

    gulp watchPush

Pull ui.content from localhost:

    gulp pullUiContent

### 4. Adding a New Component

This project uses [Yeoman](http://yeoman.io/), to automate the process of
creating a new component's starter Java, HTL (markup), Sass, and JavaScript.

Please read the *generator-gigamon-component/README.md* for instructions.

### 5. What Does the Maven/Gulp Build Do?

At a high level, building this module performs the following:

1. Compile all Sass files into a single `pagelibs.bundle.css`
2. Compile all JavaScript files into a single `pagelibs.bundle.js`
3. Build the AEM package (zip) containing the client libraries.

Refer to the *gulpfile.js* file and *build-tasks* for more information.

## 6. Further Reading
* [Adobe Documentation: AEM Client-Side Libraries](https://docs.adobe.com/docs/en/aem/6-1/develop/the-basics/clientlibs.html)
* [Yeoman](http://yeoman.io/)
