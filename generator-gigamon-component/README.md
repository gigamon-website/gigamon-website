# generator-gigamon-component

This is a Yeoman generator that creates starter source files for new AEM
components.

Files created by this generator are tightly coupled with the development
practices and patterns of the individual project. The template files may
be changed based on the project needs.

Because the template files may change based on the needs of the project,
this generator is installed and run in an atypical fashion
(as the rest of this document outlines).

## Installation / Running

It is highly recommended that you manage your node/npm installations with
the [Node Version Manager](https://github.com/creationix/nvm).

Refer to *ui.clientlibs/pom.xml*  for the version of Node this project requires.

Once you have node and npm installed, install [Yeoman](http://yeoman.io).

    npm install -g yo
    cd [project-root]/generator-gigamon-component
    npm link

Then you can run the generator to create a new starter component:

    cd [project-root]
    yo gigamon-component

The process will look something like this:

```
~/Documents/cq-workspace/[project-root] yo gigamon-component

     _-----_     ╭──────────────────────────╮
    |       |    │ Welcome to the exquisite │
    |--(o)--|    │    gigamon-component    |
   `---------´   │        generator!        │
    ( _´U`_ )    │  I create stub files for │
    /___A___\   /│    new AEM components.   │
     |  ~  |     ╰──────────────────────────╯
   __'.___.'__
 ´   `  |° ´ Y `

? What is the name of the component (lowerCamelCase)? testComponent
? What is the name of the component (with-dashes-all-lowercase)? test-component
Will use the the following configurations:
folderName:		test-component
sassName:		test-component
sassFileName:		test-component
jsModuleName:		test-component
jsObjectName:		TestComponent
jsFileName:		test-component
htlName:		TestComponent
htlTemplateName:	renderTestComponent
htlPath:		ui.apps/src/main/content/jcr_root/apps/gigamon/components/content/test-component
javaName:		TestComponent
javaPackage:		gigamon.core.models.component
javaPath:		core/src/main/java/com/hero/demo/core/models/component
? Does this look correct? Yes
Updating ui.clientlibs/client-libraries/pagelibs/pagelibs.components.js
Updating ui.clientlibs/client-libraries/pagelibs/styles/pagelibs.components.scss
   create ui.clientlibs/components/test-component/styles/test-component.scss
   create ui.clientlibs/components/test-component/test-component.js
   create ui.clientlibs/components/test-component/package.json
   create core/src/main/java/gigamon/core/models/component/TestComponent.java
   create ui.apps/src/main/content/jcr_root/apps/gigamon/components/content/test-component/TestComponent.html
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

 © [Hero Digital](http://herodigital.com)
