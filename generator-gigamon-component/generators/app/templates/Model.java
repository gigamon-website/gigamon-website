package <%= javaPackage %>;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * <%= javaName %> Component
 */
@Component(value = "<%= javaName %>",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/<%= htlName %>.html",
        tabs = {@Tab(title = "<%= javaName %>")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class <%= javaName %> extends ComponentSlingModel {

    @DialogField(fieldLabel = "Dummy Value", name = "./dummyValue", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String dummyValue;

    public String getDummyValue() {
        return dummyValue;
    }

}
