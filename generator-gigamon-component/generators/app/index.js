'use strict';

/**
 * Yeoman generator that creates all starter files for new AEM components.
 *
 * This includes:
 * - Java Model
 * - HTL/Sightly File
 * - Browserified JavaScript file
 * - Sass file
 * - Wiring up JavaScript and Sass
 */

var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var path = require('path');
var wiring = require('html-wiring');
var pathExists = require('path-exists');
var find = require('find');

var jsModuleIndexFilePath = 'ui.clientlibs/client-libraries/pagelibs/pagelibs.components.js';
var sassIndexFilePath = 'ui.clientlibs/client-libraries/pagelibs/styles/pagelibs.components.scss';

module.exports = yeoman.Base.extend({

  initializing: function() {
    if (!pathExists.sync(jsModuleIndexFilePath)) {
      this.env.error('ERROR: Are you running this command from the right location? Could not find ' + jsModuleIndexFilePath);
    }
    if (!pathExists.sync(sassIndexFilePath)) {
      this.env.error('ERROR: Are you running this command from the right location? Could not find ' + sassIndexFilePath);
    }
  },

  prompting: function() {
    var done = this.async();
    var generator = this;

    this.log(yosay(
      'Welcome to the ' + chalk.red('gigamon-component') + ' generator! \nI create stub files for new AEM components.'
    ));

    var prompts = [{
      type: 'input',
      name: 'componentNameCamel',
      message: 'What is the name of the component (lowerCamelCase)?',
      validate: function(componentNameCamel) {
        if (!/^[a-z]+([A-Z0-9][a-z0-9]+[A-Za-z0-9])*$/.exec(componentNameCamel)) {
          return 'Invalid name [' + componentNameCamel + '], name must be lowerCamelCase.';
        }
        return true;
      }
    }, {
      type: 'input',
      name: 'componentNameDashed',
      message: 'What is the name of the component (with-dashes-all-lowercase)?',
      validate: function(componentNameDashed) {
        if (!/^[a-z\-]+$/.exec(componentNameDashed)) {
          return 'Invalid name [' + componentNameDashed + '], all lowercase and dashes.';
        }
        return true;
      }
    }, {
      type: 'list',
      name: 'mode',
      message: 'What do you want to create?',
      choices: [
        {name: 'Everything', value: 'all'},
        {name: 'Frontend Only', value: 'frontend'}
      ]
    }];

    this.prompt(prompts).then(function(props) {
      this.props = props;

      this.log(chalk.cyan('Will use the the following configurations:'));

      this.props.folderName = this.props.componentNameDashed;
      this.log('folderName:\t\t' + chalk.blue(this.props.folderName));

      this.props.sassName = this.props.componentNameDashed;
      this.props.sassFileName = this.props.componentNameDashed;
      this.log('sassName:\t\t' + chalk.blue(this.props.sassName));
      this.log('sassFileName:\t\t' + chalk.blue(this.props.sassFileName));

      this.props.jsModuleName = this.props.componentNameDashed;
      this.props.jsObjectName = capitalizeFirstLetter(this.props.componentNameCamel);
      this.props.jsFileName = this.props.componentNameDashed;
      this.log('jsModuleName:\t\t' + chalk.blue(this.props.jsModuleName));
      this.log('jsObjectName:\t\t' + chalk.blue(this.props.jsObjectName));
      this.log('jsFileName:\t\t' + chalk.blue(this.props.jsFileName));

      if (props.mode != 'frontend') {
        this.props.htlName = this.props.componentNameDashed;
        this.props.htlTemplateName = 'render' + capitalizeFirstLetter(this.props.componentNameCamel);
        this.props.htlPath = findHtlComponentDir(this) + '/' + this.props.folderName;
        this.log('htlName:\t\t' + chalk.blue(this.props.htlName));
        this.log('htlTemplateName:\t' + chalk.blue(this.props.htlTemplateName));
        this.log('htlPath:\t\t' + chalk.blue(this.props.htlPath));

        this.props.javaName = capitalizeFirstLetter(this.props.componentNameCamel);
        this.props.javaPackage = findComponentModelPackage(this);
        this.props.javaPath = findComponentModelPackagePath(this);
        this.log('javaName:\t\t' + chalk.blue(this.props.javaName));
        this.log('javaPackage:\t\t' + chalk.blue(this.props.javaPackage));
        this.log('javaPath:\t\t' + chalk.blue(this.props.javaPath));
      }

      // Final confirmation prompt
      this.prompt([{
        type    : 'confirm',
        name    : 'continue',
        default : false,
        message : 'Does this look correct?'
      }]).then(function(props) {
        if (!props.continue) {
          this.log(chalk.yellow('Exiting: Please re-run with correct inputs.'));
          process.exit(1);
        }
        done();
      }.bind(this));

    }.bind(this));
  },

  writing: function() {

    /*
     * SASS FILE
     */
    this.fs.copyTpl(
      this.templatePath('componentSass.scss'),
      this.destinationPath(path.join('ui.clientlibs/components', this.props.folderName, 'styles', this.props.sassFileName + '.scss')),
      this.props
    );

    /*
     * JS FILE
     */
    this.fs.copyTpl(
      this.templatePath('componentJavaScript.js'),
      this.destinationPath(path.join('ui.clientlibs/components', this.props.folderName, this.props.jsFileName + '.js')),
      this.props
    );

    /*
     * PACKAGE FILE
     */
    this.fs.copyTpl(
      this.templatePath('package.json'),
      this.destinationPath(path.join('ui.clientlibs/components', this.props.folderName, 'package.json')),
      this.props
    );

    /*
     * JAVA FILE
     */
    if (this.props.javaPath) {
      this.fs.copyTpl(
        this.templatePath('Model.java'),
        this.destinationPath(path.join(this.props.javaPath, this.props.javaName + '.java')),
        this.props
      );
    }

    /*
     * HTL FILE
     */
    if (this.props.htlPath) {
      this.fs.copyTpl(
        this.templatePath('htl.html'),
        this.destinationPath(path.join(this.props.htlPath, this.props.htlName + '.html')),
        this.props
      );
    }

    /*
     * UPDATE JS COMPONENT INDEX
     */
    var jsToAdd = 'require(\'./../../components/' + this.props.jsFileName + '\');\n';
    var jsContent = wiring.readFileAsString(jsModuleIndexFilePath);
    if (!jsContent.includes(jsToAdd)) {
      this.log(chalk.yellow('Updating ') + jsModuleIndexFilePath);

      jsContent = jsContent + jsToAdd;
      wiring.writeFileFromString(jsContent, jsModuleIndexFilePath);
    } else {
      this.log(chalk.cyan('Skipping ') + jsModuleIndexFilePath + ' update');
    }

    /*
     * UPDATE SASS COMPONENT INDEX
     */
    var sassToAdd = '@import "../../../components/' + this.props.folderName + '/styles/' + this.props.sassFileName + '";\n';
    var sassContent = wiring.readFileAsString(sassIndexFilePath);
    if (!sassContent.includes(sassToAdd)) {
      this.log(chalk.yellow('Updating ') + sassIndexFilePath);

      sassContent = sassContent + sassToAdd;
      wiring.writeFileFromString(sassContent, sassIndexFilePath);
    } else {
      this.log(chalk.cyan('Skipping ') + sassIndexFilePath + ' update');
    }
  }
});

/*
 * START HELPER FUNCTIONS
 */

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function findHtlComponentDir(generator) {
  var files = find.dirSync(/.*\/_debugging$/, generator.destinationPath('ui.apps/src'));
  if (files.length != 1) {
    generator.env.error('ERROR: Could not locate _debugging directory');
  }
  var file = files[0];
  var path = file.replace(generator.destinationPath() + '/', '');
  path = path.replace('/_debugging', '');

  return path;
}

function findComponentModelPackage(generator) {
  var file = _findComponentModelPath(generator);
  var split = file.split('/src/main/java/');
  var path = split[1];
  path = path.replace('/ComponentSlingModel.java', '');
  path = path.replace(/\//g, '.');

  return path;
}

function findComponentModelPackagePath(generator) {
  var file = _findComponentModelPath(generator);
  var path = file.replace(generator.destinationPath() + '/', '');
  path = path.replace('/ComponentSlingModel.java', '');

  return path;
}

function _findComponentModelPath(generator) {
  var files = find.fileSync(/.*\/ComponentSlingModel\.java$/, generator.destinationPath('core/src'));
  if (files.length != 1) {
    generator.env.error('ERROR: Could not locate ComponentSlingModel.java');
  }
  return files[0];
}
