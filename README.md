# gigamon-com

This is the gigamon-com AEM project.

## Modules

Below is a high-level summary of project modules. For more information please
refer to the README.md files in each module.

* __core__: contains all core Java based functionality for the site.
* __core.author-widgets__: contains compile time code for generating dialog fields with the [cq-component-maven-plugin](https://github.com/OlsonDigital/cq-component-maven-plugin).
* __generator-gigamon-component__: [Yeoman](http://yeoman.io/learning/index.html) generator for creating starter files for new components.
* __ui.apps__: contains the _/apps_ and _/etc_ parts of the code and markup.
* __ui.author-widgets__: contains custom clientlibs to supporting custom author mode dialog fields.
* __ui.content__: contains component demos used for training and integration tests.
* __ui.clientlibs__: contains the source browserified JavaScript and Sass styles for the site.

The following modules may not be used in this project (they are standard Adobe project structure).

* __ui.tests__: Java bundle containing JUnit tests that are executed server-side. This bundle is not to be deployed onto production.
* __ui.launcher__: contains glue code that deploys the ui.tests bundle (and dependent bundles) to the server and triggers the remote JUnit execution

## I need to create a new component, what do I do?

You should look through the README.md files, here is the TL;DR version.

Create the starter files (see readme in __generator-gigamon-component__):

    yo gigamon-component

Add some dialog field @annotated methods in:

    core/src/main/java/gigamon/core/models/component/[ComponentName].java

Update the HTL/Sightly markup in:

    ui.apps/src/main/content/jcr_root/apps/gigamon/components/content/[component-name]/[component-name].html

Update the components JavaScript and CSS (Sass):

    ui.clientlibs/components/[component-name]/styles/[component-name].scss
    ui.clientlibs/components/[component-name]/[component-name].js

Deploy to your localhost author environment to test:

    mvn clean install -P autoDeployPackage

Bonus: If you are working on Sass/JS/HTL automatically push file changes to your localhost on save:

    cd ui.clientlibs
    gulp watchAll

## How to build

To build all the modules run in the project root directory the following command with Maven 3:

    mvn clean install

If you have a running AEM instance you can build and package the whole project and deploy into AEM with  

    mvn clean install -PautoInstallPackage

Or to deploy it to a publish instance, run

    mvn clean install -PautoInstallPackagePublish

## Maven settings

This project uses the "UberJar" provided by Adobe upon request. For more information refer to this [documentation](https://docs.adobe.com/docs/en/aem/6-2/develop/dev-tools/ht-projects-maven.html).

Hero Digital hosts this UberJar in our [Nexus artifact repository](https://build.herodigital.com:8081/nexus/#view-repositories).

If you would like to use this repository (as opposed to your own) please ask Hero Digital for instructions.

