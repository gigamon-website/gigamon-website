# Gigamon Project - Core Salesforce

Contains the backend code responsible for communicating with Salesforce.

This is separate from the "core" module in order to resolve conflicts that resulted from including org.glassfish.jersey.core:jersey-client.

It was observed that inclusion of jersey-client prevented the Sling Model @PostConstruct methods from getting invoked. 

