package gigamon.core.apiclient;

/**
 * @author joelepps
 *         2/6/17
 */
public class NotFoundException extends ForceApiException {

    public NotFoundException(String message, String responseMessage, int responseCode) {
        super(message, responseMessage, responseCode);
    }

}
