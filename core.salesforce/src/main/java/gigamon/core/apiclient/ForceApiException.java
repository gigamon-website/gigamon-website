package gigamon.core.apiclient;

/**
 * Indicates an inability to process the request or an API error from Salesforce.
 *
 * @author joelepps
 *         2/3/17
 */
public class ForceApiException extends Exception {

    private final String responseMessage;
    private final int responseCode;

    public ForceApiException(String message) {
        super(message);
        this.responseMessage = null;
        this.responseCode = -1;
    }

    public ForceApiException(String message, String responseMessage, int responseCode) {
        super(message);
        this.responseMessage = responseMessage;
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public int getResponseCode() {
        return responseCode;
    }

    @Override
    public String getMessage() {
        String response =  super.getMessage();
        if (responseMessage != null) response += " :: " + responseMessage;
        if (responseCode > 0) response += " :: " + responseCode;
        return response;
    }
}
