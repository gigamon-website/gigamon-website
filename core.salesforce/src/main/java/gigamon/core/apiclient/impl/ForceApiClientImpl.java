package gigamon.core.apiclient.impl;

import gigamon.core.apiclient.ForceApiClient;
import gigamon.core.apiclient.InvalidTokenException;
import gigamon.core.apiclient.cmd.ForceApiCmd;
import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.impl.*;
import gigamon.core.apiclient.models.Case;
import gigamon.core.apiclient.models.CaseComment;
import gigamon.core.apiclient.models.ContentDocumentLink;
import gigamon.core.apiclient.models.ContentDocumentLinkQueryResult;
import gigamon.core.apiclient.models.CreateResponse;
import gigamon.core.apiclient.models.DeleteResponse;
import gigamon.core.apiclient.models.LibraryContent;
import gigamon.core.apiclient.models.LibraryCreateResponse;
import gigamon.core.apiclient.models.OAuth2AccessToken;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Dictionary;
import java.util.function.Function;

/**
 * Default implementation for Salesforce API client.
 *
 * @author joelepps
 *         2/3/17
 */
@Component(metatype = true, label = "Force.com API Client")
@Service
public class ForceApiClientImpl implements ForceApiClient {
	
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Property(label = "Client ID", description = "Salesforce Application Client ID")
    public static final String CLIENT_ID = "force.api.client.id";
    private String clientId;

    @Property(label = "Client Secret", description = "Salesforce Application Client Secret")
    public static final String CLIENT_SECRET = "force.api.client.secret";
    private String clientSecret;

    @Property(label = "API User", description = "Salesforce Master User Name")
    public static final String API_USERNAME = "force.api.master.user.name";
    private String apiUsername;

    @Property(label = "API Password", description = "Salesforce Master User Password")
    public static final String API_PASSWORD = "force.api.master.user.password";
    private String apiPassword;

    @Property(label = "API Domain", description = "Salesforce API Domain", value = "na35.salesforce.com")
    public static final String API_DOMAIN = "force.api.domain";

    @Property(label = "Login Domain", description = "Salesforce Login Domain", value = "login.salesforce.com")
    public static final String LOGIN_DOMAIN = "force.login.domain";

    private WebTarget authenticateRootWebTarget;
    private WebTarget apiRootWebTarget;
    
    /** This field should only be access via {@link #getAccessToken(boolean)} */
    private OAuth2AccessToken _accessToken;
    
    static final TrustManager[] certs = new TrustManager[1];
    
    @Activate
    public void activate(ComponentContext context) {
    	
    	//TODO kill me
//        certs[0] =
//        	    new X509TrustManager() {
//
//        			@Override
//        			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
//        				// TODO Auto-generated method stub
//        				
//        			}
//
//        			@Override
//        			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//        				// TODO Auto-generated method stub
//        				
//        			}
//
//        			@Override
//        			public X509Certificate[] getAcceptedIssuers() {
//        				// TODO Auto-generated method stub
//        				return null;
//        			}
//
//        	    };
    	
        Dictionary<String, ?> properties = context.getProperties();

        clientId = toString(properties.get(CLIENT_ID));
        clientSecret = toString(properties.get(CLIENT_SECRET));
        apiUsername = toString(properties.get(API_USERNAME));
        apiPassword = toString(properties.get(API_PASSWORD));
        String apiDomain = toString(properties.get(API_DOMAIN));
        String loginDomain = toString(properties.get(LOGIN_DOMAIN));
        
//        SSLContext ctx = null;
//        try {
//            ctx = SSLContext.getInstance("TLS");
//            ctx.init(null, certs, new SecureRandom());
//        } catch (java.security.GeneralSecurityException e) {
//            log.error("", e);
//        }
//        
//        ClientBuilder clientBuilder = ClientBuilder.newBuilder();
//        try {
//            clientBuilder.sslContext(ctx);
//            clientBuilder.hostnameVerifier(new HostnameVerifier() {
// 
//				@Override
//				public boolean verify(String arg0, SSLSession arg1) {
//					// TODO Auto-generated method stub
//					return true;
//				}
//            });
//
//        } catch (Exception e) {
//            log.error("", e);
//            //throw OurExceptionUtils.wrapInRuntimeExceptionIfNecessary(e);
//        }
//
//        HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());

        //TODO This forces the client to a proxy, remove after done
        ClientConfig config = new ClientConfig();
//        config.connectorProvider(new ApacheConnectorProvider());
//        config.property(ClientProperties.PROXY_URI, "http://127.0.0.1:8888");
 
//        Client client = clientBuilder
//                .withConfig(config).build();
//
//        authenticateRootWebTarget = client.target("https://login.salesforce.com");
//        apiRootWebTarget = client.target("https://" + apiDomain + "/services/data/v38.0");
        
//TODO use this!!!!!!        
        Client client = ClientBuilder.newClient();
        authenticateRootWebTarget = client.target("https://" + loginDomain);
        apiRootWebTarget = client.target("https://" + apiDomain + "/services/data/v38.0");
    }

    @Override
    @Nonnull
    public OAuth2AccessToken authenticate() throws ForceApiException {
        OAuth2AuthenticateCmd cmd = new OAuth2AuthenticateCmd(authenticateRootWebTarget, clientId, clientSecret, apiUsername, apiPassword);
        return ForceApiCmd.execute(cmd, OAuth2AccessToken.class, true);
    }

    @Override
    @Nonnull
    public CreateResponse createCase(Case kase) throws ForceApiException {
        return invokeCmdWithBadTokenRetry(t -> { return new CreateCaseCmd(apiRootWebTarget, t, kase); }, CreateResponse.class);
    }

    @Override
    @Nonnull
    public Case getCase(String caseId) throws ForceApiException {
        return invokeCmdWithBadTokenRetry(t -> { return new GetCaseCmd(apiRootWebTarget, t, caseId); }, Case.class);
    }

    @Override
    public CreateResponse createCaseComment(CaseComment caseComment) throws ForceApiException {
        return invokeCmdWithBadTokenRetry(t -> { return new CreateCaseCommentCmd(apiRootWebTarget, t, caseComment); }, CreateResponse.class);
    }

    @Override
    public CaseComment getCaseComment(String caseCommentId) throws ForceApiException {
        return invokeCmdWithBadTokenRetry(t -> { return new GetCaseCommentCmd(apiRootWebTarget, t, caseCommentId); }, CaseComment.class);
    }
    
    @Override
    @Nonnull
    public LibraryCreateResponse updateContent(LibraryContent content) throws ForceApiException {
    	
    	try{
            return invokeCmdWithBadTokenRetry(t -> { return new CreateLibraryContentCmd(apiRootWebTarget, t, content); }, LibraryCreateResponse.class);
    	} finally {
    		try {
				content.getVersionData().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error("failed to close inputstream in content: ", e);
			}
    	}
    }    
    
    @Override
    @Nonnull
    public LibraryContent getContentVersion(String contentVersionId) throws ForceApiException {
        return invokeCmdWithBadTokenRetry(t -> { return new GetContentVersionCmd(apiRootWebTarget, t, contentVersionId); }, LibraryContent.class);
    }
    
    @Override
    @Nonnull
    public CreateResponse createContentDocumentLink(ContentDocumentLink contentDocumentLink) throws ForceApiException {
        return invokeCmdWithBadTokenRetry(t -> { return new ContentDocumentLinkCmd(apiRootWebTarget, t, contentDocumentLink); }, CreateResponse.class);
    }
    
    @Override
    @Nonnull
    public DeleteResponse deleteContentDocumentLink(String id) throws ForceApiException {
        return invokeCmdWithBadTokenRetry(t -> { return new DeleteContentDocumentLinkCmd(apiRootWebTarget, t, id); }, DeleteResponse.class);
    }
    
    @Override
    @Nonnull
    public ContentDocumentLinkQueryResult queryContentDocumentLink(String id) throws ForceApiException {
        return invokeCmdWithBadTokenRetry(t -> { return new QueryContentDocumentLinkCmd(apiRootWebTarget, t, id); }, ContentDocumentLinkQueryResult.class);
    }    

    /*
    ==================================
           START HELPER METHODS
    ==================================
     */

    // 'synchronized' does not protect against double refresh of the access token.
    // However, it appears Salesforce returns the same access token when consecutive authenticate calls are made
    // as opposed to invalidating the previous token.
    // Therefore, it looks like it it okay to make a double authenticate call.
    private synchronized OAuth2AccessToken getAccessToken(boolean forceRefresh) throws ForceApiException {
        if (forceRefresh || _accessToken == null) {
            _accessToken = authenticate();
        }
        return _accessToken;
    }

    /**
     * Helper method to invoke the {@link ForceApiCmd} and if a {@link InvalidTokenException} is encountered, retry
     * with a new {@link OAuth2AccessToken}.
     *
     * @param cmdGenerator Lambda expression to create the {@link ForceApiCmd} with the provided token.
     * @param <T> Return type of the command
     * @return Instance of {@code T} return type
     * @throws ForceApiException see javadoc
     */
    private <T> T invokeCmdWithBadTokenRetry(Function<OAuth2AccessToken, ForceApiCmd<T>> cmdGenerator, Class<T> type) throws ForceApiException {
        T responseObj = null;

        // first get the access token (likely loaded from cache), forceRefresh = false.
        OAuth2AccessToken token = getAccessToken(false);
        try {
            // attempt command execution
            ForceApiCmd<T> cmd = cmdGenerator.apply(token);
            responseObj = ForceApiCmd.execute(cmd, type, true);

        } catch (InvalidTokenException e) {
            // If token is invalid, get a new token (forceRefresh = true) and re-execute
            token = getAccessToken(true);
            ForceApiCmd<T> cmd = cmdGenerator.apply(token);
            responseObj = ForceApiCmd.execute(cmd, type, true);
        }

        return responseObj;
    }

    private static String toString(Object obj) {
        return (obj == null) ? null : obj.toString();
    }

}
