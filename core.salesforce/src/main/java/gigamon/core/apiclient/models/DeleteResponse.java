package gigamon.core.apiclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Generic response object returned by many Salesforce create object requests.
 *
 * @author joelepps
 *         2/6/17
 */
public class DeleteResponse {

    @SerializedName("errorCode")
    @Expose
    private String errorCode;

    @SerializedName("fields")
    @Expose
    private List<Object> fields;

    @SerializedName("message")
    @Expose
    private String message;

    public List<Object> getFields() {
        return fields;
    }
     
    public String getMessage() {
    	return message;
    }

    @Override
    public String toString() {
        return "DeleteResponse{" + "errorCode='" + errorCode + '\'' + ", message=" + message + ", fields=" + fields + '}';
    }
}
