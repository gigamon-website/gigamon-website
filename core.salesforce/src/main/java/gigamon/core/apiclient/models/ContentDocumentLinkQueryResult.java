package gigamon.core.apiclient.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentDocumentLinkQueryResult {

	@SerializedName("totalSize")
	@Expose
	private Integer totalSize;
	@SerializedName("done")
	@Expose
	private Boolean done;
	@SerializedName("records")
	@Expose
	private List<Record> records = null;
	
	public Integer getTotalSize() {
		return totalSize;
	}
	
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}
	
	public Boolean getDone() {
		return done;
	}
	
	public void setDone(Boolean done) {
		this.done = done;
	}
	
	public List<Record> getRecords() {
		return records;
	}
	
	public void setRecords(List<Record> records) {
		this.records = records;
	}

}