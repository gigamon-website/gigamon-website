package gigamon.core.apiclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ContentWorkspaceDoc {
	
    public ContentWorkspaceDoc (String contentDocumentId, 
    							String contentWorkspaceId, 
    							boolean IsOwner) {
        this.contentDocumentId = contentDocumentId;
        this.contentWorkspaceId = contentWorkspaceId;		
        this.isOwner = isOwner;
    }
    
    @SerializedName("ContentDocumentId")
    @Expose
    private String contentDocumentId;
    
    @SerializedName("ContentWorkspaceId")
    @Expose
    private String contentWorkspaceId;
    
    @SerializedName("IsOwner")
    @Expose
    private boolean isOwner;

}
