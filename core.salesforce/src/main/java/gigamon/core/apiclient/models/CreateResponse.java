package gigamon.core.apiclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Generic response object returned by many Salesforce create object requests.
 *
 * @author joelepps
 *         2/6/17
 */
public class CreateResponse {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("errors")
    @Expose
    private List<Object> errors;

    public String getId() {
        return id;
    }

    public boolean isSuccess() {
        return success;
    }

    public List<Object> getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        return "CreateResponse{" + "id='" + id + '\'' + ", success=" + success + ", errors=" + errors + '}';
    }
}
