package gigamon.core.apiclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Case comment object that is used for Salesforce API requests and responses.
 *
 * @author joelepps
 *         2/6/17
 */
public class CaseComment {

    public CaseComment(String parentId, String commentBody) {
        this.parentId = parentId;
        this.commentBody = commentBody;
    }

    /*
 * ================
 * IMMUTABLE FIELDS
 * ================
 */
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("CreatedDate")
    @Expose
    private Date createdDate;
    @SerializedName("CreatedById")
    @Expose
    private String createdById;
    @SerializedName("LastModifiedDate")
    @Expose
    private Date lastModifiedDate;
    @SerializedName("LastModifiedById")
    @Expose
    private String lastModifiedById;
    @SerializedName("SystemModstamp")
    @Expose
    private Date systemModstamp;
    @SerializedName("IsPublished")
    @Expose
    private Boolean isPublished;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;

    public String getId() {
        return id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedById() {
        return createdById;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getLastModifiedById() {
        return lastModifiedById;
    }

    public Date getSystemModstamp() {
        return systemModstamp;
    }

    public boolean isPublished() {
        return isPublished;
    }

    public boolean isDeleted() {
        return isDeleted;
    }
    /*
     * ==============
     * MUTABLE FIELDS
     * ==============
     */

    @SerializedName("ParentId")
    @Expose
    private String parentId;

    @SerializedName("CommentBody")
    @Expose
    private String commentBody;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public void setCommentBody(String commentBody) {
        this.commentBody = commentBody;
    }
}
