package gigamon.core.apiclient.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LibraryCreateResponse {
	
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("errors")
    @Expose
    private List<Object> errors;

    public String getId() {
        return id;
    }

    public boolean isSuccess() {
        return success;
    }

    public List<Object> getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        return "LibraryCreateResponse{" + "id='" + id + '\'' + ", success=" + success + ", errors=" + errors + '}';
    }	

}
