package gigamon.core.apiclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

public class LibraryContent {
	
    public LibraryContent() { }

    public LibraryContent(String pathOnClient, 
			  String title, 
			  String description, 
			  String firstPublishLocationId, 
			  String channelEnablement,
			  String ecosystemPartnerType,
			  String legal,
			  String otherResources,
			  String product,
			  String productLine,
			  String professionalService,
			  String programs,
			  String resourceType,
			  String solutions,
			  String support,
			  String type,   
			  String tagCsv,
			  String versionNumber,
			  String contentDocumentId,
			  String filename,
			  InputStream versionData) {
    	
        this.pathOnClient = pathOnClient;
        this.title = title;
        this.description = description;
        this.firstPublishLocationId = firstPublishLocationId;
        this.channelEnablement = channelEnablement;
        this.ecosystemPartnerType = ecosystemPartnerType;
        this.legal = legal;
        this.otherResources = otherResources;
        this.product = product;
        this.productLine = productLine;
        this.professionalService = professionalService;
        this.programs = programs;
        this.resourceType = resourceType;
        this.solutions = solutions;
        this.support = support;
        this.type = type;        
        this.versionData = versionData;
        this.tagCsv = tagCsv;
        this.isMajorVersion = false;
        this.contentDocumentId = contentDocumentId;
        this.filename = filename;
    }
    
    @SerializedName("PathOnClient")
    @Expose
    private String pathOnClient;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Description")
    @Expose
    private String description;
    
    //access to allow access to inputfilestream (not streamed)
    private InputStream versionData;
    
    public InputStream getVersionData(){
    	return versionData;
    }
    
    //method to allow access to content filename (not streamed)
    private String filename;
    
    public String getFileName(){
    	
    	return filename;
    	
    }

    @SerializedName("FirstPublishLocationId")
    @Expose
    private String firstPublishLocationId;  
    
    @SerializedName("IsMajorVersion")
    @Expose
    private boolean isMajorVersion;
    
    @SerializedName("TagCsv")
    @Expose
    private String tagCsv;
    
    @SerializedName("ContentDocumentId")
    @Expose
    private String contentDocumentId;
    
    //Custom fields
    
    @SerializedName("Channel_Enablement__c")
    @Expose
    private String channelEnablement;    
    
    @SerializedName("GPCR_Ecosystem_Partner_Type__c")
    @Expose
    private String ecosystemPartnerType;
    
    @SerializedName("Legal__c")
    @Expose
    private String legal;
    
    @SerializedName("Other_Resources__c")
    @Expose
    private String otherResources;
    
    @SerializedName("Product__c")
    @Expose
    private String product;
    
    @SerializedName("GPCR_Product_Line__c")
    @Expose
    private String productLine;
    
    @SerializedName("Professional_Service__c")
    @Expose
    private String professionalService;
    
    @SerializedName("GPCR_Programs__c")
    @Expose
    private String programs;
    
    @SerializedName("GPCR_Resource_Type__c")
    @Expose
    private String resourceType;
    
    @SerializedName("Solutions__c")
    @Expose
    private String solutions;

    @SerializedName("Support__c")
    @Expose
    private String support;

    @SerializedName("Type__c")
    @Expose
    private String type;
    
    public String getContentDocumentId() {
        return contentDocumentId;
    }    
    
}
