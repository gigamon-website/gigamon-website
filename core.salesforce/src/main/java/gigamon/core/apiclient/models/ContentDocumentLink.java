package gigamon.core.apiclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ContentDocumentLink {
	
    public ContentDocumentLink (String contentDocumentId, 
			String linkedEntityId, 
			String shareType,
			String visibility
) {
    	
		this.contentDocumentId = contentDocumentId;
		this.linkedEntityId = linkedEntityId;		
		this.shareType = shareType;
		this.visibility = visibility;

    }
    
    @SerializedName("ContentDocumentId")
    @Expose
    private String contentDocumentId;
    
    @SerializedName("LinkedEntityId")
    @Expose
    private String linkedEntityId;
    
    @SerializedName("ShareType")
    @Expose
    private String shareType;
    
    @SerializedName("Visibility")
    @Expose
    private String visibility;

}
