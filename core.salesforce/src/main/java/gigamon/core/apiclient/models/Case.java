package gigamon.core.apiclient.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Salesforce Case object that is used for requests and responses.
 *
 * @author joelepps
 *         2/6/17
 */
public class Case {

    public Case() { }

    public Case(String subject, String description, String priority, String status, String origin) {
        this.status = status;
        this.origin = origin;
        this.subject = subject;
        this.priority = priority;
        this.description = description;
    }

    /*
     * ================
     * IMMUTABLE FIELDS
     * ================
     */
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("CaseNumber")
    @Expose
    private String caseNumber;
    @SerializedName("OwnerId")
    @Expose
    private String ownerId;
    @SerializedName("CreatedDate")
    @Expose
    private Date createdDate;
    @SerializedName("CreatedById")
    @Expose
    private String createdById;
    @SerializedName("LastModifiedDate")
    @Expose
    private Date lastModifiedDate;
    @SerializedName("LastModifiedById")
    @Expose
    private String lastModifiedById;
    @SerializedName("SystemModstamp")
    @Expose
    private Date systemModstamp;
    @SerializedName("LastViewedDate")
    @Expose
    private Date lastViewedDate;
    @SerializedName("LastReferencedDate")
    @Expose
    private Date lastReferencedDate;

    public String getId() {
        return id;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedById() {
        return createdById;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getLastModifiedById() {
        return lastModifiedById;
    }

    public Date getSystemModstamp() {
        return systemModstamp;
    }

    public Date getLastViewedDate() {
        return lastViewedDate;
    }

    public Date getLastReferencedDate() {
        return lastReferencedDate;
    }

    /*
     * ==============
     * MUTABLE FIELDS
     * ==============
     */
    @SerializedName("ContactId")
    @Expose
    private Object contactId;
    @SerializedName("AccountId")
    @Expose
    private Object accountId;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Origin")
    @Expose
    private String origin;
    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("Priority")
    @Expose
    private String priority;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("IsClosed")
    @Expose
    private Boolean isClosed;
    @SerializedName("ClosedDate")
    @Expose
    private Object closedDate;
    @SerializedName("IsEscalated")
    @Expose
    private Boolean isEscalated;


    public Object getContactId() {
        return contactId;
    }

    public void setContactId(Object contactId) {
        this.contactId = contactId;
    }

    public Object getAccountId() {
        return accountId;
    }

    public void setAccountId(Object accountId) {
        this.accountId = accountId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }

    public Object getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Object closedDate) {
        this.closedDate = closedDate;
    }

    public Boolean getIsEscalated() {
        return isEscalated;
    }

    public void setIsEscalated(Boolean isEscalated) {
        this.isEscalated = isEscalated;
    }

    @Override
    public String toString() {
        return "Case{" + "id='" + id + '\'' + ", createdDate='" + createdDate + '\'' + ", lastModifiedDate='" + lastModifiedDate + '\'' + ", subject='" + subject + '\'' + '}';
    }
}
