package gigamon.core.apiclient;

/**
 * Subtype of {@link ForceApiException} that indicates the Salesforce access token is invalid.
 * <p>
 * Typical behavior upon getting this error is to get a new access token and retry the request.
 *
 * @author joelepps
 *         2/3/17
 */
public class InvalidTokenException extends ForceApiException {

    public InvalidTokenException(String message) {
        super(message);
    }

    public InvalidTokenException(String message, String responseMessage, int responseCode) {
        super(message, responseMessage, responseCode);
    }

}
