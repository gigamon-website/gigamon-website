package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractAuthenticatedApiCmd;
//import gigamon.core.apiclient.cmd.AbstractApiCmd.Method;
import gigamon.core.apiclient.models.Case;
import gigamon.core.apiclient.models.DeleteResponse;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;
import javax.ws.rs.client.WebTarget;

/**
 * Command to delete a ContentDocumentLink object from Salesforce.
 *
 * @author ant
 *         4/4/17
 */
public class DeleteContentDocumentLinkCmd extends AbstractAuthenticatedApiCmd<DeleteResponse> {

    private final String id;

    public DeleteContentDocumentLinkCmd(@Nonnull WebTarget rootWebTarget, @Nonnull OAuth2AccessToken accessToken, String id) {
        super(rootWebTarget, accessToken);
        this.id = id;
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget()
            .path("sobjects/ContentDocumentLink")
            .path(id);
    }
    
    @Override
    protected Method getMethod() {
        return Method.DELETE;
    }

	@Override
	public void postMarshallValidate(DeleteResponse result) throws ForceApiException {
		//we swallow this because delete does not return anything
	}
    
}
