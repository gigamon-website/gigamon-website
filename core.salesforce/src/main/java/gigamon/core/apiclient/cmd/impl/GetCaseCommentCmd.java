package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractAuthenticatedApiCmd;
import gigamon.core.apiclient.models.CaseComment;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;
import javax.ws.rs.client.WebTarget;

/**
 * Command to fetch a case comment from Salesforce.
 *
 * @author joelepps
 *         2/6/17
 */
public class GetCaseCommentCmd extends AbstractAuthenticatedApiCmd<CaseComment> {

    private String caseCommentId;

    public GetCaseCommentCmd(@Nonnull WebTarget rootWebTarget,
                             @Nonnull OAuth2AccessToken accessToken,
                             @Nonnull String caseCommentId) {
        super(rootWebTarget, accessToken);
        this.caseCommentId = caseCommentId;
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget()
            .path("sobjects/CaseComment")
            .path(caseCommentId);
    }
}
