package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractAuthenticatedApiCmd;
import gigamon.core.apiclient.models.Case;
import gigamon.core.apiclient.models.CreateResponse;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * Command to creates a Case object in Salesforce.
 *
 * @author joelepps
 *         2/6/17
 */
public class CreateCaseCmd extends AbstractAuthenticatedApiCmd<CreateResponse> {

    private final Case kase;

    public CreateCaseCmd(@Nonnull WebTarget rootWebTarget, @Nonnull OAuth2AccessToken accessToken, @Nonnull Case kase) {
        super(rootWebTarget, accessToken);
        this.kase = kase;
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget()
            .path("sobjects/Case");
    }

    @Nullable
    @Override
    protected Entity<String> getPayload() {
        return Entity.entity(GSON.toJson(kase), MediaType.APPLICATION_JSON_TYPE);
    }

    @Override
    protected Method getMethod() {
        return Method.POST;
    }
}
