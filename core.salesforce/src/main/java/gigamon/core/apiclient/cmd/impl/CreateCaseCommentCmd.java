package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractAuthenticatedApiCmd;
import gigamon.core.apiclient.models.CaseComment;
import gigamon.core.apiclient.models.CreateResponse;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * Command to create a new case comment.
 *
 * @author joelepps
 *         2/6/17
 */
public class CreateCaseCommentCmd extends AbstractAuthenticatedApiCmd<CreateResponse> {

    private CaseComment caseComment;

    public CreateCaseCommentCmd(@Nonnull WebTarget rootWebTarget,
                                @Nonnull OAuth2AccessToken accessToken,
                                @Nonnull CaseComment caseComment) {
        super(rootWebTarget, accessToken);
        this.caseComment = caseComment;
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget()
            .path("sobjects/CaseComment");
    }

    @Nullable
    @Override
    protected Entity<String> getPayload() {
        return Entity.entity(GSON.toJson(caseComment), MediaType.APPLICATION_JSON_TYPE);
    }

    @Override
    protected Method getMethod() {
        return Method.POST;
    }
}
