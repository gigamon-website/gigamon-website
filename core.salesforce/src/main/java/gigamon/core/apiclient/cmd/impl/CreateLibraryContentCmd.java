package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractAuthenticatedApiCmd;
import gigamon.core.apiclient.models.LibraryContent;
import gigamon.core.apiclient.models.LibraryCreateResponse;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import java.util.Collections;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.Boundary;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.StreamDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Command to creates a Case object in Salesforce.
 *
 * @author adam.trissel
 *         4/20/17
 */
public class CreateLibraryContentCmd extends AbstractAuthenticatedApiCmd<LibraryCreateResponse> {

    private final LibraryContent content;
    
    private Logger log = LoggerFactory.getLogger(this.getClass());

    public CreateLibraryContentCmd(@Nonnull WebTarget rootWebTarget, @Nonnull OAuth2AccessToken accessToken, @Nonnull LibraryContent content) {
        super(rootWebTarget, accessToken);
        this.content = content;
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget()
            .path("sobjects/ContentVersion")
            .register(MultiPartFeature.class);
    }

    /*
	final MultiPart multiPartEntity = new MultiPart()
	        .bodyPart(new BodyPart().entity("hello"))
	        .bodyPart(new BodyPart(new JaxbBean("xml"), MediaType.APPLICATION_XML_TYPE))
	        .bodyPart(new BodyPart(new JaxbBean("json"), MediaType.APPLICATION_JSON_TYPE));

	final WebTarget target = // Create WebTarget.
	final Response response = target
        .request()
        .post(Entity.entity(multiPartEntity, multiPartEntity.getMediaType()));     */

    @Nullable
    @Override
    protected Entity<MultiPart> getPayload() {

    	//return Entity.entity(GSON.toJson(content), MediaType.APPLICATION_JSON_TYPE);
    	
    	final StreamDataBodyPart versionData = new StreamDataBodyPart("VersionData", content.getVersionData(), content.getFileName());
    	final BodyPart jsonPart = new BodyPart(GSON.toJson(content), MediaType.APPLICATION_JSON_TYPE);
    	jsonPart.setContentDisposition(buildContentDisposition());
    	
    	final MultiPart multiPartEntity = new MultiPart()
    	        .bodyPart(jsonPart)
    	        .bodyPart(versionData);
    	
    	MediaType mtype = new MediaType("multipart", "form-data",
        		Collections.singletonMap(Boundary.BOUNDARY_PARAMETER, Boundary.createBoundary()));
    	
    	//multiPartEntity.setMediaType(mtype);
   
    	//log.info("MediaType: " + multiPartEntity.getMediaType().toString());

    	return Entity.entity(multiPartEntity, mtype);

    }

    @Override
    protected Method getMethod() {
        return Method.POST;
    }
    
    /**
     * Builds the body part content-disposition header which the specified
     * filename (or the default one if unspecified).
     *
     * @return ready to use content-disposition header.
     */
    protected FormDataContentDisposition buildContentDisposition() {
        FormDataContentDisposition.FormDataContentDispositionBuilder builder = FormDataContentDisposition.name("entity_content");

//        if (filename != null) {
//            builder.fileName(filename);
//        } else {
//            // Default is to set the name of the file as a form-field name.
//            builder.fileName(getName());
//        }

        return builder.build();
    }    

}
