package gigamon.core.apiclient.cmd;

import gigamon.core.apiclient.ForceApiException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * The method {@link #execute(ForceApiCmd, Class, boolean)} can be used to execute instances of this interface.
 * <p>
 * <b>Lifecycle in order of execution:</b>
 * <ol>
 *     <li>{@link #preValidate()}</li>
 *     <li>{@link #prepareWebTarget()}</li>
 *     <li>{@link #prepareBuilder(WebTarget)}</li>
 *     <li>{@link #sendRequest(Invocation.Builder)}</li>
 *     <li>{@link #postExecuteValidate(Response)}</li>
 *     <li>{@link #marshall(Response, Class)}</li>
 *     <li>{@link #postMarshallValidate(Object)}</li>
 * </ol>
 *
 * @author joelepps
 *         2/3/17
 */
public interface ForceApiCmd<T> {

    /**
     * Helper method to execute a {@link ForceApiCmd} instance.
     *
     * @param cmd Command to execute
     * @param type Response type for the command
     * @param doPostMarshallValidate {@code true} if post marshall validation should occur
     * @return {@code type} response object, may be null depending on command
     * @throws ForceApiException Indicates an inability to process the request or an API error from Salesforce.
     */
    @Nullable
    static <T> T execute(@Nonnull ForceApiCmd<T> cmd,
                         @Nullable Class<T> type,
                         boolean doPostMarshallValidate) throws ForceApiException {
        cmd.preValidate();
        WebTarget webTarget = cmd.prepareWebTarget();
        Invocation.Builder builder = cmd.prepareBuilder(webTarget);
        Response response = cmd.sendRequest(builder);
        cmd.postExecuteValidate(response);
        T result = cmd.marshall(response, type);

        if (doPostMarshallValidate) {
            cmd.postMarshallValidate(result);
        }

        return result;
    }

    /**
     * Validates that the post-construction state of this command is valid.
     *
     * @throws ForceApiException Indicates an inability to process the request or an API error from Salesforce.
     */
    void preValidate()
        throws ForceApiException;

    /**
     * Build {@link WebTarget} and include all query parameters for the request.
     *
     * @return web target that will be invoked
     * @throws ForceApiException Indicates an inability to process the request or an API error from Salesforce.
     */
    @Nonnull
    WebTarget prepareWebTarget()
        throws ForceApiException;

    /**
     * Build the {@link javax.ws.rs.client.Invocation.Builder} based off the {@code webTarget}. Include any
     * additional HTTP headers for the request.
     *
     * @param webTarget web target
     * @return builder to sendRequest
     * @throws ForceApiException Indicates an inability to process the request or an API error from Salesforce.
     */
    @Nonnull
    Invocation.Builder prepareBuilder(@Nonnull WebTarget webTarget)
        throws ForceApiException;

    /**
     * Send the request.
     *
     * @param builder builder to sendRequest
     * @return {@link Response} object
     * @throws ForceApiException Indicates an inability to process the request or an API error from Salesforce.
     */
    @Nonnull
    Response sendRequest(@Nonnull Invocation.Builder builder)
        throws ForceApiException;

    /**
     * Validate response object state, things such as the HTTP status code.
     *
     * @param response response object to validate
     * @throws ForceApiException Indicates an inability to process the request or an API error from Salesforce.
     */
    void postExecuteValidate(@Nonnull Response response)
        throws ForceApiException;

    /**
     * Marshall the {@code response} object into the requested {@code type}.
     *
     * @param response object to marshall
     * @param type object type to return, {@code null} if result object is not applicable for this command
     * @return Resulting object or null
     * @throws ForceApiException Indicates an inability to process the request or an API error from Salesforce.
     */
    @Nullable
    T marshall(@Nonnull Response response, @Nullable Class<T> type)
        throws ForceApiException;

    /**
     * Validates the object resulting from the API call is as expected.
     *
     * @param result result object or {@code null} is not applicable for this command.
     * @throws ForceApiException Indicates an inability to process the request or an API error from Salesforce.
     */
    void postMarshallValidate(@Nullable T result)
        throws ForceApiException;
}
