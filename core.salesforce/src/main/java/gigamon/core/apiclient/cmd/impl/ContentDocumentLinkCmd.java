package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractAuthenticatedApiCmd;
import gigamon.core.apiclient.models.ContentDocumentLink;
import gigamon.core.apiclient.models.CreateResponse;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class ContentDocumentLinkCmd extends AbstractAuthenticatedApiCmd<CreateResponse>{
	
    private final ContentDocumentLink contentDocumentLink;

    public ContentDocumentLinkCmd(@Nonnull WebTarget rootWebTarget, @Nonnull OAuth2AccessToken accessToken, @Nonnull ContentDocumentLink contentDocumentLink) {
        super(rootWebTarget, accessToken);
        this.contentDocumentLink = contentDocumentLink;
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget()
            .path("sobjects/ContentDocumentLink");
    }

    @Nullable
    @Override
    protected Entity<String> getPayload() {
        return Entity.entity(GSON.toJson(contentDocumentLink), MediaType.APPLICATION_JSON_TYPE);
    }

    @Override
    protected Method getMethod() {
        return Method.POST;
    }	

}
