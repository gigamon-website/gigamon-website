package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractApiCmd;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;
import javax.ws.rs.client.WebTarget;

/**
 * API command for getting an access token from Salesforce.
 *
 * @author joelepps
 *         2/3/17
 */
public class OAuth2AuthenticateCmd extends AbstractApiCmd<OAuth2AccessToken> {

    private final String clientId;
    private final String clientSecret;
    private final String username;
    private final String password;

    public OAuth2AuthenticateCmd(@Nonnull WebTarget rootWebTarget,
                                 String clientId,
                                 String clientSecret,
                                 String username,
                                 String password) {
        super(rootWebTarget);
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.username = username;
        this.password = password;
    }

    @Override
    public void preValidate() throws ForceApiException {
        super.preValidate();

        if (clientId == null) throw new ForceApiException("Client ID cannot be null");
        if (clientSecret == null) throw new ForceApiException("Client secret cannot be null");
        if (username == null) throw new ForceApiException("Username cannot be null");
        if (password == null) throw new ForceApiException("Password cannot be null");
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget()
            .path("/services/oauth2/token")
            .queryParam("client_id", clientId)
            .queryParam("client_secret", clientSecret)
            .queryParam("username", username)
            .queryParam("password", password)
            .queryParam("grant_type", "password");
    }

    @Override
    protected AbstractApiCmd.Method getMethod() {
        return Method.POST;
    }
}
