package gigamon.core.apiclient.cmd;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.InvalidTokenException;
import gigamon.core.apiclient.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * The base implementation for a {@link ForceApiCmd}.
 * <p>
 * All {@link ForceApiCmd} methods are implemented with default implementations.
 *
 * @author joelepps
 *         2/3/17
 */
public abstract class AbstractApiCmd<T> implements ForceApiCmd<T> {

    private static final Logger log = LoggerFactory.getLogger(AbstractApiCmd.class);
    private static final Logger timingLog = LoggerFactory.getLogger(AbstractApiCmd.class.getCanonicalName() + ".timing");

    protected static final Gson GSON = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    private WebTarget rootWebTarget;

    /**
     * This object holds the aggregate state of this command (api key, locale, etc). When an exception is thrown or
     * certain messages are logged, this map is included to facilitate debugging root cause after the fact.
     */
    protected final Map<String, String> debugInfo;

    public AbstractApiCmd(@Nonnull WebTarget rootWebTarget) {
        this.rootWebTarget = rootWebTarget;
        this.debugInfo = new HashMap<>();

        this.debugInfo.put("command", getClass().getCanonicalName());
    }

    @Override
    public void preValidate() throws ForceApiException {
        // default implementation does nothing
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return rootWebTarget;
    }

    @Nonnull
    @Override
    public Invocation.Builder prepareBuilder(@Nonnull WebTarget webTarget) throws ForceApiException {
        debugInfo.put("webTarget", webTarget.toString());
        return webTarget.request(MediaType.APPLICATION_JSON);
    }

    protected enum Method {GET, POST, DELETE}

    /**
     * The HTTP method for the API call. Default implementation in GET.
     *
     * @return HTTP method
     */
    protected Method getMethod() {
        return Method.GET;
    }

    /**
     * Default implementation returns null.
     *
     * @return POST payload.
     */
    @Nullable
    protected Entity<?> getPayload() {
        return null;
    }

    @Override
    @Nonnull
    public Response sendRequest(@Nonnull Invocation.Builder builder) throws ForceApiException {
        Entity<?> payload = getPayload();
        debugInfo.put("payload", payload == null ? null : payload.toString());

        Method method = getMethod();
        debugInfo.put("method", method+"");

        log.trace("sendRequest {}: payload - {}", method, payload);

        long start = System.currentTimeMillis();

        Response response;
        switch (getMethod()) {
            case GET:
                response = builder.get();
                break;
            case POST:
                response = builder.post(payload);
                break;
            case DELETE:
                response = builder.delete();
                break;
            default:
                throw new IllegalArgumentException();
        }

        long end = System.currentTimeMillis();

        //timingLog.info("{}.sendRequest() {}ms: {}", this.getClass().getSimpleName(), end-start, debugInfo);

        return response;
    }

    @Override
    public void postExecuteValidate(@Nonnull Response response) throws ForceApiException {
        if (response.getStatus() > 299 || response.getStatus() < 200) { // 2XX responses OK

            String errorBody = "Debug Info: " + debugInfo;
            String responseBody = response.readEntity(String.class);

            if (response.getStatus() == 401) {
                throw new InvalidTokenException("Invalid token. " + errorBody, responseBody, response.getStatus());
            }
            if (response.getStatus() == 404) {
                throw new NotFoundException("Resource not found. " + errorBody, responseBody, response.getStatus());
            }
            throw new ForceApiException("Error. " + errorBody, responseBody, response.getStatus());
        }
    }

    @Override
    public T marshall(@Nonnull Response response, @Nullable Class<T> type) throws InvalidTokenException, ForceApiException {
        if (type == null) {
            // if type is null then result object is not applicable or needed, just return null
            return null;
        }

        String jsonResponse = response.readEntity(String.class);
        log.trace("marshallResponse body: {}", jsonResponse);
        debugInfo.put("jsonResponse", jsonResponse);

        T result = GSON.fromJson(jsonResponse, type);
        log.trace("marshall: {} {}", response.getStatus(), result);

        log.debug("SUMMARY: code[{}], info[{}]", response.getStatus(), debugInfo);
        return result;
    }

    @Override
    public void postMarshallValidate(@Nullable T result) throws ForceApiException {
        if (result == null) throw new ForceApiException("Null result is not supported. Info " + debugInfo);
    }
}
