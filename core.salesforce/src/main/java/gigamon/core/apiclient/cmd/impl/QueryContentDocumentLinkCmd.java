package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractAuthenticatedApiCmd;
import gigamon.core.apiclient.models.Case;
import gigamon.core.apiclient.models.ContentDocumentLinkQueryResult;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;
import javax.ws.rs.client.WebTarget;

/**
 * Command to fetch a Case object from Salesforce.
 *
 * @author joelepps
 *         2/6/17
 */
public class QueryContentDocumentLinkCmd extends AbstractAuthenticatedApiCmd<ContentDocumentLinkQueryResult> {

    private final String linkId;

    public QueryContentDocumentLinkCmd(@Nonnull WebTarget rootWebTarget, @Nonnull OAuth2AccessToken accessToken, String linkId) {
        super(rootWebTarget, accessToken);
        this.linkId = linkId;
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget().path("query/").queryParam("q", "SELECT Id from ContentDocumentLink where ContentDocumentId = '" + linkId + "'");
            //.path("query/?q=SELECT+Id+from+ContentDocumentLink+where+ContentDocumentId+=+'" + linkId + "'");
    }
}