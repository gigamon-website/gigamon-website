package gigamon.core.apiclient.cmd;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.InvalidTokenException;
import gigamon.core.apiclient.models.OAuth2AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;

/**
 * Abstract {@link ForceApiCmd} implementation that should be used when the command requires an {@link OAuth2AccessToken}
 * in order to execute.
 *
 * @author joelepps
 *         2/3/17
 */
public abstract class AbstractAuthenticatedApiCmd<T> extends AbstractApiCmd<T> {

    private static final Logger log = LoggerFactory.getLogger(AbstractAuthenticatedApiCmd.class);

    private OAuth2AccessToken accessToken;

    public AbstractAuthenticatedApiCmd(@Nonnull WebTarget rootWebTarget, @Nonnull OAuth2AccessToken accessToken) {
        super(rootWebTarget);
        this.accessToken = accessToken;
    }

    public boolean isAccessTokenRequired() {
        return true;
    }

    @Override
    public void preValidate() throws ForceApiException {
        super.preValidate();

        if (isAccessTokenRequired() && accessToken == null || accessToken.getAccessToken() == null) {
            log.error("Token is null or missing: ", debugInfo);
            throw new InvalidTokenException("Token is null or missing: " + accessToken);
        }
    }

    @Nonnull
    @Override
    public Invocation.Builder prepareBuilder(@Nonnull WebTarget webTarget) throws ForceApiException {
        Invocation.Builder builder = super.prepareBuilder(webTarget);

        // Add access token to request
        if (accessToken != null && accessToken.getAccessToken() != null) {
            //log.trace("invokeWebTarget Adding authorization {} {} to {}", accessToken.getTokenType(), accessToken.getAccessToken(), webTarget);
            debugInfo.put("accessToken", accessToken.toString());
            builder.header("Authorization", accessToken.getTokenType() + " " + accessToken.getAccessToken());
        }

        return builder;
    }

}
