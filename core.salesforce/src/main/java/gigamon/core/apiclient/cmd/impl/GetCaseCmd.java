package gigamon.core.apiclient.cmd.impl;

import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.cmd.AbstractAuthenticatedApiCmd;
import gigamon.core.apiclient.models.Case;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;
import javax.ws.rs.client.WebTarget;

/**
 * Command to fetch a Case object from Salesforce.
 *
 * @author joelepps
 *         2/6/17
 */
public class GetCaseCmd extends AbstractAuthenticatedApiCmd<Case> {

    private final String caseId;

    public GetCaseCmd(@Nonnull WebTarget rootWebTarget, @Nonnull OAuth2AccessToken accessToken, String caseId) {
        super(rootWebTarget, accessToken);
        this.caseId = caseId;
    }

    @Nonnull
    @Override
    public WebTarget prepareWebTarget() throws ForceApiException {
        return super.prepareWebTarget()
            .path("sobjects/Case")
            .path(caseId);
    }
}
