package gigamon.core.apiclient;

import gigamon.core.apiclient.models.Case;
import gigamon.core.apiclient.models.CaseComment;
import gigamon.core.apiclient.models.ContentDocumentLink;
import gigamon.core.apiclient.models.ContentDocumentLinkQueryResult;
import gigamon.core.apiclient.models.CreateResponse;
import gigamon.core.apiclient.models.DeleteResponse;
import gigamon.core.apiclient.models.LibraryContent;
import gigamon.core.apiclient.models.LibraryCreateResponse;
import gigamon.core.apiclient.models.OAuth2AccessToken;

import javax.annotation.Nonnull;

/**
 * Defines all API interactions that can occur between AEM and the Salesforce REST API.
 *
 * @author joelepps
 *         2/3/17
 */
public interface ForceApiClient {

    /**
     * Authenticate and return a new access token.
     *
     * @return new access token
     * @throws ForceApiException see javadoc
     */
    @Nonnull
    OAuth2AccessToken authenticate() throws ForceApiException;

    /**
     * Create a new case.
     *
     * @param kase Case to create
     * @return Generic {@link CreateResponse} object
     * @throws ForceApiException see javadoc
     */
    @Nonnull
    CreateResponse createCase(Case kase) throws ForceApiException;

    /**
     * Get case from Salesforce.
     *
     * @param caseId case ID
     * @return Salesforce Case
     * @throws ForceApiException see javadoc
     */
    @Nonnull
    Case getCase(String caseId) throws ForceApiException;

    /**
     * Create a new case comment.
     *
     * @param caseComment Case comment to create
     * @return Generic {@link CreateResponse} object
     * @throws ForceApiException see javadoc
     */
    CreateResponse createCaseComment(CaseComment caseComment) throws ForceApiException;

    /**
     * Get a case comment from Salesforce.
     *
     * @param caseCommentId ID of case comment
     * @return Case comment object
     * @throws ForceApiException see javadoc
     */
    CaseComment getCaseComment(String caseCommentId) throws ForceApiException;
    
    /**
     * Create or Version a new Library Entry.
     *
     * @param cibtebt LibraryContent to create
     * @return Generic {@link UpdateContentResponse} object
     * @throws ForceApiException see javadoc
     */
    @Nonnull
    LibraryCreateResponse updateContent(LibraryContent content) throws ForceApiException;
    
    /**
     * Read a ContentVersion
     * @param ContentVersionId
     * @return LibraryContent object
     * @throws ForceApiException see javadoc
     * 
     */
    @Nonnull
    LibraryContent getContentVersion(String ContentVersionId) throws ForceApiException;
    
    /**
     * Create a ContentDocument Link.
     *
     * @param cibtebt contentDocumentLink to create
     * @return Generic {@link CreateResponse} object
     * @throws ForceApiException see javadoc
     */
    @Nonnull
    CreateResponse createContentDocumentLink(ContentDocumentLink contentDocumentLink) throws ForceApiException;
    
    /**
     * Delete a ContentDocument Link.
     *
     * @param cibtebt contentDocumentLink to delete
     * @return Generic {@link DeleteResponse} object
     * @throws ForceApiException see javadoc
     */
    @Nonnull
    DeleteResponse deleteContentDocumentLink(String id) throws ForceApiException;
    
    /**
     * Delete a ContentDocument Link.
     *
     * @param cibtebt contentDocumentLink to delete
     * @return Generic {@link DeleteResponse} object
     * @throws ForceApiException see javadoc
     */
    @Nonnull
    ContentDocumentLinkQueryResult queryContentDocumentLink(String id) throws ForceApiException;    
 
}
