package gigamon.core.salesforce.services;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

//Sling Imports

import org.apache.sling.api.resource.ResourceResolverFactory ;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Revision;
import com.day.cq.replication.ReplicationAction ;
import com.day.cq.tagging.Tag;

//import org.apache.sling.event.EventPropertiesMap;

import gigamon.core.apiclient.ForceApiClient;
import gigamon.core.apiclient.ForceApiException;
import gigamon.core.apiclient.models.ContentDocumentLink;
import gigamon.core.apiclient.models.ContentDocumentLinkQueryResult;
import gigamon.core.apiclient.models.CreateResponse;
import gigamon.core.apiclient.models.DeleteResponse;
import gigamon.core.apiclient.models.LibraryContent;
import gigamon.core.apiclient.models.LibraryCreateResponse;
import gigamon.core.apiclient.models.Record;


/**
 * Event hook to catch replication
 *
 * This service catches replication of dam:Assets with specific tagging and
 * pushes the content to Salesforce.
 *
 * requires the following user to be added to the Apache Sling Service User Mapper Service
 * gigamon.com.core:readservice=writeuser
 *
 * @author ant
 *         4/10/17 *
 */

@Component
@Service
@Property(name="event.topics",value= ReplicationAction.EVENT_TOPIC)
public class SalesforceReplicationService implements Runnable, EventHandler {

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private ForceApiClient sut;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Activate
    public void initializeSalesforce(){

    }

    public void handleEvent(Event event) {

        //String n[] = event.getPropertyNames();

        ReplicationAction action = ReplicationAction.fromEvent(event);

        if(action != null) {

        	/*
        	* These are the steps:
        	* 1) is the payload (action,getPath()) start with /content/dam/zeltiq and is it a dam:Asset
        	* 2) Get the tags from the asset
        	* 3) does it have the tags that would cause the asset to be replicated to Salesforce
        	* 4) push the asset to salesforce
        	* 5) on any error, note the error on a status field on the asset and update
        	* 6) put the asset in a specific library (workspaceid = 05841000000HmcA)
        	* 7) dump and load shared groups
        	*/

            Map<String, Object> param = new HashMap<String, Object>();
            param.put(ResourceResolverFactory.SUBSERVICE, "writeservice");
            ResourceResolver resolver = null;
            LibraryCreateResponse createResponse = null;

            String firstPublishLocationId = "";

            try{

                resolver = resolverFactory.getServiceResourceResolver(param);
                Session session = resolver.adaptTo(Session.class);

                Node payloadNode = session.getNode(action.getPath());

                String primaryType = payloadNode.getProperty("./jcr:primaryType").getString();

                String contentDocumentId = "";
                String currentTitle = "";

                //The asset may or may not have been 'pushed' to sales force, if it has then it has a contentDocumentId

                try{

                    contentDocumentId = payloadNode.getProperty("./jcr:content/metadata/contentDocumentId").getString();

                }catch (Exception e){

                    contentDocumentId = "";

                }

                try{

                    currentTitle = payloadNode.getProperty("./jcr:content/metadata/dc:title").getString();

                }catch (Exception e){

                    currentTitle = "";

                }


                //only process dam:Asset publish events

                if(primaryType.equals("dam:Asset")){

                    Resource r = resolver.getResource(action.getPath());
                    Asset a = r.adaptTo(Asset.class);

                    InputStream i = a.getOriginal().getStream();

                    //get revisions
                    Collection<Revision> revisions = a.getRevisions(null);

                    String label = "1";
                    String title = currentTitle;
                    String description = "";
                    String name = a.getName();  //default pathonclient for an asset with no revisions

                    for (Revision rev : revisions){

                        label = rev.getLabel();
                        //title = rev.getTitle();
                        title = currentTitle;
                        description = rev.getDescription();
                        name = rev.getName();

                        log.info("Revision: " + label) ;

                        break;  //we really only want the first one

                    }

                    if (title == null || title.equals("")){

                        title = a.getName();
                        description = a.getName();

                    }

                    Collection<String> shareGroups = null;
                    Map<String, List<String>> tagCollectionMap = null;
                    Collection<String> sharedLibraries = null;

                    try{

                        javax.jcr.Property tags = payloadNode.getProperty("./jcr:content/metadata/cq:tags");

                        Value[] tagv = tags.getValues();

                        Collection<Tag> tagCollection = getTags(resolver, tagv);

                        tagCollectionMap = getOtherTagCollectionMap(tagCollection);

                        if(contentDocumentId.equals("")){
                            firstPublishLocationId = getLibary(tagCollection);
                        }

                        sharedLibraries = getSharedLibaries(tagCollection);

                        for(Tag t : tagCollection){

                            log.info("**Tag:" + t.getName() + " : " + t.getPath() + " : " + t.getDescription());

                        }

                        shareGroups = getGroups(tagCollection);

                    } catch (Exception e){

                        log.info("No tags for for :" + action.getPath());

                    }

                    //Ok this is interesting.  If the contentDocumentId is present (the document already has been sent to salesforce
                    //Then we are free to use the firstPublishLocationId field.  if the contentDocumentId does exist then it needs to be
                    //an empty string.

                    //So we have two cases
                    //1) We get the asset from AEM Assets and check to see if there is a contentDocumentId on the assetmeta data.
                    //2) if there is a contentDocumentId on the asset then se leave the firstPublishLocationId blank and
                    //          set the ContentDocumentID field to the field on the asset.
                    //3) if the contentDocumentId does not exist on the asset, we supply the firstPublishLocationId field, leave the contentDocumentID field blank ("")
                    //          we then add the asset to salesforce, query the new file in salesforce by the return id and add that field to the asset metadata.

                    //we need either a contentDocumentID or a firstPublishLocationId to push the asset to salesforce.
                    //contentDocuemntId is the existing document the version is associated with
                    //firstPublishLocationId is where the first version will be created, which yields a contentDocumentId

                    if (!(contentDocumentId.equals("") && firstPublishLocationId == null)){

                        List<String> programs = new ArrayList<String>();
                        programs.add("Promotions");
                        programs.add("Reseller Program");

                        LibraryContent lb = new LibraryContent(name,
                            title,
                            description,
                            firstPublishLocationId,  //hard coded for the time being
                            "", //channel enablement
                            String.join(";", tagCollectionMap.get("ecosystem")), //ecosystem partner type*
                            "", //legal
                            "", //other resource
                            "", //product
                            String.join(";", tagCollectionMap.get("productLine")), //product line*
                            "", //professional service
                            String.join(";", tagCollectionMap.get("programs")), //programs*
                            String.join(";", tagCollectionMap.get("resourceType")), //resource type*
                            "", //solutions
                            "", //support
                            "", //type,
                            String.join(",", tagCollectionMap.get("tags")),
                            label,
                            contentDocumentId, //"06941000001SQ1bAAG",
                            name,
                            i);

                        createResponse = sut.updateContent(lb);

                        log.info("Replication action {} occured on {} ", action.getType().getName(), action.getPath());

                        if(createResponse.isSuccess()){
                            log.info("Returned Salesforce File ID: " + createResponse.getId());
                        }

                        LibraryContent sfAsset = sut.getContentVersion(createResponse.getId());

                        String returnedContentDocumentId = sfAsset.getContentDocumentId();

                        log.info("ContentDocumentId: " + sfAsset.getContentDocumentId());

                        //put the newly minted contentDocumentId on the asset node

                        postContentDocumentIdOnAsset(payloadNode, session, sfAsset.getContentDocumentId());

                        //proactively delete any shared groups and libraries on salesforce contentDocumen

                        deleteSharedGroups(payloadNode);
                        deleteSharedLibraries(payloadNode);

                        //share ContentDocument with designated groups, then record the group guids

                        createAndPostSharedGroups(session, payloadNode, shareGroups, returnedContentDocumentId);
                        createAndPostSharedLibraries(session, payloadNode, sharedLibraries, returnedContentDocumentId);

                        session.save();

                    }

                    i.close();

                }

            } catch (Exception e){

                log.info("failed to replicate to Salesforce:", e);

                if (createResponse != null && !createResponse.isSuccess()){

                    List<Object> errors = createResponse.getErrors();
                    for (Object error : errors){
                        log.error("Salesforce Push Error: " + (String)error);
                    }

                }

            }


        }

        log.info("end of replication");

    }

    private void queryTest(String contentDocumentId) throws ForceApiException{

        //query test, removed because it is not needed, but want to keep the format documented

        if (contentDocumentId != null && !contentDocumentId.equals("")){

            ContentDocumentLinkQueryResult cr = sut.queryContentDocumentLink(contentDocumentId);

            Collection<Record> linkRecords = cr.getRecords();

            for (Record r : linkRecords){
                log.info("Link Record Id: " + r.getId());
            }

        }

    }

    private void postContentDocumentIdOnAsset(Node payloadNode, Session session, String contentDocumentID){

        try{

            Node metaDataNode = payloadNode.getNode("./jcr:content/metadata");
            metaDataNode.setProperty("contentDocumentId", contentDocumentID);
            session.save();

        } catch (Exception e){

            log.error("failed to set contentDocumentId on node: ", e);

        }


    }

    /*
     * keep track of groups linked to asset
     */

    private void createAndPostSharedGroups(Session session, Node payloadNode, Collection<String> shareGroups, String returnedContentDocumentId){

        ArrayList<String> sharedGroupList = new ArrayList<String>();

        try{

            Node metaDataNode = payloadNode.getNode("./jcr:content/metadata");

            for (String groupid : shareGroups){

                ContentDocumentLink contentDocumentLink = new ContentDocumentLink(returnedContentDocumentId, groupid, "V", "");
                CreateResponse c = sut.createContentDocumentLink(contentDocumentLink);

                log.info("GroupId: " + groupid + " contentDocumentLinkId: " + c.getId());

                if (c.isSuccess()){

                    sharedGroupList.add(c.getId());

                }

            }

            metaDataNode.setProperty("sharedGroups", String.join("/", sharedGroupList));

        } catch (Exception e){

            log.error("Failed to log shared groups: ", e);

        }
    }

    /*
     * keep track of additional libraries associated with asset
     */

    private void createAndPostSharedLibraries(Session session, Node payloadNode, Collection<String> shareLibraries, String returnedContentDocumentId){

        ArrayList<String> sharedLibraryList = new ArrayList<String>();

        try{

            Node metaDataNode = payloadNode.getNode("./jcr:content/metadata");

            for (String libraryid : shareLibraries){

                ContentDocumentLink contentDocumentLink = new ContentDocumentLink(returnedContentDocumentId, libraryid, "I", "");
                CreateResponse c = sut.createContentDocumentLink(contentDocumentLink);

                log.info("SharedLibraryId: " + libraryid + " contentDocumentLinkId: " + c.getId());

                if (c.isSuccess()){

                    sharedLibraryList.add(c.getId());

                }

            }

            metaDataNode.setProperty("sharedLibraries", String.join("/", sharedLibraryList));

        } catch (Exception e){

            log.error("Failed to log shared groups: ", e);

        }
    }


    private void deleteSharedGroups(Node payloadNode){

        try{

            Node metaDataNode = payloadNode.getNode("./jcr:content/metadata");

            try{

                javax.jcr.Property sharedP = metaDataNode.getProperty("./sharedGroups");
                String sharedS = sharedP.getString();
                String[] existingSharedGroups = sharedS.split("/");

                for(String group : existingSharedGroups){

                    if(!group.equals("")){

                        log.info("we need to delete: " + group + " here.");

                        DeleteResponse d = sut.deleteContentDocumentLink(group);

                    }

                }

            } catch(PathNotFoundException e){

                log.info("no shared groups to delete");

            }catch(Exception e){

                log.error("failed to delete shared groups", e);

            }

        } catch (Exception e){

            log.error("failed to delete shared groups: ", e);

        }
    }

    private void deleteSharedLibraries(Node payloadNode){

        try{

            Node metaDataNode = payloadNode.getNode("./jcr:content/metadata");

            try{

             javax.jcr.Property sharedP = metaDataNode.getProperty("./sharedLibraries");
                String sharedS = " ";//sharedP.getString();
                String[] existingSharedLibraries = sharedS.split("/");

                for(String library : existingSharedLibraries){

                    if(!library.equals("")){

                        log.info("we need to delete: " + library + " here.");

                        DeleteResponse d = sut.deleteContentDocumentLink(library);

                    }

                }

            } catch(PathNotFoundException e){

                log.info("no shared libraries to delete");

            }
            catch(Exception e){

                log.error("failed to delete shared libraries", e);

            }

        } catch (Exception e){

            log.error("failed to delete shared libraries: ", e);

        }
    }

    private Collection<Tag> getTags(ResourceResolver resolver, Value[] tags) throws Exception{

        ArrayList<Tag> tagList = new ArrayList<Tag>();

        for (Value value : tags){

            String tagName = value.getString();

            if(isTagDomain("partner_portal", tagName)){

                tagName = "/etc/tags/" + tagName.replace(":", "/");

                Tag tag = resolver.getResource(tagName).adaptTo(Tag.class);

                tagList.add(tag);

            }

        }

        return tagList;
    }

    private boolean isTagDomain(String domain, String tagName){

        boolean retVal = false;

        String[] domainArray = tagName.split(":");

        String[] tagLevelArray = domainArray[1].split("/");

        if(tagLevelArray[0].equals(domain)){

            retVal = true;

        }

        return retVal;

    }


    /*
     * Get the library Guids, unique identifiers on the salesforce side, this determines firstPublishLocationId
     */

    private String getLibary(Collection<Tag> tags){

        String guid = null;

        for (Tag tag : tags){

            String[] tagLevels = tag.getPath().split("/");

            if (tagLevels[tagLevels.length - 2].equals("library")){

                log.info("Library GUID: " + tag.getDescription());
                guid = tag.getDescription();

            }

        }

        return guid;

    }

    /*
     * Get the library Guids, unique identifiers on the salesforce side, this determines firstPublishLocationId
     */

    private Collection<String> getSharedLibaries(Collection<Tag> tags){

        Collection<String> guids = new ArrayList<String>();

        for (Tag tag : tags){

            String[] tagLevels = tag.getPath().split("/");

            if (tagLevels[tagLevels.length - 2].equals("shared_library")){

                log.info("Library GUID: " + tag.getDescription());
                guids.add(tag.getDescription());

            }

        }

        return guids;

    }

    /*
     * Get the group Guids, Unique identifiers on the salesforce side, this determines the sharing on salesforce
     */

    private Collection<String> getGroups(Collection<Tag> tags){

        ArrayList<String> guids = new ArrayList<String>();

        for (Tag tag : tags){

            String[] tagLevels = tag.getPath().split("/");

            if (tagLevels[tagLevels.length - 2].equals("groups")){

                log.info("Group GUID: " + tag.getDescription());
                guids.add(tag.getDescription());

            }

        }

        return guids;

    }

    /*
     * get all other tag titles for tags on salesforce
     */

    private Map<String, List<String>> getOtherTagCollectionMap(Collection<Tag> tags){

        Map<String, List<String>> tagCollectionMap = new HashMap<String, List<String>>();

        //Prime the Arrays with known values

        tagCollectionMap.put("tags", new ArrayList<String>());
        tagCollectionMap.put("programs", new ArrayList<String>());
        tagCollectionMap.put("ecosystem", new ArrayList<String>());
        tagCollectionMap.put("resourceType", new ArrayList<String>());
        tagCollectionMap.put("productLine", new ArrayList<String>());

        for (Tag tag : tags){

            String[] tagLevels = tag.getPath().split("/");

            String tagDomain = tagLevels[tagLevels.length - 2];

            //The inverse of the above method
            if (!tagLevels[tagLevels.length - 2].equals("library") && !tagLevels[tagLevels.length - 2].equals("groups")){

                tagCollectionMap.get("tags").add(tag.getTitle());

                try{

                    switch (tagDomain){
                        case "programs":
                            tagCollectionMap.get("programs").add(tag.getTitle());
                            break;
                        case "ecosystem_partner":
                            tagCollectionMap.get("ecosystem").add(tag.getTitle());
                            break;
                        case "resource_type":
                            tagCollectionMap.get("resourceType").add(tag.getTitle());
                            break;
                        case "product_line":
                            tagCollectionMap.get("productLine").add(tag.getTitle());
                            break;
                        default:
                            break;

                    };

                }catch(Exception e){

                    log.error("switch error:", e);

                }

            }

        }

        return tagCollectionMap;

    }

    public void run() {

        log.info("Salesforce Replicaton Service Running...");

    }

}
