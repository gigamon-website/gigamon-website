import org.jsoup.nodes.Document
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements


class ParseResources {
	
	static void main(String[] args) {

		def myClass = new ParseResources();

		def options = myClass.getOptions(args);
		
		def outputDir = "sysout";
		
		if (options.o) {
			
			outputDir = options.o;
			
		}
		
		if (options.u) {

			myClass.processURL(outputDir, options.u);

		}
		
	}

	OptionAccessor getOptions(args){

		def cli = new CliBuilder(usage:'fileRead [options] <url>')
		 cli.u(longOpt:'URL', args:1, argName:'url', 'url to read from');
		 cli.f(longOpt:'filename', args:1, argName:'filename', 'file to read from');
		 cli.o(longOpt:'outputDir', args:1, argName:'outputDir', 'directory to write to');
		 
		def options = cli.parse(args)

		return options;

	}

	void processURL(outputDir, url){
		
		String pageHTML = "";
		
		try{
			
			Document doc = Jsoup.connect(url).get();
			
			Elements els = doc.select(".resource");
			
			els.each {element ->
				
				def href = element.attr("href");
				
					
				def keywords = element.getElementsByClass("keywords");
				
				keywords.each {keyword ->
					
					println(href + '\t' + keyword.text());
					
					if(href.endsWith(".pdf")){
						
						pullFileFromURL(outputDir, href)
						
					}
					
				}
				
			}

		} catch(Exception e){
			e.printStackTrace();			
		}
		
	}
	
	void pullFileFromURL(outputDir, url){
		
		if (!outputDir.endsWith('/')){
			outputDir = outputDir + "/";
		}
		
		def filename = url.tokenize("/")[-1];
		filename = outputDir + filename;
		
		//println("Output File Name: " + filename)
		
		def file = new File(filename).newOutputStream()
		file << new URL(url).openStream()
		file.close()
		
	}

}
