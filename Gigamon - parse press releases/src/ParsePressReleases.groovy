import org.jsoup.nodes.Document
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements


class ParsePressReleases {
	
	class Url {
		
		String title;
		String content;
		String filename;
		boolean isValid;
		
	}
	
	static void main(String[] args) {

		def myClass = new ParsePressReleases();

		def options = myClass.getOptions(args);
		
		def outputDir = "sysout";
		
		if (options.o) {
			
			outputDir = options.o;
			
		}
		
		if (options.f) {
			
			myClass.readFile(outputDir, options.f);
			
		}

		if (options.u) {

			myClass.processURL(options.u);

		}
		
	}

	OptionAccessor getOptions(args){

		def cli = new CliBuilder(usage:'fileRead [options] <url>')
		 cli.u(longOpt:'URL', args:1, argName:'url', 'url to read from');
		 cli.f(longOpt:'filename', args:1, argName:'filename', 'file to read from');
		 cli.o(longOpt:'outputDir', args:1, argName:'outputDir', 'directory to write to');
		 
		def options = cli.parse(args)

		return options;

	}
	
	void writeToFile(outputDir, myURL){
		
		if (outputDir.equals("sysout")){
			
			println(myURL.filename);
			
		} else {
			
			if (myURL.isValid){

				File file = new File(outputDir + myURL.filename + ".html");
				file << myURL.content;

			} else {
				
				println("invalid page: " + myURL.filename);
				
			}
			
			
		}
		
		
	}
	
	 Url processURL(url){
		
		def myURL = new Url();
		def title = "";
		
		myURL.isValid = false;
		
		String pageHTML = "";
		
		try{
			
			Document doc = Jsoup.connect(url).get();
			title = doc.title();
			
			Elements els = doc.getElementsByClass("MainContent")
			
			els.each {element ->
				Elements row = element.getElementsByClass("row");
				
				row.each{rowElement ->
					//println(rowElement);
					pageHTML = rowElement;
					myURL.isValid = true;
				}
				
			}

		} catch(Exception e){
			myURL.isValid = false;			}
		
		def comp = url.split("/");
		
		def filename = comp[comp.length - 1];
		
		myURL.title = title;
		myURL.content = pageHTML;
		
		myURL.filename = filename;
		
		return myURL;
		
	}

	void readFile(outputDir, filename){
		
		new File(filename).eachLine{line ->

			def list = line.split("\t");

			def url = list[0];
			
			def myURL = processURL(url);
			
			writeToFile(outputDir, myURL);

		}			

	}
}
