package gigamon.core.servlets;

import java.io.IOException;
import java.rmi.ServerException;

import javax.jcr.Node;
import javax.jcr.Property;

import org.apache.felix.scr.annotations.Reference;

import org.apache.felix.scr.annotations.sling.SlingServlet;

import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFactory;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//Sling Imports

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

//AEM Tagging Imports
import com.day.cq.tagging.JcrTagManagerFactory;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

import com.day.cq.commons.RangeIterator;

@SlingServlet(paths = "/bin/tagservlet", methods = "GET", metatype = true)
public class TagServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 2598426539166789515L;
	private Session session;

	/** Default log. */
	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@Reference
	JcrTagManagerFactory tmf;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {

		String type = request.getParameter("addition");

		String ref_Tag= request.getParameter("ref_tag");
		String[] ref_Tag_find = new String[1];
		ref_Tag_find[0]= request.getParameter("ref_tag");
		String addTag = request.getParameter("add");

		String old_Tag = request.getParameter("old_tag");
		String[] old_Tag_find = new String[1];
		old_Tag_find[0]= request.getParameter("old_tag");
		String newTag = request.getParameter("new");
		String searchpath =request.getParameter("basepath");
		
		boolean has_tag =false;
	
		
	
		try {
			
			
			ResourceResolver resourceResolver = request.getResourceResolver();
			session = resourceResolver.adaptTo(Session.class);

			
			 TagManager tMgr = resourceResolver.adaptTo(TagManager.class);
			
			RangeIterator<Resource> tagsIt=null;
			Tag newtagid = null;
		
			
			if ("0".equals(type) && !"".equals(old_Tag)) {
				response.getWriter().println("Replace Process has started ::");
				response.getWriter().println("Replacing old tag :: "+old_Tag+" with new tag :: " +newTag);
				log.info("Replace Process has started ::");
				log.info("Replacing old tag :: "+old_Tag+" with new tag :: " +newTag);
				
			
				tagsIt = tMgr.find(searchpath,old_Tag_find,true);
				
			}
			if ("1".equals(type) && !"".equals(ref_Tag) ) {
				response.getWriter().println("Addition Process has started ::");
				response.getWriter().println("Reference tag :: "+ref_Tag+" resources are added with a new tag :: " +addTag);
				log.info("Addition Process has started ::");
				log.info("Reference tag :: "+ref_Tag+" resources are added with a new tag :: " +addTag);
				tagsIt = tMgr.find(searchpath,ref_Tag_find,true);
				
			}
			//response.getWriter().println("Reference Resources Size : " + tagsIt.getSize());
			
			if (null != tagsIt) {
				log.info("Reference Resources Size : " + tagsIt.getSize());
				response.getWriter().println("Reference Resources Size : " + tagsIt.getSize());
				while (tagsIt.hasNext()) {
					Resource myResource = (Resource) tagsIt.next();
					response.getWriter().println("Path of each resources :: " + myResource.getPath());
					log.info("Path of each resources :: " + myResource.getPath());
					Node node = myResource.adaptTo(Node.class);
					Property tags = node.getProperty("cq:tags");
					ValueFactory valueFactory = session.getValueFactory();
					Value[] tagss = tags.getValues();
					int length = tagss.length;
					int newsize = length + 1;
					newtagid =tMgr.resolve(addTag);
				
					Value[] values = null;
					if ("1".equals(type) && null!=newtagid && !addTag.equals("") ) {
						response.getWriter().println("Inside addition main");
						log.info("Inside addition main");
						values = new Value[newsize];
						
							for (int i = 0; i < tagss.length; i++) {
								log.info("Inside Addition");
								values[i] = tagss[i];
								response.getWriter().println("Before addition tag values are "+ values[i].getString() + "value f taggg name ::: "+tagss[i].toString());
								if(tagss[i].toString().equals(ref_Tag)){has_tag=true;}
								
							}
							if(has_tag){
								response.getWriter().println("coorect tag ::: "+has_tag);
							values[newsize - 1] = valueFactory.createValue(addTag);}
						
							response.getWriter().println("Addition process completed :::");
						}
					newtagid =tMgr.resolve(newTag);

					if ("0".equals(type) && null!=newtagid && !newTag.equals("")  ) {
						
						//response.getWriter().println("Inside replace main");
						values = new Value[tagss.length];
						for (int i = 0; i < tagss.length; i++) {
							log.info("inside replace");
							response.getWriter().println("Tags Before replace ::: " +tagss[i].getString());
							if (tagss[i].getString().equals(old_Tag)) {
							
								log.info("Entered new tag :: "+newTag+ "is not empty or in-valid");
									
									values[i] = valueFactory.createValue(newTag);
									
								
							} else {

								values[i] = tagss[i];
								
							}
							response.getWriter().println("Tags after replace ::: " + values[i].toString());
							log.info("Tags after replace ::: " + values[i].toString());
							
							
						}
						
						response.getWriter().println("After save replace process completed :::");
					}
					log.info("before setproperty");
					if (null != values) {
						node.setProperty("cq:tags", values);
						node.save();
					}
					log.info("after save");

				}
			}

		} catch (Exception e) {
			log.info("*** COUNT ERROR: " + e.toString());
			e.printStackTrace();
		}

	}

}
