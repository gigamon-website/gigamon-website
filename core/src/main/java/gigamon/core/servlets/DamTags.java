package gigamon.core.servlets;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.Session;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.felix.scr.annotations.Reference;

import com.day.cq.dam.api.AssetManager;
import com.day.cq.dam.api.Asset;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.opencsv.CSVWriter;


@SlingServlet(paths = "/bin/getDamTags")
public class DamTags extends
		org.apache.sling.api.servlets.SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private QueryBuilder builder;

	private static final Logger LOGGER = LoggerFactory.getLogger(DamTags.class);

	protected void doGet(SlingHttpServletRequest slingRequest,
			SlingHttpServletResponse slingResponse) {
		try {

			String excelpath = slingRequest.getParameter("excelpath");
			File f = new File("dam-asset-list.csv");
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF8"));
			CSVWriter csvWriter = new CSVWriter(writer,
					CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER,
					CSVWriter.DEFAULT_ESCAPE_CHARACTER,
					CSVWriter.DEFAULT_LINE_END);

			String[] headerRecord = { "Asset Name", "Description", "Path",
					"Tags", "Part No", "Version No" };
			csvWriter.writeNext(headerRecord);
			String path = slingRequest.getParameter("path");
			ResourceResolver resourceresolver = slingRequest
					.getResourceResolver();
			Session session = resourceresolver.adaptTo(Session.class);

			Map<String, String> queryMap = new HashMap<String, String>();
			queryMap.put("path", path);
			queryMap.put("type", "dam:Asset");
			Query query = builder.createQuery(PredicateGroup.create(queryMap),
					session);
			query.setHitsPerPage(3000);
			SearchResult result = query.getResult();
			Iterator<Resource> it = result.getResources();
			while (it.hasNext()) {
				Resource r = it.next();
				Resource metadataResource = r.getChild("jcr:content/metadata");
				ValueMap properties = ResourceUtil
						.getValueMap(metadataResource);
				Asset asset = r.adaptTo(Asset.class);
				String[] tags = { "no tags" };
				if (properties.containsKey("cq:tags")) {
					tags = properties.get("cq:tags", String[].class);
				}
				StringBuilder builder = new StringBuilder();
				for (String s : tags) {
					builder.append(s + "; ");
				}
				String str = builder.toString();
				String[] assetColArr = { asset.getName(),
						properties.get("dc:description", "Description").replace("\n"," ").replace(",", " "),
						asset.getPath(), str,
						properties.get("part_number", "Not Available"),
						properties.get("version_number", "Not Available") };
				csvWriter.writeNext(assetColArr);
					}

			AssetManager manager = resourceresolver.adaptTo(AssetManager.class);
			writer.close();
			csvWriter.close();
			InputStream is = null;
			is = new FileInputStream(f);
			manager.createAsset(excelpath + "/DAMAssetMetadata.csv", is,
					"text/comma-separated-values", true);
			slingResponse.getWriter().print(
                "DAM Asset Metadata Spread is sheet ready.Please find the excel output at :"+excelpath+"/DAMAssetMetadata.csv");
			session.logout();
		}catch (RuntimeException e) {
		    throw e;
		  }
		catch (Exception e) {
			LOGGER.info("Exception while fetching DAM assets tags "
					+ e.getMessage());
		}
	}
}
