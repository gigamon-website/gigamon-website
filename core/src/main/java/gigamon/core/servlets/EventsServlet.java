package gigamon.core.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import com.day.cq.replication.ReplicationStatus;
import org.apache.sling.api.resource.LoginException;
import com.day.cq.wcm.api.Page;
import javax.jcr.Node;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;


@SlingServlet(paths = "/bin/eventservlet", methods = "GET", metatype=true)
@Property(name="sling.auth.requirements", value = "-/bin/eventservlet")
public class EventsServlet extends SlingSafeMethodsServlet {
	private static final long serialVersionUID = 2598426539166789515L;

	ReplicationStatus status = null;

	@Reference
	private ResourceResolverFactory resolverFactory;

	protected final Logger log = LoggerFactory.getLogger(EventsServlet.class);

    private ResourceResolver getResourceResolver() {

	Map<String, Object> sysuser = new HashMap<String, Object>();

	sysuser.put(resolverFactory.SUBSERVICE, "eventsservice");

		try {

			return resolverFactory.getServiceResourceResolver(sysuser);
		}
		catch (LoginException e) {

			log.error("Exception whle getting ResourceResolver :"+e);
		}

	return null;
}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException{


		JSONArray jsonarray = new JSONArray();
		try {

            response.addHeader("Access-control-Allow-Origin", "*");
			ResourceResolver resourceResolver = getResourceResolver();

			Resource getResource = resourceResolver.getResource("/content/gigamon/en_us/company/news-and-events/events");

			Page eventpage = getResource.adaptTo(Page.class);
			Iterator<Page> childList = eventpage.listChildren();

			while (childList.hasNext()) {

				Page childpage = (Page) childList.next();

				status = childpage.adaptTo(ReplicationStatus.class);

				if (status.isActivated()) {


					String eventtitletext = "No title information";
					String eventdesc = "No desc information";
					String eventdatetext = "No date information";
					String eventloctext = "No location information";
					String titlelink = "No link information";
					String requestPath = childpage.getPath() + "/jcr:content/par/text_jumbo";

					Node content = resourceResolver.getResource(requestPath).adaptTo(Node.class);
					String eventhtml = content.getProperty("text").toString();

					Document document = Jsoup.parse(eventhtml);
					Elements title = document.getElementsByClass("event-title");
					Elements descelements = document.getElementsByClass("event-desc");
					Elements date = document.getElementsByClass("date-display-single");
					Elements loc = document.getElementsByClass("event-location");
					Elements link = document.select(".event-title a");
					titlelink = link.attr("href");


						for (Element eventtitle : title) {
							if (eventtitle.hasText() && !eventtitle.text().contains("\u00a0")) {
							eventtitletext = eventtitle.text();
							}
						}
						for (Element eventdesctext : descelements) {
							if (eventdesctext.hasText() && !eventdesctext.text().contains("\u00a0")) {
								eventdesc = eventdesctext.text().replaceAll("\"","\'");
							}
						}

						for (Element eventdate : date) {
							if (eventdate.hasText() && !eventdate.text().contains("\u00a0")) {
							eventdatetext = eventdate.text();
							}
						}

						for (Element eventloc : loc) {

							if (eventloc.hasText() && !eventloc.text().contains("\u00a0")) {
								eventloctext = eventloc.text().replace("Location:","").trim();
							}
						}

						JSONObject target = new JSONObject();
						target.put("Title", eventtitletext);
						target.put("Desc", eventdesc);
						target.put("Date", eventdatetext);
						target.put("Location", eventloctext);
						target.put("Link", titlelink);
						jsonarray.put(target);

				}
			}
				response.getWriter().write("EventsList = " + jsonarray.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
