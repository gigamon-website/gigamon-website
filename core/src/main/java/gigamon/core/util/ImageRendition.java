package gigamon.core.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Canonical record of all web/JPEG renditions supported by this site.
 * <p>
 * The JavaScript version of this file is {@code gigamon/ui.clientlibs/components/image/imageRendition.js}
 * <p>
 * Any changes to the Java or JavaScript version of this file should be propagated to both.
 *
 * @author joelepps
 *
 */
public enum ImageRendition {

    // Web Renditions (JPEG)
    FULL_WIDTH_X2_DESKTOP   (3840,  1500),
    FULL_WIDTH_x1_DESKTOP   (1920,  750),
    FULL_WIDTH_x2_MOBILE    (960,   375),
    FULL_WIDTH_X1_MOBILE    (960,   375), // Currently, X1 is the same as X2
    HALF_WIDTH_LARGE        (1000,  1000),
    HALF_WIDTH_SMALL        (500,   500),

    ORIGINAL                (0,     0);

    private final int width;
    private final int height;

    ImageRendition(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public String getRendition(String imagePath) {
        if (StringUtils.isBlank(imagePath)) return imagePath;

        String ext = getExtension(imagePath);
        if (this == ORIGINAL || "svg".equals(ext)) {
            return imagePath + ".imgo." + ext;
        }
        // web images should always be jpg extension
        return imagePath + ".imgw." + width + "." + height + "." + "jpg";
    }

    private String getExtension(String imagePath) {
    	imagePath = imagePath.toLowerCase();
        if (imagePath != null && imagePath.contains(".png")) {
            return "png";
        } else if (imagePath != null && imagePath.contains(".jpg")) {
            return "jpg";
        } else if (imagePath != null && imagePath.contains(".svg")) {
            return "svg";
        } else {
            return "jpg"; // default
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
