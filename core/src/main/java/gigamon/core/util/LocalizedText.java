package gigamon.core.util;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import gigamon.core.models.component.ComponentSlingModel;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.day.cq.wcm.foundation.List.log;
import static com.sun.corba.se.spi.activation.IIOP_CLEAR_TEXT.value;

/**
 * Helper class that holds getters for all supported localized text strings.
 * <p>
 * Implementation works on the assumption that localized text Strings are stored on jcr:content
 * page nodes and that inheritance model applies to retrieval.
 *
 * @author joelepps
 *         10/3/16
 */
public class LocalizedText {

    private static final String APPLY = "label.general.apply";
    private static final String CLOSE = "label.general.close";
    private static final String SHOW_MORE = "label.general.showmore";
    private static final String SHOW_LESS = "label.general.showless";
    private static final String SEARCH = "label.general.search";

    @Nullable
    private ComponentSlingModel componentSlingModel;

    @Nullable
    private InheritanceValueMap iValueMap;

    /**
     * Preferred constructor for LocalizedText.
     * <p>
     * Using this constructor allows for Author Error Message support if requested localized text is null.
     *
     * @param slingModel ComponentSlingModel
     */
    public LocalizedText(@Nonnull ComponentSlingModel slingModel) {
        this(slingModel.getResource());
        this.componentSlingModel = slingModel;
    }

    /**
     * Fallback constructor if you cannot use {@link #LocalizedText(ComponentSlingModel)}.
     * <p>
     * Will not have Author Error Message support.
     *
     * @param resource resource
     */
    public LocalizedText(@Nonnull Resource resource) {
        ResourceResolver resolver = resource.getResourceResolver();
        PageManager pageManager = resolver.adaptTo(PageManager.class);
        if (pageManager != null) {
            Page page = pageManager.getContainingPage(resource);
            if (page != null) {
                Resource contentResource = page.getContentResource();
                if (contentResource != null) {
                    iValueMap = new HierarchyNodeInheritanceValueMap(contentResource);
                }
            }
        }
    }

    public String get(String key) {
        if (iValueMap == null) {
            log.warn("Cannot get {}, iValueMap is not initialized");
            return null;
        }

        return iValueMap.getInherited(key, String.class);
    }

    public String getApply() {
        return get(APPLY);
    }

    public String getClose() {
        return get(CLOSE);
    }

    public String getShowMore() {
        return get(SHOW_MORE);
    }

    public String getShowLess() {
        return get(SHOW_LESS);
    }

    public String getSearch() {
        return get(SEARCH);
    }

}
