package gigamon.core.util;
import gigamon.core.models.ResourceCatalog.GridResource;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List; 

public class ResourceChainedComparator  implements Comparator<GridResource> {
	  private List<Comparator<GridResource>> listComparators;
	  
	    @SafeVarargs
	    public ResourceChainedComparator(Comparator<GridResource>... comparators) {
	        this.listComparators = Arrays.asList(comparators);
	    }
	 
	    @Override
	    public int compare(GridResource emp1, GridResource emp2) {
	        for (Comparator<GridResource> comparator : listComparators) {
	            int result = comparator.compare(emp1, emp2);
	            if (result != 0) {
	                return result;
	            }
	        }
	        return 0;
	    }
}
