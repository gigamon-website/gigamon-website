package gigamon.core.util;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;

/**
 * Assorted mix of helper methods.
 *
 * @author joelepps
 *
 */
public class ServiceUtils {

    private static final Logger log = LoggerFactory.getLogger(ServiceUtils.class);

    /**
     * Parses out a page's jcr:content node.
     * <p>
     * If jcr:content is not on the path, it's assumed path is to the page
     * itself and jcr:content is appended.
     * <p>
     * This method does not validate existence of returned path.
     *
     * @param path Path to evaluate
     * @return jcr:content path
     */
    public static String getJcrContentPath(String path) {
        if (StringUtils.isBlank(path)) return null;

        if (path.endsWith("jcr:content")) return path;

        int idx = path.indexOf("/jcr:content");
        if (idx >= 0) {
            return path.substring(0, idx+"/jcr:content".length());
        }

        idx = path.indexOf("/jcr:frozenNode");
        if (idx >= 0) {
            return path.substring(0, idx+"/jcr:frozenNode".length());
        }

        if (!path.endsWith("/")) path += "/";
        return path + "jcr:content";
    }

    /**
     * Append the html extension to a {@code url}.
     *
     * @param resolver ResourceResolver
     * @param url URL to evaluate
     * @return URL with extension (if needed)
     */
    public static String appendLinkExtension(ResourceResolver resolver, String url) {
        Resource resource = resolver.getResource(url);
        String result = url;
        if (resource != null) {
            Page page = resource.adaptTo(Page.class);
            if (page != null) {
                result = url + ".html";
            // } else {
            //     Asset asset = resource.adaptTo(Asset.class);
            //     if (asset != null) {
            //         if (asset.getMetadata().containsKey("dc:url")) {
            //             if (!asset.getMetadataValue("dc:url").isEmpty()) {
            //                 result = asset.getMetadataValue("dc:url");
            //             }
            //         }
            //     }
            }
        }

        log.trace("Append link extension: {} -> {}", url, result);
        return result;
    }

    /**
     * Pick the first non-null and non-empty string.
     *
     * @param strings Strings to coalesce
     * @return first non-null, non-empty string
     */
    public static String coalesce(String... strings) {
        if (strings == null || strings.length == 0) return null;
        for (String s : strings) {
            if (!StringUtils.isBlank(s)) return s;
        }
        return null;
    }

    /**
     * Concatenate multiple strings into one. Skips any null or empty strings.
     *
     * @param delimiter Delimeter string
     * @param strings Array of strings
     *
     * @return Concatonated string
     */
    public static String concat(String delimiter, String... strings) {
        if (strings == null || strings.length == 0) return null;
        if (StringUtils.isBlank(delimiter)) delimiter = ",";

        StringBuilder sb = new StringBuilder();
        for (String s : strings) {
            if (StringUtils.isNotBlank(s)) {
                sb.append(s).append(delimiter);
            }
        }

        if (sb.length() > 0) {
            //fence post, remove trailing delimiter
            return sb.substring(0, sb.length()-delimiter.length());
        } else {
            return null;
        }
    }

    /**
     * Checks if a {@code candidate} tag is a descendant of the {@code parent} tag.
     * {@code candidate} does not have to be an immediate child of {@code parent}.
     * <p>
     * If true, then returned value is the corresponding immediate child tag of {@code parent}.
     * If false, then returned value is null.
     * <p>
     * Example:
     * <br>
     * {@code parent: tagfamily:united-states}<br>
     * {@code candidate: tagfamily:united-states/california/san-francisco/tenderloin}<br>
     * {@code return: tagfamily:united-states/california}
     *
     * @param parent parent tag
     * @param candidate candidate child tag
     * @return child tag of parent or null
     */
    public static Tag isDescendantTag(Tag parent, Tag candidate) {
        if (parent == null || candidate == null) return null;

        String candidateId = candidate.getTagID();
        String parentId = parent.getTagID();

        if (candidateId.startsWith(parentId) && candidateId.length() != parentId.length()) {
            Tag currentCandidate = candidate;
            while (currentCandidate != null) {
                Tag parentCandidate = currentCandidate.getParent();

                if (parent.equals(parentCandidate)) {
                    return currentCandidate;
                }

                currentCandidate = parentCandidate;
            }
        }

        return null;
    }

    public static boolean isGhostNode(Resource r) {
        if (r == null) return false;
        return "wcm/msm/components/ghost".equals(r.getResourceType());
    }
    
    public static Page getHomePage(Page page) {
        if (page == null) return null;

        Resource contentResource = page.getContentResource();
        if (contentResource == null) {
            // skip and continue up
            return getHomePage(page.getParent());
        }

        if ("gigamon/components/structure/pageHome".equals(contentResource.getResourceType())) {
            return page;
        } else {
            return getHomePage(page.getParent());
        }
    }
    
    public static boolean isPreProd(SlingSettingsService slingSettingsService) {
    	Set<String> runmodes = slingSettingsService.getRunModes();
    	if(runmodes.contains("production") || runmodes.contains("prod")) {
    		return false;
    	} else {
    		return true;
    	}
    }
}
