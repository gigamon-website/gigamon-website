package gigamon.core.models;

public class AnalyticsData {
	protected DataPage page;
	
	protected AnalyticsData() {
		this.page = new DataPage();
	}
	
	protected static class DataPage {
		protected DataPageInfo pageInfo;
		protected DataPageCategory category;
		
		protected DataPage() {
			this.pageInfo = new DataPageInfo();
			this.category = new DataPageCategory();
		}
		
		protected static class DataPageInfo {
			protected String pageName;
			protected String onsiteSearchTerm;
		}
		
		protected static class DataPageCategory {
			protected String pageType;
			protected String primaryCategory;
			protected String subCategory1;
			protected String subCategory2;
		}
	}
}
