package gigamon.core.models.component;


import gigamon.core.models.ResourceCatalog;
import gigamon.core.models.ResourceCatalog.GridResource;
import gigamon.dialog.touch.heading.Heading;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Listener;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * ResourceGrid Component
 */
@Component(value = "ResourceGrid",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/resource-grid.html",
        tabs = {@Tab(title = "Resource Grid"), @Tab(title = "Grid Options")},
        listeners = {@Listener (name = "afterinsert", value = "REFRESH_PAGE")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ResourceGrid extends ComponentSlingModel{

	//Tab1 - Resource grid
	@DialogField(fieldLabel = "Load More Text", name = "./loadMoreText", ranking = 1, fieldDescription="Text for the load more button")
	@TextField
	@Inject @ValueMapValue
	private String loadMoreText;
	
	public String getLoadMoreText() {
		return loadMoreText;
	}
	
	@DialogField(ranking = 2)
    @Heading(level = 4, text = "Empty Results Options")
    private final String heading1 = null;
	
	@DialogField(fieldLabel = "No Results Title", name = "./noResultsTitle", ranking = 5, defaultValue = "No Results")
	@TextField
	@Inject @ValueMapValue @Default(values = "No Results")
	private String noResultsTitle;
	
	public String getNoResultsTitle() {
		return noResultsTitle;
	}

    @DialogField(fieldLabel = "No Results Text", name = "./noResultsText", ranking = 7, defaultValue = "Try again ...")
	@TextField
	@Inject @ValueMapValue @Default(values = "Try again ...")
	private String noResultsText;
	
	public String getNoResultsText() {
		return noResultsText;
	}
    
    //Tab 2- Grid Options
    
    @DialogField(fieldLabel = "Root Path", name = "./resourceRoot", tab = 2, ranking = 1, defaultValue = "/content/gigamon/en", fieldDescription = "The child page(s), under this path, shouldn't have 'Home Page' template.")
    @PathField(rootPath="/content")
    @Inject @ValueMapValue @Default(values = "/content/gigamon/en")
    private String resourceRoot;
    
    public String getResourceRoot(){
    	return resourceRoot;
    }

    @DialogField(fieldLabel = "CTA Text", name = "./ctaText", tab = 2, ranking = 2, defaultValue = "CTA Text")
	@TextField
	@Inject @ValueMapValue
	private String ctaText;
	
	public String getCtaText() {
		return ctaText;
	}

	//resource Grid model
	ResourceCatalog catalog = new ResourceCatalog();
	
	@Override
	public void postConstruct() throws Exception {
		String resourceRootRaw = getProperties().get("resourceRoot", String.class);
		Resource resourceRoot = getResourceResolver().getResource(resourceRootRaw);
		
		if(resourceRoot != null) {
			buildCatalog(resourceRoot);
			catalog.sortResources();
		}
	}
	
	private void buildCatalog(Resource root) {
		for(Resource gridResourceCandidateResource : root.getChildren()) {
			if(!"cq:Page".equals(gridResourceCandidateResource.getValueMap().get("jcr:primaryType"))) {
				continue; //skip jcr:content and anything under it
			}
			
			GridResource gridResource = gridResourceCandidateResource.adaptTo(GridResource.class);
			if(gridResource != null) {
				catalog.addResource(gridResource);
			}
			
			buildCatalog(gridResourceCandidateResource);
		}
	}
	
	public ResourceCatalog getResources() {
		return catalog;
	} 

}

