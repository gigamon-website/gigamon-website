package gigamon.core.models.component.traits.image;

import gigamon.core.util.ImageRendition;

/**
 * Version of "Rendition Support" mixin for *Mobile* Image.
 * <p>
 * Any {@link gigmaon.core.models.component.traits.ComponentTrait}
 * that uses the Sightly {@code image.html}
 * template or renders an image on the page must implement this mixin
 * somewhere in the inheritance tree.
 * <p>
 * This mixin provides the bare minimum required fields necessary for rendering
 * an image on the page. It provides the image URL for the supported
 * image rendition sizes.
 * <p>
 * Implementing classes must implement {@link #getMobileImageFileReference()}.
 *
 * @author joelepps
 *
 */
public interface MobileImageRenditionSupportTrait {

    /**
     * Returns the raw JCR path for the DAM image.
     * <p>
     * Rendition methods use this path as the basis for the rendition URLs.
     * <p>
     * Components should never use this method for the {@code <img>} {@code src} attribute.
     * Instead the rendition methods must be used.
     * <p>
     * This method can be used as an existence check for the component.
     * Non-null value returned means author has configured an image.
     *
     * @return JCR path of DAM image.
     */
    String getMobileImageFileReference();

	/*
	 * START RENDITION METHODS
	 * These will be dynamically invoked from Sightly.
	 */

    // ORIGINAL

    default String getMobileImageOriginal() {
        String fileRef = getMobileImageFileReference();
        return ImageRendition.ORIGINAL.getRendition(fileRef);
    }

    // JPEG

    /**
     * @return Path for Full Width x2 Desktop rendition
     */
    default String getMobileImageFWX2D() {
        String fileRef = getMobileImageFileReference();
        return ImageRendition.FULL_WIDTH_X2_DESKTOP.getRendition(fileRef);
    }

    /**
     * @return Path for Full Width x1 Desktop rendition
     */
    default String getMobileImageFWX1D() {
        String fileRef = getMobileImageFileReference();
        return ImageRendition.FULL_WIDTH_x1_DESKTOP.getRendition(fileRef);
    }

    /**
     * @return Path for Full Width x2 Mobile rendition
     */
    default String getMobileImageFWX2M() {
        String fileRef = getMobileImageFileReference();
        return ImageRendition.FULL_WIDTH_x2_MOBILE.getRendition(fileRef);
    }

    /**
     * @return Path for Full Width x1 Mobile rendition
     */
    default String getMobileImageFWX1M() {
        String fileRef = getMobileImageFileReference();
        return ImageRendition.FULL_WIDTH_X1_MOBILE.getRendition(fileRef);
    }

    /**
     * @return Path for Half Width large rendition
     */
    default String getMobileImageHWL() {
        String fileRef = getMobileImageFileReference();
        return ImageRendition.HALF_WIDTH_LARGE.getRendition(fileRef);
    }

    /**
     * @return Path for Half Width large small
     */
    default String getMobileImageHWS() {
        String fileRef = getMobileImageFileReference();
        return ImageRendition.HALF_WIDTH_SMALL.getRendition(fileRef);
    }

	/*
	 * END RENDITION METHODS
	 */

}
