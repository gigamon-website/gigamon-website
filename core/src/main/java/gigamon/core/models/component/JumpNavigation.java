package gigamon.core.models.component;


import gigamon.dialog.classic.multifield.MultiCompositeField;

import java.util.List;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import gigamon.core.models.data.JumpNav;
import javax.inject.Inject;

/**
 * JumpNavigation Component
 */
@Component(value = "JumpNavigation",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/jump-navigation.html",
        tabs = {@Tab(title = "Jump Navigation")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class JumpNavigation extends ComponentSlingModel {
	
	 @DialogField(fieldLabel = "Main Title", name = "./maintitle", ranking = 1,
	            fieldDescription = "main text displayed in component")
	    @TextField
	    @Inject @ValueMapValue
	    private String maintitle;
	 

		public String getMaintitle() {
			return maintitle;
		}
	 

	 @DialogField(fieldLabel = "Navigation links", name = "./links", ranking = 5, tab=1)
	   @MultiCompositeField
	   @Inject @ChildResource(name = "links")
	   private List<JumpNav> links;
	
	 public List<JumpNav> getLinks() {
			return links;
		}
	 
	
}
