package gigamon.core.models.component.traits.background.fields;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.ComponentTrait;

/**
 * Contains vertical spacing options that apply at all screen sizes.
 */
public interface BackgroundYUniversalSpacingTrait extends ComponentTrait {

    String TOP_SPACING = "backgroundTopSpacing";
    String BOTTOM_SPACING = "backgroundBottomSpacing";

    String NONE = "_empty_";

    String TOP_SPACING_XS = "vert-pad-top-xs"; // 15
    String TOP_SPACING_SM = "vert-pad-top-sm"; // 30
    String TOP_SPACING_MD = "vert-pad-top-md"; // 60

    String BOTTOM_SPACING_XS = "vert-pad-bottom-xs"; // 15
    String BOTTOM_SPACING_SM = "vert-pad-bottom-sm"; // 30
    String BOTTOM_SPACING_MD = "vert-pad-bottom-md"; // 60

    interface DialogFields {

        @DialogField(fieldLabel = "Top Spacing", name = "./"+TOP_SPACING, ranking = 3)
        @Selection(type="select", options = {
                @Option(text = "None", value = NONE),
                @Option(text = "XS (15)", value = TOP_SPACING_XS),
                @Option(text = "SM (30)", value = TOP_SPACING_SM),
                @Option(text = "MD (60)", value = TOP_SPACING_MD),
        })
        void topSpacing();

        @DialogField(fieldLabel = "Bottom Spacing", name = "./"+BOTTOM_SPACING, ranking = 4)
        @Selection(type="select", options = {
                @Option(text = "None", value = NONE),
                @Option(text = "XS (15)", value = BOTTOM_SPACING_XS),
                @Option(text = "SM (30)", value = BOTTOM_SPACING_SM),
                @Option(text = "MD (60)", value = BOTTOM_SPACING_MD),
        })
        void bottomSpacing();

    }

    default String getBackgroundTopSpacing() {
        String value = getProperties().get(TOP_SPACING, String.class);
        return NONE.equals(value) ? null : value;
    }

    default String getBackgroundBottomSpacing() {
        String value = getProperties().get(BOTTOM_SPACING, String.class);
        return NONE.equals(value) ? null : value;
    }

}
