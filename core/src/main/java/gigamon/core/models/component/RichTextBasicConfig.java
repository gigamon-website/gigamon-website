package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.RichTextEditor;
import com.citytechinc.cq.component.annotations.widgets.rte.*;

/**
 * This class contains rich text configurations that are to be referenced by other components.
 * <p>
 * A component wishing to use this functionality must implement the {@link gigamon.core.models.component.traits.richtext.RichTextFieldTrait}
 * and add the following @Component annotation fields.
 *
 * <pre>
 * inPlaceEditingActive = true,
 * inPlaceEditingEditorType = "text",
 * inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-basic-config/dialog/items/tabs/items/RichTextBasicConfigurations/items/basicRichText",
 * </pre>
 *
 * @author joelepps
 *         3/17/16
 */
@Component(value = "Rich Text Basic Configurations", group = ComponentSlingModel.COMPONENT_GROUP_HIDDEN)
public class RichTextBasicConfig {

    @DialogField(fieldLabel = "IGNORE", name = "./IGNORE")
    @RichTextEditor(
            paraformat={@ParaFormat(description = "Paragraph", tag = "p")},
            edit={@Edit},
            misctools={@MiscTools},
            justify={@Justify(justifycenter = false, justifyleft = false, justifyright = false)},
            lists={@Lists(indent = false, ordered = false, outdent = false, unordered = false)},
            styles={}
    )
    void basicRichText() {

    }

}
