package gigamon.core.models.component;

import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

import com.citytechinc.cq.component.annotations.*;
import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.widgets.TextArea;

import gigamon.core.models.component.traits.background.BackgroundYTraits;
import gigamon.dialog.touch.collapsible.Collapsible;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Html Component.
 * <p>
 * Use of this component is not recommended as it crosses the line between code and content.
 */
@Component(value = "HTML", 
    group = ComponentSlingModel.COMPONENT_GROUP_MISC, 
    helpPath = "/editor.html/content/gigamon-components/html.html", 
    disableTargeting = true)
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Html extends ComponentSlingModel implements BackgroundYTraits {

    @DialogField(fieldLabel = "HTML", name = "./htmlContent", ranking = 1)
    @TextArea
    @Inject @ValueMapValue
    private String htmlContent;

    public String getHtmlContent() {
        return htmlContent;
    }


    @DialogField(ranking = 10)
    @Collapsible(title = "Background")
    public final DialogFields background = null;

}