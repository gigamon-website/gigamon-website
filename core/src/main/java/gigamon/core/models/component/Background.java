package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

/**
 * Stand-alone background component providing author's ability to add a background that may encompass
 * multiple components at once, or provide a background to a component without built-in background
 * support.
 * <p>
 * Use of this component should be rare since most every component should already include background
 * support.
 *
 * @author joelepps
 *         3/17/16
 */
@Component(value = "Background",
        group = ComponentSlingModel.COMPONENT_GROUP_MISC,
        disableTargeting = true)
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Background extends ComponentSlingModel implements BackgroundXYTraits {

    @DialogField
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;

}
