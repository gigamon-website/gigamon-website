package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.component.traits.tabs.MultiTabTrait;
import gigamon.dialog.classic.multifield.MultiCompositeField;
import java.util.List;
import javax.inject.Inject;

/**
 * MultiTabs Component
 */

@Component(value = "MultiTabs", group = ComponentSlingModel.COMPONENT_GROUP_CONTENT, disableTargeting = true, helpPath = "/editor.html/content/gigamon-components/multi-tabs.html", tabs = {
		@Tab(title = "MultiTabs"), @Tab(title = "Background") })
@Model(adaptables = { Resource.class,
		SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MultiTabs extends ComponentSlingModel implements BackgroundXYTraits {

	public static final String ALIGN_LEFT = "left";
	public static final String ALIGN_CENTER = "center";

	public static final String TAB_ALIGN_LEFT = "tab-left";
	public static final String TAB_ALIGN_CENTER = "tab-center";

	public static final String H1 = "h1";
	public static final String H2 = "h2";
	public static final String H3 = "h3";

	public static final String LIGHT = "light";
	public static final String BOLD = "bold";
	public static final String MEDIUM = "medium";
	public static final String EXTRA_BOLD = "extra-bold";

	@DialogField(fieldLabel = "Title", name = "./title", ranking = 1, tab = 1)
	@TextField
	@Inject
	@ValueMapValue
	private String title;

	public String getTitle() {
		return title;
	}

	@DialogField(fieldLabel = "Title Size", name = "./titleSize", tab = 1, ranking = 2, defaultValue = H2, fieldDescription = "There should only ever be one H1 on a page, at the very top of the page")
	@Selection(type = "select", options = { @Option(text = "Medium", value = H2), @Option(text = "Small", value = H3),
			@Option(text = "H1-large", value = H1), })
	@Inject
	@ValueMapValue
	private String titleSize;

	public String getTitleSize() {
		return titleSize;
	}

	@DialogField(fieldLabel = "Font Weight", name = "./fontWeight", tab = 1, ranking = 2, defaultValue = BOLD)
	@Selection(type = "select", options = { @Option(text = "Bold", value = BOLD),
			@Option(text = "Light", value = LIGHT), @Option(text = "Medium", value = MEDIUM),
			@Option(text = "Extra bold", value = EXTRA_BOLD), })
	@Inject
	@ValueMapValue
	private String fontWeight;

	public String getFontWeight() {
		return fontWeight;
	}

	@DialogField(fieldLabel = "Alignment", name = "./textAlignment", tab = 1, ranking = 3, defaultValue = ALIGN_LEFT)
	@Selection(type = "select", options = { @Option(text = "Left", value = ALIGN_LEFT),
			@Option(text = "Center", value = ALIGN_CENTER), })
	@Inject
	@ValueMapValue
	private String textAlignment;

	public String getTextAlignment() {
		return textAlignment;
	}

	@DialogField(fieldLabel = "Color", name = "./textColor", ranking = 4, tab = 1)
	@Selection(type = "select", options = { @Option(text = "Dark", value = "dark-text"),
			@Option(text = "Light", value = "white-text") })
	@Inject
	@ValueMapValue
	private String textColor;

	public String getTextColor() {
		return textColor;
	}

	@DialogField(fieldLabel = "Tabs Text alignment", name = "./tabsAlignment", tab = 1, ranking = 5, defaultValue = TAB_ALIGN_LEFT)
	@Selection(type = "select", options = { @Option(text = "Left", value = TAB_ALIGN_LEFT),
			@Option(text = "Center", value = TAB_ALIGN_CENTER),

	})
	@Inject
	@ValueMapValue
	private String tabsAlignment;

	public String getTabsAlignment() {
		return tabsAlignment;
	}

	@DialogField(fieldLabel = "Tabs", name = "./tabs", ranking = 6, tab = 1)
	@MultiCompositeField(maxLimit = 4)
	@Inject
	@ChildResource(name = "tabs")
	private List<MultiTabTrait> tabs;

	public List<MultiTabTrait> getTabs() {
		return tabs;
	}

	@DialogField(fieldLabel = "Active Tab Color", name = "./activecolor", tab = 1, ranking = 6, defaultValue = "default-active")

	@Selection(type = "select", options = { @Option(text = "Default", value = "default-active"),
			@Option(text = "Orange", value = "orange-active"), @Option(text = "Aqua", value = "aqua-active"),
			@Option(text = "Blue", value = "blue-active"), @Option(text = "Purple", value = "purple-active"),
			@Option(text = "Teal", value = "teal-active"), @Option(text = "White", value = "white-active"),
			@Option(text = "Grey", value = "grey-active") })
	@Inject
	@ValueMapValue
	private String activecolor;

	public String getActivecolor() {
		return activecolor;
	}

	@DialogField(fieldLabel = "Tab Color", name = "./tabcolor", tab = 1, ranking = 6, defaultValue = "default-tab")
	@Selection(type = "select", options = { @Option(text = "Default", value = "default-tab"),
			@Option(text = "Orange", value = "orange-tab"), @Option(text = "Aqua", value = "aqua-tab"),
			@Option(text = "Blue", value = "blue-tab"), @Option(text = "Purple", value = "purple-tab"),
			@Option(text = "Teal", value = "teal-tab"), @Option(text = "White", value = "white-tab"),
			@Option(text = "Grey", value = "grey-tab") })
	@Inject
	@ValueMapValue
	private String tabcolor;

	public String getTabcolor() {
		return tabcolor;
	}

	@DialogField(tab = 2)
	@DialogFieldSet
	public final BackgroundXYTraits.DialogFields background = null;

}
