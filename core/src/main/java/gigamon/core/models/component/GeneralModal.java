package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * GeneralModal Component
 */
@Component(value = "GeneralModal",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/general-modal.html",
        tabs = {@Tab(title = "GeneralModal")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GeneralModal extends ComponentSlingModel {

    @DialogField(fieldLabel = "Modal ID", name = "./modalID", ranking = 1,
            fieldDescription = "Modal ID is the link of the CTA. For example: if Modal ID is 'cta-modal-1' then CTA link should be '#cta-modal-1'.")
    @TextField
    @Inject @ValueMapValue
    private String modalID;

    public String getModalID() {
        return modalID;
    }
}
