package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.*;
import gigamon.core.models.data.SimpleLink;
import gigamon.core.util.LocalizedText;
import gigamon.dialog.classic.multifield.MultiCompositeField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.util.List;

/**
 * Navbar Component. Should be placed on the home page in an iparsys.
 */
@Component(value = "Navbar",
        group = ComponentSlingModel.COMPONENT_GROUP_MISC,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/navbar.html",
        tabs = {@Tab(title = "Navbar"), @Tab(title = "DD1"), @Tab(title = "DD2")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Navbar extends ComponentSlingModel {

    private static final String MODE_FIXED = "navbar-fixed-top";
    private static final String MODE_STATIC = "navbar-static-top";

    private LocalizedText localizedText;

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();
        localizedText = new LocalizedText(this);
    }

    /*
     * TAB 1
     */

    @DialogField(fieldLabel = "Home Page", name = "./homePage", ranking = 1)
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String homePage;

    @DialogField(fieldLabel = "Logo Path", name = "./logoPath", ranking = 2)
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String logoPath;

    @DialogField(fieldLabel = "Search Page", name = "./searchPage", ranking = 3)
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String searchPage;

    @DialogField(fieldLabel = "Mode", name = "./navbarMode", ranking = 5)
    @Selection(type="select", options = {
            @Option(text = "Fixed (Sticky)", value = MODE_FIXED),
            @Option(text = "Static", value = MODE_STATIC),
    })
    @Inject @ValueMapValue @Default(values = MODE_FIXED)
    private String navbarMode;

    /*
     * TAB 2
     */

    @DialogField(fieldLabel = "Title", name = "./dd1Title", ranking = 1, tab = 2)
    @TextField
    @Inject @ValueMapValue
    private String dd1Title;

    @DialogField(fieldLabel = "Links", name = "./dd1Links", ranking = 2, tab = 2)
    @MultiCompositeField
    @Inject @ChildResource(name = "dd1Links")
    private List<SimpleLink> dd1Links;

    /*
     * TAB 3
     */

    @DialogField(fieldLabel = "Title", name = "./dd2Title", ranking = 1, tab = 3)
    @TextField
    @Inject @ValueMapValue
    private String dd2Title;

    @DialogField(fieldLabel = "Links", name = "./dd2Links", ranking = 2, tab = 3)
    @MultiCompositeField
    @Inject @ChildResource(name = "dd2Links")
    private List<SimpleLink> dd2Links;

    public String getNavbarMode() {
        return navbarMode;
    }

    public String getHomePage() {
        return homePage + ".html";
    }

    public String getLogoPath() {
        return logoPath;
    }

    public String getSearchPage() {
        return searchPage + ".html";
    }

    public String getDd1Title() {
        return dd1Title;
    }

    public List<SimpleLink> getDd1Links() {
        return dd1Links;
    }

    public String getDd2Title() {
        return dd2Title;
    }

    public List<SimpleLink> getDd2Links() {
        return dd2Links;
    }

    public LocalizedText getLocalizedText() {
        return localizedText;
    }
}
