package gigamon.core.models.component;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.NumberField;

/**
 * FilterContainer Component
 */
@Component(value = "FilterContainer",
        group = ComponentSlingModel.COMPONENT_GROUP_HIDDEN,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/filter-container.html",
        tabs = {@Tab(title = "Filter Container")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FilterContainer extends ComponentSlingModel {

   @DialogField(fieldLabel = "Number of Filters", name = "./numFilters", ranking = 1, defaultValue = "3", fieldDescription = "Max = 5 and Min = 1")
    @NumberField
	@Inject @ValueMapValue @Default(values = "3")
	private long numFilters;
	
	public long getNumFilters() {
		return numFilters;
	}
	
	public List<String> getItemIds(){
		List<String> itemIds = new ArrayList<>();
		for(int i=0; i<numFilters; i++){
			itemIds.add("filter"+i);
		}
		return itemIds;
	}
}
