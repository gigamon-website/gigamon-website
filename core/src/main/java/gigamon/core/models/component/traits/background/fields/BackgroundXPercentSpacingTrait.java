package gigamon.core.models.component.traits.background.fields;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.ComponentTrait;

public interface BackgroundXPercentSpacingTrait extends ComponentTrait {

    String HORIZONTAL_SPACING = "backgroundHorizontalSpacing";

    String NONE = "_empty_";

    String HORIZONTAL_SPACING_0 = "adjust-column-width-max";
    String HORIZONTAL_SPACING_1 = "adjust-column-width-70";
    String HORIZONTAL_SPACING_2 = "adjust-column-width-90";

    interface DialogFields {

        @DialogField(fieldLabel = "Horizontal Spacing", name = "./"+HORIZONTAL_SPACING, ranking = 10, defaultValue = HORIZONTAL_SPACING_0)
        @Selection(type="select", options = {
                @Option(text = "100% (max-width)", value = HORIZONTAL_SPACING_0),
                @Option(text = "100% (Fluid)", value = NONE),
                @Option(text = "90%", value = HORIZONTAL_SPACING_2),
                @Option(text = "70%", value = HORIZONTAL_SPACING_1),
        })
        void horizontalSpacing();

    }

    default String getBackgroundHorizontalSpacing() {
        String value = getProperties().get(HORIZONTAL_SPACING, String.class);
        return NONE.equals(value) ? null : value;
    }

}
