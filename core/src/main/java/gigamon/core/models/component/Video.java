package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Listener;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import gigamon.core.models.component.traits.analytics.AnalyticsTrait;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.component.traits.image.PrimaryImageWithAltTextTrait;
import gigamon.core.models.component.traits.video.VideoTrait;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import javax.inject.Inject;

/**
 * Video Component that support YouTube.
 */
@Component(value = "Video",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/video.html",
        tabs = {@Tab(title = "Video"), @Tab(title = "Image"), @Tab(title = "Background")},
        listeners = {
            @Listener(name = "afteredit", value = "REFRESH_PAGE")
        })
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Video extends ComponentSlingModel implements PrimaryImageWithAltTextTrait, VideoTrait, BackgroundXYTraits, AnalyticsTrait {

    @DialogField
    @DialogFieldSet
    public final VideoTrait.DialogFields video = null;

    @DialogField(tab = 3)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;

    @DialogField(tab = 2)
    @DialogFieldSet
    public final PrimaryImageWithAltTextTrait.DialogFields image = null;


}
