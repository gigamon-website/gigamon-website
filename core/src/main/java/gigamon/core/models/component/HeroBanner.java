package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.analytics.AnalyticsTrait;
import gigamon.core.models.component.traits.cta.CtaTrait;
import gigamon.core.models.component.traits.cta.CtaWithBtnStyleTrait;
import gigamon.core.models.component.traits.image.PrimaryImageFileTrait;
import gigamon.core.models.component.traits.richtext.RichTextFieldTrait;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import gigamon.dialog.touch.collapsible.Collapsible;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.util.UUID;

/**
 * HeroBanner Component
 */
@Component(value = "Hero Banner",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/hero-banner.html",
        inPlaceEditingEditorType = "text",
        inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-config/dialog/items/tabs/items/RichTextConfigurations/items/standardRichText",
        tabs = {@Tab(title = "Hero Banner"), @Tab(title = "Image"), @Tab(title = "CTA"), @Tab(title = "Analytics")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeroBanner extends ComponentSlingModel implements PrimaryImageFileTrait, CtaWithBtnStyleTrait, RichTextFieldTrait, AnalyticsTrait {

    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_CENTER = "center";

    public static final String TEXT_DARK = "dark-text";
    public static final String TEXT_WHITE = "white-text";

    public static final String NONE = "_empty_";
    public static final String TITLE_SMALL = "h1-small";

    @DialogField(fieldLabel = "Title", name = "./title", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String title;
    
    @DialogField(fieldLabel = "Title Color", name = "./titleColor", ranking = 2, fieldDescription = "Applies to Title text only")
    @Selection(type="select", options = {
            @Option(text = "White", value = TEXT_WHITE),
            @Option(text = "Dark", value = TEXT_DARK),
    })
    @Inject @ValueMapValue
    private String titleColor;

    @DialogField(fieldLabel = "Title Size", name = "./titleSize", ranking = 3)
    @Selection(type="select", options = {
            @Option(text = "Normal", value = NONE),
            @Option(text = "Small", value = TITLE_SMALL),
    })
    @Inject @ValueMapValue
    private String titleSize;

    @DialogField(fieldLabel = "Super Title", name = "./superTitle", ranking = 4)
    @TextField
    @Inject @ValueMapValue
    private String superTitle;

    @DialogField(fieldLabel = "Text Color", name = "./heroBannerTextColor", ranking = 5, fieldDescription = "Applies to Super Title and the RTE text only")
    @Selection(type="select", options = {
            @Option(text = "White", value = TEXT_WHITE),
            @Option(text = "Dark", value = TEXT_DARK),
    })
    @Inject @ValueMapValue
    private String heroBannerTextColor;

    @DialogField(fieldLabel = "Alignment", name = "./heroBannerAlignment", ranking = 6)
    @Selection(type="select", options = {
            @Option(text = "Left", value = ALIGN_LEFT),
            @Option(text = "Center", value = ALIGN_CENTER),
    })
    @Inject @ValueMapValue
    private String heroBannerAlignment;


    public static final String HEIGHT_LG = "lg-height";
    public static final String HEIGHT_MD = "md-height";

    @DialogField(fieldLabel = "Height Variation", name = "./heightVar", ranking = 7)
    @Selection(type="select", options = {
            @Option(text = "Large", value = HEIGHT_LG),
            @Option(text = "Medium", value = HEIGHT_MD),
    })
    @Inject @ValueMapValue
    private String heightVar;


    @DialogField(fieldLabel = "Hide Background Image in Mobile", name = "./heroBannerHideImageMobile", ranking = 8, value = "true")
    @CheckBoxTouch(text = "Hide Background Image in Mobile")
    @Inject @ValueMapValue
    private String heroBannerHideImageMobile;

    public String getTitle() {
        return title;
    }
    
    public String getTitleColor()
    {
    	return titleColor;
    }
    public String getSuperTitle() {
        return superTitle;
    }

    public String getTitleSize() {
        return NONE.equals(titleSize) ? null : titleSize;
    }

    public String getHeroBannerTextColor() {
        return heroBannerTextColor;
    }

    public String getHeroBannerAlignment() {
        return heroBannerAlignment;
    }

    public String getHeightVar() {
        return heightVar;
    }

    public boolean getHeroBannerHideImageMobile() {
        return BooleanUtils.toBoolean(heroBannerHideImageMobile);
    }

    @DialogField(tab = 2)
    @DialogFieldSet
    public final PrimaryImageFileTrait.DialogFields image = null;

    @DialogField(tab = 3)
    @DialogFieldSet
    public final CtaWithBtnStyleTrait.DialogFields cta = null;

    // @DialogField(tab = 3)
    // @DialogFieldSet
    // public final CtaTrait.DialogFields cta = null;

    @DialogField(tab = 4)
    @DialogFieldSet
    public final AnalyticsTrait.DialogFields analytics = null;

}
