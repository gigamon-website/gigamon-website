package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.*;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.background.fields.BackgroundColorTrait;
import gigamon.core.models.component.traits.background.fields.BackgroundVisibilityTrait;
import gigamon.core.models.component.traits.background.fields.BackgroundXDesktopSpacingTrait;
import gigamon.core.models.component.traits.background.fields.BackgroundYDesktopSpacingTrait;
import gigamon.dialog.touch.heading.Heading;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * The child "Column" component for the {@link ResponsiveLayout} component.
 * <p>
 * Provides the author with layout options pertaining to the individual column.
 */
@Component(value = "Responsive Layout Column",
    group = ComponentSlingModel.COMPONENT_GROUP_HIDDEN,
    path = "responsive-layout",
    name = "column",
    disableTargeting = true,
    isContainer = true,
    helpPath = "/editor.html/content/gigamon-components/responsive-layout.html",
    tabs = {
        @Tab(title = "Column"),
        @Tab(title = "Background"),
        @Tab(title = "Column (XS)"),
        @Tab(title = "Column (SM)"),
        @Tab(title = "Column (LG)"),
    },
    listeners = {
        @Listener(name = "afteredit", value = "REFRESH_PAGE")
    })
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ResponsiveLayoutColumn extends ComponentSlingModel
    implements BackgroundColorTrait, BackgroundXDesktopSpacingTrait, BackgroundYDesktopSpacingTrait, BackgroundVisibilityTrait {

    private static final String INHERIT = "inherit";
    private static final String UNDEFINED = "_undefined_";

    private String defaultColumnSize;
    private String gridStartingAtBreakpoint = "md";

    /*
     * TAB 1
     */

    @DialogField(ranking = 1)
    @Heading(level = 2, text = "Basic")
    private final String heading0 = null;

    @DialogField(ranking = 2)
    @Heading(level = 4, text = "This tab controls the column layout starting at the MD breakpoint.")
    private final String heading1 = null;

    @DialogField(fieldLabel = "Size", name = "./columnSize", ranking = 3)
    @Selection(type="select", options = {
        @Option(text = "Inherit Default", value = INHERIT),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
        @Option(text = "12 / 12", value = "12"),
    })
    @Inject @ValueMapValue
    private String columnSize;

    @DialogField(ranking = 10)
    @Heading(level = 2, text = "Advanced")
    private final String heading2 = null;

    @DialogField(fieldLabel = "Offset", name = "./columnOffset", ranking = 11,
        fieldDescription = "Amount this columns is offset from its normal position.")
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = "0")
    private String columnOffset;

    @DialogField(fieldLabel = "Push", name = "./pushMd", ranking = 12,
        fieldDescription = "Amount this columns is pushed from its normal position.")
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = "0")
    private String pushMd;

    @DialogField(fieldLabel = "Pull", name = "./pullMd", ranking = 13,
        fieldDescription = "Amount this columns is pulled from its normal position.")
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = "0")
    private String pullMd;

    /*
     * TAB 2
     */

    @DialogField(tab = 2, ranking = 1)
    @DialogFieldSet
    private final BackgroundColorTrait.DialogFields colorFields = null;

    @DialogField(tab = 2, ranking = 10)
    @Heading(level = 2, text = "Advanced")
    private final String headingAdvanced = null;

    @DialogField(tab = 2, ranking = 11)
    @DialogFieldSet
    private final BackgroundXDesktopSpacingTrait.DialogFields xPaddingFields = null;

    @DialogField(tab = 2, ranking = 12)
    @DialogFieldSet
    private final BackgroundYDesktopSpacingTrait.DialogFields ySpacingFields = null;

    @DialogField(tab = 2, ranking = 13)
    @DialogFieldSet
    private final BackgroundVisibilityTrait.DialogFields visibilityFields = null;

    /*
     * TAB 3
     */

    @DialogField(ranking = 1, tab = 3)
    @Heading(level = 2, text = "Advanced")
    private final String heading0c = null;

    @DialogField(ranking = 2, tab = 3)
    @Heading(level = 4, text = "You should rarely, if ever, need to make updates to this tab.")
    private final String heading11c = null;

    @DialogField(ranking = 2.5, tab = 3)
    @Heading(level = 4, text = "This tab controls the column layout starting at the XS breakpoint.")
    private final String heading1c = null;

    @DialogField(fieldLabel = "Size", name = "./columnSizeXs", ranking = 3, tab = 3)
    @Selection(type="select", options = {
        @Option(text = "Default (12 / 12)", value = "12"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = "12")
    private String columnSizeXs;

    @DialogField(fieldLabel = "Offset", name = "./columnOffsetXs", ranking = 11, tab = 3)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String columnOffsetXs;

    @DialogField(fieldLabel = "Push", name = "./pushXs", ranking = 12, tab = 3)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String pushXs;

    @DialogField(fieldLabel = "Pull", name = "./pullXs", ranking = 13, tab = 3)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String pullXs;

    /*
     * TAB 4
     */

    @DialogField(ranking = 1, tab = 4)
    @Heading(level = 2, text = "Advanced")
    private final String heading0d = null;

    @DialogField(ranking = 2, tab = 4)
    @Heading(level = 4, text = "You should rarely, if ever, need to make updates to this tab.")
    private final String heading11d = null;

    @DialogField(ranking = 2.5, tab = 4)
    @Heading(level = 4, text = "This tab controls the column layout starting at the SM breakpoint.")
    private final String heading1d = null;

    @DialogField(fieldLabel = "Size", name = "./columnSizeSm", ranking = 3, tab = 4)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
        @Option(text = "12 / 12", value = "12"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String columnSizeSm;

    @DialogField(fieldLabel = "Offset", name = "./columnOffsetSm", ranking = 11, tab = 4)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String columnOffsetSm;

    @DialogField(fieldLabel = "Push", name = "./pushSm", ranking = 12, tab = 4)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String pushSm;

    @DialogField(fieldLabel = "Pull", name = "./pullSm", ranking = 13, tab = 4)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String pullSm;

    /*
     * TAB 5
     */

    @DialogField(ranking = 1, tab = 5)
    @Heading(level = 2, text = "Advanced")
    private final String heading0e = null;

    @DialogField(ranking = 2, tab = 5)
    @Heading(level = 4, text = "You should rarely, if ever, need to make updates to this tab.")
    private final String heading11e = null;

    @DialogField(ranking = 2.5, tab = 5)
    @Heading(level = 4, text = "This tab controls the column layout starting at the LG breakpoint.")
    private final String heading1e = null;

    @DialogField(fieldLabel = "Size", name = "./columnSizeLg", ranking = 3, tab = 5)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
        @Option(text = "12 / 12", value = "12"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String columnSizeLg;

    @DialogField(fieldLabel = "Offset", name = "./columnOffsetLg", ranking = 11, tab = 5)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String columnOffsetLg;

    @DialogField(fieldLabel = "Push", name = "./pushLg", ranking = 12, tab = 5)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String pushLg;

    @DialogField(fieldLabel = "Pull", name = "./pullLg", ranking = 13, tab = 5)
    @Selection(type="select", options = {
        @Option(text = "Default", value = UNDEFINED),
        @Option(text = "0 / 12", value = "0"),
        @Option(text = "1 / 12", value = "1"),
        @Option(text = "2 / 12", value = "2"),
        @Option(text = "3 / 12", value = "3"),
        @Option(text = "4 / 12", value = "4"),
        @Option(text = "5 / 12", value = "5"),
        @Option(text = "6 / 12", value = "6"),
        @Option(text = "7 / 12", value = "7"),
        @Option(text = "8 / 12", value = "8"),
        @Option(text = "9 / 12", value = "9"),
        @Option(text = "10 / 12", value = "10"),
        @Option(text = "11 / 12", value = "11"),
    })
    @Inject @ValueMapValue @Default(values = UNDEFINED)
    private String pullLg;

    public void setDefaultColumnSize(String defaultColumnSize) throws Exception {
        this.defaultColumnSize = defaultColumnSize;
        postConstruct();
    }

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();

        if (StringUtils.isBlank(columnSize) || INHERIT.equals(columnSize)) {
            columnSize = defaultColumnSize;
        }
        gridStartingAtBreakpoint = getResource().getParent().getValueMap().get(ResponsiveLayout.GRID_STARTING_AT_BREAKPOINT, "md");
    }

    @Override
    public String getHorizontalPaddingBreakpoint() {
        return gridStartingAtBreakpoint;
    }

    @Override
    public String getVerticalPaddingBreakpoint() {
        return gridStartingAtBreakpoint;
    }

    /**
     * Classes that {@link ResponsiveLayout} will use. The column component itself will not use these.
     *
     * @return column classes
     */
    public String getClassesForUseByResponsiveLayout() {
        StringBuilder sb = new StringBuilder();

        sb.append(" col-md-").append(columnSize);

        if (!UNDEFINED.equals(columnOffset)) sb.append(" col-md-offset-").append(columnOffset);
        if (!UNDEFINED.equals(pullMd)) sb.append(" col-md-pull-").append(pullMd);
        if (!UNDEFINED.equals(pushMd)) sb.append(" col-md-push-").append(pushMd);

        String visibilityClasses = getVisibilityClasses();
        if (visibilityClasses != null) sb.append(" ").append(visibilityClasses);

        if (!UNDEFINED.equals(columnSizeXs)) {
            sb.append(" col-xs-").append(columnSizeXs);
            if (!UNDEFINED.equals(columnOffsetXs)) sb.append(" col-xs-offset-").append(columnOffsetXs);
            if (!UNDEFINED.equals(pullXs)) sb.append(" col-xs-pull-").append(pullXs);
            if (!UNDEFINED.equals(pushXs)) sb.append(" col-xs-push-").append(pushXs);
        }

        if (!UNDEFINED.equals(columnSizeSm)) {
            sb.append(" col-sm-").append(columnSizeSm);
            if (!UNDEFINED.equals(columnOffsetSm)) sb.append(" col-sm-offset-").append(columnOffsetSm);
            if (!UNDEFINED.equals(pullSm)) sb.append(" col-sm-pull-").append(pullSm);
            if (!UNDEFINED.equals(pushSm)) sb.append(" col-sm-push-").append(pushSm);
        }

        if (!UNDEFINED.equals(columnSizeLg)) {
            sb.append(" col-lg-").append(columnSizeLg);
            if (!UNDEFINED.equals(columnOffsetLg)) sb.append(" col-lg-offset-").append(columnOffsetLg);
            if (!UNDEFINED.equals(pullLg)) sb.append(" col-lg-pull-").append(pullLg);
            if (!UNDEFINED.equals(pushLg)) sb.append(" col-lg-push-").append(pushLg);
        }

        return sb.toString();
    }
}
