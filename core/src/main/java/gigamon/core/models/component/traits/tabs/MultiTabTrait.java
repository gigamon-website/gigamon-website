package gigamon.core.models.component.traits.tabs;

import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import com.citytechinc.cq.component.annotations.DialogField;
import gigamon.core.models.component.ComponentSlingModel;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.citytechinc.cq.component.annotations.widgets.PathField;



@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MultiTabTrait extends ComponentSlingModel {

	 @DialogField(fieldLabel = "Tab Name", name = "./tabName", ranking = 1,
	            fieldDescription = "Tab Name to be displayed")
	    @TextField
	    @Inject @ValueMapValue
	    private String tabName;
	 
	 	public String getTabName() {
	 			return tabName;
	 		}

	 
	 @DialogField(fieldLabel = "Tab ID", name = "./tabId", ranking = 2)
	 @TextField
     @Inject @ValueMapValue
     private String tabId;

	 public String getTabId() {
		return tabId;
	}

	 @DialogField(fieldLabel = "Background Image Link", name = "./imgLink", ranking = 3)
     @PathField(rootPath = "/content")
     @Inject @ValueMapValue
     private String imgLink;

	 public String getImgLink() {
		return imgLink;
	}
		
}


