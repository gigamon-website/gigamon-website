package gigamon.core.models.component.traits.image;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.Html5SmartImage;
import gigamon.core.models.component.traits.ComponentTrait;

/**
 * Supplies the core image fields for the component's mobile image.
 *
 * @author joelepps
 *
 */
public interface MobileImageFileTrait extends ComponentTrait, MobileImageRenditionSupportTrait {

    interface DialogFields {

        @DialogField(fieldLabel = "Image", name = "./mobileImage", ranking = 1)
        @Html5SmartImage(
                allowCrop=false,
                allowMap=false,
                allowUpload=false,
                allowRotate=false,
                disableZoom=true,
                tab=false,
                isSelf=false,
                useHtml5 = true,
                height=250)
        void mobileImage(); //name must match field name
    }


    // for touch ui field generation, this method must match the name of of the node
    default String getMobileImage() {
        return getProperties().get("./mobileImage/fileReference", String.class);
    }

    @Override
    default String getMobileImageFileReference() {
        return getMobileImage();
    }
}
