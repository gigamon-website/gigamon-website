package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Carousel Component compatible with <a
 * href="http://kenwheeler.github.io/slick/">Slick</a> carousel framework.
 */
@Component(value = "Carousel", group = ComponentSlingModel.COMPONENT_GROUP_CONTENT, disableTargeting = true, helpPath = "/editor.html/content/gigamon-components/carousel.html", tabs = {
    @Tab(title = "Carousel"), @Tab(title = "Background") })
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Carousel extends ComponentSlingModel implements BackgroundXYTraits {

    private static final Gson GSON = new GsonBuilder()
        .excludeFieldsWithoutExposeAnnotation().create();

	/*
	 * The below fields are the slick.js configuration variables.
	 *
	 * The @Exposed fields are serialized to JSON and stored in the data-slick
	 * html attribute that slick will read.
	 */

    @Expose
    private int slidesToShow;

    @Expose
    private int slidesToScroll;

    @DialogField(fieldLabel = "Enable Dots", name = "./dots", value = "true", ranking = 1)
    @CheckBoxTouch(text = "Enable Dots")
    @Inject
    @ValueMapValue
    @Expose
    private boolean dots;

    @DialogField(fieldLabel = "Move Dots Up", name = "./moveDotsUp", value = "true", ranking = 2, fieldDescription = "Pull dots up into the content of the slide.")
    @CheckBoxTouch(text = "Move Dots Up")
    @Inject
    @ValueMapValue
    private boolean moveDotsUp;

    @DialogField(fieldLabel = "Enable Arrows", name = "./arrows", value = "true", ranking = 3)
    @CheckBoxTouch(text = "Enable Arrows")
    @Inject
    @ValueMapValue
    @Expose
    private boolean arrows;

    @DialogField(fieldLabel = "Adaptive Height", name = "./adaptiveHeight", value = "true", ranking = 4)
    @CheckBoxTouch(text = "Adaptive Height")
    @Inject
    @ValueMapValue
    @Expose
    private boolean adaptiveHeight;

    @DialogField(fieldLabel = "Autoplay", name = "./autoplay", value = "true", ranking = 5)
    @CheckBoxTouch(text = "Autoplay")
    @Inject
    @ValueMapValue
    @Expose
    private boolean autoplay;

    @DialogField(fieldLabel = "Autoplay Speed (ms)", name = "./autoplaySpeed", ranking = 10, fieldDescription = "Autoplay speed, default is 3000 ms")
    @TextField
    @Inject
    @ValueMapValue
    @Default(intValues = 3000)
    @Expose
    private int autoplaySpeed;

    @DialogField(fieldLabel = "Animation Speed (ms)", name = "./speed", ranking = 11, fieldDescription = "Speed of slide transition, default is 300 ms")
    @TextField
    @Inject
    @ValueMapValue
    @Default(intValues = 300)
    @Expose
    private int speed;

    @DialogField(fieldLabel = "Arrow Color", name = "./arrowcolor", ranking = 20)
    @Selection(type = "select", options = {
        @Option(text = "gray", value = "grayarrows"),
        @Option(text = "black", value = "blackarrows"),
        @Option(text = "white", value = "whitearrows"),
        @Option(text = "aqua", value = "aquaarrows"),
        @Option(text = "blue", value = "bluearrows"),
        @Option(text = "dark-gray", value = "darkgrayarrows"),
        @Option(text = "hash", value = "hasharrows"),
        @Option(text = "light-gray", value = "light-grayarrows"),
        @Option(text = "orange", value = "orangearrows"),
    })
    @Inject
    @ValueMapValue
    @Default(values = "grayarrows")
    private String arrowcolor;

    public String getArrowcolor() {
        return arrowcolor;
    }

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();

        slidesToScroll = 1;
        slidesToShow = 1;
    }

    public String getCarouselConfigAttribute() {
        return GSON.toJson(this);
    }

    public boolean isMoveDotsUp() {
        return moveDotsUp;
    }

    public boolean isArrows() {
        return arrows;
    }

    @DialogField(tab = 2)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;

}
