package gigamon.core.models.component.traits.image;

import org.apache.commons.lang3.StringUtils;

import gigamon.core.models.component.traits.ComponentTrait;
import gigamon.core.models.component.traits.image.PrimaryImageWithAltTextTrait.DamHelper;
import gigamon.core.util.ImageRendition;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.Html5SmartImage;
import com.citytechinc.cq.component.annotations.widgets.TextField;

public interface MegaBannerImageFileTrait extends ComponentTrait {
	

    interface DialogFields {

        @DialogField(fieldLabel = "Image", name = "./megaimage", ranking = 1)
        @Html5SmartImage(
                allowCrop=false,
                allowMap=false,
                allowUpload=false,
                allowRotate=false,
                disableZoom=true,
                tab=false,
                isSelf=false,
                useHtml5 = true,
                height=250)
        void megaimage(); //name must match field name
        
        
        @DialogField(fieldLabel = "Title", name = "./megaimage/imageTitle", ranking = 2,
                fieldDescription = "Title attribute for image. Shown to visitors as tooltip on mouse hover.")
        @TextField
        void imageTitle();

        @DialogField(fieldLabel = "Alt", name = "./megaimage/imageAlt", ranking = 3,required=true,
                fieldDescription = "Alt attribute for image. Describes the content of the image.")
        @TextField
        void imageAlt();
    }


    // for touch ui field generation, this method must match the name of of the node (ie. 'image')
    default String getmegaImage() {
        return getProperties().get("./megaimage/fileReference", String.class);
    }

  // this method doesn't involve rendition. for rendition use primaryimagefiletrait 
    default String getMegaImageFileReference() {
        return getmegaImage();
    }
    
    default String getImageTitle() {
        String value = getProperties().get("./megaimage/imageTitle", String.class);
        if (StringUtils.isBlank(value)) {
            DamHelper damHelper = new DamHelper(getmegaImage(), getResourceResolver());
            value = damHelper.getTitle();
        }
        return value;
    }

    default String getImageAlt() {
        String value = getProperties().get("./megaimage/imageAlt", String.class);
        if (StringUtils.isBlank(value)) {
            DamHelper damHelper = new DamHelper(getmegaImage(), getResourceResolver());
            value = damHelper.getAlt();
        }
        return value;
    }
    
    
    
    /*
	 * START RENDITION METHODS
	 * These will be dynamically invoked from Sightly.
	 */

    // ORIGINAL

    default String getMegaImageOriginal() {
        String fileRef = getMegaImageFileReference();
        return ImageRendition.ORIGINAL.getRendition(fileRef);
    }

    // JPEG

    /**
     * @return Path for Full Width x2 Desktop rendition
     */
    default String getMegaImageFWX2D() {
        String fileRef = getMegaImageFileReference();
        return ImageRendition.FULL_WIDTH_X2_DESKTOP.getRendition(fileRef);
    }

    /**
     * @return Path for Full Width x1 Desktop rendition
     */
    default String getMegaImageFWX1D() {
        String fileRef = getMegaImageFileReference();
        return ImageRendition.FULL_WIDTH_x1_DESKTOP.getRendition(fileRef);
    }

    /**
     * @return Path for Full Width x2 Mobile rendition
     */
    default String getMegaImageFWX2M() {
        String fileRef = getMegaImageFileReference();
        return ImageRendition.FULL_WIDTH_x2_MOBILE.getRendition(fileRef);
    }

    /**
     * @return Path for Full Width x1 Mobile rendition
     */
    default String getMegaImageFWX1M() {
        String fileRef = getMegaImageFileReference();
        return ImageRendition.FULL_WIDTH_X1_MOBILE.getRendition(fileRef);
    }

    /**
     * @return Path for Half Width large rendition
     */
    default String getMegaImageHWL() {
        String fileRef = getMegaImageFileReference();
        return ImageRendition.HALF_WIDTH_LARGE.getRendition(fileRef);
    }

    /**
     * @return Path for Half Width large small
     */
    default String getMegaImageHWS() {
        String fileRef = getMegaImageFileReference();
        return ImageRendition.HALF_WIDTH_SMALL.getRendition(fileRef);
    }

	/*
	 * END RENDITION METHODS
	 */

}
