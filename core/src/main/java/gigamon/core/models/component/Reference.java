package gigamon.core.models.component;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;

import gigamon.dialog.touch.pathbrowser.PathBrowser;

/**
 * Reference Component
 */
@Component(value = "Reference",
        group = ComponentSlingModel.COMPONENT_GROUP_MISC,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/reference.html",
        tabs = {@Tab(title = "Reference")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Reference extends ComponentSlingModel {

    @DialogField(fieldLabel = "Reference", name = "./reference", ranking = 1)
    @PathBrowser(rootPath = "/content", predicate = "nosystem")
    @Inject @ValueMapValue
    private String reference;

    public String getReference() {
        return reference+"/jcr:content/par";
    }
    
    public String checkReference(){
    	return reference;
    }
}
