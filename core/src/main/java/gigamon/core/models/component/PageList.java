package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.day.cq.wcm.api.PageFilter;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.component.traits.pagelist.PageListerTrait;
import gigamon.core.models.data.SimpleLink;
import gigamon.core.util.PageLister;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Simple component providing a list of page links.
 */
@Component(value = "Page List",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/page-list.html",
        tabs = {@Tab(title = "PageList"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PageList extends ComponentSlingModel implements PageListerTrait, BackgroundXYTraits {

    private List<SimpleLink> links;

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();

        PageLister lister = getPageLister(new PageFilter());

        links = lister.getPageStream()
            .map(page -> page.getContentResource().adaptTo(SimpleLink.class))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    public List<SimpleLink> getLinks() {
        return links;
    }

    @DialogField
    @DialogFieldSet
    public final PageListerTrait.DialogFieldsWithSort list = null;

    @DialogField(tab = 2)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;

}
