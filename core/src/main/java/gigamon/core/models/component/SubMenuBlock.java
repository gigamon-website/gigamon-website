package gigamon.core.models.component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Property;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.TextArea;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;

import gigamon.core.models.component.traits.analytics.AnalyticsTrait;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.component.traits.image.PrimaryImageAltTextNoTitleTrait;
import gigamon.core.models.component.traits.image.PrimaryImageWithAltTextTrait;
import gigamon.core.models.component.traits.pagelist.PageListerTrait;
import gigamon.core.models.data.SimpleLink;
import gigamon.core.util.PageLister;
import gigamon.core.util.ServiceUtils;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;

/**
 * SubMenuBlock Component - this is the column, (1 of 4 for example)
 */
@Component(value = "SubMenuBlock",
    group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
    disableTargeting = true,
    helpPath = "/content/gigamon-components/sub-menu-block.html",
    tabs = {@Tab(title = "SubMenu Block"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SubMenuBlock extends ComponentSlingModel implements PrimaryImageWithAltTextTrait {

    public static final Logger log = LoggerFactory.getLogger(SubMenuBlock.class);

    private static final String LIST_FROM = "listFrom";
    private static final String DEFAULT_LIST_TYPE = "static";

    @DialogField
    @DialogFieldSet
    private final PrimaryImageAltTextNoTitleTrait.DialogFields imageFields = null;

    @DialogField(fieldLabel = "Heading", fieldDescription = "Will be ignored if Image is present", name = "./blockHeading", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String blockHeading;

    @DialogField(fieldLabel = "Heading Link", fieldDescription = "Add if heading should be a link", name = "./blockHeadingLink", ranking = 2)
    @PathField(rootPath = "/content/")
    @Inject @ValueMapValue
    private String blockHeadingLink;

    @DialogField(fieldLabel = "New Tab", name = "./blockHeadingLinkNewTab", ranking = 3, value = "true")
    @CheckBoxTouch(text = "New Tab")
    @Inject @ValueMapValue
    private String blockHeadingLinkNewTab;


    @DialogField(fieldLabel = "Analytics", name = "./headingLinkAnalyticsAttributes", ranking = 4,
        fieldDescription = "Example: data-key1=value1,data-key2=value2. Attribute name cannot be '" +
            AnalyticsTrait.DATA_COMPONENT_NAME + "'",
        additionalProperties={
            @Property(name = "regex", value = "^(?:(?:data-[\\\\-a-z0-9]+=[^,]+),?)*$"),
            @Property(name = "foundation-validation", value = "regex")})
    @TextArea
    @Inject @ValueMapValue
    private String headingLinkAnalyticsAttributes;

    public String getBlockHeading() {
        return blockHeading;
    }

    public String getBlockHeadingLink() {
        return ServiceUtils.appendLinkExtension(getResourceResolver(), blockHeadingLink);
    }

    public String getBlockHeadingLinkNewTab() {
        return blockHeadingLinkNewTab;
    }

    public boolean getBlockHeadingLinkNewTabBool() {
        return "true".equals(getBlockHeadingLinkNewTab());
    }

    public String getheadingLinkAnalyticsAttributes() {
        return headingLinkAnalyticsAttributes;
    }

    public Map<String, String> getAnalyticsAttributeMap() {
        List<AnalyticsTrait.AnalyticsAttribute> attributeList =
            AnalyticsTrait.getAnalyticsAttributeList(getheadingLinkAnalyticsAttributes(), ",", "=");
        return AnalyticsTrait.getAnalyticsAttributeMap(attributeList, "ctaItem");
    }

    private List<SimpleLink> links;

    public List<SimpleLink> getLinks() {
        return links;
    }

    private List<List<SimpleLink>> linkChildren;

    public List<List<SimpleLink>> getLinkChildren() {
        return linkChildren;
    }

    public String listFrom;

    public String getListFrom() {
        return getProperties().get(LIST_FROM,DEFAULT_LIST_TYPE );
    }

    public String headerImage;

    public String getHeaderImage() {
        return getImage();
    }

    public String imageAltText;

    public String getImageAltText() {
        return getImageAlt();
    }

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();

        PageLister lister = getPageLister(new PageFilter());
        if (getListFrom().equals("adhoc")) {
            links = lister.getSimpleLinksList();
        } else {
            links = new ArrayList<>();
            linkChildren = new ArrayList<>();
            Iterator<Page> pages = lister.getPages();

            while(pages.hasNext()) {
                Page page = pages.next();
                if(page != null) {
                    links.add(page.getContentResource().adaptTo(SimpleLink.class));

                    if(lister.getIncludeSubChildren()) {
                        List<SimpleLink> childrenLinks = new ArrayList<>();
                        Iterator<Page> childrenPages = page.listChildren();

                        while(childrenPages.hasNext()) {
                            Page childPage = childrenPages.next();

                            if(childPage != null && !childPage.isHideInNav()) {
                                childrenLinks.add(childPage.getContentResource().adaptTo(SimpleLink.class));
                            }
                        }
                        linkChildren.add(childrenLinks);
                    }
                }
            }
        }
    }

    PageLister getPageLister(PageFilter pageFilter) {
        return new PageLister(getRequest(), getResource(), pageFilter);
    }

    @DialogField(ranking = 5)
    @DialogFieldSet
    public final PageListerTrait.DialogFieldsWithSort list = null;

    @DialogField(tab = 2)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;
}

