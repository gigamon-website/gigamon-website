package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.background.BackgroundYTraits;
import gigamon.core.models.component.traits.richtext.RichTextFieldTrait;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Article Component
 * <p>
 * Renders large bodies of text in a multi-column format.
 */
@Component(value = "Article",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        inPlaceEditingEditorType = "text",
        helpPath = "/editor.html/content/gigamon-components/article.html",
        inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-basic-config/dialog/items/tabs/items/RichTextBasicConfigurations/items/basicRichText",
        tabs = {@Tab(title = "Article"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Article extends ComponentSlingModel implements RichTextFieldTrait, BackgroundYTraits {

    public static final String H1 = "h1";
    public static final String H2 = "h2";
    public static final String H3 = "h3";

    @DialogField(fieldLabel = "Title", name = "./title", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String title;

    @DialogField(fieldLabel = "Title Size", name = "./articleTitleSize", ranking = 2,
            fieldDescription = "There should only ever be one H1 on a page, at the very top of the page")
    @Selection(type="select", options = {
            @Option(text = "H2", value = H2),
            @Option(text = "H3", value = H3),
            @Option(text = "H1", value = H1),
    })
    @Inject @ValueMapValue @Default(values = H3)
    private String articleTitleSize;

    @DialogField(fieldLabel = "Column Count", name = "./articleColumnCount", ranking = 3)
    @TextField
    @Inject @ValueMapValue
    private String articleColumnCount;


    public String getTitle() {
        return title;
    }

    public String getArticleTitleSize() {
        return articleTitleSize;
    }

    public String getArticleColumnCount() {
        return articleColumnCount;
    }

    @DialogField(tab = 2)
    @DialogFieldSet
    public final BackgroundYTraits.DialogFields background = null;

}
