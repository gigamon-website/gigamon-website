package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.*;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;

import gigamon.core.models.component.traits.background.BackgroundXYTraits;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import gigamon.dialog.touch.checkbox.CheckBoxTouch;

import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.citytechinc.cq.component.annotations.widgets.TextField;

import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Columns Component
 * <p>
 * Generic columns component supporting authorable number of columns.
 */
@Component(value = "Columns",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/columns.html",
        tabs = {@Tab(title = "Columns"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Columns extends ComponentSlingModel implements BackgroundXYTraits {

    /* These must match the names of the templates in the Sightly columns.html */
    public static final String LAYOUT_2 = "two";
    public static final String LAYOUT_3 = "three";
    public static final String LAYOUT_4 = "four";
    public static final String LAYOUT_5 = "75-25";
    public static final String LAYOUT_6 = "25-75";
    
    public static final String FLUSH = "flush";
    public static final String NOT_FLUSH = "not-flush";
    public static final String SECONDARY = "secondary-banner";
    public static final String TILE = "large-tile";

    @DialogField(fieldLabel = "Title", name = "./title", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String title;

    public String getTitle() {
        return title;
    }

    @DialogField(fieldLabel = "Title size", name = "./titleSize", ranking = 2, defaultValue = "h3")
    @Selection(type = "select", options = {
    		@Option(text = "H1", value = "h1"),
    		@Option(text = "H2", value = "h2"),
    		@Option(text = "H3", value = "h3")
    })
    @Inject @ValueMapValue
    public String titleSize;
    
    public String getTitleSize() {
    	return titleSize;
    }
    
    @DialogField(fieldLabel = "Column Layout", name = "./columnLayout", ranking = 3, defaultValue = LAYOUT_3,
    		additionalProperties = {
            @Property(name = "class", value = "multi-field-showhide"),
            @Property(name = "multi-field-showhide-target", value = ".list-option-BS-class")
        })
    @Selection(type="select", options = {
            @Option(text = "50 | 50", value = LAYOUT_2),
            @Option(text = "33 | 33 | 33", value = LAYOUT_3),
            @Option(text = "25 | 25 | 25 | 25", value = LAYOUT_4),
            @Option(text = "75 | 25", value = LAYOUT_5),
            @Option(text = "25 | 75", value = LAYOUT_6),
    })
    @Inject @ValueMapValue @Default(values = LAYOUT_2)
    private String columnLayout;

    public String getColumnLayout() {
        return columnLayout;
    }

    @DialogField(fieldLabel = "Column Spacing", name = "./columnSpacing", ranking = 4, defaultValue = NOT_FLUSH)
    @Selection(type="select", options = {
            @Option(text = "Not Flush", value = NOT_FLUSH),
            @Option(text = "Flush", value = FLUSH),
            @Option(text = "Secondary Banner", value = SECONDARY),
            @Option(text = "Tile", value = TILE),
    })
    @Inject @ValueMapValue @Default(values = NOT_FLUSH)
    private String columnSpacing;

    public String getColumnSpacing() {
        return columnSpacing;
    }

    @DialogField(fieldLabel = "Hoverable", name = "./columnHoverable", ranking = 5, value = "hoverable")
    @CheckBoxTouch(text = "Hoverable")
    @Inject @ValueMapValue
    private String columnHoverable;

    public String getColumnHoverable() {
        return columnHoverable;
    }
    

    
    @DialogField(fieldLabel = "BS class for 1st column", name = "./bsClass_2_1", ranking = 9,
    		additionalProperties = {
            @Property(name = "class", value = "list-option-BS-class"),
            @Property(name = "showhidetargetvalue", value = LAYOUT_2),
        })
    @Selection(type="select", options = {
    		@Option(text = "None", value = "None"),
            @Option(text = "col-md-push-6", value = "col-md-push-6"),
            @Option(text = "col-md-pull-6", value = "col-md-pull-6"),
            @Option(text = "col-sm-push-6", value = "col-sm-push-6"),
            @Option(text = "col-sm-pull-6", value = "col-sm-pull-6"),
            @Option(text = "col-lg-push-6", value = "col-lg-push-6"),
            @Option(text = "col-lg-pull-6", value = "col-lg-pull-6"),
    })
    @Inject @ValueMapValue @Default(values = NOT_FLUSH)
    private String bsClass_2_1;

    public String getbsClass_2_1() {
        return bsClass_2_1;
    }
    
    @DialogField(fieldLabel = "BS class for 2ndcolumn", name = "./bsClass_2_2", ranking = 9,
    		additionalProperties = {
            @Property(name = "class", value = "list-option-BS-class"),
            @Property(name = "showhidetargetvalue", value = LAYOUT_2),
        })
    @Selection(type="select", options = {
    		@Option(text = "None", value = "None"),
            @Option(text = "col-md-push-6", value = "col-md-push-6"),
            @Option(text = "col-md-pull-6", value = "col-md-pull-6"),
            @Option(text = "col-sm-push-6", value = "col-sm-push-6"),
            @Option(text = "col-sm-pull-6", value = "col-sm-pull-6"),
            @Option(text = "col-lg-push-6", value = "col-lg-push-6"),
            @Option(text = "col-lg-pull-6", value = "col-lg-pull-6"),
    })
    @Inject @ValueMapValue @Default(values = NOT_FLUSH)
    private String bsClass_2_2;

    public String getbsClass_2_2() {
        return bsClass_2_2;
    }
    
    @DialogField(tab = 2)
    @DialogFieldSet
    private final BackgroundXYTraits.DialogFields backgroundFields = null;

}
