package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.*;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.designer.Style;
import gigamon.core.models.component.traits.analytics.AnalyticsTrait;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.data.SimpleLink;
import gigamon.core.services.BreadcrumbsService;
import gigamon.core.util.SlingModelUtils;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import gigamon.dialog.classic.multifield.MultiCompositeField;
import gigamon.dialog.touch.collapsible.Collapsible;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

/**
 * Breadcrumbs Component that supports a static list of links are dynamic list based on current page hierarchy.
 * <p>
 * Authorable configurations are pull from component first and fallback to design dialog.
 */
@Component(value = "Breadcrumbs",
        group = ComponentSlingModel.COMPONENT_GROUP_MISC,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/breadcrumbs.html",
        tabs = {@Tab(title = "Breadcrumbs"), @Tab(title = "Background"), @Tab(title = "Analytics")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Breadcrumbs extends ComponentSlingModel implements BackgroundXYTraits, AnalyticsTrait {

    private static final Logger log = LoggerFactory.getLogger(Breadcrumbs.class);

    private static final String LIST_TYPE = "listType";
    private static final String DYNAMIC = "dynamic";
    private static final String STATIC = "static";

    public static final String TEXT_DARK = "dark-text";
    public static final String TEXT_WHITE = "white-text";

    /*
     * GLOBAL FIELDS
     */

    @DialogField(fieldLabel = "Text Color", name = "./breadcrumbsTextColor", ranking = 3)
    @Selection(type="select", options = {
            @Option(text = "Light", value = TEXT_WHITE),
            @Option(text = "Dark", value = TEXT_DARK),
    })
    @Inject @ValueMapValue
    private String breadcrumbsTextColor;

    @DialogField(fieldLabel = "Build list using",
            name = "./"+ LIST_TYPE,
            ranking = 2,
            defaultValue = DYNAMIC,
            additionalProperties = {
                    @Property(name = "class", value = "cq-dialog-dropdown-showhide"),
                    @Property(name = "cq-dialog-dropdown-showhide-target", value = ".list-option-listfrom-showhide-target")
            })
    @Selection(type="select", options = {
            @Option(text = "Dynamic Values", value = DYNAMIC),
            @Option(text = "Static Values", value = STATIC)
    })
    @Inject @ValueMapValue
    private String listType;

    /*
     * DYNAMIC FIELDS
     */

    @DialogField(fieldLabel = "Options for Dynamic Values", ranking = 5,
            additionalProperties = {
                    @Property(name = "class", value = "hide list-option-listfrom-showhide-target foundation-layout-util-vmargin"),
                    @Property(name = "showhidetargetvalue", value = DYNAMIC)
            })
    @DialogFieldSet
    private final DynamicFields dynamicFields = null;

    /*
     * STATIC FIELDS
     */

    @DialogField(fieldLabel = "Options for Static Values", ranking = 6,
            additionalProperties = {
                    @Property(name = "class", value = "hide list-option-listfrom-showhide-target foundation-layout-util-vmargin"),
                    @Property(name = "showhidetargetvalue", value = STATIC)
            })
    @DialogFieldSet
    private final StaticFields staticFields = null;

    /*
     * NON-AUTHOR INJECTIONS
     */

    @Inject @OSGiService
    private BreadcrumbsService breadcrumbsService;

    @Inject
    private Page currentPage;

    @Inject
    private Style currentStyle;

    private List<SimpleLink> navigationItems;

    @Override
    public void postConstruct() throws Exception {
        if (getListType().equals(STATIC)) {
            navigationItems = SlingModelUtils.buildComponentSlingModels(getResource().getChild("staticPagePaths"), getRequest(), SimpleLink.class, false);
        } else {
            int startLevelAbs = getConfig("startLevelAbs", 2, Integer.class);
            int stopLevelRel = getConfig("stopLevelRel", 0, Integer.class);

            log.debug("Building breadcrumbs with startLevel {}, stopLevel {}", startLevelAbs, stopLevelRel);

            navigationItems = breadcrumbsService.getBreadcrumbs(getResourceResolver(), currentPage, startLevelAbs, stopLevelRel);
        }
    }

    public String getBreadcrumbsTextColor() {
        return getProperties().get("breadcrumbsTextColor", String.class);
    }

    /*
     * MAIN SELECT FIELD
     * Toggles visibility of below fields
     */

    public String getListType() {
        return getProperties().get(LIST_TYPE, DYNAMIC);
    }

    public List<SimpleLink> getNavigationItems() {
        return navigationItems;
    }

    @DialogField(fieldLabel = "Display current page as h1", name = "./showCurrentAsTitle", ranking = 5, value = "true")
        @CheckBoxTouch(text = "Display current page as h1")
        @Inject @ValueMapValue
        private String showCurrentAsTitle;

    public String getShowCurrentAsTitle() {
            return showCurrentAsTitle;
        }

    /**
     * Get property with preference to current node followed by current style.
     *
     */
    private <T> T getConfig(String key, T defaultValue, Class<T> clazz) {
        T nodeProp = getProperties().get(key, clazz);
        if (nodeProp != null) {
            if (clazz.equals(String.class)) {
                if (!StringUtils.isBlank(nodeProp + "")) {
                    return nodeProp;
                }
            } else {
                return nodeProp;
            }
        }

        T styleProp = currentStyle.get(key, defaultValue);
        log.trace("Using design fallback: {} = {}", key, styleProp);
        return styleProp;
    }

    private interface DynamicFields {
        @DialogField(fieldLabel = "Start Level Override (Absolute)", fieldDescription = "2 (default) for /content/wdc-com/en-us. Applies to this page only. Inherits from design if not set.",
                defaultValue = "2", name = "./startLevelAbs", ranking= 1)
        @TextField
        int startLevelAbs();

        @DialogField(fieldLabel = "Stop Level Override (Relative)", fieldDescription = "0 (default) for current page. Applies to this page only. Inherits from design if not set.",
                defaultValue = "0", name = "./stopLevelRel", ranking= 2)
        @TextField
        int stopLevelRel();
    }

    private interface StaticFields {
        @DialogField(fieldLabel = "Static Items", name = "./staticPagePaths", fieldDescription = "Override the dynamic breadcrumbs with manual list of pages.", ranking= 3)
        @MultiCompositeField
        List<SimpleLink> staticLinks();
    }

    @DialogField(tab = 2)
    @DialogFieldSet(title = "Background")
    public final BackgroundXYTraits.DialogFields background = null;

    @DialogField(tab = 3)
    @DialogFieldSet(title = "Analytics")
    public final AnalyticsTrait.DialogFields analytics = null;

}
