package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.*;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.Selection;

import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

import gigamon.core.util.ServiceUtils;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;

import gigamon.core.models.component.traits.background.BackgroundYTraits;
import gigamon.core.models.component.traits.image.PrimaryImageRenditionPickerTrait;
import gigamon.core.models.component.traits.image.PrimaryImageWithAltTextTrait;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.citytechinc.cq.component.annotations.widgets.TextField;

import javax.inject.Inject;

/**
 * Standalone image component.
 */
@Component(value = "Image",
        group = ComponentSlingModel.COMPONENT_GROUP_MISC,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/image.html",
        tabs = {@Tab(title = "Image"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Image extends ComponentSlingModel implements BackgroundYTraits, PrimaryImageWithAltTextTrait, PrimaryImageRenditionPickerTrait {

    @DialogField
    @DialogFieldSet
    private final PrimaryImageWithAltTextTrait.DialogFields imageFields = null;

    @DialogField
    @DialogFieldSet
    private final PrimaryImageRenditionPickerTrait.DialogFields imageRenditionFields = null;

    @DialogField(tab = 2)
    @DialogFieldSet
    private final BackgroundYTraits.DialogFields backgroundFields = null;

    public static final String ORIGINAL = "original";
    public static final String FLUID = "scale";

    @DialogField(fieldLabel = "Scale Image", name = "./scaleImage", ranking = 2, defaultValue = ORIGINAL)
    @Selection(type="select", options = {
            @Option(text = "original", value = ORIGINAL),
            @Option(text = "scale", value = FLUID),
    })
    @Inject @ValueMapValue @Default(values = ORIGINAL)
    private String scaleImage;

    public String getScaleImage() {
        return scaleImage;
    }

    @DialogField(fieldLabel = "Text", name = "./imgText", ranking = 3)
    @TextField
    @Inject @ValueMapValue
    private String imgText;

    public String getImgText() {
        return imgText;
    }

    @DialogField(fieldLabel = "Link to", name = "./imgLink", ranking = 4)
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String imgLink;

    public String getImgLink() {
        return imgLink;
    }

    @DialogField(fieldLabel = "New Tab", name = "./imgLinkNewTab", ranking = 5, value = "true")
        @CheckBoxTouch(text = "New Tab")
        @Inject @ValueMapValue
        private String imgLinkNewTab;

    public String getImgLinkNewTab() {
            return imgLinkNewTab;
        }
    
    
   
    @DialogField(fieldLabel = "Crop Image", name = "./cropimage", ranking = 6, value = "true")
	@CheckBoxTouch(text = "Crop Image")
	@Inject
	@ValueMapValue
	private String cropimage;
    
    public Boolean getCropimage() {
		return "true".equals(cropimage);
	}

    
    @Override
    public void postConstruct() {
        imgLink = ServiceUtils.appendLinkExtension(getResourceResolver(), imgLink);
    }


}
