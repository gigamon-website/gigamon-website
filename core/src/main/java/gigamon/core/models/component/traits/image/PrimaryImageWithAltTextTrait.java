package gigamon.core.models.component.traits.image;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.day.cq.dam.api.Asset;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;

/**
 * Adds alt and title text support to component that use an image.
 *
 * @author joelepps
 *
 */
@Model(adaptables=Resource.class)
public interface PrimaryImageWithAltTextTrait extends PrimaryImageFileTrait {

    interface DialogFields extends PrimaryImageFileTrait.DialogFields {

        @DialogField(fieldLabel = "Title", name = "./image/imageTitle", ranking = 2,
                fieldDescription = "Title attribute for image. Shown to visitors as tooltip on mouse hover.")
        @TextField
        void imageTitle();

        @DialogField(fieldLabel = "Alt", name = "./image/imageAlt", ranking = 3,required=true,
                fieldDescription = "Alt attribute for image. Describes the content of the image.")
        @TextField
        void imageAlt();
    }

    default String getImageTitle() {
        String value = getProperties().get("./image/imageTitle", String.class);
        if (StringUtils.isBlank(value)) {
            DamHelper damHelper = new DamHelper(getImage(), getResourceResolver());
            value = damHelper.getTitle();
        }
        return value;
    }

    default String getImageAlt() {
        String value = getProperties().get("./image/imageAlt", String.class);
        if (StringUtils.isBlank(value)) {
            DamHelper damHelper = new DamHelper(getImage(), getResourceResolver());
            value = damHelper.getAlt();
        }
        return value;
    }

    /*
     * This can be used if values from DAM should be used.
     * However, adds some performance overhead since fileReference
     * must be resolved to Asset type for every value.
     */
    class DamHelper {

        private Asset asset;

        public DamHelper(String fileReference, ResourceResolver resolver) {
            if (fileReference == null) {
                return;
            }
            init(fileReference, resolver);
        }

        private void init(String fileReference, ResourceResolver resolver) {
            Resource imageResource = resolver.getResource(fileReference);
            if (imageResource == null) {
                return;
            }
            Asset imageAsset = imageResource.adaptTo(Asset.class);
            if (imageAsset == null) {
                return;
            }

            asset = imageAsset;
        }

        public boolean isEmpty() {
            return asset == null;
        }

        public String getTitle() {
            if (asset == null) return null;
            return asset.getMetadataValue("dc:title");
        }

        public String getAlt() {
            if (asset == null) return null;
            return asset.getMetadataValue("dc:description");
        }

    }
}