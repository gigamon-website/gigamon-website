package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.NumberField;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.background.BackgroundYTraits;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * TwitterTimeline Component
 */
@Component(value = "Twitter Timeline",
        group = ComponentSlingModel.COMPONENT_GROUP_MISC,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/twitter-timeline.html",
        tabs = {@Tab(title = "Twitter Timeline"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TwitterTimeline extends ComponentSlingModel implements BackgroundYTraits {

    @DialogField(fieldLabel = "Title", name = "./title", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String title;

    @DialogField(fieldLabel = "Link Text", name = "./linkText", ranking = 2, fieldDescription = "Placeholder {0} will be replaced with account name. Example: by @{0}")
    @TextField
    @Inject @ValueMapValue
    private String linkText;

    @DialogField(required = true, fieldLabel = "Twitter Account", name = "./account", ranking = 5, fieldDescription = "Twitter handle WITHOUT the @ symbol")
    @TextField
    @Inject @ValueMapValue
    private String account;

    @DialogField(fieldLabel = "Max Tweets", name = "./maxTweets", ranking = 10, defaultValue = "6")
    @NumberField(allowNegative = false)
    @Inject @ValueMapValue
    private String maxTweets;

    public String getTitle() {
        return title;
    }

    public String getLinkText() {
        return linkText;
    }

    public String getAccount() {
        return account;
    }

    public String getAccountUrl() {
        return (getAccount() == null) ? null : "https://twitter.com/" + getAccount();
    }

    public String getMaxTweets() {
        return maxTweets;
    }

    @DialogField(tab = 2)
    @DialogFieldSet
    public final BackgroundYTraits.DialogFields background = null;

}
