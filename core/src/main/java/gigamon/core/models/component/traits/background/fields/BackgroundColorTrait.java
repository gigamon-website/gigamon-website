package gigamon.core.models.component.traits.background.fields;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.ComponentTrait;

public interface BackgroundColorTrait extends ComponentTrait {

    String FIELD_BACKGROUND_COLOR = "backgroundColor";

    String WHITE = "white-bg";
    String NONE = "_empty_";
    String GREY_LIGHT_1 = "light-grey-bg";
    String GREY_DARK_1 = "dark-grey-bg";
    String AQUA = "aqua-bg";
    String BLUE_1 = "blue-bg";
    String HASH_1 = "hash-bg";
    String ORANGE_1 = "orange-bg";
    String ORANGE_LIGHT = "light-orange-bg";
    String BLUE_LIGHT = "light-blue-bg";

    interface DialogFields {

        @DialogField(fieldLabel = "Color", name = "./"+FIELD_BACKGROUND_COLOR, defaultValue = NONE, ranking = 1)
        @Selection(type="select", options = {
            @Option(text = "None", value = NONE),
            @Option(text = "Aqua", value = AQUA),
            @Option(text = "Blue", value = BLUE_1),
            @Option(text = "Dark Grey", value = GREY_DARK_1),
            @Option(text = "Hash", value = HASH_1),
            @Option(text = "Light Grey", value = GREY_LIGHT_1),
            @Option(text = "Orange", value = ORANGE_1),
            @Option(text = "White", value = WHITE),
            @Option(text = "Light Orange", value = ORANGE_LIGHT),
            @Option(text = "Light Blue", value = BLUE_LIGHT)
        })
        void color();

    }

    default String getBackgroundColor() {
        String value = getProperties().get(FIELD_BACKGROUND_COLOR, String.class);
        return NONE.equals(value) ? null : value;
    }

}
