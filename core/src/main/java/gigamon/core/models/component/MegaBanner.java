package gigamon.core.models.component;


import java.util.List;


import gigamon.core.models.component.traits.cta.CtaWithBtnStyleTrait;

import gigamon.core.models.component.traits.image.MegaBannerImageFileTrait;
import gigamon.core.models.component.traits.image.MobileImageFileTrait;
import gigamon.core.models.component.traits.image.PrimaryImageFileTrait;
import gigamon.core.models.component.traits.image.PrimaryImageRenditionPickerTrait;

import gigamon.core.models.component.traits.richtext.RichTextFieldTrait;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Property;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.NumberField;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextArea;
import com.citytechinc.cq.component.annotations.widgets.TextField;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * MegaBanner Component
 */
@Component(value = "MegaBanner",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/mega-banner.html",
        inPlaceEditingEditorType = "text",
        inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-config/dialog/items/tabs/items/RichTextConfigurations/items/standardRichText",
        tabs = {@Tab(title = "Background"),@Tab(title = "Columns"),@Tab(title = "Text"),@Tab(title = "Image"),@Tab(title = "CTA")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MegaBanner extends ComponentSlingModel  implements CtaWithBtnStyleTrait, PrimaryImageFileTrait, RichTextFieldTrait, MobileImageFileTrait, PrimaryImageRenditionPickerTrait,MegaBannerImageFileTrait{



    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_CENTER = "center";

    public static final String H1 = "h1";
    public static final String H2 = "h2";
    public static final String H3 = "h3";
    
    
    public static final String LIGHT = "light";
    public static final String BOLD = "bold";
    public static final String MEDIUM = "medium";
    public static final String EXTRA_BOLD = "extra-bold";


    /*background tab*/



	   @DialogField(fieldLabel = "Use Fixed Height",
		        name = "./desktopUseFixedHeight",
		        ranking = 1,
		        additionalProperties = {
		            @Property(name = "class", value = "multi-field-showhide"),
		            @Property(name = "multi-field-showhide-target", value = ".list-option-desktop-fixed-height-target")
		        })
		    @Selection(type="select", options = {
		        @Option(text = "No", value = "false"),
		        @Option(text = "Yes", value = "true"),
		    })
		    @Inject @ValueMapValue @Default(values = "false")
		    private String desktopUseFixedHeight;

		    @DialogField(fieldLabel = "Height", name = "./desktopHeight", ranking = 10, defaultValue = "500",
		        additionalProperties = {
		            @Property(name = "class", value = "list-option-desktop-fixed-height-target"),
		            @Property(name = "showhidetargetvalue", value = "true"),
		        })
		    @NumberField(min = "0")
		    @Inject @ValueMapValue @Default(values = "500")
		    private String desktopHeight;

		    @DialogField(fieldLabel = "Height Unit", name = "./desktopHeightUnit", ranking = 11,
		        fieldDescription = "When using vh, config only shows with 'View as Published'",
		        additionalProperties = {
		            @Property(name = "class", value = "list-option-desktop-fixed-height-target"),
		            @Property(name = "showhidetargetvalue", value = "true"),
		        })
		    @Selection(type="select", options = {
		        @Option(text = "px", value = "px"),
		        @Option(text = "vh (% of screen)", value = "vh"),
		    })
		    @Inject @ValueMapValue @Default(values = "vh")
		    private String desktopHeightUnit;

		    @DialogField(fieldLabel = "Vertical Focal Point", name = "./desktopVertFP", ranking = 15,
		        fieldDescription = "0 for top, 100 for bottom", defaultValue = "50")
		    @NumberField(min = "0", max = "100")
		    @Inject @ValueMapValue @Default(values = "50")
		    private String desktopVertFP;

		    @DialogField(fieldLabel = "Horizontal Focal Point", name = "./desktopHorzFP", ranking = 16,
		        fieldDescription = "0 for left, 100 for right", defaultValue = "50")
		    @NumberField(min = "0", max = "100")
		    @Inject @ValueMapValue @Default(values = "50")
		    private String desktopHorzFP;

		    @DialogField(ranking = 17)
		    @DialogFieldSet
		    private final PrimaryImageFileTrait.DialogFields image = null;


		    /*mobile fields*/

		    @DialogField(fieldLabel = "Use Mobile Image",
		            name = "./useMobileImage",
		            ranking = 20,

		            additionalProperties = {
		                @Property(name = "class", value = "multi-field-showhide"),
		                @Property(name = "multi-field-showhide-target", value = ".list-option-use-mobile-target")
		            })
		        @Selection(type="select", options = {
		            @Option(text = "No", value = "false"),
		            @Option(text = "Yes", value = "true"),
		        })
		        @Inject @ValueMapValue @Default(values = "false")
		        private String useMobileImage;

		        @DialogField(fieldLabel = "Use Fixed Height",
		            name = "./mobileUseFixedHeight",
		            ranking = 21,

		            additionalProperties = {
		                @Property(name = "class", value = "multi-field-showhide list-option-use-mobile-target"),
		                @Property(name = "showhidetargetvalue", value = "true"),
		                @Property(name = "multi-field-showhide-target", value = ".list-option-mobile-fixed-height-target")
		            })
		        @Selection(type="select", options = {
		            @Option(text = "No", value = "false"),
		            @Option(text = "Yes", value = "true"),
		        })
		        @Inject @ValueMapValue @Default(values = "false")
		        private String mobileUseFixedHeight;

		        @DialogField(fieldLabel = "Height", name = "./mobileHeight", ranking = 22,  defaultValue = "500",
		            additionalProperties = {
		                @Property(name = "class", value = "list-option-mobile-fixed-height-target"),
		                @Property(name = "showhidetargetvalue", value = "true"),
		            })
		        @NumberField(min = "0")
		        @Inject @ValueMapValue @Default(values = "500")
		        private String mobileHeight;

		        @DialogField(fieldLabel = "Height Unit", name = "./mobileHeightUnit",  ranking = 23,
		            fieldDescription = "When using vh, config only shows with 'View as Published'",
		            additionalProperties = {
		                @Property(name = "class", value = "list-option-mobile-fixed-height-target"),
		                @Property(name = "showhidetargetvalue", value = "true"),
		            })
		        @Selection(type="select", options = {
		            @Option(text = "px", value = "px"),
		            @Option(text = "vh (% of screen)", value = "vh"),
		        })
		        @Inject @ValueMapValue @Default(values = "vh")
		        private String mobileHeightUnit;

		        @DialogField(fieldLabel = "Vertical Focal Point", name = "./mobileVertFP", ranking = 24,
		            fieldDescription = "0 for top, 100 for bottom", defaultValue = "50",
		            additionalProperties = {
		                @Property(name = "class", value = "list-option-use-mobile-target"),
		                @Property(name = "showhidetargetvalue", value = "true"),
		            })
		        @NumberField(min = "0", max = "100")
		        @Inject @ValueMapValue @Default(values = "50")
		        private String mobileVertFP;

		        @DialogField(fieldLabel = "Horizontal Focal Point", name = "./mobileHorzFP", ranking = 25,
		            fieldDescription = "0 for left, 100 for right", defaultValue = "50",
		            additionalProperties = {
		                @Property(name = "class", value = "list-option-use-mobile-target"),
		                @Property(name = "showhidetargetvalue", value = "true"),
		            })
		        @NumberField(min = "0", max = "100")
		        @Inject @ValueMapValue @Default(values = "50")
		        private String mobileHorzFP;

		        @DialogField(ranking = 26,
		            additionalProperties = {
		                @Property(name = "class", value = "list-option-use-mobile-target"),
		                @Property(name = "showhidetargetvalue", value = "true"),
		            })
		        @DialogFieldSet
		        private final MobileImageFileTrait.DialogFields mobileImage = null;

				   /* methods for background image*/


			    @Override
			    public String getMobileImage() {
			        if (!isUseMobileImage()) return null;
			        return MobileImageFileTrait.super.getMobileImage();
			    }

		    public String getDesktopImageStyles() {
		        return " " + desktopHorzFP + "% " + desktopVertFP + "% / cover";
		    }

		    public String getDesktopHeight() {
		        return desktopHeight;
		    }

		    public String getDesktopHeightUnit() {
		        return desktopHeightUnit;
		    }

		    public boolean getDesktopUseFixedHeight() {
		        return "true".equals(desktopUseFixedHeight);
		    }

		    public String getDesktopHeightCss() {
		        if (getDesktopUseFixedHeight()) {
		            return getDesktopHeight() + getDesktopHeightUnit();
		        }
		        return "auto";
		    }

		    public boolean isUseMobileImage() {
		        return "true".equals(useMobileImage);
		    }

		    public String getMobileImageStyles() {
		        if (!isUseMobileImage()) return getDesktopImageStyles();
		        return " " + mobileHorzFP + "% " + mobileVertFP + "% / cover";
		    }

		    public String getMobileHeight() {
		        if (!isUseMobileImage()) return getDesktopHeight();
		        return mobileHeight;
		    }

		    public String getMobileHeightUnit() {
		        if (!isUseMobileImage()) return getDesktopHeightUnit();
		        return mobileHeightUnit;
		    }

		    public boolean getMobileUseFixedHeight() {
		        if (!isUseMobileImage()) return getDesktopUseFixedHeight();
		        return "true".equals(mobileUseFixedHeight);
		    }

		    public String getMobileHeightCss() {
		        if (getMobileUseFixedHeight()) {
		            return getMobileHeight() + getMobileHeightUnit();
		        }
		        return "auto";
		    }


		        /*columns field*/


		        @DialogField(fieldLabel = "Column Layout", name = "./columnLayout", ranking = 3,tab=2)
		        @Selection(type="select", options = {
		                @Option(text = "50 | 50", value = "5050"),
		                @Option(text = "75 | 25", value = "7525"),
		                @Option(text = "25 | 75", value ="2575"),
		        })
		        @Inject @ValueMapValue @Default(values = "5050")
		        private String columnLayout;

		        public String getColumnLayout() {
		            return columnLayout;
		        }

		        public String getLeftColumnLayout() {
		            if(getColumnLayout().equals("5050"))
		            {
		            	return "col-xs-12 col-md-6";
		            }
		            else if(getColumnLayout().equals("7525"))
		            {
		            	return "col-xs-12 col-md-8";
		            }
		            else if(getColumnLayout().equals("2575"))
		            {
		            	return "col-xs-12 col-md-4";
		            }
		            return "col-xs-12 col-md-6";
		        }


		        public String getRightColumnLayout() {
		            if(getColumnLayout().equals("5050"))
		            {
		            	return "col-xs-12 col-md-6";
		            }
		            else if(getColumnLayout().equals("7525"))
		            {
		            	return "col-xs-12 col-md-4";
		            }
		            else if(getColumnLayout().equals("2575"))
		            {
		            	return "col-xs-12 col-md-8";
		            }
		            return "col-xs-12 col-md-6";
		        }





				@DialogField(fieldLabel = "Horizontal Spacing", name ="./columnsHorizontalSpacing" , ranking = 10, defaultValue = "adjust-column-width-max",tab=2)
		        @Selection(type="select", options = {
		                @Option(text = "100% (max-width)", value ="adjust-column-width-max"),
		                @Option(text = "100% (Fluid)", value = "_empty_"),
		                @Option(text = "90%", value = "adjust-column-width-90"),
		                @Option(text = "70%", value = "adjust-column-width-70"),
		        })
		        @Inject @ValueMapValue
		        String columnsHorizontalSpacing;

		        public String getColumnsHorizontalSpacing() {
					return columnsHorizontalSpacing;
				}

		        @DialogField(fieldLabel = "LeftColTopSpacing", name = "./topspacingleftcolumn", ranking = 3,tab=2)
		        @Selection(type="select", options = {
		                @Option(text = "None", value = "_empty_"),
		                @Option(text = "XS (15)", value = "vert-pad-top-xs"),
		                @Option(text = "SM (30)", value = "vert-pad-top-sm"),
		                @Option(text = "MD (60)", value = "vert-pad-top-md"),
		                @Option(text = "LG (90)", value = "vert-pad-top-lg"),
		        })
		        @Inject @ValueMapValue
		        String topspacingleftcolumn;

		        @DialogField(fieldLabel = "LeftColBottomSpacing", name = "./btmspacingleftcolumn", ranking = 4,tab=2)
		        @Selection(type="select", options = {
		                @Option(text = "None", value = "_empty_"),
		                @Option(text = "XS (15)", value = "vert-pad-bottom-xs"),
		                @Option(text = "SM (30)", value = "vert-pad-bottom-sm"),
		                @Option(text = "MD (60)", value = "vert-pad-bottom-md"),
		                @Option(text = "LG (90)", value = "vert-pad-bottom-lg"),
		        })
		        @Inject @ValueMapValue
		        String btmspacingleftcolumn;


		        public String getTopspacingleftcolumn() {
		        	if("_empty_".equals(topspacingleftcolumn))
		        	{return "";}
		        	else{return topspacingleftcolumn;}
				}




				@DialogField(fieldLabel = "RightColTopSpacing", name = "./topspacingrightcolumn", ranking = 5,tab=2)
			        @Selection(type="select", options = {
			                @Option(text = "None", value = "_empty_"),
			                @Option(text = "XS (15)", value = "vert-pad-top-xs"),
			                @Option(text = "SM (30)", value = "vert-pad-top-sm"),
			                @Option(text = "MD (60)", value = "vert-pad-top-md"),
			                @Option(text = "LG (90)", value = "vert-pad-top-lg"),
			        })
			        @Inject @ValueMapValue
			        String topspacingrightcolumn;

			        @DialogField(fieldLabel = "RightColBottomSpacing", name = "./btmspacingrightcolumn", ranking = 6,tab=2)
			        @Selection(type="select", options = {
			                @Option(text = "None", value = "_empty_"),
			                @Option(text = "XS (15)", value = "vert-pad-bottom-xs"),
			                @Option(text = "SM (30)", value = "vert-pad-bottom-sm"),
			                @Option(text = "MD (60)", value = "vert-pad-bottom-md"),
			                @Option(text = "LG (90)", value = "vert-pad-bottom-lg"),
			        })
			        @Inject @ValueMapValue
			        String btmspacingrightcolumn;

			        public String getTopspacingrightcolumn() {
			        	if("_empty_".equals(topspacingrightcolumn))
			        	{
			        		return "";
			        	}
			        	else{
						return topspacingrightcolumn;
			        	}
					}



					public String getBtmspacingleftcolumn() {
						if("_empty_".equals(btmspacingleftcolumn))
			        	{return "";}
						else{return btmspacingleftcolumn;}
						}

						public String getBtmspacingrightcolumn() {
							if("_empty_".equals(btmspacingrightcolumn))
				        	{return "";}
							else{return btmspacingrightcolumn;}
						}


                    /* CTA fields*/
                    @DialogField(fieldLabel = "BannerId", name = "./bannerid", ranking = 1,tab=5)
                    @TextField
                    @Inject @ValueMapValue @Default(values = "megabanner")
                    private String bannerid;

                    public String getBannerid() {
    return bannerid;
}

                     @DialogField(fieldLabel = "Use Analytics Link", name = "./useanalyticslink", ranking = 1, value = "true",tab=5)
                        @CheckBoxTouch(text = "Use Analytics Link")
                        @Inject
                        @ValueMapValue
                        private String useanalyticslink;

                        public Boolean getUseanalyticslink() {
                            return "true".equals(useanalyticslink);
                    }




				@DialogField(tab = 5)
		        @DialogFieldSet
		        public final CtaWithBtnStyleTrait.DialogFields cta = null;


	public String getAnalyticsLink() {
		List<CtaModelWithBtnStyle> ctalist = getCtaList();
		for (CtaModelWithBtnStyle button : ctalist) {
			String link = button.getLink();
			if (getUseanalyticslink()) {
				if (link.indexOf('?') > -1 && link.indexOf('=') > -1) {
					return button.getLink() + "&intcid=" + getBannerid();

				} else {
					return button.getLink() + "?intcid=" + getBannerid();
				}
			}
			else{return link;}
		}

		return "";
	}

		        /*primary text fields*/


				@DialogField(fieldLabel = "Title", name = "./title", ranking = 1,tab=3)
		        @TextField
		        @Inject @ValueMapValue
		        private String title;

		        @DialogField(fieldLabel = "Title Size", name = "./textJumboTitleSize",tab=3, ranking = 2, defaultValue = H2,
		                fieldDescription = "There should only ever be one H1 on a page, at the very top of the page")
		        @Selection(type="select", options = {
		                @Option(text = "Medium", value = H2),
		                @Option(text = "Small", value = H3),
		                @Option(text = "H1", value = H1),
		        })
		        @Inject @ValueMapValue
		        private String textJumboTitleSize;
		        
		        
		        @DialogField(fieldLabel = "Font Weight", name = "./fontWeight",tab=3, ranking = 2.5, defaultValue = BOLD)
		        @Selection(type="select", options = {
		      		  @Option(text = "Bold", value = BOLD),
		                @Option(text = "Light", value = LIGHT),
		                @Option(text = "Medium", value = MEDIUM),
		                @Option(text = "Extra bold", value = EXTRA_BOLD),
		        })
		        @Inject @ValueMapValue
		        private String fontWeight;
		  	  
		  	  public String getFontWeight() {
		            return fontWeight;
		        }
		  	  

		        @DialogField(fieldLabel = "Alignment", name = "./textJumboAlignment",tab=3, ranking = 3, defaultValue = ALIGN_LEFT)
		        @Selection(type="select", options = {
		                @Option(text = "Left", value = ALIGN_LEFT),
		                @Option(text = "Center", value = ALIGN_CENTER),
		        })
		        @Inject @ValueMapValue
		        private String textJumboAlignment;

		        @DialogField(fieldLabel = "Color", name = "./textJumboColor", ranking = 5,tab=3)
		        @Selection(type = "select", options = {
		        		@Option(text = "Dark", value = "dark-text"),
		        		@Option(text = "Light", value = "white-text")
		        })
		        @Inject @ValueMapValue
		        private String textJumboColor;

		        public String getTitle() {
		            return title;
		        }

		        public String getTextJumboTitleSize() {
		            return textJumboTitleSize;
		        }

		        public String getTextJumboAlignment() {
		            return textJumboAlignment;
		        }

		        public String getTextJumboColor() {
		        	return textJumboColor;
		        }


		        /*secondary text*/

		        @DialogField(fieldLabel = "Secondary Text", name = "./secondarytext", ranking = 6,tab=3)
		        @TextArea
		        @Inject @ValueMapValue
		        private String secondarytext;

		        public String getSecondarytext() {
					return secondarytext;
				}

		        /*image fields*/





				@DialogField(tab=4,ranking=1)
		        @DialogFieldSet
		        private final MegaBannerImageFileTrait.DialogFields imageFields = null;


				 @DialogField(fieldLabel = "Image Path", name = "./imgpath", ranking = 2,tab=4,fieldDescription = "This field will take priority over the image drag & drop section please give path to the image endition will be added automatically")
			        @TextField
			        @Inject @ValueMapValue
			        private String imgpath;


				public String getImgpath() {
					if(imgpath!=null){
					return imgpath+"/jcr:content/renditions/cq5dam.thumbnail.400.400.png";
					}
					else{
						return imgpath;
					}
				}


				@DialogField(tab=4,ranking=2)
		        @DialogFieldSet
		        private final PrimaryImageRenditionPickerTrait.DialogFields imageRenditionFields = null;


				  public static final String ORIGINAL = "original";
				    public static final String FLUID = "scale";

				    @DialogField(fieldLabel = "Scale Image", name = "./scaleImage",tab=4, ranking = 3, defaultValue = ORIGINAL)
				    @Selection(type="select", options = {
				            @Option(text = "original", value = ORIGINAL),
				            @Option(text = "scale", value = FLUID),
				    })
				    @Inject @ValueMapValue @Default(values = ORIGINAL)
				    private String scaleImage;

				    public String getScaleImage() {
				        return scaleImage;
				    }


		        @DialogField(fieldLabel = "Text", name = "./imgText", ranking = 4,tab=4)
		        @TextField
		        @Inject @ValueMapValue
		        private String imgText;

		        public String getImgText() {
		            return imgText;
		        }

		        @DialogField(fieldLabel = "Link to", name = "./imgLink", ranking = 5,tab=4)
		        @PathField(rootPath = "/content")
		        @Inject @ValueMapValue
		        private String imgLink;

		        public String getImgLink() {
		            return imgLink;
		        }

		        @DialogField(fieldLabel = "New Tab", name = "./imgLinkNewTab", ranking = 6, value = "true",tab=4)
		            @CheckBoxTouch(text = "New Tab")
		            @Inject @ValueMapValue
		            private String imgLinkNewTab;

		        public String getImgLinkNewTab() {
		                return imgLinkNewTab;
		            }



		        @DialogField(fieldLabel = "Crop Image", name = "./cropimage", ranking = 7, value = "true",tab=4)
		    	@CheckBoxTouch(text = "Crop Image")
		    	@Inject
		    	@ValueMapValue
		    	private String cropimage;

		        public Boolean getCropimage() {
		    		return "true".equals(cropimage);
		    	}

		       /* @DialogField(fieldLabel = "Image Overflow", name = "./imageoverflow", ranking = 8, value = "true",tab=4)
		    	@CheckBoxTouch(text = "Image Overflow")
		    	@Inject
		    	@ValueMapValue
		    	private String imageoverflow;

		        public Boolean getImageoverflow() {
		    		return "true".equals(imageoverflow);
		    	}*/
		        
		        @DialogField(fieldLabel = "Image Overflow", name = "./imageoverflow", ranking = 5.5,tab=4, defaultValue = "none")
		        @Selection(type="select", options = {
		        		@Option(text = "none", value = "none"),
		                @Option(text = "Small-25", value = "vert-mar-top--sm"),
		                @Option(text = "Medium-60", value = "vert-mar-top--md"),
		                @Option(text = "XLarge-90", value ="vert-mar-top--xl"),
		        })
		        @Inject @ValueMapValue @Default(values = "none")
		        private String imageoverflow;

				public String getImageoverflow() {
					return imageoverflow;
				}
}
