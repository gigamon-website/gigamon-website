package gigamon.core.models.component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import gigamon.core.models.component.traits.ComponentTrait;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.*;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Sling Model based implementation of the {@link ComponentTrait}.
 * <p>
 * Implementations must add the {@link Model} annotation.
 *
 * @author joelepps
 * @see <a href="https://sling.apache.org/documentation/bundles/models.html">Sling Models</a>
 */
public abstract class ComponentSlingModel implements ComponentTrait {

    public static final String COMPONENT_GROUP_CONTENT = "Gigamon Content";
    public static final String COMPONENT_GROUP_MISC = "Gigamon Misc";
    public static final String COMPONENT_GROUP_HIDDEN = ".hidden";

    private Map<String, String> debugModel;
    private Set<Class<?>> debugTypes;

    @Inject @Required @SlingObject
    private Resource resource;

    @Inject @Required @SlingObject
    private ResourceResolver resourceResolver;

    @Inject @Optional @SlingObject
    private SlingHttpServletRequest request;

    @Inject @Required @OSGiService
    private SlingSettingsService slingSettingsService;

    private Logger logger;

    /**
     * Initialization method necessary for Sling Sling models.
     */
    protected ComponentSlingModel() {
        // All fields will be set dynamically via Sling Models.
    }

    @Override
    @Nonnull
    public Resource getResource() {
        return resource;
    }

    @Override
    @Nonnull
    public ValueMap getProperties() {
        return getResource().getValueMap();
    }

    @Override
    @Nonnull
    public ResourceResolver getResourceResolver() {
        return resourceResolver;
    }

    @Nonnull
    @Override
    public Logger getLogger() {
        if (logger == null) {
            logger = LoggerFactory.getLogger(getClass());
        }
        return logger;
    }

    @Override
    @Nonnull
    public SlingSettingsService getSlingSettingsService() {
        return slingSettingsService;
    }

    @Override
    @Nullable
    public SlingHttpServletRequest getRequest() {
        return request;
    }

    @PostConstruct
    @Override
    public void postConstruct() throws Exception {
        // stub for subclasses to inherit
    }

    /**
     * When this Sling Model is instantiated from a {@link Resource} (typically via {@code resource.adaptTo(...)}) it
     * will not have a {@link SlingHttpServletRequest object set.
     * <p>
     * This method is provided so that you can manually set the request object prior to invoking methods that may
     * require the request object.
     *
     * @param request {@link SlingHttpServletRequest} object.
     */
    public void setRequest(SlingHttpServletRequest request) {
        this.request = request;
    }

    /*
     * START DEBUGGING HELPER CODE
     * Helpful during component development. Should never be invoked by production code.
     */

    /**
     * Refer to the following for instructions on use:
     * <p>
     * {@code /apps/gigamon/components/content/_debugging/_debugging.html}
     */
    public Map<String, String> getDebugModel() throws Exception {
        initDebug();
        return debugModel;
    }

    /**
     * Refer to the following for instructions on use:
     * <p>
     * {@code /apps/gigamon/components/content/_debugging/_debugging.html}
     */
    public List<String> getDebugModelKeys() throws Exception {
        List<String> keys = new ArrayList<String>(getDebugModel().keySet());
        Collections.sort(keys);
        return keys;
    }

    /**
     * Refer to the following for instructions on use:
     * <p>
     * {@code /apps/gigamon/components/content/_debugging/_debugging.html}
     */
    public List<String> getDebugTypes() throws Exception {
        initDebug();
        return debugTypes.stream().map(x -> x.toString()).sorted().collect(Collectors.toList());
    }

    private void initDebug() throws Exception {
        if (debugModel != null && debugTypes != null) return;

        Map<String, String> model = new HashMap<String, String>();
        Set<Class<?>> types = new HashSet<Class<?>>();

        ComponentModelDebugHelper.appendToDebugModel("model", getClass(), this, model, types);

        this.debugModel = model;
        this.debugTypes = types;
    }

}
