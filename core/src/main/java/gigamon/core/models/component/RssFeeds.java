package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Property;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import com.citytechinc.cq.component.annotations.widgets.NumberField;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;

import javax.inject.Inject;

/**
 * RssFeeds Component
 */
@Component(value = "RssFeeds",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/rss-feeds.html",
        tabs = {@Tab(title = "RssFeeds"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RssFeeds extends ComponentSlingModel implements BackgroundXYTraits  {

    @DialogField(fieldLabel = "Feed URL", name = "./feedurl", ranking = 1,
            fieldDescription = "Default Gigamon blog URL is given")
    @TextField
    @Inject @ValueMapValue @Default(values="https://blog.gigamon.com/feed/")
    private String feedurl;

    public String getFeedurl() {
        return feedurl;
    }
    
    @DialogField(fieldLabel = "Max Count", name = "./maxCount", ranking = 2, defaultValue = "5")
    @NumberField(allowNegative = false)
    @Inject @ValueMapValue
    private String maxCount;
    
    public String getMaxCount() {
        return maxCount;
    }
    
    @DialogField(tab = 2)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;



}
