package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.component.traits.cta.CtaTrait;
import gigamon.core.models.component.traits.cta.CtaWithBtnStyleTrait;
import gigamon.core.models.component.traits.image.PrimaryImageFileTrait;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Standalone CTA component. Should rarely, if ever, be used. Components that need a
 * CTA can use the cta trait.
 */
@Component(value = "CTA",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/cta.html",
        tabs = {@Tab(title = "CTA"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Cta extends ComponentSlingModel implements CtaWithBtnStyleTrait, BackgroundXYTraits {

    @DialogField(fieldLabel = "Alignment", name = "./ctaAlignment", ranking = 1)
    @Selection(type="select", options = {
        @Option(text = "Left", value = "left"),
        @Option(text = "Center", value = "center"),
        @Option(text = "Right", value = "right")
    })
    @Inject @ValueMapValue
    private String ctaAlignment;

    public String getCtaAlignment() {
        return ctaAlignment;
    }    
    
    @DialogField(ranking = 5)
    @DialogFieldSet
    public final CtaWithBtnStyleTrait.DialogFields cta = null;

    @DialogField(tab = 2)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;

}
