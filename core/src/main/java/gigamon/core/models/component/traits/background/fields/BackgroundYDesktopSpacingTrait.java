package gigamon.core.models.component.traits.background.fields;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.ComponentTrait;
import gigamon.dialog.touch.heading.Heading;

/**
 * Contains vertical spacing options that apply at desktop screen sizes only.
 */
public interface BackgroundYDesktopSpacingTrait extends ComponentTrait {

    String TOP_SPACING = "backgroundTopSpacing";
    String BOTTOM_SPACING = "backgroundBottomSpacing";

    String NONE = "_empty_";

    String TOP_SPACING_XS = "vert-pad-%s-top-xs"; // 15
    String TOP_SPACING_SM = "vert-pad-%s-top-sm"; // 30
    String TOP_SPACING_MD = "vert-pad-%s-top-md"; // 60
    String TOP_SPACING_LG = "vert-pad-%s-top-lg"; // 90

    String BOTTOM_SPACING_XS = "vert-pad-%s-bottom-xs"; // 15
    String BOTTOM_SPACING_SM = "vert-pad-%s-bottom-sm"; // 30
    String BOTTOM_SPACING_MD = "vert-pad-%s-bottom-md"; // 60
    String BOTTOM_SPACING_LG = "vert-pad-%s-bottom-lg"; // 90

    interface DialogFields {

        @DialogField(ranking = 2)
        @Heading(text = "Top and bottom spacing applies to desktop screen sizes only. This typically is the 'MD' breakpoint, but may be overridden by individual components.", level = 4)
        void heading();

        @DialogField(fieldLabel = "Top Spacing", name = "./"+TOP_SPACING, ranking = 3)
        @Selection(type="select", options = {
            @Option(text = "None", value = NONE),
            @Option(text = "XS (15)", value = TOP_SPACING_XS),
            @Option(text = "SM (30)", value = TOP_SPACING_SM),
            @Option(text = "MD (60)", value = TOP_SPACING_MD),
            @Option(text = "LG (90)", value = TOP_SPACING_LG),
        })
        void topSpacing();

        @DialogField(fieldLabel = "Bottom Spacing", name = "./"+BOTTOM_SPACING, ranking = 4)
        @Selection(type="select", options = {
            @Option(text = "None", value = NONE),
            @Option(text = "XS (15)", value = BOTTOM_SPACING_XS),
            @Option(text = "SM (30)", value = BOTTOM_SPACING_SM),
            @Option(text = "MD (60)", value = BOTTOM_SPACING_MD),
            @Option(text = "LG (90)", value = BOTTOM_SPACING_LG),
        })
        void bottomSpacing();

    }

    /**
     * This is the breakpoint at which the padding rule kicks in. May be overridden by concrete class to customize.
     *
     * @return {@code md} by default.
     */
    default String getVerticalPaddingBreakpoint() {
        return "md";
    }

    default String getBackgroundTopSpacing() {
        String value = getProperties().get(TOP_SPACING, String.class);
        if (value != null) value = String.format(value, getVerticalPaddingBreakpoint());
        return NONE.equals(value) ? null : value;
    }

    default String getBackgroundBottomSpacing() {
        String value = getProperties().get(BOTTOM_SPACING, String.class);
        if (value != null) value = String.format(value, getVerticalPaddingBreakpoint());
        return NONE.equals(value) ? null : value;
    }

}
