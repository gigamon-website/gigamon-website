package gigamon.core.models.component.traits;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static org.osgi.util.measurement.Unit.s;

/**
 * This interface guarantees the minimum set of fields that all Component Models must provide.
 *
 * @author joelepps
 */
public interface ComponentTrait {

    /**
     * @return The {@link Resource} object for the current component instance.
     */
    @Nonnull
    Resource getResource();

    /**
     * @return The {@link ValueMap} object for the current {@link Resource}.
     */
    @Nonnull
    ValueMap getProperties();

    /**
     * @return The {@link ResourceResolver} object.
     */
    @Nonnull
    ResourceResolver getResourceResolver();

    /**
     * @return The logger object for the concrete class.
     */
    @Nonnull
    Logger getLogger();

    /**
     * @return The {@link SlingSettingsService}
     */
    @Nonnull
    SlingSettingsService getSlingSettingsService();

    /**
     * @return The {@link SlingHttpServletRequest}. May be null.
     */
    @Nullable
    SlingHttpServletRequest getRequest();

    /**
     * Perform any post construction initialization.
     * <p>
     * This operation may be called multiple times.
     *
     * @see <a href="https://sling.apache.org/documentation/bundles/models.html#postconstruct-methods">Sling Models Post Construct</a>
     */
    void postConstruct() throws Exception;

    /**
     * Returned value will be suitable for uses like the HTML id attribute.
     *
     * @return ID of the component.
     * @throws NoSuchAlgorithmException Should never happen.
     */
    default String getComponentId() throws NoSuchAlgorithmException {
        String path = getResource().getPath();
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(path.getBytes(), 0, path.length());
        // HTML id must start with a letter
        return "i" + new BigInteger(1, m.digest()).toString(16);
    }
}
