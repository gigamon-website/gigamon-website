package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.*;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.NumberField;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.image.MobileImageFileTrait;
import gigamon.core.models.component.traits.image.PrimaryImageFileTrait;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Background Image component provides control over the background image used at both desktop and mobile screen sizes.
 * <p>
 * Also provides control over height and image focal point.
 *
 * @author joelepps
 */
@Component(value = "Background Image",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/background-image.html",
        tabs = {
            @Tab(title = "Desktop Image"),
            @Tab(title = "Mobile Image"),
            @Tab(title = "Content"),
        })
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BackgroundImage extends ComponentSlingModel
    implements PrimaryImageFileTrait, MobileImageFileTrait {

    /*
        DESKTOP FIELDS
     */

    @DialogField(fieldLabel = "Use Fixed Height",
        name = "./desktopUseFixedHeight",
        ranking = 1,
        additionalProperties = {
            @Property(name = "class", value = "multi-field-showhide"),
            @Property(name = "multi-field-showhide-target", value = ".list-option-desktop-fixed-height-target")
        })
    @Selection(type="select", options = {
        @Option(text = "No", value = "false"),
        @Option(text = "Yes", value = "true"),
    })
    @Inject @ValueMapValue @Default(values = "false")
    private String desktopUseFixedHeight;

    @DialogField(fieldLabel = "Height", name = "./desktopHeight", ranking = 10, defaultValue = "500",
        additionalProperties = {
            @Property(name = "class", value = "list-option-desktop-fixed-height-target"),
            @Property(name = "showhidetargetvalue", value = "true"),
        })
    @NumberField(min = "0")
    @Inject @ValueMapValue @Default(values = "500")
    private String desktopHeight;

    @DialogField(fieldLabel = "Height Unit", name = "./desktopHeightUnit", ranking = 11,
        fieldDescription = "When using vh, config only shows with 'View as Published'",
        additionalProperties = {
            @Property(name = "class", value = "list-option-desktop-fixed-height-target"),
            @Property(name = "showhidetargetvalue", value = "true"),
        })
    @Selection(type="select", options = {
        @Option(text = "px", value = "px"),
        @Option(text = "vh (% of screen)", value = "vh"),
    })
    @Inject @ValueMapValue @Default(values = "vh")
    private String desktopHeightUnit;

    @DialogField(fieldLabel = "Vertical Focal Point", name = "./desktopVertFP", ranking = 15,
        fieldDescription = "0 for top, 100 for bottom", defaultValue = "50")
    @NumberField(min = "0", max = "100")
    @Inject @ValueMapValue @Default(values = "50")
    private String desktopVertFP;

    @DialogField(fieldLabel = "Horizontal Focal Point", name = "./desktopHorzFP", ranking = 16,
        fieldDescription = "0 for left, 100 for right", defaultValue = "50")
    @NumberField(min = "0", max = "100")
    @Inject @ValueMapValue @Default(values = "50")
    private String desktopHorzFP;

    @DialogField(ranking = 50)
    @DialogFieldSet
    private final PrimaryImageFileTrait.DialogFields image = null;

    /*
        MOBILE FIELDS
     */

    @DialogField(fieldLabel = "Use Mobile Image",
        name = "./useMobileImage",
        ranking = 1,
        tab = 2,
        additionalProperties = {
            @Property(name = "class", value = "multi-field-showhide"),
            @Property(name = "multi-field-showhide-target", value = ".list-option-use-mobile-target")
        })
    @Selection(type="select", options = {
        @Option(text = "No", value = "false"),
        @Option(text = "Yes", value = "true"),
    })
    @Inject @ValueMapValue @Default(values = "false")
    private String useMobileImage;

    @DialogField(fieldLabel = "Use Fixed Height",
        name = "./mobileUseFixedHeight",
        ranking = 9,
        tab = 2,
        additionalProperties = {
            @Property(name = "class", value = "multi-field-showhide list-option-use-mobile-target"),
            @Property(name = "showhidetargetvalue", value = "true"),
            @Property(name = "multi-field-showhide-target", value = ".list-option-mobile-fixed-height-target")
        })
    @Selection(type="select", options = {
        @Option(text = "No", value = "false"),
        @Option(text = "Yes", value = "true"),
    })
    @Inject @ValueMapValue @Default(values = "false")
    private String mobileUseFixedHeight;

    @DialogField(fieldLabel = "Height", name = "./mobileHeight", ranking = 10, tab = 2, defaultValue = "500",
        additionalProperties = {
            @Property(name = "class", value = "list-option-mobile-fixed-height-target"),
            @Property(name = "showhidetargetvalue", value = "true"),
        })
    @NumberField(min = "0")
    @Inject @ValueMapValue @Default(values = "500")
    private String mobileHeight;

    @DialogField(fieldLabel = "Height Unit", name = "./mobileHeightUnit", tab = 2, ranking = 11,
        fieldDescription = "When using vh, config only shows with 'View as Published'",
        additionalProperties = {
            @Property(name = "class", value = "list-option-mobile-fixed-height-target"),
            @Property(name = "showhidetargetvalue", value = "true"),
        })
    @Selection(type="select", options = {
        @Option(text = "px", value = "px"),
        @Option(text = "vh (% of screen)", value = "vh"),
    })
    @Inject @ValueMapValue @Default(values = "vh")
    private String mobileHeightUnit;

    @DialogField(fieldLabel = "Vertical Focal Point", name = "./mobileVertFP", ranking = 15, tab = 2,
        fieldDescription = "0 for top, 100 for bottom", defaultValue = "50",
        additionalProperties = {
            @Property(name = "class", value = "list-option-use-mobile-target"),
            @Property(name = "showhidetargetvalue", value = "true"),
        })
    @NumberField(min = "0", max = "100")
    @Inject @ValueMapValue @Default(values = "50")
    private String mobileVertFP;

    @DialogField(fieldLabel = "Horizontal Focal Point", name = "./mobileHorzFP", ranking = 16, tab = 2,
        fieldDescription = "0 for left, 100 for right", defaultValue = "50",
        additionalProperties = {
            @Property(name = "class", value = "list-option-use-mobile-target"),
            @Property(name = "showhidetargetvalue", value = "true"),
        })
    @NumberField(min = "0", max = "100")
    @Inject @ValueMapValue @Default(values = "50")
    private String mobileHorzFP;

    @DialogField(ranking = 50, tab = 2,
        additionalProperties = {
            @Property(name = "class", value = "list-option-use-mobile-target"),
            @Property(name = "showhidetargetvalue", value = "true"),
        })
    @DialogFieldSet
    private final MobileImageFileTrait.DialogFields mobileImage = null;

    /*
        CONTENT FIELDS
     */

    @DialogField(fieldLabel = "Vertical Alignment", name = "./verticalAlignment", tab = 3, ranking = 10)
    @Selection(type="select", options = {
        @Option(text = "Center", value = "v-center"),
        @Option(text = "Top", value = "v-top"),
        @Option(text = "Bottom", value = "v-bottom"),
    })
    @Inject @ValueMapValue @Default(values = "v-center")
    private String verticalAlignment;

    /*
        METHODS
     */

    public String getVerticalAlignment() {
        return verticalAlignment;
    }

    public String getDesktopImageStyles() {
        return " " + desktopHorzFP + "% " + desktopVertFP + "% / cover";
    }

    public String getDesktopHeight() {
        return desktopHeight;
    }

    public String getDesktopHeightUnit() {
        return desktopHeightUnit;
    }

    public boolean getDesktopUseFixedHeight() {
        return "true".equals(desktopUseFixedHeight);
    }

    public String getDesktopHeightCss() {
        if (getDesktopUseFixedHeight()) {
            return getDesktopHeight() + getDesktopHeightUnit();
        }
        return "auto";
    }

    public boolean isUseMobileImage() {
        return "true".equals(useMobileImage);
    }

    public String getMobileImageStyles() {
        if (!isUseMobileImage()) return getDesktopImageStyles();
        return " " + mobileHorzFP + "% " + mobileVertFP + "% / cover";
    }

    public String getMobileHeight() {
        if (!isUseMobileImage()) return getDesktopHeight();
        return mobileHeight;
    }

    public String getMobileHeightUnit() {
        if (!isUseMobileImage()) return getDesktopHeightUnit();
        return mobileHeightUnit;
    }

    public boolean getMobileUseFixedHeight() {
        if (!isUseMobileImage()) return getDesktopUseFixedHeight();
        return "true".equals(mobileUseFixedHeight);
    }

    public String getMobileHeightCss() {
        if (getMobileUseFixedHeight()) {
            return getMobileHeight() + getMobileHeightUnit();
        }
        return "auto";
    }

    @Override
    public String getMobileImage() {
        if (!isUseMobileImage()) return null;
        return MobileImageFileTrait.super.getMobileImage();
    }
}
