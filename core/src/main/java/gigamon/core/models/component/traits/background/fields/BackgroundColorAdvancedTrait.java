package gigamon.core.models.component.traits.background.fields;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.ComponentTrait;

/**
 * Contains the author fields for hex based background color.
 */
public interface BackgroundColorAdvancedTrait extends ComponentTrait {

	String FIELD_BACKGROUND_HEX_COLOR = "backgroundHex";

	interface DialogFields {
		@DialogField(fieldLabel = "Hex Code", name = "./"+FIELD_BACKGROUND_HEX_COLOR, ranking = 2,
				fieldDescription = "Will override regular background color")
		@TextField
		void hexColor();
	}

	default String getBackgroundHexStyle() {
		String hexValue = getProperties().get(FIELD_BACKGROUND_HEX_COLOR, String.class);
		if(hexValue != null && hexValue.trim().charAt(0) != '#') {
			hexValue = "#" + hexValue;
		}
		return hexValue != null ? "background-color:" + hexValue + ";" : "";
	}
}
