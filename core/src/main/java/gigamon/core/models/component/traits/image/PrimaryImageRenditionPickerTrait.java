package gigamon.core.models.component.traits.image;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.ComponentTrait;

/**
 * Generally not recommended to provide to authors.
 *
 * Allows author to select the rendition of the image.
 *
 * @author joelepps
 *         9/29/16
 */
public interface PrimaryImageRenditionPickerTrait extends ComponentTrait {

    String UNSET = "n/a";
    String FULL = "full";
    String HALF = "half";
    String ORIGINAL = "original";

    interface DialogFields {

        @DialogField(fieldLabel = "File Size Override", name = "./image/imageSize", ranking = 4,
                fieldDescription = "Used to manually control the file size of the image.")
        @Selection(type="select", options = {
                @Option(text = "Default", value = UNSET),
                @Option(text = "Full Screen Width", value = FULL),
                @Option(text = "Half Screen Width", value = HALF),
                @Option(text = "Original (Not Recommended)", value = ORIGINAL),
        })
        void imageSize();

    }

    default String getImageSizeRendition() {
        String value = getProperties().get("./image/imageSize", String.class);
        if (UNSET.equals(value)) return null;
        return (value == null) ? "original" : value;
    }

}
