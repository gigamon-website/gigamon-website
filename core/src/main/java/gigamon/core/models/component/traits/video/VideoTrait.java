package gigamon.core.models.component.traits.video;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.ComponentTrait;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.sling.api.SlingHttpServletRequest;

import java.security.NoSuchAlgorithmException;

/**
 * Supplies all fields necessary for video rendering.
 *
 * @author joelepps
 *         4/5/16
 */
public interface VideoTrait extends ComponentTrait {

    String PROVIDER_YOUTUBE = "youtube";
    String PROVIDER_VIMEO = "vimeo";

    interface DialogFields {

        @DialogField(fieldLabel = "Provider", name = "./videoProvider", ranking = 99, defaultValue = PROVIDER_YOUTUBE)
        @Selection(type="select", options = {
            @Option(text = "YouTube", value = PROVIDER_YOUTUBE),
            @Option(text = "Vimeo", value = PROVIDER_VIMEO)
        })
        void videoProvider();

        @DialogField(fieldLabel = "Video ID", name = "./videoId", ranking = 100,
            fieldDescription = "YouTube or Vimeo video ID.")
        @TextField
        void videoId();

        @DialogField(fieldLabel = "Auto Play", name = "./autoPlay", ranking = 101, value = "true")
        @CheckBoxTouch(text = "Auto Play")
        void autoPlay();


        @DialogField(fieldLabel = "Play in modal", name = "./playModal", ranking = 102, value = "true",
        	fieldDescription = "Thumbnail MUST be authored and enabled in order to play in modal.")
        @CheckBoxTouch(text = "Play in modal")
        void playModal();

        @DialogField(fieldLabel = "Show Thumbnail", name = "./showThumbnail", ranking = 104, value = "true")
        @CheckBoxTouch(text = "Show Thumbnail")
        void showThumbnail();

        @DialogField(fieldLabel = "CTA Text", name = "./ctaText", ranking = 103,
            fieldDescription = "CTA Text")
        @TextField
        void ctaText();

    }

    /**
     * @return {@code true} if vimeo script needs to be added to page, {@code false} if script has already been included.
     */
    default boolean getIncludeVimeoScript() {
        if (PROVIDER_VIMEO.equals(getVideoProvider())) {
            SlingHttpServletRequest request = getRequest();
            if (request == null) {
                getLogger().warn("Request is null, cannot determine vimeo script include: {}", getResource());
                return false;
            }
            String key = VideoTrait.class.getName() + ".vimeo.script.loaded";

            String loaded = (String) request.getAttribute(key);
            if (loaded == null) {
                request.setAttribute(key, "true");
                return true;
            }
        }

        return false;
    }

    default String getVideoProvider() {
        return getProperties().get("videoProvider", String.class);
    }

    default String getVideoId() {
        return getProperties().get("videoId", String.class);
    }

    default String getVideoPlayerId() throws NoSuchAlgorithmException {
        return "vid" + getComponentId();
    }

    default boolean isAutoPlay() {
        return BooleanUtils.toBoolean(getProperties().get("autoPlay", String.class));
    }

    default boolean isPlayModal() {
        return BooleanUtils.toBoolean(getProperties().get("playModal", String.class));
    }

    default boolean isShowThumbnail() {
        return BooleanUtils.toBoolean(getProperties().get("showThumbnail", String.class));
    }

    default String getCtaText() {
        return getProperties().get("ctaText", String.class);
    }

}
