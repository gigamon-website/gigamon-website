package gigamon.core.models.component;

import javax.inject.Inject;

import gigamon.dialog.touch.checkbox.CheckBoxTouch;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.TextField;

/**
 * SimpleNavigation Component
 */
@Component(value = "SimpleNavigation", group = ComponentSlingModel.COMPONENT_GROUP_CONTENT, disableTargeting = true, helpPath = "/editor.html/content/gigamon-components/simple-navigation.html", tabs = {
		@Tab(title = "Logo"), @Tab(title = "CTA") })
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SimpleNavigation extends ComponentSlingModel {

	/*
	 * @DialogField
	 * 
	 * @DialogFieldSet public final PrimaryImageWithAltTextTrait.DialogFields
	 * image = null;
	 */

	@DialogField(fieldLabel = "Home Page", name = "./homePage", ranking = 1)
	@PathField(rootPath = "/content")
	@Inject
	@ValueMapValue
	private String homePage;

	@DialogField(fieldLabel = "Logo Path", name = "./logoPath", ranking = 2)
	@PathField(rootPath = "/content")
	@Inject
	@ValueMapValue
	private String logoPath;

	@DialogField(tab = 2, fieldLabel = "Title", name = "./title", ranking = 100, fieldDescription = "The text of the CTA", required = true)
	@TextField
	@Inject
	@ValueMapValue
	private String title;

	@DialogField(tab = 2, fieldLabel = "Link", name = "./link", ranking = 102)
	@PathField(rootPath = "/content/")
	@Inject
	@ValueMapValue
	private String link;

	@DialogField(tab = 2, fieldLabel = "New Tab", name = "./newTab", ranking = 103, value = "true")
	@CheckBoxTouch(text = "New Tab")
	@Inject
	@ValueMapValue
	private String newTab;
	
	
	  @DialogField(fieldLabel = "Hide Button", name = "./hidebutton", ranking = 104, value = "true",tab = 2)
		@CheckBoxTouch(text = "Hide Button In Simple Nav")
		@Inject
		@ValueMapValue
		private String hidebutton;
	    
	    public Boolean getHidebutton() {
			return "true".equals(hidebutton);
		}

	public String getTitle() {
		return title;
	}

	public String getLink() {
		return link + ".html";
	}

	public String getNewTab() {
		if ("true".equals(newTab))
			return "_blank";
		else
			return "";
	}

	public String getHomePage() {
		return homePage + ".html";
	}

	public String getLogoPath() {
		return logoPath;
	}

}
