package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.SubMenuBlock;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gigamon.core.models.component.traits.background.BackgroundYTraits;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

/**
 * SubMenu Component
 */
@Component(value = "SubMenu",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/content/gigamon-components/sub-menu.html",
        tabs = {@Tab(title = "SubMenu"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SubMenu extends ComponentSlingModel {

    private static final Logger log = LoggerFactory.getLogger(SubMenu.class);

    private List<SubMenuBlock> nonFeatureSubMenuBlocks;

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();

        Resource subMenuResource = getResource();
        nonFeatureSubMenuBlocks = new ArrayList<>();
        int count = 0;
        int columnCount = (numColumns == null) ? 0 : Integer.parseInt(numColumns);
        for (Resource columnResource : subMenuResource.getChildren()) {
            if (count < columnCount) {
                for (Resource subMenuBlockResource : columnResource.getChildren()) {
                    if (subMenuBlockResource.getResourceType().equals("gigamon/components/content/sub-menu-block")) {
                    	nonFeatureSubMenuBlocks.add(subMenuBlockResource.adaptTo(SubMenuBlock.class));
                    }
                }
            }

            count++;
        }
    }

    @DialogField(fieldLabel = "# of Columns", name = "./numColumns", ranking = 1)
    @Selection(type="select", options = {
        @Option(text = "0", value = "0"),
        @Option(text = "1", value = "1"),
        @Option(text = "2", value = "2"),
        @Option(text = "3", value = "3"),
        @Option(text = "4", value = "4"),
        @Option(text = "5", value = "5"),
    })
    @Inject
    @ValueMapValue
    private String numColumns;

    public String getNumColumns() {
        return numColumns;
    }

    @DialogField(fieldLabel = "# of Feature Columns", name = "./numFeatureColumns", fieldDescription = "Feature columns are rendered on the right",ranking = 2)
    @Selection(type="select", options = {
        @Option(text = "0", value = "0"),
        @Option(text = "1", value = "1"),
        @Option(text = "2", value = "2"),
        @Option(text = "3", value = "3"),
        @Option(text = "4", value = "4"),
        @Option(text = "5", value = "5"),
    })
    @Inject @ValueMapValue
    private String numFeatureColumns;

    public String getNumFeatureColumns() {
        return numFeatureColumns;
    }

    @DialogField(tab = 2)
    @DialogFieldSet
    private final BackgroundYTraits.DialogFields backgroundFields = null;

    private String[] numColArray = null;
    public String[] getNumColArray() {
        String n = getNumColumns();
        int num = Integer.parseInt(n);
        numColArray=new String[num];

        return numColArray;
    }

    private String[] numFeaturesColArray = null;
    public String[] getNumFeaturesColArray() {
        String n = getNumFeatureColumns();
        int num = Integer.parseInt(n);
        numFeaturesColArray=new String[num];

        return numFeaturesColArray;
    }

    private String[] totalNumColumnsArray = null;
    public String[] getTotalNumColumnsArray(){
        int regularColumns = Integer.parseInt(getNumColumns());
        int featureColumns = Integer.parseInt(getNumFeatureColumns());
        int total = regularColumns + featureColumns;
        String[] numColumnArray=new String[total];

        return numColumnArray;
    }

    private int totalNumColumns = 0;
    public int getTotalNumColumns(){
        int regularColumns = Integer.parseInt(getNumColumns());
        return regularColumns;
    }

    public List<SubMenuBlock> getNonFeatureSubMenuBlocks() {
        return nonFeatureSubMenuBlocks;
    }
}
