package gigamon.core.models.component.traits.cta;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.ComponentTrait;
import gigamon.core.util.SlingModelUtils;
import gigamon.dialog.classic.multifield.MultiCompositeField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;
import java.util.List;

/**
 * Alternate CTA trait that allows authors to select the CTA button style.
 *
 * @author joelepps
 *         8/18/16
 */
public interface CtaWithBtnStyleTrait extends ComponentTrait {

    interface DialogFields {

        @DialogField(fieldLabel = "CTA List", name = "./cta", ranking = 101)
        @MultiCompositeField(maxLimit = 2) // will inspect generic (CtaModel) dialog annotations
        List<CtaModelWithBtnStyle> ctaList();

    }

    default List<CtaModelWithBtnStyle> getCtaList() {
        Resource ctaListResource = getResource().getChild("cta");
        return SlingModelUtils.buildComponentSlingModels(ctaListResource, getRequest(), CtaModelWithBtnStyle.class, true);
    }

    @Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
            defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
    class CtaModelWithBtnStyle extends CtaTrait.CtaModel {

        private static final String DEFAULT = "_default_";
        private static final String SECONDARY = "btn-secondary";
        private static final String LINK = "btn-link";
        private static final String LINKNOARROW = "btn-link-no-arrow";

        @DialogField(fieldLabel = "Style", name = "./style", defaultValue = DEFAULT, ranking=102.5
        	        )
        @Selection(type="select", options = {
                @Option(text = "Button", value = DEFAULT),
                @Option(text = "Button Secondary", value = SECONDARY),
                @Option(text = "Link", value = LINK),
                @Option(text = "Link no Arrow", value = LINKNOARROW),
        })
        @Inject @ValueMapValue
        private String style;

        public String getCtaStyle() {
            if (DEFAULT.equals(style)) return null;
            return style;
        }
        
        @DialogField(fieldLabel = "Stroke", name = "./stroke", ranking = 104,defaultValue="default-stroke" ,fieldDescription = "Stroke can be applied for only style Button "
        		 
               
        	        )
        @Selection(type="select", options = {
        		@Option(text = "Default", value = "default-stroke"),
        	    @Option(text = "No Stroke", value = "no-stroke"),
                @Option(text = "White", value = "white-stroke"),
                @Option(text = "Grey", value = "grey-stroke"),
                @Option(text = "Orange", value = "orange-stroke")
        })
        @Inject @ValueMapValue
        private String stroke;

		public String getStroke() {
			return stroke;
		}
		
        
		  @DialogField(fieldLabel = "Fill", name = "./buttonfill", ranking = 105,defaultValue="default-fill" ,fieldDescription = "Fill can be applied for only style Button " )
	        		
	        		
	        	       
	        @Selection(type="select", options = {
	        	      @Option(text = "Default", value = "default-fill"),
	        		  @Option(text = "No Fill", value = "no-fill"),
	        		  @Option(text = "Orange", value = "orange-fill"),
	                  @Option(text = "Aqua", value = "aqua-fill"),
	                  @Option(text = "Blue", value = "blue-fill"),
	                  @Option(text = "Purple", value = "purple-fill"),
	                  @Option(text = "Teal", value = "teal-fill"),
	                  @Option(text = "White", value = "white-fill"),
	                  @Option(text = "Grey", value = "grey-fill")
	        })
	        @Inject @ValueMapValue
	        private String buttonfill;

		public String getButtonfill() {
			return buttonfill;
		}

			
    }

}
