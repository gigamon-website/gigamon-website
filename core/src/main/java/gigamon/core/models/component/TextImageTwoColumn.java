package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.analytics.AnalyticsTrait;
import gigamon.core.models.component.traits.background.BackgroundYTraits;
import gigamon.core.models.component.traits.cta.CtaTrait;
import gigamon.core.models.component.traits.cta.CtaWithBtnStyleTrait;
import gigamon.core.models.component.traits.image.PrimaryImageWithAltTextTrait;
import gigamon.core.models.component.traits.richtext.RichTextFieldTrait;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Text Image Two Column Component
 * <p>
 * Supports standard use case of responsive component that renders text in one column and image in another.
 */
@Component(value = "Text Image 2 Column",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/text-image-2-colum.html",
        inPlaceEditingEditorType = "text",
        inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-config/dialog/items/tabs/items/RichTextConfigurations/items/standardRichText",
        tabs = {@Tab(title = "Text Image 2 Column"), @Tab(title = "Image"), @Tab(title = "CTA"), @Tab(title = "Background"), @Tab(title = "Analytics")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TextImageTwoColumn extends ComponentSlingModel implements PrimaryImageWithAltTextTrait, CtaWithBtnStyleTrait, RichTextFieldTrait, BackgroundYTraits, AnalyticsTrait {

    public static final String RENDITION_IMAGE_RIGHT = "imageRight";
    public static final String RENDITION_IMAGE_LEFT = "imageLeft";

    @DialogField(fieldLabel = "Title", name = "./title", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String title;

    @DialogField(fieldLabel = "Super Title", name = "./superTitle", ranking = 2)
    @TextField
    @Inject @ValueMapValue
    private String superTitle;

    @DialogField(fieldLabel = "Rendition", name = "./textImage2ColumnRendition", ranking = 5)
    @Selection(type="select", options = {
            @Option(text = "Image | Text", value = RENDITION_IMAGE_LEFT),
            @Option(text = "Text | Image", value = RENDITION_IMAGE_RIGHT),
    })
    @Inject @ValueMapValue
    private String textImage2ColumnRendition;

    public String getTitle() {
        return title;
    }

    public String getSuperTitle() {
        return superTitle;
    }

    public String getTextImage2ColumnRendition() {
        return textImage2ColumnRendition;
    }

    public String getTextColClasses() {
        String rendition = getTextImage2ColumnRendition();
        if (RENDITION_IMAGE_RIGHT.equals(rendition)) {
            return "col-md-pull-4";
        }
        return null;
    }

    public String getImageColClasses() {
        String rendition = getTextImage2ColumnRendition();
        if (RENDITION_IMAGE_RIGHT.equals(rendition)) {
            return "col-md-push-6";
        }
        return null;
    }

    @DialogField(tab = 2)
    @DialogFieldSet
    public final PrimaryImageWithAltTextTrait.DialogFields image = null;

    @DialogField(tab = 3)
    @DialogFieldSet
    public final CtaWithBtnStyleTrait.DialogFields cta = null;
    // public final CtaTrait.DialogFields cta = null;

    @DialogField(tab = 4)
    @DialogFieldSet
    public final BackgroundYTraits.DialogFields background = null;

    @DialogField(tab = 5)
    @DialogFieldSet
    public final AnalyticsTrait.DialogFields analytics = null;

}
