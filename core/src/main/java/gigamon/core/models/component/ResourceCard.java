package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;

import gigamon.core.util.ServiceUtils;

import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;

import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.component.traits.image.PrimaryImageWithAltTextTrait;
import gigamon.core.models.component.traits.richtext.RichTextFieldTrait;
import org.apache.sling.api.SlingHttpServletRequest;
import gigamon.core.models.component.traits.cta.CtaTrait;
import gigamon.core.models.component.traits.cta.CtaWithBtnStyleTrait;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * ResourceCard Component
 */
@Component(value = "ResourceCard",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/resource-card.html",
        inPlaceEditingEditorType = "text",
        inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-config/dialog/items/tabs/items/RichTextConfigurations/items/standardRichText",
        tabs = {@Tab(title = "ResourceCard"), @Tab(title = "Image"), @Tab(title = "Background"), @Tab(title = "CTA")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ResourceCard extends ComponentSlingModel implements CtaWithBtnStyleTrait, PrimaryImageWithAltTextTrait, RichTextFieldTrait, BackgroundXYTraits {

    @DialogField(fieldLabel = "Title", name = "./resourceCardTitle", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String resourceCardTitle;

    public String getResourceCardTitle() {
        return resourceCardTitle;
    }


    @DialogField(fieldLabel = "Super Title", name = "./supertitle", ranking = 2)
    @TextField
    @Inject @ValueMapValue
    private String supertitle;

    public String getSupertitle() {
        return supertitle;
    }

    @DialogField(fieldLabel = "Is Resource Square", name = "./isresourcesquare", ranking = 4, value = "true")
   	@CheckBoxTouch(text = "is resource square")
   	@Inject
   	@ValueMapValue
   	private String isresourcesquare;

       public Boolean getIsresourcesquare() {
   		return "true".equals(isresourcesquare);
   	}



       @DialogField(fieldLabel = "Border", name = "./border",ranking =5,defaultValue="no-border")



       @Selection(type="select", options = {
       	      @Option(text = "No", value = "no-border"),
       		  @Option(text = "Yes", value = "border"),
       })
       @Inject @ValueMapValue
       private String border;

       public String getBorder() {
   		return border;
   	}


    @DialogField(fieldLabel = "Meta Data", name = "./metaData", ranking = 2)
    @TextField
    @Inject @ValueMapValue
    private String metaData;

    public String getMetaData() {
        return metaData;
    }

    public static final String CROP = "crop";
    public static final String ORIGINAL = "original";

    @DialogField(fieldLabel = "Crop Image", name = "./cropImage", ranking = 5, defaultValue = CROP)
    @Selection(type="select", options = {
            @Option(text = "Crop", value = CROP),
            @Option(text = "Original", value = ORIGINAL),
    })
    @Inject @ValueMapValue @Default(values = CROP)
    private String cropImage;

    public String getCropImage() {
        return cropImage;
    }


    @DialogField(tab = 2)
    @DialogFieldSet
    public final PrimaryImageWithAltTextTrait.DialogFields image = null;

    @DialogField(tab = 3)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;

    @DialogField(tab = 4)
    @DialogFieldSet
    public final CtaWithBtnStyleTrait.DialogFields cta = null;

}
