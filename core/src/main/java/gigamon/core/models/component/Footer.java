package gigamon.core.models.component;

import gigamon.core.util.ServiceUtils;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.TextField;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Footer Component
 */
@Component(value = "Footer",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/footer.html",
        tabs = {@Tab(title = "Contact Us/careers"), @Tab(title = "Portal"), @Tab(title = "Additional Options"), @Tab(title = "Social Share"), @Tab(title = "T&C/policy")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Footer extends ComponentSlingModel {

	//TAB1 (2 fields) --> Contact Us/careers
    @DialogField(fieldLabel = "Contact Us text", name = "./contactUsText", tab = 1, ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String contactUsText;

    public String getContactUsText() {
        return contactUsText;
    }

    @DialogField(fieldLabel = "Contact Us redirect", name = "./contactUsUrl", tab = 1, ranking = 2, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String contactUsUrl;

    public String getContactUsUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), contactUsUrl);
    }

    @DialogField(fieldLabel = "Careers text", name = "./careerText", tab = 1, ranking = 3)
    @TextField
    @Inject @ValueMapValue
    private String careerText;

    public String getCareerText() {
        return careerText;
    }

    @DialogField(fieldLabel = "Careers redirect", name = "./careersUrl", tab = 1, ranking = 4, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String careersUrl;

    public String getCareersUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), careersUrl);
    }

	//TAB2 (2 fields) --> Portal
    @DialogField(fieldLabel = "Customer Portal text", name = "./customerPortalText", tab = 2, ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String customerPortalText;

    public String getCustomerPortalText() {
        return customerPortalText;
    }

    @DialogField(fieldLabel = "Customer Portal redirect", name = "./customerPortalUrl", tab = 2, ranking = 2, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String customerPortalUrl;

    public String getCustomerPortalUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), customerPortalUrl);
    }

    @DialogField(fieldLabel = "Partner Portal text", name = "./partnerPortalText", tab = 2, ranking = 3)
    @TextField
    @Inject @ValueMapValue
    private String partnerPortalText;

    public String getPartnerPortalText() {
        return partnerPortalText;
    }

    @DialogField(fieldLabel = "Partner Portal redirect", name = "./partnerPortalUrl", tab = 2, ranking = 4, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String partnerPortalUrl;

    public String getPartnerPortalUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), partnerPortalUrl);
    }

	//TAB3 (3 fields) --> Additional Options
    @DialogField(fieldLabel = "Company Information text", name = "./companyInfoText", tab = 3, ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String companyInfoText;

    public String getCompanyInfoText() {
        return companyInfoText;
    }

    @DialogField(fieldLabel = "Company Information redirect", name = "./companyInfoUrl", tab = 3, ranking = 2, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String companyInfoUrl;

    public String getCompanyInfoUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), companyInfoUrl);
    }


    @DialogField(fieldLabel = "In the news text", name = "./inNewsText", tab = 3, ranking = 3)
    @TextField
    @Inject @ValueMapValue
    private String inNewsText;

    public String getInNewsText() {
        return inNewsText;
    }

    @DialogField(fieldLabel = "In the news redirect", name = "./inNewsUrl", tab = 3, ranking = 4, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String inNewsUrl;

    public String getInNewsUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), inNewsUrl);
    }

    @DialogField(fieldLabel = "Blog text", name = "./blogText", tab = 3, ranking = 5)
    @TextField
    @Inject @ValueMapValue
    private String blogText;

    public String getBlogText() {
        return blogText;
    }

    @DialogField(fieldLabel = "Blog redirect", name = "./blogUrl", tab = 3, ranking = 6, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String blogUrl;

    public String getBlogUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), blogUrl);
    }

	//TAB4 (5 fields) --> Social Share\

    @DialogField(fieldLabel = "Blogger redirect", name = "./bloggerUrl", tab = 4, ranking = 1, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String bloggerUrl;

    public String getBloggerUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), bloggerUrl);
    }

    @DialogField(fieldLabel = "Facebook redirect", name = "./facebookUrl", tab = 4, ranking = 2, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String facebookUrl;

    public String getFacebookUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), facebookUrl);
    }

    @DialogField(fieldLabel = "Twitter redirect", name = "./twitterUrl", tab = 4, ranking = 3, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String twitterUrl;

    public String getTwitterUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), twitterUrl);
    }

    @DialogField(fieldLabel = "Linkedin redirect", name = "./linkedInUrl", tab = 4, ranking = 4, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String linkedInUrl;

    public String getLinkedInUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), linkedInUrl);
    }

    @DialogField(fieldLabel = "Youtube redirect", name = "./youtubeUrl", tab = 4, ranking = 5, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String youtubeUrl;

    public String getYoutubeUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), youtubeUrl);
    }

    //TAB5 (3 fields) --> T&C/policy
    @DialogField(fieldLabel = "Gigamon Copy Right text", name = "./copyRightText", tab = 5, ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String copyRightText;

    public String getCopyRightText() {
        return copyRightText;
    }

    @DialogField(fieldLabel = "Terms & Condition text", name = "./tncText", tab = 5, ranking = 2)
    @TextField
    @Inject @ValueMapValue
    private String tncText;

    public String getTncText() {
        return tncText;
    }

    @DialogField(fieldLabel = "Terms & Condition redirect", name = "./tncUrl", tab = 5, ranking = 3, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String tncUrl;

    public String getTncUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), tncUrl);
    }

    @DialogField(fieldLabel = "Privacy Policy text", name = "./privacyPolicyText", tab = 5, ranking = 4)
    @TextField
    @Inject @ValueMapValue
    private String privacyPolicyText;

    public String getPrivacyPolicyText() {
        return privacyPolicyText;
    }

    @DialogField(fieldLabel = "Privacy Policy redirect", name = "./privacyPolicyUrl", tab = 5, ranking = 5, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String privacyPolicyUrl;

    public String getPrivacyPolicyUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), privacyPolicyUrl);
    }




    @DialogField(fieldLabel = "Cookie Policy text", name = "./cookiePolicyText", tab = 5, ranking = 6)
    @TextField
    @Inject @ValueMapValue
    private String cookiePolicyText;

    public String getCookiePolicyText() {
        return cookiePolicyText;
    }

    @DialogField(fieldLabel = "Cookie Policy redirect", name = "./cookiePolicyUrl", tab = 5, ranking = 7, fieldDescription = "Choose path from repository or enter the external link")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String cookiePolicyUrl;

    public String getCookiePolicyUrl() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), cookiePolicyUrl);
    }



}
