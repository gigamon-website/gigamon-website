package gigamon.core.models.component.traits.background.fields;

import com.citytechinc.cq.component.annotations.DialogField;
import gigamon.core.models.component.traits.ComponentTrait;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import gigamon.dialog.touch.heading.Heading;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * Contains options for controlling component visibility.
 */
public interface BackgroundVisibilityTrait extends ComponentTrait {

    String HIDDEN_XS = "hiddenXs";
    String HIDDEN_SM = "hiddenSm";
    String HIDDEN_MD = "hiddenMd";
    String HIDDEN_LG = "hiddenLg";

	interface DialogFields {
        @DialogField(ranking = 49)
        @Heading(level = 4, text = "Hide this component at different breakpoints. In edit mode a red background is added if the component would be hidden at the current screen size.")
        void heading1();

        @DialogField(fieldLabel = "Hide at Extra Small", name = "./"+HIDDEN_XS, ranking = 50, value = "true")
        @CheckBoxTouch(text = "Hide at Extra Small")
        @Inject
        @ValueMapValue
        void hiddenXs();

        @DialogField(fieldLabel = "Hide at Small", name = "./"+HIDDEN_SM, ranking = 51, value = "true")
        @CheckBoxTouch(text = "Hide at Small")
        @Inject @ValueMapValue
        void hiddenSm();

        @DialogField(fieldLabel = "Hide at Medium", name = "./"+HIDDEN_MD, ranking = 52, value = "true")
        @CheckBoxTouch(text = "Hide at Medium")
        @Inject @ValueMapValue
        void hiddenMd();

        @DialogField(fieldLabel = "Hide at Large", name = "./"+HIDDEN_LG, ranking = 53, value = "true")
        @CheckBoxTouch(text = "Hide at Large")
        @Inject @ValueMapValue
        void hiddenLg();
	}

	default String getVisibilityClasses() {
		String xs = getProperties().get(HIDDEN_XS, String.class);
		String sm = getProperties().get(HIDDEN_SM, String.class);
        String md = getProperties().get(HIDDEN_MD, String.class);
        String lg = getProperties().get(HIDDEN_LG, String.class);

        String classes = "";
        if (xs != null) classes += " hidden-xs";
        if (sm != null) classes += " hidden-sm";
        if (md != null) classes += " hidden-md";
        if (lg != null) classes += " hidden-lg";

        return StringUtils.trimToNull(classes);
	}
}
