package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.analytics.AnalyticsTrait;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.component.traits.cta.CtaTrait;
import gigamon.core.models.component.traits.cta.CtaWithBtnStyleTrait;
import gigamon.core.models.component.traits.richtext.RichTextFieldTrait;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import gigamon.dialog.touch.collapsible.Collapsible;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static com.day.cq.wcm.foundation.List.log;

/**
 * This is the generic one-size-fits-all component used whenever there is a need for any combination of
 * title, rich text, and cta.
 *
 * @author joelepps
 */
@Component(value = "Text Jumbo",
    group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
    disableTargeting = true,
    helpPath = "/editor.html/content/gigamon-components/text-jumbo.html",
    inPlaceEditingEditorType = "text",
    inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-config/dialog/items/tabs/items/RichTextConfigurations/items/standardRichText",
    tabs = {@Tab(title = "Text Jumbo"), @Tab(title = "CTA"), @Tab(title = "Background"), @Tab(title = "Analytics")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TextJumbo extends ComponentSlingModel implements CtaWithBtnStyleTrait, BackgroundXYTraits, RichTextFieldTrait, AnalyticsTrait {

    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_CENTER = "center";

    public static final String H1 = "h1";
    public static final String H2 = "h2";
    public static final String H3 = "h3";

    public static final String LIGHT = "light";
    public static final String BOLD = "bold";
    public static final String MEDIUM = "medium";
    public static final String EXTRA_BOLD = "extra-bold";
    
    @DialogField(fieldLabel = "Title", name = "./title", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String title;

    @DialogField(fieldLabel = "Title Size", name = "./textJumboTitleSize", ranking = 2, defaultValue = H2,
        fieldDescription = "There should only ever be one H1 on a page, at the very top of the page")
    @Selection(type="select", options = {
        @Option(text = "Medium", value = H2),
        @Option(text = "Small", value = H3),
        @Option(text = "H1", value = H1),
    })
    @Inject @ValueMapValue
    private String textJumboTitleSize;
    
    
    @DialogField(fieldLabel = "Font Weight", name = "./fontWeight",tab=1, ranking = 2.5, defaultValue = BOLD)
    @Selection(type="select", options = {
  		  @Option(text = "Bold", value = BOLD),
            @Option(text = "Light", value = LIGHT),
            @Option(text = "Medium", value = MEDIUM),
            @Option(text = "Extra bold", value = EXTRA_BOLD),
    })
    @Inject @ValueMapValue
    private String fontWeight;
    
    @Inject @ValueMapValue
    private String textJumClasses;
	  
	  public String getFontWeight() {
        return fontWeight;
    }
	  
	public String getTextJumClasses(){
		String font = getFontWeight();
		String titleclass = getTextJumboTitleClasses();
		String classes =" ";
	
		if(null != font)
			
				{
			classes=classes+font+" ";
			
				}
		
		if(null != titleclass)
				{
			classes=classes+titleclass;  
			
				}
		return classes;
		 
	}
	

    @DialogField(fieldLabel = "Alignment", name = "./textJumboAlignment", ranking = 3, defaultValue = ALIGN_LEFT)
    @Selection(type="select", options = {
        @Option(text = "Left", value = ALIGN_LEFT),
        @Option(text = "Center", value = ALIGN_CENTER),
    })
    @Inject @ValueMapValue
    private String textJumboAlignment;

    @DialogField(fieldLabel = "Color", name = "./textJumboColor", ranking = 5)
    @Selection(type = "select", options = {
        @Option(text = "Dark", value = "dark-text"),
        @Option(text = "Light", value = "white-text"),
        @Option(text = "Orange", value = "orange-text")
    })
    @Inject @ValueMapValue
    private String textJumboColor;

    public String getTitle() {
        return title;
    }

    public String getTextJumboTitleSize() {
        return textJumboTitleSize;
    }

    public String getTextJumboAlignment() {
        return textJumboAlignment;
    }

    public String getTextJumboTitleClasses() {
        
        String value = getTextJumboTitleSize();
        if (H1.equals(value)) {
            return "h1-small";
        }
        return null;
    }

    public String getTextJumboColor() {
        return textJumboColor;
    }

    @DialogField(fieldLabel = "Header w/ CTA", name = "./headerWithCTA", ranking = 6, value = "align-horz")
    @CheckBoxTouch(text = "Header w/ CTA")
    @Inject @ValueMapValue
    private String headerWithCTA;

    public String getHeaderWithCTA() {
        return headerWithCTA;
    }

    @DialogField(tab = 2)
    @DialogFieldSet
    public final CtaWithBtnStyleTrait.DialogFields cta = null;

    @DialogField(tab = 3)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;

    @DialogField(tab = 4)
    @DialogFieldSet
    public final AnalyticsTrait.DialogFields analytics = null;

}
