package gigamon.core.models.component.traits.background;

import com.citytechinc.cq.component.annotations.DialogField;
import gigamon.core.models.component.traits.background.fields.BackgroundColorTrait;
import gigamon.core.models.component.traits.background.fields.BackgroundYUniversalSpacingTrait;
import gigamon.dialog.touch.heading.Heading;

/**
 * Grouping of background fields. Provides color and vertical (y) spacing options.
 */
public interface BackgroundYTraits
        extends BackgroundColorTrait, BackgroundYUniversalSpacingTrait {

    interface DialogFields
            extends BackgroundColorTrait.DialogFields,
            BackgroundYUniversalSpacingTrait.DialogFields {

        @DialogField(ranking = -1)
        @Heading(text = "These values should be set only on the outermost component. " +
                "For instance, values should be left empty if this component is nested inside of a Columns component.")
        void description();

    }

}
