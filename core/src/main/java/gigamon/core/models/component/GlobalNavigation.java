package gigamon.core.models.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.day.cq.wcm.api.Page;

import gigamon.core.models.data.BrandLogoItem;
import gigamon.core.models.data.GlobalNavigationItem;
import gigamon.core.models.data.SimpleLink;
import gigamon.core.util.ServiceUtils;
import gigamon.dialog.classic.multifield.MultiCompositeField;
import org.apache.sling.models.annotations.Default;

/**
 * GlobalNavigation Component
 * Created by Sanketh 3/23/2017
 */
@Component(value = "Global Navigation",
    group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
    disableTargeting = true,
    tabs = {@Tab(title = "General"),
            @Tab(title = "Navigation"),
            @Tab(title = "Secondary Nav"),
            @Tab(title = "Language Selector"),
    		@Tab(title = "Contact Sales")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
    defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GlobalNavigation extends ComponentSlingModel {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalNavigation.class);

    private static final String THEME_LIGHT = "light-theme";
    private static final String THEME_DARK = "dark-theme";
    private static final String URL_PARAMETER_THEME = "theme";
    private static final String URL_PARAMETER_HEX = "hex";
    private static final String URL_PARAMETER_SHOW_PROMO = "showPromo";

    private String backgroundHex;
    private String theme;
    private boolean showPromo;

    private boolean external = false;
    private List<List<BrandLogoItem>> mobileBrandLogos;

    @Inject
    private Page currentPage;

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();

        SlingHttpServletRequest request = getRequest();
        String requestTheme = request.getParameter(URL_PARAMETER_THEME);
        String requestHex = request.getParameter(URL_PARAMETER_HEX);
        String requestPromo = request.getParameter(URL_PARAMETER_SHOW_PROMO);
        theme = "light".equals(requestTheme) ? THEME_LIGHT : THEME_DARK;
        backgroundHex = requestHex != null ? "#" + requestHex : null;
        showPromo = "false".equals(requestPromo) ? false : true;

        String[] selectorArr = request.getRequestPathInfo().getSelectors();
		if(selectorArr !=null)
		{
			List<String> selectors = new ArrayList<String>(Arrays.asList(selectorArr));
			if(selectors.contains("external"))
			{
				external = true;
			}

		}

        mobileBrandLogos = new ArrayList<>();

        // Split up brand logos in groups of 2 to support markup
        if (brandLogos != null) {
            for (int i = 0; i < brandLogos.size(); i++) {
                if ((i % 2) == 0) {
                    List<BrandLogoItem> nestedList = new ArrayList<>();
                    BrandLogoItem item1 = brandLogos.get(i);
                    BrandLogoItem item2 = ((brandLogos.size() - 1) != i) ? brandLogos.get(i + 1) : null;

                    if (item1 != null) {
                        nestedList.add(item1);
                    }

                    if (item2 != null) {
                        nestedList.add(item2);
                    }

                    if (!nestedList.isEmpty()) {
                        mobileBrandLogos.add(nestedList);
                    }
                }
            }
        }
    }

    @DialogField(fieldLabel = "Homepage Path", name = "./homepageURL", ranking = 1, tab = 1, fieldDescription = "Path to instance homepage (ex. en-us/)")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String homepageURL;

    public String getHomepageURL() {
        return ServiceUtils.appendLinkExtension(getResourceResolver(), homepageURL);
    }
    
    @DialogField(fieldLabel = "Search Placeholder Text", name = "./searchText", ranking = 2, tab = 1)
    @TextField
    @Inject @ValueMapValue
    private String searchText;
    
    public String getSearchText() {
    	return searchText;
    }

    @DialogField(fieldLabel = "Search Page Path", name = "./searchPageURL", ranking = 3, tab = 1, fieldDescription = "Path to instance search results (ex. en-us/search-results)")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String searchPageURL;

    public String getSearchPageURL() {
        return searchPageURL;
    }
    
    @DialogField(fieldLabel = "Request a Demo Text", name = "./requestDemoText", ranking = 4, tab = 1)
    @TextField
    @Inject @ValueMapValue
    private String requestDemoText;
    
    public String getRequestDemoText() {
    	return requestDemoText;
    }

    @DialogField(fieldLabel = "Request a Demo URL", name = "./requestDemoURL", ranking = 5, tab = 1, fieldDescription = "Path to instance Request Demo URL (ex. en-us/request-demo)")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String requestDemoURL;

    public String getRequestDemoURL() {
        return ServiceUtils.appendLinkExtension(getResourceResolver(), requestDemoURL);
    }

    @DialogField(fieldLabel = "Light Logo", name = "./lightLogo", ranking = 6, tab = 1, fieldDescription = "Specifying a logo will override the default logo.")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String lightLogo;

    @DialogField(fieldLabel = "Dark Logo", name = "./darkLogo", ranking = 7, tab = 1, fieldDescription = "Specifying a logo will override the default logo.")
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String darkLogo;

    public String getLogo() {
    	return THEME_LIGHT.equals(theme) ? lightLogo : darkLogo;
    }
    
    @DialogField(fieldLabel = "Brand Logos", name = "./brandLogos", ranking = 8, tab = 1, fieldDescription = "Specify other brand logos.")
    @MultiCompositeField
    @Inject @ChildResource(name = "brandLogos")
    private List<BrandLogoItem> brandLogos;

    public List<BrandLogoItem> getBrandLogos() {
    	return brandLogos;
    }
    
    @DialogField(fieldLabel = "Navigation Items", name = "./navigationItems", ranking = 1, tab = 2)
    @MultiCompositeField
    @Inject @ChildResource(name = "navigationItems")
    private List<GlobalNavigationItem> navigationItems;

    public List<GlobalNavigationItem> getNavigationItems() {
    	return navigationItems;
    }
    
    @DialogField(fieldLabel = "Contact Text", name = "./contactText", ranking = 1, tab = 3)
    @TextField
    @Inject @ValueMapValue
    private String contactText;
    
    public String getContactText() {
    	return contactText;
    }
    
    @DialogField(fieldLabel = "Contact Link", name = "./contactLink", ranking = 2, tab = 3)
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String contactLink;
    
    public String getContactLink() {
    	return ServiceUtils.appendLinkExtension(getResourceResolver(), contactLink);
    }
    
    @DialogField(fieldLabel = "Login Text", name = "./loginText", ranking = 3, tab = 3)
    @TextField
    @Inject @ValueMapValue
    private String loginText;

    public String getLoginText() {
    	return loginText;
    }
    
    @DialogField(fieldLabel = "Login Links", name = "./loginLinks", ranking = 4, tab = 3)
    @MultiCompositeField
    @Inject @ChildResource(name = "loginLinks")
    private List<SimpleLink> loginLinks;
    
    public List<SimpleLink> getLoginLinks() {
    	return loginLinks;
    }
    
    @DialogField(fieldLabel = "Languages", name = "./languages", ranking = 1, tab = 4)
    @MultiCompositeField
    @Inject @ChildResource(name = "languages")
    private List<SimpleLink> languages;
    
    public List<SimpleLink> getLanguages() {
    	return languages;
    }
    
    
    //contact sales button added
    @DialogField(fieldLabel = "Contact Sales Text", name = "./contactsalestext", ranking = 1, tab = 5,fieldDescription = "Specifying a contact sales text will override the default text")
    @TextField
    @Inject @ValueMapValue @Default(values="CONTACT SALES")
    private String contactsalestext;

  	public String getContactsalestext() {
		return contactsalestext;
	}

	@DialogField(fieldLabel = "Link", name = "./link", ranking = 2, tab = 5,fieldDescription = "Specifying a link will override the default contact sales Link")
    @PathField(rootPath = "/content/")
    @Inject @ValueMapValue  @Default(values="/content/gigamon/en/contact-sales")
    private String link;
    
    public String getLink() {
        return link;
    }
  
    
   
    public String getBackgroundHex() {
    	return backgroundHex;
    }
    
    public String getTheme() {
        return theme;
    }

    public boolean isShowPromo() {
        return showPromo;
    }

	public boolean isExternal() {
		return external;
	}

    public List<List<BrandLogoItem>> getMobileBrandLogos() {
        return mobileBrandLogos;
    }
    
    public String getActiveLanguage() {
    	String homePath = ServiceUtils.appendLinkExtension(getResourceResolver(), ServiceUtils.getHomePage(currentPage).getPath());
    	if(languages != null) {
    		for(SimpleLink link : languages) {
    			String langPath = link.getLink();
    			if(langPath != null && langPath.equals(homePath)) {
    				return link.getTitle();
    			}
    		}
    	}
    	return "";
    }
}
