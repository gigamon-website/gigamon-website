package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import gigamon.core.models.component.traits.background.BackgroundXYTraits;
import gigamon.core.models.component.traits.image.PrimaryImageWithAltTextTrait;
import gigamon.core.models.component.traits.richtext.RichTextFieldTrait;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.inject.Inject;

/**
 * BlockQuote Component
 */
@Component(value = "BlockQuote",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/block-quote.html",
        inPlaceEditingEditorType = "text",
        inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-config/dialog/items/tabs/items/RichTextConfigurations/items/standardRichText",
        tabs = {@Tab(title = "BlockQuote"), @Tab(title = "Image"), @Tab(title = "Background")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BlockQuote extends ComponentSlingModel implements PrimaryImageWithAltTextTrait, RichTextFieldTrait, BackgroundXYTraits {

    public static final String ALIGN_LEFT = "left";
    public static final String ALIGN_CENTER = "center";

    public static final String TEXT_LIGHT = "text-light";
    public static final String TEXT_DARK = "text-dark";

    @DialogField(fieldLabel = "Author Name", name = "./authorName", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String authorName;

    public String getAuthorName() {
        return authorName;
    }

    @DialogField(fieldLabel = "Author Role", name = "./authorRole", ranking = 2)
    @TextField
    @Inject @ValueMapValue
    private String authorRole;

    public String getAuthorRole() {
        return authorRole;
    }

    @DialogField(fieldLabel = "Alignment", name = "./textQuoteAlignment", ranking = 3, defaultValue = ALIGN_CENTER)
    @Selection(type="select", options = {
            @Option(text = "Center", value = ALIGN_CENTER),
            @Option(text = "Left", value = ALIGN_LEFT),
    })
    @Inject @ValueMapValue
    private String textQuoteAlignment;

    public String getTextQuoteAlignment() {
        return textQuoteAlignment;
    }

    @DialogField(fieldLabel = "Text Color", name = "./textQuoteColor", ranking = 4, defaultValue = TEXT_LIGHT)
    @Selection(type="select", options = {
            @Option(text = "White", value = TEXT_LIGHT),
            @Option(text = "Dark", value = TEXT_DARK),
    })
    @Inject @ValueMapValue
    private String textQuoteColor;

    public String getTextQuoteColor() {
        return textQuoteColor;
    }

    @DialogField(tab = 2)
    @DialogFieldSet
    public final PrimaryImageWithAltTextTrait.DialogFields image = null;

    @DialogField(tab = 3)
    @DialogFieldSet
    public final BackgroundXYTraits.DialogFields background = null;

}
