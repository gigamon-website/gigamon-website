package gigamon.core.models.component;

import gigamon.core.models.data.AemTag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.TagInputField;
import com.day.cq.wcm.api.Page;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * FilterDropdownResource Component
 */
@Component(value = "FilterDropdownResource",
        group = ComponentSlingModel.COMPONENT_GROUP_HIDDEN,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/filter-dropdown-resource.html",
        tabs = {@Tab(title = "FilterDropdownResource")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FilterDropdownResource extends ComponentSlingModel {

	@Inject
		private Page currentPage;
	
	@DialogField(fieldLabel = "Tag", name = "./filterDropdownTags", ranking = 3, fieldDescription="Parent tag for dropdown. Limit 1.")
	@TagInputField(multiple = false)
	@Inject @ValueMapValue 
	private String filterDropdownTags;
	
	@DialogField(fieldLabel = "Exclusions", name = "./filterDropdownExclusions", ranking = 4, fieldDescription="List of tags to exclude from the dropdown.")
	@TagInputField(multiple = false)
	@Inject @ValueMapValue 
	private String filterDropdownExclusions;
	
	private static final Logger log = LoggerFactory.getLogger(ResourceGrid.class);
	
	public AemTag getFilterDropdownLabelTag() {
		String[] tagIds = getProperties().get("filterDropdownTags", new String[0]);
		if (tagIds == null || tagIds.length < 1) {
			return null;
		}
		Locale locale = currentPage.getLanguage(false);
		return AemTag.createAemTag(tagIds[0], getResourceResolver(), locale, false);
	}
	
	public List<AemTag> getFilterDropdownTags() {
		String[] tagIds = getProperties().get("filterDropdownTags", new String[0]);
		List<AemTag> tags = resolveTags(tagIds, true);
		tags.removeAll(getFilterDropdownExclusions());
		
		Collections.sort(tags, new Comparator<AemTag>() {

			public int compare(AemTag t1, AemTag t2) {
					return t1.getTitle().compareToIgnoreCase( t2.getTitle());
			}
		});
		
		return tags;
	}
	
	public List<AemTag> getFilterDropdownExclusions() {
		String[] tagIds = getProperties().get("filterDropdownExclusions", new String[0]);
		return resolveTags(tagIds, false);
	}

	/**
	 * Re-usable method for turning a array of tag ID strings into a list of AemTag objects.
	 * 
	 * @param tagIds List of tag IDs.
	 * @param expand If true, only evaluates first tag and expands children. 
	 * If false, {@code tagIds} is treated as static list and no expansion is performed. 
	 * @return
	 */
	private List<AemTag> resolveTags(String[] tagIds, boolean expand) {
		if (tagIds == null || tagIds.length < 1) {
			return new ArrayList<>();
		}
		
		Locale locale = currentPage.getLanguage(false);
		log.debug("Using locale[{}] for filter bar", locale);
		
		// Build list
		List<AemTag> list = Arrays.stream(tagIds)
			.map(tagId -> AemTag.createAemTag(tagId, getResourceResolver(), locale, expand))
			.filter(Objects::nonNull)
			.collect(Collectors.toList());
		
		if (expand && !list.isEmpty()) {
			list = list.get(0).getChildren();
		}
		
		return list;
	}

}
