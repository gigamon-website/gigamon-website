package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.DialogFieldSet;
import com.citytechinc.cq.component.annotations.widgets.Selection;

import gigamon.core.models.component.traits.background.fields.BackgroundColorTrait;
import gigamon.core.models.component.traits.background.fields.BackgroundVisibilityTrait;
import gigamon.core.models.component.traits.background.fields.BackgroundXUniversalSpacingTrait;
import gigamon.core.models.component.traits.background.fields.BackgroundYUniversalSpacingTrait;
import gigamon.core.util.ServiceUtils;
import gigamon.core.util.SlingModelUtils;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import gigamon.dialog.touch.heading.Heading;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A nestable layout component supporting control of the Bootstrap 12 column
 * grid system.
 * <p>
 * This component is also responsible for rendering out some of the
 * {@link ResponsiveLayoutColumn} configurations (html classes).
 */
@Component(value = "Responsive Layout", group = ComponentSlingModel.COMPONENT_GROUP_CONTENT, disableTargeting = true, helpPath = "/editor.html/content/gigamon-components/responsive-layout.html", tabs = {
		@Tab(title = "Responsive Layout"), @Tab(title = "Advanced"),
		@Tab(title = "Background") })
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ResponsiveLayout extends ComponentSlingModel implements
		BackgroundColorTrait, BackgroundXUniversalSpacingTrait,
		BackgroundYUniversalSpacingTrait, BackgroundVisibilityTrait {

	private static final Logger log = LoggerFactory
			.getLogger(ResponsiveLayout.class);

	private static final String DEFAULT = "default";

	public static final String GRID_STARTING_AT_BREAKPOINT = "gridStartingAtBreakpoint";

	private List<Column> columns;

	/*
	 * TAB 1
	 */

	@DialogField(ranking = 1)
	@Heading(level = 4, text = "All settings on this tab apply to the desktop screen size only. At tablet/mobile all columns stack.")
	private final String heading1 = null;

	@DialogField(fieldLabel = "Sort components by title", name = "./canSort", ranking = 3, value = "true")
	@CheckBoxTouch(text = "Sort components by title")
	@Inject
	@ValueMapValue
	private String canSort;

	@DialogField(fieldLabel = "Default Columns Size", name = "./defaultColumnSize", ranking = 5, fieldDescription = "This is the default column size at the MD breakpoint used by child columns.")
	@Selection(type = "select", options = {
			@Option(text = "6 / 12 (Default)", value = "6"),
			@Option(text = "1 / 12", value = "1"),
			@Option(text = "2 / 12", value = "2"),
			@Option(text = "3 / 12", value = "3"),
			@Option(text = "4 / 12", value = "4"),
			@Option(text = "5 / 12", value = "5"),
			@Option(text = "7 / 12", value = "7"),
			@Option(text = "8 / 12", value = "8"),
			@Option(text = "9 / 12", value = "9"),
			@Option(text = "10 / 12", value = "10"),
			@Option(text = "11 / 12", value = "11"),
			@Option(text = "12 / 12", value = "12"), })
	@Inject
	@ValueMapValue
	@Default(values = "6")
	private String defaultColumnSize;

	@DialogField(fieldLabel = "Vertical Align", name = "./verticalAlign", ranking = 6, fieldDescription = "Vertical alignment of child columns.")
	@Selection(type = "select", options = {
			@Option(text = "Default", value = DEFAULT),
			@Option(text = "Stretch", value = "align-stretch"),
			@Option(text = "Top", value = "align-top"),
			@Option(text = "Center", value = "align-center"),
			@Option(text = "Bottom", value = "align-bottom"), })
	@Inject
	@ValueMapValue
	private String verticalAlign;

	@DialogField(fieldLabel = "Horizontal Align", name = "./horizontalAlign", ranking = 7, fieldDescription = "Horizontal alignment of child columns.")
	@Selection(type = "select", options = {
			@Option(text = "Default", value = DEFAULT),
			@Option(text = "Left", value = "justify-left"),
			@Option(text = "Center", value = "justify-center"),
			@Option(text = "Right", value = "justify-right"), })
	@Inject
	@ValueMapValue
	private String horizontalAlign;

	/*
	 * TAB 2
	 */

	@DialogField(fieldLabel = "Gutter Size", name = "./gutterSize", ranking = 5, tab = 2, fieldDescription = "The gutter is the amount of left/right padding between two columns.")
	@Selection(type = "select", options = {
			@Option(text = "30px (Default)", value = "30"),
			@Option(text = "0px", value = "0"),
			@Option(text = "10px", value = "10"),
			@Option(text = "20px", value = "20"),
			@Option(text = "40px", value = "40"),
			@Option(text = "50px", value = "50"),
			@Option(text = "60px", value = "60"), })
	@Inject
	@ValueMapValue
	private String gutterSize;

	@DialogField(fieldLabel = "Grid starts at", name = "./"
			+ GRID_STARTING_AT_BREAKPOINT, ranking = 6, tab = 2, fieldDescription = "When layout changes from 1 column stacked to a grid. This is used to control column padding rules that apply to the grid layout only.")
	@Selection(type = "select", options = { @Option(text = "MD", value = "md"),
			@Option(text = "All Screen Sizes", value = "xs"),
			@Option(text = "SM", value = "sm"),
			@Option(text = "LG", value = "lg"), })
	@Inject
	@ValueMapValue
	@Default(values = "md")
	private String gridStartingAtBreakpoint;

	/*
	 * TAB 3
	 */

	@DialogField(tab = 3)
	@DialogFieldSet
	private final BackgroundColorTrait.DialogFields colorFields = null;

	@DialogField(tab = 3)
	@DialogFieldSet
	private final BackgroundXUniversalSpacingTrait.DialogFields xPaddingFields = null;

	@DialogField(tab = 3)
	@DialogFieldSet
	private final BackgroundYUniversalSpacingTrait.DialogFields ySpacingFields = null;

	@DialogField(tab = 3)
	@DialogFieldSet
	private final BackgroundVisibilityTrait.DialogFields visibilityFields = null;

	@Override
	public void postConstruct() throws Exception {
		super.postConstruct();

		columns = new ArrayList<>();
		for (Resource r : getResource().getChildren()) {

			if (r.getName().equals("colpar")) {
				addColumn(r);
				break;
			}
			if (ServiceUtils.isGhostNode(r)) {
				// If column was deleted on live-copy, force to a full width
				// column.
				// Author can still re-link while and full width should not
				// break layout too badly.
				columns.add(new Column(r.getPath(), r.getResourceType(), "col-"
						+ gridStartingAtBreakpoint + "-12", ""));
			} else {
				ResponsiveLayoutColumn column = SlingModelUtils
						.buildComponentSlingModel(r, getRequest(),
								ResponsiveLayoutColumn.class);
				if (column == null) {
					log.warn("Column is null for {}", r);
					continue;
				}

				column.setDefaultColumnSize(defaultColumnSize);
				columns.add(new Column(r.getPath(), r.getResourceType(), column
						.getClassesForUseByResponsiveLayout(), ""));
			}
		}
	}

	public void addColumn(Resource r) throws Exception {

		String title = "", resource = "default";

		for (Resource child : r.getChildren()) {

			// to place empty titled images at last
			title = "z";
			
			if(child.isResourceType("gigamon/components/content/resource-card"))
				resource="resourceCard";

			if ((child.isResourceType("gigamon/components/content/image") || (child
					.isResourceType("gigamon/components/content/resource-card")))) {

				if (child.hasChildren()
						&& child.getChild("image").adaptTo(Node.class)
								.hasProperty("imageTitle")) {
					title = child.getChild("image").adaptTo(Node.class)
							.getProperty("imageTitle").getString();
				}
			}

			if (ServiceUtils.isGhostNode(child)) {
				// If column was deleted on live-copy, force to a full width
				// column.
				// Author can still re-link while and full width should not
				// break layout too badly.
				columns.add(new Column(child.getPath(),
						child.getResourceType(), "col-"
								+ gridStartingAtBreakpoint + "-12", title));
			} else {
				ResponsiveLayoutColumn column = SlingModelUtils
						.buildComponentSlingModel(child, getRequest(),
								ResponsiveLayoutColumn.class);
				if (column == null) {
					log.warn("Column is null for {}", child);
					continue;
				}

				column.setDefaultColumnSize(defaultColumnSize);
				columns.add(new Column(child.getPath(),
						child.getResourceType(), column
								.getClassesForUseByResponsiveLayout(), title));
			}

		}

		Collections.sort(columns, new Comparator<Column>() {
			public int compare(Column c1, Column c2) {
				return c1.getTitle().compareToIgnoreCase(c2.getTitle());
			}
		});
		
		if(resource.equals("resourceCard"))
		Collections.reverse(columns);

	}

	public String getRowClasses() {
		String classes = "";
		if (verticalAlign != null && !DEFAULT.equals(verticalAlign)) {
			classes += " " + verticalAlign;
		}
		if (horizontalAlign != null && !DEFAULT.equals(horizontalAlign)) {
			classes += " " + horizontalAlign;
		}
		if (!StringUtils.isBlank(classes)) {
			classes += " layout-flex " + classes;
		}

		if (gutterSize != null)
			classes += " gutter-" + gridStartingAtBreakpoint + "-" + gutterSize;

		classes += " grid-at-" + gridStartingAtBreakpoint;

		return classes;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public String getNewColumnPath() {
		return getResource().getPath() + "/col_" + System.currentTimeMillis();
	}

	public boolean getCanSort() {
		return "true".equals(canSort);
	}

	public static class Column {
		private final String path;
		private final String resourceType;
		private final String classes;
		private final String title;

		public Column(String path, String resourceType, String classes,
				String title) {
			this.path = path;
			this.resourceType = resourceType;
			this.classes = classes;
			this.title = title;
		}

		public String getResourceType() {
			return resourceType;
		}

		public String getPath() {
			return path;
		}

		public String getClasses() {
			return classes;
		}

		public String getTitle() {
			return title;
		}
	}

}
