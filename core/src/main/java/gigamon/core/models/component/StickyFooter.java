package gigamon.core.models.component;

import java.util.List;
import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import gigamon.core.util.ServiceUtils;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import gigamon.dialog.classic.multifield.MultiCompositeField;
import gigamon.core.models.data.SimpleLinkWithImage;
import gigamon.core.models.data.SimpleLink;
import javax.inject.Inject;

/**
 * StickyFooter Component
 */
@Component(value = "StickyFooter",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/sticky-footer.html",
        tabs = {@Tab(title = "CTA")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class StickyFooter extends ComponentSlingModel {

   /*REMOVED FOR NEW STICKY FOOTER
    @DialogField(fieldLabel = "Share Text", name = "./shareText", ranking = 1,tab=1)
    @TextField
    @Inject @ValueMapValue
    private String shareText;

    public String getShareText() {
        return shareText;
    }

    @DialogField(fieldLabel = "Share Link", name = "./shareLink", ranking = 2,tab=1)
    @TextField
    @Inject @ValueMapValue
    private String shareLink;

    public String getShareLink() {
        return shareLink;
    }*/


   /* @DialogField(fieldLabel = "Contact Link", name = "./contactLink", ranking = 4,tab=2)
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String contactLink;
    
    public String getContactLink() {
        return ServiceUtils.appendLinkExtension(getResourceResolver(), contactLink);
    }
    
    @DialogField(fieldLabel = "Contact Text", name = "./contactText", ranking = 3,tab=2)
    @TextField
    @Inject @ValueMapValue
    private String contactText;

    public String getContactText() {
        return contactText;
    }*/
   @DialogField(fieldLabel = "buttons", name = "./buttons", ranking = 5, tab=1)
   @MultiCompositeField
   @Inject @ChildResource(name = "buttons")
   private List<SimpleLink> buttons;

    public List<SimpleLink> button() {
        return buttons;
    }



   /* REMOVED FOR NEW STICKY FOOTER
     @DialogField(fieldLabel = "Social", name = "./social", ranking = 5, tab=3)
    @MultiCompositeField
    @Inject @ChildResource(name = "social")
    private List<SimpleLinkWithImage> social;
    
    public List<SimpleLinkWithImage> getSocial() {
        return social;
    }*/
    

}
