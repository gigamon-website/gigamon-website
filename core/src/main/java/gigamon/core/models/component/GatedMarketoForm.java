package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Tab;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;

import javax.inject.Inject;

/**
 * GatedMarketoForm Component
 */
@Component(value = "GatedMarketoForm",
        group = ComponentSlingModel.COMPONENT_GROUP_CONTENT,
        disableTargeting = true,
        helpPath = "/editor.html/content/gigamon-components/gated-marketo-form.html",
        tabs = {@Tab(title = "GatedMarketoForm")})
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GatedMarketoForm extends ComponentSlingModel {
	 @DialogField(fieldLabel = "Marketo Form ID", name = "./formId", ranking = 1)
	    @TextField
	    @Inject @ValueMapValue
	    private String formId;

	    @DialogField(fieldLabel = "Disable Disclaimer", name = "./disclaimer", value = "true", ranking = 2)
	    @CheckBoxTouch(text = "Disable Disclaimer")
	    @Inject
	    @ValueMapValue
	    private boolean disclaimer;


	    public String getFormId() {
	        return formId;
	    }
	    public String getMarketoFormId() {
	        return "mktoForm_"+formId;
	    }

	    public boolean isDisclaimer() {
	        return disclaimer;
	    }


}
