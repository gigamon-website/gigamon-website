package gigamon.core.models.component.traits.pagelist;

import javax.inject.Inject;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.Property;
import com.citytechinc.cq.component.annotations.widgets.*;
import com.day.cq.wcm.api.PageFilter;

import gigamon.core.models.component.traits.ComponentTrait;
import gigamon.core.models.data.SimpleLink;
import gigamon.core.util.PageLister;
import gigamon.dialog.classic.multifield.MultiCompositeField;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;

/**
 * PageLister trait provides the dialog fields necessary for creating a {@link PageLister}.
 *
 * @author joelepps
 *         4/21/16
 */
public interface PageListerTrait extends ComponentTrait {

    interface DialogFields {

        /*
         * MAIN SELECT FIELD
         * Toggles visibility of below fields
         */

        @DialogField(fieldLabel = "Build list using",
                name = "./"+ PageLister.SOURCE_PROPERTY_NAME,
                ranking = 1,
                defaultValue = "children",
                additionalProperties = {
                        @Property(name = "class", value = "cq-dialog-dropdown-showhide"),
                        @Property(name = "cq-dialog-dropdown-showhide-target", value = ".list-option-listfrom-showhide-target")
                })
        @Selection(type="select", options = {
                @Option(text = "Child Pages", value = "children"),
                @Option(text = "Descendant Pages", value = "descendants"),
                @Option(text = "Fixed List", value = "static"),
                @Option(text = "Adhoc List", value = "adhoc"),
                @Option(text = "Tags", value = "tags"),
        })
        void listType();

        /*
         * CHILD PAGE OPTIONS
         */

        @DialogField(fieldLabel = "Options for Child Pages", ranking = 101,
                additionalProperties = {
                        @Property(name = "class", value = "hide list-option-listfrom-showhide-target foundation-layout-util-vmargin"),
                        @Property(name = "showhidetargetvalue", value = "children")
                })
        @DialogFieldSet
        ChildrenOptions children();

        interface ChildrenOptions {

            @DialogField(fieldLabel = "Parent page", name = "./"+PageLister.PARENT_PAGE_PROPERTY_NAME, ranking = 1)
            @PathField(rootPath = "/content")
            void parentPage();
            
            @DialogField(fieldLabel = "Include children's children", name = "./"+PageLister.PARENT_PAGE_SUB_CHILDREN_PROPERTY_NAME, ranking = 2, value = "true")
            @CheckBoxTouch(text = "Include children's children")
            void parentPageChildren();

        }

        /*
         * DESCENDANT PAGE OPTIONS
         */

        @DialogField(fieldLabel = "Options for Descendant Pages", ranking = 102,
                additionalProperties = {
                        @Property(name = "class", value = "hide list-option-listfrom-showhide-target foundation-layout-util-vmargin"),
                        @Property(name = "showhidetargetvalue", value = "descendants")
                })
        @DialogFieldSet
        DescendantOptions descendants();

        interface DescendantOptions {

            @DialogField(fieldLabel = "Descendants of", name = "./"+PageLister.ANCESTOR_PAGE_PROPERTY_NAME, ranking = 1)
            @PathField(rootPath = "/content")
            void descendants();

        }

        /*
         * STATIC PAGE OPTIONS
         */

        @DialogField(fieldLabel = "Options for Fixed list", ranking = 103,
                additionalProperties = {
                        @Property(name = "class", value = "hide list-option-listfrom-showhide-target foundation-layout-util-vmargin"),
                        @Property(name = "showhidetargetvalue", value = "static")
                })
        @DialogFieldSet
        StaticOptions statics();

        interface StaticOptions {

            @DialogField(fieldLabel = "Pages", name = "./"+PageLister.PAGES_PROPERTY_NAME, ranking = 1)
            @MultiField
            @PathField(rootPath = "/content")
            void pages();

        }
        
        /*
         * ADHOC PAGE OPTIONS
         */

        @DialogField(fieldLabel = "Options for Adhoc list", ranking = 104,
            additionalProperties = {
                @Property(name = "class", value = "hide list-option-listfrom-showhide-target foundation-layout-util-vmargin"),
                @Property(name = "showhidetargetvalue", value = "adhoc")
            })
        @DialogFieldSet
        AdhocOptions adhoc();

        interface AdhocOptions {
            @DialogField(fieldLabel = "Adhoc Links", ranking = 2)
            @MultiCompositeField
            @Inject
            SimpleLink adhocItems();
        }
        void adhocItems();

        /*
         * TAG OPTIONS
         */

        @DialogField(fieldLabel = "Options for Tags", ranking = 104,
                additionalProperties = {
                        @Property(name = "class", value = "hide list-option-listfrom-showhide-target foundation-layout-util-vmargin"),
                        @Property(name = "showhidetargetvalue", value = "tags")
                })
        @DialogFieldSet
        TagOptions tags();

        interface TagOptions {

            @DialogField(fieldLabel = "Parent page", name = "./"+PageLister.TAGS_SEARCH_ROOT_PROPERTY_NAME, ranking = 1,
                fieldDescription = "Leave empty to use current page")
            @PathField(rootPath = "/content")
            void parentPage();

            @DialogField(fieldLabel = "Tags", name = "./"+PageLister.TAGS_PROPERTY_NAME, ranking = 2)
            @TagInputField
            void tags();

            @DialogField(fieldLabel = "Match",
                    name = "./"+ PageLister.TAGS_MATCH_PROPERTY_NAME,
                    ranking = 3,
                    defaultValue = "any")
            @Selection(type="select", options = {
                    @Option(text = "any tag", value = "any"),
                    @Option(text = "all tags", value = "all"),
            })
            void match();

        }
    }

    interface DialogFieldsWithSort extends DialogFields {

        /*
         * ORDER BY OPTIONS
         */

        @DialogField(fieldLabel = "Order by", name = "./"+ PageLister.ORDER_BY_PROPERTY_NAME, ranking = 105)
        @Selection(type="select", options = {
                @Option(text = "jcr:title", value = "jcr:title"),
                @Option(text = "jcr:created", value = "jcr:created"),
                @Option(text = "cq:lastModified", value = "cq:lastModified"),
        })
        void order();
    }

    /**
     * Get the PageLister based on this component's authored configurations.
     *
     * @param pageFilter Delegate for determining if page is in result
     * @return new PageLister instance
     */
    default PageLister getPageLister(PageFilter pageFilter) {
        return new PageLister(getRequest(), pageFilter);
    }

    /*
     * Below getter fields are for debugging only.
     * PageLister will be the one that reads these fields directly, as needed.
     */

    default String getListFrom() {
        return getProperties().get(PageLister.SOURCE_PROPERTY_NAME, String.class);
    }

    default String getParentPage() {
        return getProperties().get(PageLister.PARENT_PAGE_PROPERTY_NAME, String.class);
    }

    default String getAncestorPage() {
        return getProperties().get(PageLister.ANCESTOR_PAGE_PROPERTY_NAME, String.class);
    }

    default String getPages() {
        return getProperties().get(PageLister.PAGES_PROPERTY_NAME, String.class);
    }

    default String getTagsSearchRoot() {
        return getProperties().get(PageLister.TAGS_SEARCH_ROOT_PROPERTY_NAME, String.class);
    }

    default String getTags() {
        return getProperties().get(PageLister.TAGS_PROPERTY_NAME, String.class);
    }

    default String getTagsMatch() {
        return getProperties().get(PageLister.TAGS_MATCH_PROPERTY_NAME, String.class);
    }

    default String getOrderBy() {
        return getProperties().get(PageLister.ORDER_BY_PROPERTY_NAME, String.class);
    }

}
