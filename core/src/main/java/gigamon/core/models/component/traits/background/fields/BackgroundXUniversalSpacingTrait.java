package gigamon.core.models.component.traits.background.fields;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Option;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import gigamon.core.models.component.traits.ComponentTrait;
import gigamon.dialog.touch.heading.Heading;

/**
 * Contains the horizontal spacing options that apply for all screen sizes.
 */
public interface BackgroundXUniversalSpacingTrait extends ComponentTrait {

    String HORIZONTAL_SPACING_LEFT = "backgroundHorizontalSpacingLeft";
    String HORIZONTAL_SPACING_RIGHT = "backgroundHorizontalSpacingRight";

    String NONE = "_empty_";

    String LEFT_SPACING_XS = "horz-pad-left-xs"; // 15
    String LEFT_SPACING_SM = "horz-pad-left-sm"; // 30
    String LEFT_SPACING_MD = "horz-pad-left-md"; // 60
    String LEFT_SPACING_LG = "horz-pad-left-lg"; // 90

    String RIGHT_SPACING_XS = "horz-pad-right-xs"; // 15
    String RIGHT_SPACING_SM = "horz-pad-right-sm"; // 30
    String RIGHT_SPACING_MD = "horz-pad-right-md"; // 60
    String RIGHT_SPACING_LG = "horz-pad-right-lg"; // 90

    interface DialogFields {

        @DialogField(ranking = 9)
        @Heading(text = "Left and Right spacing apply to all screen sizes.", level = 4)
        void heading();

        @DialogField(fieldLabel = "Left Spacing", name = "./"+HORIZONTAL_SPACING_LEFT, ranking = 10)
        @Selection(type="select", options = {
                @Option(text = "None", value = NONE),
                @Option(text = "XS (15)", value = LEFT_SPACING_XS),
                @Option(text = "SM (30)", value = LEFT_SPACING_SM),
                @Option(text = "MD (60)", value = LEFT_SPACING_MD),
                @Option(text = "LG (90)", value = LEFT_SPACING_LG),
        })
        void horizontalSpacingLeft();

        @DialogField(fieldLabel = "Right Spacing", name = "./"+HORIZONTAL_SPACING_RIGHT, ranking = 11)
        @Selection(type="select", options = {
            @Option(text = "None", value = NONE),
            @Option(text = "XS (15)", value = RIGHT_SPACING_XS),
            @Option(text = "SM (30)", value = RIGHT_SPACING_SM),
            @Option(text = "MD (60)", value = RIGHT_SPACING_MD),
            @Option(text = "LG (90)", value = RIGHT_SPACING_LG),
        })
        void horizontalSpacingRight();

    }

    default String getBackgroundLeftSpacing() {
        String value = getProperties().get(HORIZONTAL_SPACING_LEFT, String.class);
        return NONE.equals(value) ? null : value;
    }

    default String getBackgroundRightSpacing() {
        String value = getProperties().get(HORIZONTAL_SPACING_RIGHT, String.class);
        return NONE.equals(value) ? null : value;
    }

}
