package gigamon.core.models.component;

import com.citytechinc.cq.component.annotations.Component;
import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.RichTextEditor;
import com.citytechinc.cq.component.annotations.widgets.rte.Edit;
import com.citytechinc.cq.component.annotations.widgets.rte.MiscTools;
import com.citytechinc.cq.component.annotations.widgets.rte.ParaFormat;
import com.citytechinc.cq.component.annotations.widgets.rte.Style;

/**
 * This class contains rich text configurations that are to be referenced by other components.
 * <p>
 * A component wishing to use this functionality must implement the {@link gigamon.core.models.component.traits.richtext.RichTextFieldTrait}
 * and add the following @Component annotation fields.
 *
 * <pre>
 * inPlaceEditingActive = true,
 * inPlaceEditingEditorType = "text",
 * inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-config/dialog/items/tabs/items/RichTextConfigurations/items/standardRichText",
 * </pre>
 *
 * @author joelepps
 *         3/17/16
 */
@Component(value = "Rich Text Configurations", group = ComponentSlingModel.COMPONENT_GROUP_HIDDEN)
public class RichTextConfig {

    @DialogField(fieldLabel = "IGNORE", name = "./IGNORE")
    @RichTextEditor(
            paraformat={
                    @ParaFormat(description = "Paragraph", tag = "p"),
                    @ParaFormat(description = "Heading 3", tag = "h3"),
                    @ParaFormat(description = "Heading 4", tag = "h4"),
                    @ParaFormat(description = "Heading 5", tag = "h5"),
                    @ParaFormat(description = "Heading 6", tag = "h6"),
                    @ParaFormat(description = "\\[None]", tag = "simple"),
            },
            edit={@Edit},
            misctools={@MiscTools},
            styles={@Style(cssName = "text-muted", text = "Text Muted")}
    )
    void standardRichText() {

    }

}
