package gigamon.core.models.component.traits.richtext;

import gigamon.core.models.component.traits.ComponentTrait;

/**
 * Rich text does not require any dialog fields since the editor is inline.
 * <p>
 * If using this trait you must add the following @Component annotation fields:
 * <pre>
 * inPlaceEditingActive = true,
 * inPlaceEditingEditorType = "text",
 * inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-config/dialog/items/tabs/items/RichTextConfigurations/items/standardRichText",
 * </pre>
 * OR
 * <pre>
 * inPlaceEditingActive = true,
 * inPlaceEditingEditorType = "text",
 * inPlaceEditingConfigPath = "/apps/gigamon/components/content/rich-text-basic-config/dialog/items/tabs/items/RichTextBasicConfigurations/items/basicRichText",
 * </pre>
 *
 * @author joelepps
 *         3/17/16
 */
public interface RichTextFieldTrait extends ComponentTrait {

    interface DialogFields {}

    default String getText() {
        return getProperties().get("text", String.class);
    }

}
