package gigamon.core.models;

import java.util.*;

import gigamon.core.models.data.PageMetadata;

import com.adobe.cq.sightly.WCMUsePojo;

/**
 * Provides Sightly access to the collection of {@link MetaTag} instances for this page.
 *
 * @author joelepps
 *
 */
public class MetaTagModel extends WCMUsePojo {

    private PageMetadata pageMetadata;

    @Override
    public void activate() throws Exception {
        pageMetadata = getCurrentPage().adaptTo(PageMetadata.class);
    }

    public String getTitle() {
        return pageMetadata.getSeoTitle();
    }

    public String getCanonicalUrl() {
        return pageMetadata.getCanonicalUrl();
    }

    public String getLocale() {
        if (getWcmMode().isEdit()) return "en-US";
        return pageMetadata.getLocaleCode();
    }

    public List<MetaTag> getMetaTags() {
        List<MetaTag> metaTags = new ArrayList<>();

        new MetaTag("description", pageMetadata.getDescription())
            .addSelfToListIfValid(metaTags);
        new MetaTag("robots", pageMetadata.getRobotOptions())
            .addSelfToListIfValid(metaTags);
        new MetaTag("twitter:card", "summary")
            .addSelfToListIfValid(metaTags);
        new MetaTag("twitter:site", pageMetadata.getTwitterSiteHandle())
            .addSelfToListIfValid(metaTags);
        new MetaTag("property", "og:title", pageMetadata.getSeoTitle())
            .addSelfToListIfValid(metaTags);
        new MetaTag("property", "og:description", pageMetadata.getDescription())
            .addSelfToListIfValid(metaTags);
        new MetaTag("property", "og:image", pageMetadata.getImage())
            .addSelfToListIfValid(metaTags);
        new MetaTag("property", "og:type", "website")
            .addSelfToListIfValid(metaTags);
        new MetaTag("property", "og:url", pageMetadata.getCanonicalUrl())
            .addSelfToListIfValid(metaTags);

        return metaTags;
    }

    public static class MetaTag {
        private String nameAttribute;
        private String nameValue;
        private String contentAttribute;
        private String contentValue;

        public MetaTag(String nameAttribute, String nameValue, String contentAttribute, String contentValue) {
            this.nameAttribute = nameAttribute;
            this.nameValue = nameValue;
            this.contentAttribute = contentAttribute;
            this.contentValue = contentValue;
        }

        public MetaTag(String nameAttribute, String nameValue, String contentValue) {
            this(nameAttribute, nameValue, "content", contentValue);
        }

        public MetaTag(String nameValue, String contentValue) {
            this("name", nameValue, contentValue);
        }

        public Map<String, String> getMetaAttributeMap() {
            Map<String, String> map = new HashMap<>();
            if (nameAttribute != null) map.put(nameAttribute, nameValue);
            if (contentAttribute != null) map.put(contentAttribute, contentValue);
            return map;
        }

        protected void addSelfToListIfValid(List<MetaTag> list) {
            if (list == null) return;

            if (nameAttribute == null) return;
            if (nameValue == null) return;
            if (contentAttribute == null) return;
            if (contentValue == null) return;

            list.add(this);
        }
    }

}
