package gigamon.core.models.data;



import javax.inject.Inject;

import gigamon.core.models.component.ComponentSlingModel;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.citytechinc.cq.component.annotations.widgets.Selection;
import com.citytechinc.cq.component.annotations.Option;

import org.apache.sling.models.annotations.Default;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class JumpNav extends ComponentSlingModel {

	 @DialogField(fieldLabel = "Link Text", name = "./linktext", ranking = 1,
	            fieldDescription = "link text displayed in component")
	    @TextField
	    @Inject @ValueMapValue
	    private String linktext;
	 
	 	public String getLinktext() {
	 			return linktext;
	 		}

	@DialogField(fieldLabel = "Link Type",
		        name = "./linktype",
		        ranking = 2
		       )
		    @Selection(type="select", options = {
		        @Option(text = "page", value = "page"),
		        @Option(text = "section", value = "section"),
		    })
		    @Inject @ValueMapValue @Default(values = "false")
		    private String linktype;
	 
	 public String getLinktype() {
			return linktype;
		}
	 
	 @DialogField(fieldLabel = "Link to", name = "./link", ranking = 3)
     @PathField(rootPath = "/content")
     @Inject @ValueMapValue
     private String link;

	 public String getLink() {
		return link;
	}

	@DialogField(fieldLabel = "Section Id", name = "./sectionid", ranking = 3,fieldDescription = "Id of the section")
	    @TextField
	    @Inject @ValueMapValue
	    private String sectionid;

	 	public String getSectionid() {
	 			return sectionid;
	 		}

		
}
