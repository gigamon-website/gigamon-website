package gigamon.core.models.data;

import gigamon.core.models.component.ComponentSlingModel;
import gigamon.core.util.ServiceUtils;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.PathField;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Brand Logo Item used in the GlobalNavigation
 *
 * Created by Sanketh on 3/23/17.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BrandLogoItem extends ComponentSlingModel {

    private static final Logger LOG = LoggerFactory.getLogger(BrandLogoItem.class);

    @Override
    public void postConstruct() throws Exception {
        brandLogoLink = ServiceUtils.appendLinkExtension(getResourceResolver(), brandLogoLink);
    }

    @DialogField(fieldLabel = "Logo Path", name = "./brandLogo", ranking = 1)
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String brandLogo;

    @DialogField(fieldLabel = "Logo Link", name = "./brandLogoLink", ranking = 2)
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String brandLogoLink;

    public String getBrandLogo() {
        return brandLogo;
    }

    public String getBrandLogoLink() {
        return brandLogoLink;
    }
}
