package gigamon.core.models.data;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.Property;
import com.citytechinc.cq.component.annotations.widgets.TextField;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gigamon.core.models.component.ComponentSlingModel;
import gigamon.core.models.component.SubMenu;
import gigamon.core.models.component.SubMenuBlock;
import gigamon.core.util.ServiceUtils;
import gigamon.dialog.touch.pathbrowser.PathBrowser;

import javax.inject.Inject;

import java.util.List;

/**
 * Global Navigiation Item used in GlobalNavigation
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GlobalNavigationItem extends ComponentSlingModel {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalNavigationItem.class);

    public static final String NAVIGATION_TYPE_LINK = "link";
    public static final String NAVIGATION_TYPE_SUB_MENU = "subMenu";

    private SubMenu subMenu;
    private List<SubMenuBlock> subMenuBlocks;

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();
        //link = ServiceUtils.appendLinkExtension(getResourceResolver(), link);

        Resource subMenuResource = getResourceResolver().getResource(subMenuReferencePath);

        if (subMenuResource != null) {
            subMenu = subMenuResource.adaptTo(SubMenu.class);
        } else {
            subMenuReferencePath = null;
        }
    }

    @DialogField(fieldLabel = "Title", name = "./title", ranking = 1)
    @TextField
    @Inject @ValueMapValue
    private String title;

    public String getTitle() {
        return title;
    }

    @DialogField(fieldLabel = "Path to page", name = "./pageHref", ranking = 2, fieldDescription = "If item also represents a landing page, with the value present button is split in mobile view.")
    @PathBrowser(rootPath = "/content", predicate = "nosystem")
    @Inject @ValueMapValue
    private String pageHref;

    public String getPageHref() {
        return ServiceUtils.appendLinkExtension(getResourceResolver(), pageHref);
    }

    /*
    @DialogField(fieldLabel = "Navigation Type", name = "./navigationType", ranking = 2,
        additionalProperties = {
            @Property(name = "class", value = "mf-selection-show-hide"),
            @Property(name = "mf-selection-show-hide-target", value = ".media-option-listfrom-showhide-target")
        })
    @Selection(type = "select", options = {
        @Option(text = "Link", value = NAVIGATION_TYPE_LINK, selected = true),
        @Option(text = "Sub Menu", value  = NAVIGATION_TYPE_SUB_MENU)
    })
    @Inject @ValueMapValue
    private String navigationType;

    @DialogField(fieldLabel = "Link", name = "./link", ranking = 3,
        additionalProperties = {
            @Property(name = "class", value = "media-option-listfrom-showhide-target foundation-layout-util-vmargin"),
            @Property(name = "showhidetargetvalue", value = NAVIGATION_TYPE_LINK)
        })
    @PathField(rootPath = "/content")
    @Inject @ValueMapValue
    private String link;
    */
    @DialogField(fieldLabel = "Sub Menu Reference Path", name = "./subMenuReferencePath", ranking = 4)
    @PathBrowser(rootPath = "/content", predicate = "nosystem")
    @Inject @ValueMapValue
    private String subMenuReferencePath;

    public String getSubMenuReferencePath() {
        return subMenuReferencePath;
    }
    
    /*
    public String getNavigationType() {
        return navigationType;
    }

    public String getLink() {
        return link;
    }
    */


    public SubMenu getSubMenu() {
        return subMenu;
    }
}
