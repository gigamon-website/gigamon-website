package gigamon.core.models.data;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import gigamon.core.util.ImageRendition;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * Provides raw data that is typically used for HTML &lt;meta&gt; tags but can also be used by any piece of functionality
 * that needs meta information for a given resource.
 *
 * @author joelepps
 *
 */
public class PageMetadata {

    private static final Logger log = LoggerFactory.getLogger(PageMetadata.class);

    private static final String SEO_DESCRIPTION = "seoDescription";
    private static final String SEO_PAGE_TITLE = "seoPageTitle";
    private static final String SEO_PAGE_TITLE_SUFFIX = "seoTitleSuffix";
    private static final String SEO_TWITTER_HANDLE = "seoTwitterHandle";
    private static final String ANALYTICS_PAGE_ID = "pageID";
    private static final String ROBOTS_OPTIONS = "robotsOptions";

    private final Externalizer externalizer;

    private final ResourceResolver resolver;
    private final Page page;
    private final ValueMap pageProps;
    private final InheritanceValueMap pageInheritProps;

    /**
     * @param resource Resource object for page, or any child resource of the page.
     * @param externalizer Externalizer instance
     */
    public PageMetadata(Resource resource, Externalizer externalizer) {
        this.externalizer = externalizer;
        this.resolver = resource.getResourceResolver();

        PageManager pageManager = resolver.adaptTo(PageManager.class);
        if (pageManager == null) {
            throw new IllegalStateException("Cannot adapt to PageManager from resource: " + resource);
        }

        this.page = pageManager.getContainingPage(resource);
        if (this.page == null) {
            throw new IllegalStateException("Cannot find page for resource: " + resource);
        }

        this.pageProps = page.getProperties();
        this.pageInheritProps = new HierarchyNodeInheritanceValueMap(page.getContentResource());
    }

    public String getPageID() {
        String pageId = pageProps.get(ANALYTICS_PAGE_ID, String.class);

        log.trace("Page ID: {}", pageId);
        return pageId;
    }

    public String getTitle() {
        String title = pageProps.get(NameConstants.PN_PAGE_TITLE, String.class);
        if (StringUtils.isBlank(title)) title = pageProps.get(NameConstants.PN_TITLE, String.class);
        if (StringUtils.isBlank(title)) title = page.getName();

        log.trace("Title: {}", title);
        return title;
    }

    public String getSeoTitle() {
        String title = pageProps.get(SEO_PAGE_TITLE, String.class);
        String titleSuffix = pageInheritProps.getInherited(SEO_PAGE_TITLE_SUFFIX, "");

        if (StringUtils.isBlank(title)) {
            title = pageProps.get(NameConstants.PN_PAGE_TITLE, String.class);
            if (StringUtils.isBlank(title)) title = pageProps.get(NameConstants.PN_TITLE, String.class);
            if (StringUtils.isBlank(title)) title = page.getName();
        }

        if (!StringUtils.isBlank(titleSuffix)) {
            title += titleSuffix;
        }

        log.trace("SEO Title: {}", title);
        return title;
    }

    public String getDescription() {
        String description = pageProps.get(SEO_DESCRIPTION, String.class);
        if (StringUtils.isBlank(description)) description = pageProps.get(NameConstants.PN_DESCRIPTION, String.class);

        log.trace("Description: {}", description);
        return description;
    }

    public String getLocaleCode() {
        Locale locale = page.getLanguage(false);

        String localeCode = null;
        if (locale != null) {
            localeCode = locale.toLanguageTag();
        }

        log.trace("Locale: {}", localeCode);
        return localeCode;
    }

    public String getCanonicalUrl() {
        String url = externalizer.publishLink(resolver, page.getPath()+".html");

        log.trace("Canonical URL for {} is {}", page.getPath(), url);
        return url;
    }

    public String getPath() {
        String path = resolver.map(page.getPath()) + ".html";

        log.trace("Path: {}", path);
        return path;
    }

    public String getContentType() {
        String template = pageProps.get(NameConstants.PN_TEMPLATE, String.class);
        if (template != null) {
            int idx = template.indexOf("templates/");
            if (idx > 0) {
                template = template.substring(idx);
            }
        }

        log.trace("Content type: {}", template);
        return template;
    }

    public String getRobotOptions() {
        String robotsValue = pageProps.get(ROBOTS_OPTIONS, String.class);

        if ("default".equals(robotsValue)) robotsValue = null;

        log.trace("Robot Options: {}", robotsValue);
        return robotsValue;
    }

    public String getTwitterSiteHandle() {
        String handle = pageInheritProps.getInherited(SEO_TWITTER_HANDLE, String.class);

        log.trace("Handle: {}", handle);
        return handle;
    }

    public String getImage() {
        String pageImage = pageProps.get("image/fileReference", String.class); // page image

        String imgUrl = null;
        if (pageImage != null) {
            imgUrl = externalizer.publishLink(resolver, ImageRendition.HALF_WIDTH_LARGE.getRendition(pageImage));
        }

        log.trace("Image: {}", imgUrl);
        return imgUrl;
    }

}
