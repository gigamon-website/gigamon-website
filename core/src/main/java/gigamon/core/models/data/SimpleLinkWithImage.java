package gigamon.core.models.data;

import gigamon.core.models.data.SimpleLink;
import com.citytechinc.cq.component.annotations.DialogField;
import gigamon.core.models.component.ComponentSlingModel;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;





import javax.inject.Inject;

/**
 * Generic model for standard HREF link.
 * <p>
 * This class is an instance of a {@link ComponentSlingModel} that can be constructed from two kinds of resources.
 *
 * <ol>
 *     <li>Page or jcr:content node</li>
 *     <li>Static properties from nt:unstructured node</li>
 * </ol>
 *
 * When constructed from a static nt:unstructured node, the node must have the following properties:
 * <ul>
 *     <li>title</li>
 *     <li>link</li>
 *     <li>newTab</li>
 * </ul>
 *
 * @author joelepps
 *         4/1/16
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SimpleLinkWithImage extends SimpleLink {

    @DialogField(fieldLabel = "social Image", name = "./socialImage",
           resourceType = "commerce/gui/components/common/assetpicker")
    @Inject @ValueMapValue
    private String socialImage;

    public String getSocialImage() {
        return socialImage;
    }

    @DialogField(fieldLabel = "Share page URL", name = "./currentUrl", value = "true")
    @CheckBoxTouch(text = "Share page URL")
    @Inject @ValueMapValue
    private String currentUrl;


    public String getCurrentUrl() {
        return currentUrl;
    }
    
}
