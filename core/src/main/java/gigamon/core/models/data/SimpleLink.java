package gigamon.core.models.data;

import com.citytechinc.cq.component.annotations.DialogField;
import com.citytechinc.cq.component.annotations.widgets.PathField;
import com.citytechinc.cq.component.annotations.widgets.TextField;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import gigamon.core.models.component.ComponentSlingModel;
import gigamon.core.util.ServiceUtils;
import gigamon.dialog.touch.checkbox.CheckBoxTouch;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Required;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Generic model for standard HREF link.
 * <p>
 * This class is an instance of a {@link ComponentSlingModel} that can be constructed from two kinds of resources.
 *
 * <ol>
 *     <li>Page or jcr:content node</li>
 *     <li>Static properties from nt:unstructured node</li>
 * </ol>
 *
 * When constructed from a static nt:unstructured node, the node must have the following properties:
 * <ul>
 *     <li>title</li>
 *     <li>link</li>
 *     <li>newTab</li>
 * </ul>
 *
 * @author joelepps
 *         4/1/16
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SimpleLink extends ComponentSlingModel {

    private static final Logger log = LoggerFactory.getLogger(SimpleLink.class);

    public static final String DISPLAY_ONLY = "navDisplayOnly";

    @Inject @Required
    private Resource resourcePage;

    @Inject @Self @Required
    private Resource self;

    @DialogField(fieldLabel = "Title", name = "./title", ranking = 100,
            fieldDescription = "If empty and link is an AEM page, title will automatically be pulled from page name.")
    @TextField
    @Inject @ValueMapValue
    private String title;

    @DialogField(fieldLabel = "Link", name = "./link", ranking = 102)
    @PathField(rootPath = "/content/")
    @Inject @ValueMapValue
    private String link;

    @DialogField(fieldLabel = "New Tab", name = "./newTab", ranking = 103, value = "true")
    @CheckBoxTouch(text = "New Tab")
    @Inject @ValueMapValue
    private String newTab;

    private String navigationTitle;

    public String getTitle() {
        return title;
    }

    public String getNavigationTitle() {
        return navigationTitle;
    }

    public String getLink() {
        return link;
    }

    public String getNewTab() {
        return newTab;
    }

    public boolean getNewTabBool() {
        return "true".equals(newTab);
    }

    @Override
    public void postConstruct() throws Exception {
        super.postConstruct();

        String primaryType = self.getValueMap().get("jcr:primaryType", String.class);

        if ("cq:PageContent".equals(primaryType) || "cq:Page".equals(primaryType)) {
            initFromResourcePage();
        } else {
            initFromStaticProperties();
        }
    }

    private void initFromResourcePage() {
        String nodeName = resourcePage.getName();
        String nodePath = resourcePage.getPath();
        Resource jcrContent = resourcePage.getChild("jcr:content");
        if (jcrContent == null) {
            log.warn("Cannot initialize simple link from {}", resourcePage);
            return;
        }

        ValueMap valueMap = jcrContent.getValueMap();
        this.title = buildTitle(valueMap, nodeName);
        this.navigationTitle = title;
        this.link = buildLink(nodePath, valueMap);
    }

    private void initFromStaticProperties() {
        String rawLink = link;
        link = ServiceUtils.appendLinkExtension(getResourceResolver(), link);

        PageManager pageManager = getResourceResolver().adaptTo(PageManager.class);
        if (pageManager != null) {
            Page page = pageManager.getContainingPage(rawLink);
            if (page != null) {
            	if (title == null) {
                    navigationTitle = ServiceUtils.coalesce(page.getNavigationTitle(), page.getPageTitle(), page.getTitle());
                    title = ServiceUtils.coalesce(page.getPageTitle(), page.getNavigationTitle(), page.getTitle());
            	} else {
            		navigationTitle = title;
            	}
                
                String altPath = page.getProperties().get("altPath", "");
                if(StringUtils.isNotBlank(altPath)) {
                	link = ServiceUtils.appendLinkExtension(getResourceResolver(), altPath);
                }
            }
        }
    }

    private static String buildTitle(ValueMap jcrContentValueMap, String fallback) {
        String title = jcrContentValueMap.get(NameConstants.PN_NAV_TITLE, String.class);
        if (StringUtils.isBlank(title)) title = jcrContentValueMap.get(NameConstants.PN_PAGE_TITLE, String.class);
        if (StringUtils.isBlank(title)) title = jcrContentValueMap.get("metadata/dc:title", String.class);
        if (StringUtils.isBlank(title)) title = jcrContentValueMap.get("metadata/pdf:Title", String.class);
        if (StringUtils.isBlank(title)) title = jcrContentValueMap.get(NameConstants.PN_TITLE, String.class);
        if (StringUtils.isBlank(title)) title = fallback;
        return title;
    }

    private String buildLink(String path, ValueMap jcrContentValueMap) {
        if (jcrContentValueMap.get(DISPLAY_ONLY, false)) {
            return null;
        }
        
        String altPath = jcrContentValueMap.get("altPath", "");
        if(StringUtils.isNotBlank(altPath)) {
        	return ServiceUtils.appendLinkExtension(getResourceResolver(), altPath);
        }
        
        return path + ".html";
    }

}
