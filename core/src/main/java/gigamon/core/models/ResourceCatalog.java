package gigamon.core.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gigamon.core.adapters.CatalogedAdapterFactory;
import gigamon.core.models.data.AemTag;
//import org.apache.commons.collections.comparators.ComparatorChain;
import gigamon.core.util.ResourceChainedComparator;


public class ResourceCatalog {
	private static final Logger log = LoggerFactory.getLogger(CatalogedAdapterFactory.class);
	private List<GridResource> resources;

	public List<GridResource> getResources() {
		return resources;
	}

	public void setResources(List<GridResource> resources) {
		this.resources = resources;
	}

	public void addResource(GridResource resource) {
		if (resources == null) resources = new ArrayList<>();
		resources.add(resource);
	}
	
	/*public void sortResources() {
		Collections.sort(resources, new Comparator<GridResource>() {
			@Override
			public int compare(GridResource r1, GridResource r2) {
				// most recently modified first
				return Long.compare(r2.getLastModified(), r1.getLastModified());
			}
		});
	} changed by virtusa to sort resources in alphabetical order author: uday kiran */
	
	
	 Comparator<GridResource> byLastModified = new Comparator<GridResource>() {
	        @Override
			public int compare(GridResource r1, GridResource r2) {
				// most recently modified first
				return Long.compare(r2.getLastModified(), r1.getLastModified());
			}
	    };
	    
	    Comparator<GridResource> byTitle = new Comparator<GridResource>() {
	    	@Override
			public int compare(GridResource r1, GridResource r2) {
				String str1=r1.getTitle();
				String str2=r2.getTitle();
				int res = String.CASE_INSENSITIVE_ORDER.compare(str1, str2);
		        if (res == 0) {
		            res = str1.compareTo(str2);
		        }
		        return res;
			}
	    };
	    
	    
	    ResourceChainedComparator chain = new ResourceChainedComparator(byTitle,byLastModified);
	    
	
	public void sortResources() {
		Collections.sort(resources,chain);
	}

	public static final class GridResource {

		private String title;
		private String url;
		private String iconType;
		private String gridCtaTextOverride;
		private String gridUrl;
		private Boolean gridCtaNewTab;
		private Long lastModified;
		private List<AemTag> tags = new ArrayList<>(); // initialize empty
		private String gridBackgroundImage;
		private String description;
		private String damFilePath;
		private String damFileType;
		private String damFileSize;
		
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
		
		public String getIconType() {
			return iconType;
		}

		public void setIconType(String iconType) {
			this.iconType = iconType;
		}
		
		public String getGridCtaTextOverride() {
			return gridCtaTextOverride;
		}

		public void setGridCtaTextOverride(String gridCtaTextOverride) {
			this.gridCtaTextOverride = gridCtaTextOverride;
		}

		public String getGridUrl() {
			return gridUrl;
		}

		public void setGridUrl(String gridUrl) {
			this.gridUrl = gridUrl;
		}

		public Boolean getGridCtaNewTab() {
			return gridCtaNewTab;
		}

		public void setGridCtaNewTab(Boolean gridCtaNewTab) {
			this.gridCtaNewTab = gridCtaNewTab;
		}

		public Long getLastModified() {
			return lastModified;
		}

		public void setLastModified(Long lastModified) {
			this.lastModified = lastModified;
		}

		public List<AemTag> getTags() {
			return tags;
		}

		public void setTags(List<AemTag> tags) {
			this.tags = (tags == null) ? new ArrayList<>() : tags;
		}
		
		public void addTag(AemTag tag) {
			if (tags == null) tags = new ArrayList<>();
			tags.add(tag);
		}
		
		public String getTagIds() {
			List<String> tagIds = tags.stream().map(AemTag::getId).collect(Collectors.toList());
			return StringUtils.join(tagIds, " ");
		}
		
		public String getGridBackgroundImage(){
			return gridBackgroundImage;
		}
		
		public void setGridBackgroundImage(String gridBackgroundImage){
			this.gridBackgroundImage = gridBackgroundImage;
		}
		
		public String getDescription() {
			return description;
		}
		
		public void setDescription(String description){
			this.description = description;
		}

		public String getDamFilePath() {
			return damFilePath;
		}

		public void setDamFilePath( String damFilePath){
			this.damFilePath = damFilePath;
		}
		
		public String getDamFileType() {
			if(damFileType != null) {
				if(damFileType.equals("application/pdf")) {
					damFileType = "PDF";
				}
				else if (damFileType.equals("image/png")) {
					damFileType = "PNG";
				}
				else if(damFileType.equals("image/svg+xml")) {
					damFileType = "SVG";
				}
				else if(damFileType.equals("image/jpeg")) {
					damFileType = "JPEG";
				}
				else if(damFileType.equals("application/zip")) {
					damFileType = "ZIP";
				}
				else if(damFileType.equals("application/java-archive")) {
					damFileType = "JAR";
				}
				else if(damFileType.equals("image/gif")) {
					damFileType = "GIF";
				}
				
				return damFileType;
			} else {
				return "";
			}
		}
		
		public void setDamFileType(String damFileType) {
			this.damFileType = damFileType;
		}
		
		public String getDamFileSize() {
			if(damFileSize != null) {
				Long i = Long.valueOf(damFileSize) / new Long(1024 * 1024);
				if(i > 0) {
					return (i + "MB");
				}
				else {
					return ((Long.valueOf(damFileSize) / new Long(1024)) + "KB");
				}
			} else {
				return "";
			}
		}
		
		public void setDamFileSize(String damFileSize){
			this.damFileSize = damFileSize;
		}
		
		@Override
		public String toString() {
			log.info("the file Type is:" + damFileType);
			return "GridResource [title=" + title + ", url=" + url + ", lastModified=" + lastModified + ", tags=" + tags + ", gridBackgroundImage=" + gridBackgroundImage + ", description=" + description + ", damFilePath=" + damFilePath + ", damFileType=" + damFileType + ", damFileSize=" + damFileSize + "]";
		}

	}

}
