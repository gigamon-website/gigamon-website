package gigamon.core.models;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.settings.SlingSettingsService;

import com.adobe.cq.sightly.WCMUsePojo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import gigamon.core.util.ServiceUtils;

public class AnalyticsDataModel extends WCMUsePojo {
	
	private static final Gson GSON = new GsonBuilder()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
			.create();
	
	private static final Gson GSON_PRETTY = new GsonBuilder()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
			.setPrettyPrinting()
			.create();
	
	private Boolean isPreProd = true; //default to true
	private String digitalData;
	
	@Override
	public void activate() throws Exception {
		SlingSettingsService settingsService = getSlingScriptHelper().getService(SlingSettingsService.class);
		isPreProd = ServiceUtils.isPreProd(settingsService);
		
		AnalyticsData analyticsData = buildAnalyticsData();
		
		if(isPreProd) {
			digitalData = GSON_PRETTY.toJson(analyticsData);
		} else {
			digitalData = GSON.toJson(analyticsData);
		}
	}

	private AnalyticsData buildAnalyticsData() {
		AnalyticsData analyticsData = new AnalyticsData();
		
		String pagePath = getPagePath();
		
		analyticsData.page.pageInfo.pageName = pagePath;
		analyticsData.page.pageInfo.onsiteSearchTerm = getRequest().getParameter("q");
		
		analyticsData.page.category.pageType = getPageType();
		analyticsData.page.category.primaryCategory = getPrimaryCategory(pagePath);
		analyticsData.page.category.subCategory1 = getSubCategory1(pagePath);
		analyticsData.page.category.subCategory2 = getSubCategory2(pagePath);
		
		return analyticsData;
	}
	
	/**
	 * Safely retrieves the page path. Avoids null pointers and changes
	 * delimiters to ':'
	 * 
	 * @return page path
	 */
	private String getPagePath() {
		String pagePath = getCurrentPage().getPath();
		String homePagePath = ServiceUtils.getHomePage(getCurrentPage()).getPath();
		
		if(pagePath != null) {
			pagePath = pagePath.replace(homePagePath, "");
			pagePath = pagePath.replaceAll("^/+", ""); //remove leading slashes
			pagePath = pagePath.replace("/", ":"); //change '/' to ':'
		}
		
		if(StringUtils.isBlank(pagePath)) {
			pagePath = "homepage";
		}
		
		return pagePath;
	}
	
	/**
	 * Safely retrieves the current page template's name because
	 * getCurrentPage().getTemplate() returns null for anonymous users.
	 * 
	 * @return page template name
	 */
	private String getPageType() {
		String pageType = null;
		try {
			pageType = getCurrentPage().getTemplate().getName();
		} catch(Exception e) {
			pageType = getCurrentPage().getProperties().get("cq:template", "");
			if(pageType != null && !pageType.trim().equals("")) {
				pageType = pageType.substring(pageType.lastIndexOf('/') + 1, pageType.length());
			}
		}
		return pageType;
	}
	
	/**
	 * Gets the top level section of the site the current page is in
	 * 
	 * @param pagePath constructed from getPagePath()
	 * @return section name
	 */
	private String getPrimaryCategory(String pagePath) {
		String[] pagePathParts = pagePath.split(":");
		return pagePathParts[0];
	}
	
	/**
	 * Gets the first level subsection of the site the current page is in
	 * 
	 * @param pagePath constructed from getPagePath()
	 * @return section name
	 */
	private String getSubCategory1(String pagePath) {
		String[] pagePathParts = pagePath.split(":");
		if(pagePathParts.length > 1) {
			return pagePathParts[1];
		} else {
			return "n/a";
		}
	}
	
	/**
	 * Gets the second level subsection of the site the current page is in
	 * 
	 * @param pagePath constructed from getPagePath()
	 * @return section name
	 */
	private String getSubCategory2(String pagePath) {
		String[] pagePathParts = pagePath.split(":");
		if(pagePathParts.length > 2) {
			return pagePathParts[2];
		} else {
			return "n/a";
		}
	}
	
	public String getDigitalData() {
		return digitalData;
	}
	
	public Boolean getPreProd() {
		return isPreProd;
	}
}
