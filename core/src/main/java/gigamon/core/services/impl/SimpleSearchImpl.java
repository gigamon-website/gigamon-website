package gigamon.core.services.impl;

import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.statistics.StatisticsService;
import com.day.crx.statistics.result.RelatedQueriesReport;
import com.day.text.Text;

import gigamon.core.services.SimpleSearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <h2>HERO Comment</h2>
 *
 * This file is copied from /libs/cq/platform/install/cq-search-ext-1.0.6/com/day/cq/search/ext/impl/SimpleSearchImpl.java
 */
public class SimpleSearchImpl implements SimpleSearch
{
  private final Logger log = LoggerFactory.getLogger(SimpleSearchImpl.class);

  private String query;

  private long start;

  private List<String> searchProperties = new ArrayList<>(Arrays.asList(new String[] { ".", "jcr:title", "jcr:description" }));

  // HERO modifications
  private List<String> searchIn;

  private long hitsPerPage = 10L;

  private final StatisticsService statsService;

  private final Resource resource;

  private Session session;

  private SearchResult result;

  // private Trends trends;

  private QueryBuilder builder;

  private List<Predicate> customPredicates = new ArrayList<>();

  private List<String> defaultSearchedTypes = new ArrayList<>(Arrays.asList(new String[] { "cq:Page" ,"dam:Asset" }));

  public SimpleSearchImpl(Resource resource, QueryBuilder builder, StatisticsService statsService)
  {
    this.resource = resource;
    this.builder = builder;
    this.statsService = statsService;
    // HERO modifications
    this.searchIn = new ArrayList<>();
    this.searchIn.add(Text.getAbsoluteParent(resource.getPath(), 0));
  }

  public SearchResult getResult() throws RepositoryException {
    if (result == null) {
      String queryString = getQuery();
      if ((queryString.length() == 0) && (customPredicates.size() == 0)) {
        return null;
      }

      Map<String, String> map = new HashMap<>();

      // HERO modifications
      // map.put("path", getSearchIn().get(0));
      if(getSearchIn().size() == 0) {
    	  map.put("path", "");
      } else if(getSearchIn().size() == 1) {
    	  map.put("path", getSearchIn().get(0));
      } else {
    	  setPaths();
      }

      boolean typeOverwrite = false;
      for (Predicate p : customPredicates)
      {
        if (hasNodeTypeInPredicateTree(p)) {
          typeOverwrite = true;
          break;
        }
      }

      if (!typeOverwrite)
      {
        setDefaultSearchedTypes();
      }

      int counter;
      if (queryString.startsWith("related:"))
      {
        String path = queryString.substring("related:".length());
        map.put("similar", path + "/" + "jcr:content");
        map.put("similar.local", "jcr:content");
      }
      else {
        counter = 0;
        map.put("group.p.or", "true");
        for (String prop : searchProperties) {
          map.put("group." + counter + "_fulltext", queryString);

          StringBuffer path = new StringBuffer();
          if (!prop.equals(".")) {
            String[] segments = Text.explode(prop, 47);
            for (int i = 0; i < segments.length; i++) {
              if (i > 0) {
                path.append("/");
              }
              if (i == segments.length - 1)
              {
                path.append("@");
              }
              path.append(segments[i]);
            }
          }
          map.put("group." + counter + "_fulltext.relPath", path.toString());
          counter++;
        }
      }

      Query query = builder.createQuery(PredicateGroup.create(map), getSession());

      for (Predicate p : customPredicates) {
        query.getPredicates().add(p);
      }

      query.setExcerpt(true);
      query.setStart(start);
      query.setHitsPerPage(hitsPerPage);

      result = query.getResult();
    }
    return result;
  }

  /* HERO modifications
  public Trends getTrends() {
    if (trends == null) {
      trends = new TrendsImpl(getSession(), statsService);
    }
    return trends;
  }
  */

  public List<String> getRelatedQueries() throws RepositoryException {
    List<String> related = new ArrayList<>();
    RelatedQueriesReport report = new RelatedQueriesReport(statsService.getPath() + "/results", getQuery());

    Iterator it = statsService.runReport(getSession(), report);
    while (it.hasNext()) {
      Object[] data = (Object[])it.next();
      related.add((String)data[0]);
    }
    return related;
  }

  public String getQuery() {
    return query != null ? query : "";
  }

  public void setQuery(String query) {
    this.query = query;
    resetResult();
  }

  public long getHitsPerPage() {
    return hitsPerPage;
  }

  public void setHitsPerPage(long num) {
    hitsPerPage = num;
    resetResult();
  }

  // HERO modifications
  public List<String> getSearchIn() {
    return searchIn;
  }

  public void setSearchIn(List<String> searchIn) {
	  if(searchIn == null || searchIn.isEmpty()) {
		  searchIn = new ArrayList<>();
	  } else {
		  for(int i = 0; i < searchIn.size(); i++) {
			  String si = searchIn.get(i);
			  if(si == null) {
				  searchIn.set(i, "");
			  }
			  if(si.endsWith("/")) {
				  searchIn.set(i, si.substring(0, si.length() - 1));
			  }
		  }
	  }
	  this.searchIn = searchIn;
	  resetResult();
  }
  // end HERO modifications

  public String getSearchProperties() {
    return Text.implode((String[])searchProperties.toArray(new String[searchProperties.size()]), ", ");
  }

  public void setSearchProperties(String properties)
  {
    searchProperties.clear();
    String[] props = Text.explode(properties, 44);
    for (String p : props) {
      searchProperties.add(p.trim());
    }
    resetResult();
  }

  public long getStart() {
    return start;
  }

  public void setStart(long start) {
    this.start = start;
    resetResult();
  }

  public void addPredicate(Predicate predicate) {
    customPredicates.add(predicate);
    resetResult();
  }

  Session getSession()
  {
    if (session == null) {
      session = ((Session)resource.getResourceResolver().adaptTo(Session.class));
    }
    return session;
  }


  private void resetResult()
  {
    result = null;
  }

  private void setDefaultSearchedTypes() {
    PredicateGroup nodeTypes = new PredicateGroup("nodeTypes");
    for (String type : defaultSearchedTypes) {
      if (StringUtils.isNotEmpty(type)) {
        Predicate predicate = new Predicate("type", "type");
        predicate.set("type", type);
        nodeTypes.add(predicate);
      }
    }
    nodeTypes.setAllRequired(false);
    addPredicate(nodeTypes);
    log.info("SimpleSearch is searching with the types: {}", Arrays.toString(defaultSearchedTypes.toArray()));
  }

  // HERO modifications
  private void setPaths() {
	  PredicateGroup paths = new PredicateGroup("paths");
	  for(String path : getSearchIn()) {
		  if(StringUtils.isNotEmpty(path)) {
			  Predicate predicate = new Predicate("path", "path");
			  predicate.set("path", path);
			  paths.add(predicate);
		  }
	  }
	  paths.setAllRequired(false);
	  addPredicate(paths);
  }

  private boolean hasNodeTypeInPredicateTree(Predicate predicate) {
    if (predicate == null) {
      return false;
    }
    if ((predicate instanceof PredicateGroup)) {
      PredicateGroup group = (PredicateGroup)predicate;

      if (hasNodeType(group)) {
        return true;
      }

      for (Predicate p : group) {
        if (hasNodeTypeInPredicateTree(p)) {
          return true;
        }
      }
    }

    return hasNodeType(predicate);
  }

  private boolean hasNodeType(Predicate predicate) {
    return (predicate != null) && ("type".equals(predicate.getType()));
  }
}
