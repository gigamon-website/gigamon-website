package gigamon.core.services.impl;

import gigamon.core.services.SearchGateKeeper;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Default implementation for search gate keeper.
 *
 * @author joelepps
 *         10/8/16
 */
@Component(immediate = true, metatype = true, label = "Search Gate Keeper",
        description = "Rate limits the number of concurrent search requests.")
@Service(SearchGateKeeper.class)
@Properties({
        @Property(name = Constants.SERVICE_VENDOR, value = "Hero Digital"),
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "Rate limits the number of concurrent search requests."),
})
public class SearchGateKeeperImpl implements SearchGateKeeper {

    private static final Logger log = LoggerFactory.getLogger(SearchGateKeeper.class);

    private int activeSearches;

    @Property(label = "Max", longValue = 5, description = "The maximum number of concurrent search requests.")
    public static final String MAX = "max.searches";
    private int max;

    @Activate
    public void activate(ComponentContext context) {
        max = PropertiesUtil.toInteger(context.getProperties().get(MAX), 5);
    }

    @Deactivate
    public void deactivate() {
        activeSearches = 0;
    }

    @Override
    @Nonnull
    public synchronized Ticket requestTicket(@Nonnull SlingHttpServletRequest request) {

        String message = "Search count at " + activeSearches + " (max " + max + ") for "
            + request.getPathInfo() + " " + request.getQueryString();

        if (activeSearches >= max) {
            log.debug("requestTicket: Search DENIED: " + message);
            return new Ticket(request, false, message);
        } else {
            activeSearches++;
            log.debug("requestTicket: Search ALLOWED: " + message);
            return new Ticket(request, true, message);
        }
    }

    @Override
    public synchronized void releaseTicket(@Nonnull Ticket ticket) {
        SlingHttpServletRequest request = ticket.getRequest();

        if (!ticket.isSearchAllowed()) {
            log.trace("releaseTicket: No action needed for ticket release as search was not allowed: {} {}",
                request.getPathInfo(), request.getQueryString());
            return;
        }

        log.debug("releaseTicket: {} {} with active count {}", request.getPathInfo(), request.getQueryString(),
            activeSearches);

        if (activeSearches == 0) {

            // this should never happen
            log.error("Count is at 0, cannot decrement further. {} {}",
                request.getPathInfo(),
                request.getQueryString());

        } else {

            log.debug("releaseTicket: released {} {}, decrementing {}",
                request.getPathInfo(),
                request.getQueryString(),
                activeSearches);

            activeSearches--;
        }
    }
}
