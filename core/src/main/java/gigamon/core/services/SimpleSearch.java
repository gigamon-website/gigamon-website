package gigamon.core.services;

import com.day.cq.search.Predicate;
import com.day.cq.search.result.SearchResult;
import java.util.List;
import javax.jcr.RepositoryException;

/**
 * <h2>HERO Comment</h2>
 *
 * This file is copied from /com/day/cq/search/SimpleSearch.java
 */
public abstract interface SimpleSearch
{
  public static final String RELATED_PREFIX = "related:";
  
  public abstract void setQuery(String paramString);
  
  public abstract String getQuery();
  
  public abstract void setSearchIn(List<String> paramString);
  
  public abstract List<String> getSearchIn();
  
  public abstract void setHitsPerPage(long paramLong);
  
  public abstract long getHitsPerPage();
  
  public abstract void setStart(long paramLong);
  
  public abstract long getStart();
  
  public abstract void addPredicate(Predicate paramPredicate);
  
  public abstract SearchResult getResult()
    throws RepositoryException;
  
  // public abstract Trends getTrends();
  
  public abstract List<String> getRelatedQueries()
    throws RepositoryException;
  
  public abstract void setSearchProperties(String paramString);
  
  public abstract String getSearchProperties();
}
