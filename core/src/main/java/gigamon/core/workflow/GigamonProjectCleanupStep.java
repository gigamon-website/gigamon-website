package gigamon.core.workflow;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import org.apache.felix.scr.annotations.*;
import org.apache.felix.scr.annotations.Property;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import javax.jcr.query.InvalidQueryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/*
 * Note to future me:  This only works in the context of starting the workflow from workflow models.  If you start
 * it from timeline, the assets are fed in individually, which is not what we want. We need to submit the asset transitions folder
 * not the individual files in the folder
 */


@Service(value = WorkflowProcess.class)

@Component(label = "Gigamon Project Close Out Step", metatype = false, immediate = true)
@Properties({@Property(name = Constants.SERVICE_DESCRIPTION, value = "Cleanup project assets, tag and move to general population"),
        @Property(name = Constants.SERVICE_VENDOR, value = "Hero Digital"),
        @Property(name = "process.label", value = "Gigamon Project Cleanup Process")})

public class GigamonProjectCleanupStep implements WorkflowProcess {

    //static list of tag domains
    final List<String> tagDomains = Arrays.asList(
            "gigamon:partner_portal/resource_type",
            "gigamon:resource_library/resource_type",
            "gigamon:website_assets/resource_type"
    );

    /**
     * Default log.
     */
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    private Replicator replicator;

    @Override
    public void execute(WorkItem workitem, WorkflowSession workflowSession, MetaDataMap metadataMap) throws WorkflowException {
        log.info("# # # # # # # # # # # # # # # # # # # # # # # # # # # # #");

        WorkflowData workflowData = workitem.getWorkflowData();
        String type = workflowData.getPayloadType();
        log.info("type: " + type);

        if (type.equalsIgnoreCase("JCR_PATH")) {

            String path = workflowData.getPayload().toString();
            Session jcrSession = null;

            try {

                //get a session and run like you stole something
                jcrSession = workflowSession.adaptTo(Session.class);
                Node templateNode = (Node) jcrSession.getItem(path);

                if (templateNode.hasProperty("./projectPath")) { // Payload is a project

                    //get the template node name. for some strange reason this is an array. get the first value.
                    Value[] projectPaths = templateNode.getProperty("./projectPath").getValues();
                    String projectPath = projectPaths[0].getString();
                    log.info("Project Path: " + projectPath);

                    Node projectNode = (Node) jcrSession.getItem(projectPath + "/jcr:content");
                    log.info("Project Node Path: " + projectNode.getPath());

                    String projectTemplate = projectNode.getProperty("./cq:template").getValue().toString();
                    log.info("Project Template:" + projectTemplate);

                    String damFolderPath = projectNode.getProperty("./damFolderPath").getValue().toString();
                    log.info("DAM Folder Path:" + damFolderPath);

                    processProject(jcrSession, projectNode, damFolderPath);

                } else { // Payload is a DAM folder

                    log.info("DAM Folder Path:" + path);
                    processProject(jcrSession, null, path);

                }

            } catch (Exception e) {

                // If an error occurs that prevents the Workflow from completing/continuing - Throw a WorkflowException
                // and the WF engine will retry the Workflow later (based on the AEM Workflow Engine configuration).
                log.error("Unable to complete processing the Workflow Process step, for path: " + path, e);

                //pretty sure that retrying is not going to help in this case...
                //throw new WorkflowException("Unable to complete processing the Workflow Process step", e);
                
            } finally {
            	
                if (jcrSession != null) {
                    try {
                        jcrSession.save();
                    } catch (RepositoryException e) {
                        log.error("Unable to complete processing the Workflow Process step, for path: " + path, e);
                    }
                }
                
            }
            
            log.info("ending: " + path);
        }
    }

    /*
     * Each Project could have radically different bi* attributes, and these
     * attributes can either be singles or arrays. So we are going to process them
     * Independently.  However if there are "standard" attributes, feel free to create a
     * centralized method to handle them
     *
     * scratch that. one ring to rule them all.
     *
     */
    private void processProject(Session session, Node projectNode, String damFolderPath) throws Exception {
    	
        //retrieve DAM assets contained within the damFolderPath
        List<Node> assets = getProjectAssets(session, damFolderPath);
        String projectNumber = "";
        String projectName = "";

        if (projectNode != null) {
            HashMap<String, javax.jcr.Property> projectAttributes = getNodeAttributes(projectNode);
            List<String> tagList = getProjectTags(projectAttributes);
            applyTagsToAssets(assets, tagList);

            if (projectAttributes.containsKey("gigamon.projectnumber")) {
                javax.jcr.Property projectNumberP = projectAttributes.get("gigamon.projectnumber");
                projectNumber = projectNumberP.getString();
            }

            if (projectAttributes.containsKey("jcr:title")) {
                javax.jcr.Property projectNameP = projectAttributes.get("jcr:title");
                projectName = projectNameP.getString();
            }
        }

        moveAssets(session, projectNumber, projectName, assets);
    }

    private HashMap<String, javax.jcr.Property> getNodeAttributes(Node node) {
    	
        HashMap<String, javax.jcr.Property> attributes = new HashMap<String, javax.jcr.Property>();
        log.info("------------------attributes----------------------");

        try {
            PropertyIterator it = node.getProperties();

            while (it.hasNext()) {
                javax.jcr.Property p = (javax.jcr.Property) it.next();
                if (p.getName().startsWith("bi") || p.getName().equals("jcr:title")) {
                    attributes.put(p.getName(), p);
                }
            }
        } catch (RepositoryException e) {
            log.error("failed to get attributes");
            log.error(e.getMessage());
        }

        log.info("------------------end attributes----------------------");
        return attributes;

    }

    private Boolean isTagValue(String tag) {
        Boolean success = false;

        for (String prefix : tagDomains) {
            if (tag.startsWith(prefix)) {
                success = true;
                break;
            }
        }

        return success;
    }

	
	/*
     * This might be a bit overkill, but look at all the returned values and determine of any of them "look"
	 * like tags i.e. they start with a known tag domain. 
	 */

    private List<String> getProjectTags(HashMap<String, javax.jcr.Property> attributes) throws Exception {
        List<String> tags = new ArrayList<String>();

        for (String key : attributes.keySet()) {
            javax.jcr.Property p = attributes.get(key);

            //Look for multiples first
            if (p.isMultiple()) {
                Value[] values = p.getValues();

                for (int i = 0; i < values.length; i++) {
                    String tag = values[i].toString();
                    if (isTagValue(tag)) {
                        tags.add(tag);
                    }
                }
            } else {
                Value value = p.getValue();
                String tag = value.getString();
                if (isTagValue(tag)) {
                    tags.add(tag);
                }
            }

        }

        log.info("------------------start tags---------------");

        //TODO debug
        for (String tag : tags) {
            log.info("Tag: " + tag);
        }

        log.info("------------------end tags---------------");

        return tags;
    }

    private List<Node> getProjectAssets(Session session, String damFolderPath) {
    	
        List<Node> assetNodes = new ArrayList<Node>();
        String q = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([" + damFolderPath + "])";
        Query myQuery;

        try {
        	
            myQuery = session.getWorkspace().getQueryManager().createQuery(q, Query.JCR_SQL2);
            QueryResult result = myQuery.execute();
            NodeIterator ni = result.getNodes();

            while (ni.hasNext()) {
                Node n = (Node) ni.next();
                assetNodes.add(n);
                log.info("Asset: " + n.getPath());
            }
        } catch (InvalidQueryException e) {
        	
            // TODO Auto-generated catch block
            log.error(e.getMessage());
            
        } catch (RepositoryException e) {
        	
            // TODO Auto-generated catch block
            log.error(e.getMessage());
            
        }

        return assetNodes;
    }

    /*Apply the tags to the assets, with the following rules
        1) Existing tags on the assets must be preserved
        2) the tag list must be unique
    */
    private void applyTagsToAssets(List<Node> assets, List<String> tagList) {

        //get all the existing tags on the asset
        for (Node n : assets) {
            List<String> tagArray = new ArrayList<String>();

            //deep copy the array
            List<String> assetTagList = new ArrayList<String>(tagList);

            try {
            	
                Node metaNode = n.getNode("./jcr:content/metadata");
                javax.jcr.Property tagProperty = metaNode.getProperty("cq:tags");
                Value[] tags = tagProperty.getValues();

                for (Value tagValue : tags) {
                    tagArray.add(tagValue.getString());
                    
                }
                
            } catch (PathNotFoundException e) {
            	
                log.error("Error: " + e.getMessage());
                e.printStackTrace();
                
            } catch (RepositoryException e) {
            	
                log.error("Error: " + e.getMessage());
                e.printStackTrace();
                
            }

            //We might have an empty tagArray, as there might be no tags on the original asset
            for (String tag : tagArray) {
            	
                if (!assetTagList.contains(tag)) {
                    assetTagList.add(tag);
                }
                
            }

            //try and set the value on the metadata subnode
            try {
            	
                Node metaNode = n.getNode("./jcr:content/metadata");
                metaNode.setProperty("./cq:tags", (String[]) assetTagList.toArray(new String[assetTagList.size()]));
                
            } catch (Exception e) {
            	
                e.printStackTrace();
                log.error(e.getMessage());
                
            }
        }
    }

    /*
     * here we need to move the assets, if and only if they meet these requirements:
     *
     * 1) they have an assetType and Brand (which comes from the Project) This defines where they should be move to
     * 2) If they have an assetUse tag, they must also have a gallery order
     *
     */
    private void moveAssets(Session session, String projectNumber, String projectName, List<Node> assets) {
    	
        for (Node n : assets) {
            try {
            	
                Node meta = n.getNode("./jcr:content/metadata");

                //Get the tags on the asset
                Boolean productCategoryExists = false;
                Boolean tooManyCategories = false;

                String productCategory = null;

                int productCategoryCounter = 0;

                List<String> tagList = new ArrayList<String>();
                javax.jcr.Property tagP = meta.getProperty("cq:tags");
                Value[] values = tagP.getValues();

                //scan for product category tags, one is just right; more than that is an error
                for (Value v : values) {
                    String tag = v.getString();
                    tagList.add(tag);

                    //TODO
                    if (tagList.contains(tag))    

                    if (tag.contains("resource_type")) 
                    {
                        productCategory = tag;
                        productCategoryCounter++;

                        if (productCategoryCounter > 1) {
                            tooManyCategories = true;
                        }
                    }
                }

				/* 
                 * we now have all the pieces.
				 * 
				 * in order to be moved, the asset must have one and only one product category
				 * if the asset has a web_image_gallery asset use it must have a gallery_order
				 * 
				 */
                if (projectNumber != null && projectNumber.length() > 0) {
                    meta.setProperty("projectNumber", projectNumber);
                }
                if (projectName != null && projectName.length() > 0) {
                    meta.setProperty("projectName", projectName);
                }

                if (productCategory != null && !tooManyCategories) {
                    String destinationFolderPath = getPathFromProductCategoryTag(productCategory);
                    String destinationNodePath = destinationFolderPath + "/" + n.getName();

                    if (!destinationNodePath.equals("")) {
                        meta.setProperty("projectMigration", "Success");
                        //session.move(srcAbsPath, destAbsPath);
                        String origin = n.getPath();

                        try {
                            JcrUtil.createPath(destinationFolderPath, "sling:OrderedFolder", session);
                            session.move(origin, destinationNodePath);
                        } catch (Exception e) {
                            meta.setProperty("projectMigration", "Asset not moved, error in move itself: " + e.getMessage());
                        }
                    } else {
                        meta.setProperty("projectMigration", "Node not moved, product category tags oddly malformed");
                    }
                } else if (productCategoryExists && tooManyCategories) {
                    meta.setProperty("projectMigration", "Not moved, too many product categories specified");
                } else if (!productCategoryExists) {
                    meta.setProperty("projectMigration", "Not moved, no product category specified");
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e.getMessage());
            }
        }
    }

    /*
     * Code to Activate (publish) the asset
     */
    private Boolean publishAsset(Session session, String destination) {
        
    	Boolean successful = true;

        try {
        	
            replicator.replicate(session, ReplicationActionType.ACTIVATE, destination);
        
        } catch (ReplicationException e) {
        
        	log.error("Failed to publish asset: " + e.getMessage());
            successful = false;
        
        }

        return successful;
    }


    /*
     * derive JCR location from product category tag
     *
     * if this returns an empty field, then there was something wrong with the productCategoryTag;
     */
    private String getPathFromProductCategoryTag(String resourceTypeTag) {
    	
    	String contentLocation = resourceTypeTag;
        String retVal = "/content/dam/gigamon";
    	
    	for (String domain : tagDomains){
    		
    		contentLocation = contentLocation.replace(domain, "");
    		
    	}
    	
    	retVal = retVal + contentLocation;

        return retVal;
    }

}
