package gigamon.core.adapters;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import gigamon.core.models.data.PageMetadata;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.adapter.AdapterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Adapter factory for {@link PageMetadata}.
 *
 * @author joelepps
 *         1/17/17
 */
@Component(immediate=true, metatype=false, label="Page Metadata Adapter Factory", description="Adapter factory for Page Metadata")
@Service(AdapterFactory.class)
public class PageMetadataAdapterFactory implements AdapterFactory {

    private static final Logger log = LoggerFactory.getLogger(PageMetadataAdapterFactory.class);

    @Property(name= AdapterFactory.ADAPTABLE_CLASSES)
    public static final String[] ADAPTABLE_CLASSES = {Page.class.getName()};

    @Property(name=AdapterFactory.ADAPTER_CLASSES)
    public static final String[] ADAPTER_CLASSES = {PageMetadata.class.getName()};

    @Reference
    private Externalizer externalizer;

    @Override
    public <AdapterType> AdapterType getAdapter(@Nonnull Object adaptable, @Nonnull Class<AdapterType> type) {
        if (adaptable == null) return null;

        try {
            if (adaptable instanceof Page) {
                Page page = (Page) adaptable;
                return (AdapterType) new PageMetadata(page.getContentResource(), externalizer);
            }

            log.warn("{} is not supported", adaptable.getClass());
        } catch (Exception e) {
            log.error("Failed to during construction of " + getClass().getName() + " for " + adaptable, e);
        }

        return null;
    }
}
