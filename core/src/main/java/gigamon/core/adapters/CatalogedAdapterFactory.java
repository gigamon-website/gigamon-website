package gigamon.core.adapters;

import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.adapter.AdapterFactory;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;

import gigamon.core.models.ResourceCatalog;
import gigamon.core.models.ResourceCatalog.GridResource;
import gigamon.core.models.data.AemTag;
import gigamon.core.util.ServiceUtils;

/**
 * Adapt {@link Resource} to {@link ResourceCatalog.GridResource}. 
 * <p>
 * Resource must be for a page with the resource page template or a component placed on the resource page.
 * <p>
 * If resource does not match criteria, null is returned.
 * 
 * @author Sanketh
 */
@Component(immediate=true, metatype=false, label="ResourceCatalog.GridResource Item Adapter Factory", description="Adapter factory for ResourceCatalog.GridResource")
@Service(AdapterFactory.class)
public class CatalogedAdapterFactory implements AdapterFactory {
	
	private static final Logger log = LoggerFactory.getLogger(CatalogedAdapterFactory.class);
	
	@Property(name=AdapterFactory.ADAPTABLE_CLASSES)
	public static final String[] ADAPTABLE_CLASSES = {Resource.class.getName()};
	
	@Property(name=AdapterFactory.ADAPTER_CLASSES)
	public static final String[] ADAPTER_CLASSES = {ResourceCatalog.GridResource.class.getName()};
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getAdapter(Object adaptable, Class<T> type) {
		if (adaptable == null) return null;
		
		try {
			if (adaptable instanceof Resource) {
				return (T) buildGridResource((Resource) adaptable);
			}
			
			log.warn("{} is not supported", adaptable.getClass());
		} catch (Exception e) {
			log.error("Failed to during construction of " + getClass().getName() + " for " + adaptable, e);
		}
		
		return null;
	}

	private GridResource buildGridResource(Resource resource) throws LoginException {
		log.trace("Building customer item from {}", resource);
		resource = getPageResource(resource);
		
		ValueMap valueMap = resource.getValueMap();
		Locale locale = fetchLocale(resource);
		
		String pageTitle = valueMap.get("jcr:content/pageTitle", String.class);
		String jcrTitle = valueMap.get("jcr:content/jcr:title", String.class);
		String url = ServiceUtils.appendLinkExtension(resource.getResourceResolver(), resource.getPath());
		String ctaTextOverride = valueMap.get("jcr:content/ctaText", String.class);
		String ctaDestination = valueMap.get("jcr:content/ctaDestination", String.class);
		String ctaOtherLink = ServiceUtils.appendLinkExtension(resource.getResourceResolver(), valueMap.get("jcr:content/ctaOtherLink", String.class));
		Boolean ctaNewTab = valueMap.get("jcr:content/ctaNewTab", Boolean.class);
		String jcrDescription = valueMap.get("jcr:content/jcr:description", String.class);
		String damFilePath = valueMap.get("jcr:content/damFilePath", String.class);
		
		GridResource gridResource = new GridResource();
		gridResource.setTitle(ServiceUtils.coalesce(pageTitle, jcrTitle));
		gridResource.setUrl(url);
		gridResource.setLastModified(convertToMillis(valueMap.get("jcr:content/cq:lastModified", Date.class)));
		gridResource.setDescription(jcrDescription);
		gridResource.setDamFilePath(damFilePath);
		
		
		if(StringUtils.isNotBlank(ctaTextOverride)) {
			gridResource.setGridCtaTextOverride(ctaTextOverride);
		}
		if("otherPage".equals(ctaDestination)) {
			gridResource.setGridUrl(ctaOtherLink);
		} else if("damFile".equals(ctaDestination)) {
			gridResource.setGridUrl(damFilePath);
		} else if("downloadDamFile".equals(ctaDestination)) {
			gridResource.setGridUrl("download");
		} else {
			gridResource.setGridUrl(url);
		}
		gridResource.setGridCtaNewTab(ctaNewTab);
		
		
		//Getting the image path set from the page properties
		Resource childResource = resource.getChild("jcr:content/image");
		if(childResource != null){
			ValueMap map = childResource.getValueMap();
			gridResource.setGridBackgroundImage(map.get("fileReference", String.class));
		}
		
		//Getting the DAM asset meta data (file type and file size)
		Resource damResource = resource.getResourceResolver().getResource(damFilePath);
		if(damResource !=null){
			ValueMap map = damResource.getValueMap();
			gridResource.setDamFileType(map.get("jcr:content/metadata/dc:format", String.class));
			gridResource.setDamFileSize(map.get("jcr:content/metadata/dam:size", String.class));
		}
		
		//To check the values in log.
		for(String key: childResource.getValueMap().keySet()){
			log.info(key);
			log.info(childResource.getValueMap().get(key, String.class));
		}
		
		String[] tagsRaw = valueMap.get("jcr:content/cq:tags", new String[0]);
		for (String tagRaw : tagsRaw) {
			AemTag aemTag = AemTag.createAemTag(tagRaw, resource.getResourceResolver(), locale, false);
			if (aemTag != null) {
				gridResource.addTag(aemTag);
			}
		}
		return gridResource;
	}
	
	/**
	 * If resource is located underneath 'jcr:content' segment, returns parent of 'jcr:content' (the page).
	 * <p>
	 * Else returns {@code candidateResource}.
	 * <p>
	 * This allows for this adapter factory to be used on both page and component resource objects.
	 * 
	 * @param candidateResource
	 * @return
	 */
	private Resource getPageResource(Resource candidateResource) {
		String path = candidateResource.getPath();
		int jcrContentIndex = path.indexOf("/jcr:content");
		if (jcrContentIndex > 0) {
			Resource resource = candidateResource.getResourceResolver().resolve(path.substring(0, jcrContentIndex));
			log.trace("[{}] -> [{}]", candidateResource, resource);
			return resource;
		} else {
			return candidateResource;
		}
	}
	
	private Locale fetchLocale(Resource pageResource) {
		Page page = pageResource.adaptTo(Page.class);
		if (page == null) return null;
		return page.getLanguage(false);
	}
	
	private Long convertToMillis(Date d) {
		if(d == null) {
			return (long) 0;
		}
		return d.getTime();
	}

}

