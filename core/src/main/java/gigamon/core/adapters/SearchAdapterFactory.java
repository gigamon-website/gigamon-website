package gigamon.core.adapters;

import com.day.cq.search.QueryBuilder;
import com.day.cq.statistics.StatisticsService;

import gigamon.core.services.SimpleSearch;
import gigamon.core.services.impl.SimpleSearchImpl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.adapter.AdapterFactory;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(metatype=false)
@Service({AdapterFactory.class})
@Properties({@Property(name="service.description", value={"Search Adapter Factory"})})
public class SearchAdapterFactory
  implements AdapterFactory
{
  private static final Logger log = LoggerFactory.getLogger(SearchAdapterFactory.class);
  
  private static final Class<Resource> RESOURCE_CLASS = Resource.class;
  private static final Class<SimpleSearch> SEARCH_CLASS = SimpleSearch.class;
  @Property(name="adaptables")
  protected static final String[] ADAPTABLE_CLASSES = { RESOURCE_CLASS.getName() };
  
  @Property(name="adapters")
  protected static final String[] ADAPTER_CLASSES = { SEARCH_CLASS.getName() };
  
  @Reference
  private StatisticsService statsService;
  
  @Reference
  protected QueryBuilder builder;
  

  public SearchAdapterFactory() {}
  
  public <AdapterType> AdapterType getAdapter(Object adaptable, Class<AdapterType> type)
  {
    if ((adaptable instanceof Resource)) {
      return getAdapter((Resource)adaptable, type);
    }
    log.warn("Unable to handle adaptable {}", adaptable.getClass().getName());
    
    return null;
  }
  


  private <AdapterType> AdapterType getAdapter(Resource resource, Class<AdapterType> type)
  {
    if (type == SEARCH_CLASS) {
      return (AdapterType) new SimpleSearchImpl(resource, builder, statsService);
    }
    log.debug("Unable to adapt resource to type {}", type.getName());
    return null;
  }
  
  protected void bindStatsService(StatisticsService paramStatisticsService)
  {
    statsService = paramStatisticsService;
  }
  
  protected void unbindStatsService(StatisticsService paramStatisticsService)
  {
    if (statsService == paramStatisticsService) {
      statsService = null;
    }
  }
  
  protected void bindBuilder(QueryBuilder paramQueryBuilder)
  {
    builder = paramQueryBuilder;
  }
  
  protected void unbindBuilder(QueryBuilder paramQueryBuilder)
  {
    if (builder == paramQueryBuilder) {
      builder = null;
    }
  }
}
