package gigamon.core.servlets;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.ComponentContext;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Servlet that returns static build info.
 * <p>
 * When debugging an issue this servlet provides a quick answer to the question of
 * <b>"Did the new code get picked up?"</b>.
 * <p>
 * AEM will sometimes fail to load new versions of bundles.
 * <p>
 * Below are troubleshooting options that can be taken if the version returned by this servlet is older than expected.
 *
 * <ul>
 *     <li>Restart AEM</li>
 *     <li>Re-deploy package</li>
 *     <li>Delete deployed JAR files in crx/de and re-deploy package</li>
 * </ul>
 *
 *  @author joelepps
 *         1/17/17
 */
@SlingServlet(
    label = "Build Number Servlet",
    paths = "/bin/buildNumber.txt",
    methods = { "GET" })
@Properties({
    @Property(name = "build.number", label = "Build Number"),
    @Property(name = "build.time", label = "Build Time"),
})
public class BuildNumberServlet extends SlingSafeMethodsServlet {

    // This value is filled in at Maven build time.
    private static final String JAVA_BUILD_NUMBER = "${project.version}";
    // This value is filled in at Maven build time.
    private static final String JAVA_BUILD_TIME = "${timestamp}";

    private String osgiBuildNumber;
    private String osgiBuildTime;

    @Activate
    public void activate(ComponentContext context) {
        osgiBuildNumber = context.getProperties().get("build.number") + "";
        osgiBuildTime = context.getProperties().get("build.time") + "";
    }

    @Override
    protected void doGet(@Nonnull SlingHttpServletRequest request,
                         @Nonnull SlingHttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        response.setHeader("X-Robots-Tag", "noindex");
        response.getOutputStream().println("Java Build Number: " + JAVA_BUILD_NUMBER);
        response.getOutputStream().println("Java Build Time: " + JAVA_BUILD_TIME);
        response.getOutputStream().println("OSGi Build Number: " + osgiBuildNumber);
        response.getOutputStream().println("OSGi Build Time: " + osgiBuildTime);
        response.getOutputStream().close();
    }
}
