# Gigamon Project - Core

The primary bundle for Java code in this AEM project.

* Component Models
* Services
* Event Listeners
* Servlets
* Workflow Processes

## Component Development

### Creating a New Component

Refer to the __generator-gigamon-component__ README for instructions on creating
starter files for new components.

### Model Classes

Component models are written using the following frameworks:

* [Sling Models](https://sling.apache.org/documentation/bundles/models.html)
* [cq-component-maven-plugin](https://github.com/OlsonDigital/cq-component-maven-plugin)

Sling Models allows for `@annotation` based marshalling of model classes from the
JCR.

cq-component-maven-plugin allows for the creation of AEM classic or touchui
dialog fields based on `@annotations` on Java fields or getter methods.

### HTL (Sightly)

The majority of components will have their markup written in HTL. Refer to the
README in __ui.apps__ for more information.

### CSS and JavaScript

All CSS (Sass) and JavaScript is managed withing the __ui.clientlibs__ module.
Refer to the README for more information.
