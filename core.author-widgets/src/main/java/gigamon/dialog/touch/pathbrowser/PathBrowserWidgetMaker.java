package gigamon.dialog.touch.pathbrowser;

import com.citytechinc.cq.component.dialog.exception.InvalidComponentFieldException;
import com.citytechinc.cq.component.touchuidialog.TouchUIDialogElement;
import com.citytechinc.cq.component.touchuidialog.exceptions.TouchUIDialogGenerationException;
import com.citytechinc.cq.component.touchuidialog.widget.maker.AbstractTouchUIWidgetMaker;
import com.citytechinc.cq.component.touchuidialog.widget.maker.TouchUIWidgetMakerParameters;
import org.codehaus.plexus.util.StringUtils;

/**
 * Created by Sanketh on 03/28/17.
 */
public class PathBrowserWidgetMaker extends AbstractTouchUIWidgetMaker<PathBrowserWidgetParameters> {

    public PathBrowserWidgetMaker(TouchUIWidgetMakerParameters parameters) {
        super(parameters);
    }

    @Override
    public TouchUIDialogElement make(PathBrowserWidgetParameters widgetParameters) throws ClassNotFoundException,
        InvalidComponentFieldException, TouchUIDialogGenerationException, IllegalAccessException, InstantiationException {

        PathBrowser pathBrowserAnnotation = getAnnotation(PathBrowser.class);

        widgetParameters.setRootPath(getRootPathForField(pathBrowserAnnotation));
        widgetParameters.setPredicate(getPredicateForField(pathBrowserAnnotation));

        return new PathBrowserWidget(widgetParameters);
    }

    public String getRootPathForField(PathBrowser annotation) {
        if (annotation != null && StringUtils.isNotBlank(annotation.rootPath())) {
            return annotation.rootPath();
        }

        return null;
    }

    public String getPredicateForField(PathBrowser annotation) {
        if (annotation != null && StringUtils.isNotBlank(annotation.predicate())) {
            return annotation.predicate();
        }

        return null;
    }
}
