package gigamon.dialog.touch.pathbrowser;

import com.citytechinc.cq.component.touchuidialog.widget.DefaultTouchUIWidgetParameters;

/**
 * Created by Sanketh on 03/28/17.
 */
public class PathBrowserWidgetParameters extends DefaultTouchUIWidgetParameters {

    private String rootPath;
    private String predicate;

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }

    @Override
    public String getResourceType() {
        return PathBrowserWidget.RESOURCE_TYPE;
    }

    @Override
    public void setResourceType(String resourceType) {
        super.setResourceType(resourceType);
    }
}
