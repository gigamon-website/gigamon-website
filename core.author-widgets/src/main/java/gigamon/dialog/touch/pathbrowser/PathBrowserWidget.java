package gigamon.dialog.touch.pathbrowser;

import com.citytechinc.cq.component.annotations.config.TouchUIWidget;
import com.citytechinc.cq.component.touchuidialog.widget.AbstractTouchUIWidget;

/**
 * Created by Sanketh on 03/28/17.
 */
@TouchUIWidget(annotationClass = PathBrowser.class, makerClass = PathBrowserWidgetMaker.class, resourceType = PathBrowserWidget.RESOURCE_TYPE)
public class PathBrowserWidget extends AbstractTouchUIWidget {

    public static final String RESOURCE_TYPE = "granite/ui/components/foundation/form/pathbrowser";

    private final String rootPath;
    private final String predicate;

    public PathBrowserWidget(PathBrowserWidgetParameters parameters) {
        super(parameters);

        rootPath = parameters.getRootPath();
        predicate = parameters.getPredicate();
    }

    public String getRootPath() {
        return rootPath;
    }

    public String getPredicate() {
        return predicate;
    }
}
