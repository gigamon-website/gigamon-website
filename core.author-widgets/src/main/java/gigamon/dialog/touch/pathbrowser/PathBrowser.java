package gigamon.dialog.touch.pathbrowser;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Sanketh on 03/23/17.
 */
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.METHOD, ElementType.FIELD })
public @interface PathBrowser {

    public static final String ROOT_PATH_DEFAULT = "/";
    public static final String PREDICATE_DEFAULT = "";

    String rootPath() default ROOT_PATH_DEFAULT;

    String predicate() default PREDICATE_DEFAULT;

}
